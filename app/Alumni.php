<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Alumni extends Model
{
    use SoftDeletes;

    protected $table = 'alumni';

    protected $guard = ['id'];

    protected $fillable = [
        'uuid',
        'first_name',
        'middle_name',
        'last_name',
        'email',
        'birth_date',
        'contact_number',
        'address',
        'civil_status',
        'gender',
        'status',
        'image',
        'email_verified_at',
        'created_at',
        'updated_at'
    ];
}
