<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Alumni_education extends Model
{
    use SoftDeletes;

    protected $table = 'alumni_educations';

    protected $fillable = ['uuid', 'degree', 'college', 'course', 'year_graduated', 'is_first'];

    protected $hidden = ['create_at', 'updated_at', 'deleted_at'];
}
