<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Alumni_employment extends Model
{
    use SoftDeletes;

    protected $table = 'alumni_employments';

    protected $fillable = [
        'uuid',
        'status',
        'company_name',
        'industry',
        'occupation',
        'job_level',
        'place_of_work',
        'reason',
        'is_first'
    ];
}
