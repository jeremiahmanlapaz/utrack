<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Applicant extends Model
{
    use SoftDeletes;

    protected $table = 'applicants';

    protected $fillable = [
        'uuid',
        'first_name',
        'middle_name',
        'last_name',
        'gender',
        'email',
        'address',
        'birth_date',
        'contact_number',
        'civil_status',
        'image',
        'degree',
        'college',
        'course',
        'year_graduated',
        'occupation',
        'company_name',
        'industry',
        'job_level',
        'place_of_work',
        'reason',
        'status',
        'application_status',
        'created_at',
        'updated_at'
    ];
}
