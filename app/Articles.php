<?php

namespace App;

use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Articles extends Model
{
    use SoftDeletes;

    use Searchable;

    protected $table = 'articles';

    protected $fillable = ['title', 'body', 'posted_by', 'cover', 'tags'];

    public function scopeSearch($query, $s)
    {
        return $query->where('title', 'like', '%' . $s . '%')
            ->orWhere('body', 'like', '%' . $s . '%');
    }
}
