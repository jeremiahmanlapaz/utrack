<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Carousel extends Model
{
    use SoftDeletes;

    protected $table = 'carousel';

    protected $guarded = ['created_at', 'updated_at'];

    protected $fillable = ['title', 'image', 'heading', 'carousel'];
}
