<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class College extends Model
{
    protected $table = 'colleges';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guard = ['id'];
    protected $hidden = ['created_at', 'updated_at', 'id'];
    protected $fillable = ['name', 'code'];
}
