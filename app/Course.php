<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $table = 'course';

    protected $fillable = ['college', 'name', 'degree'];

    protected $hidden = ['created_at', 'updated_at'];
}
