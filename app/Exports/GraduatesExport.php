<?php

namespace App\Exports;

use App\Graduate;
use Maatwebsite\Excel\Concerns\FromCollection;

class GraduatesExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Graduate::all();
    }
}
