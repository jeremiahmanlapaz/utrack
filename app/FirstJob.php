<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FirstJob extends Model
{
    protected $table = 'first_job';

    protected $fillable = ['uuid', 'is_first_job', 'reason_accepting', 'how_long_did_take', 'competency_skills'];
}
