<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Graduate extends Model
{

    use SoftDeletes;

    protected $table = 'graduates';

    protected $hidden = ['created_at', 'updated_at'];

    protected $fillable = [
        'first_name',
        'middle_name',
        'last_name',
        'college',
        'course',
        'degree',
        'year_graduated'
    ];
}
