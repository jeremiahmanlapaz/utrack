<?php

namespace App\Http\Controllers;

use App\Alumni;
use App\Alumni_education;
use App\Alumni_employment;
use App\Mail\UpdateForm;
use Exception;
use Illuminate\Support\Facades\Mail;

class AlumniController extends Controller
{

    public function index()
    {
        return view('admin.member.index');
    }

    public function all()
    {
        $alumni = Alumni::all();
        return $alumni;
    }

    public function delete($uuid)
    {
        Alumni::where('uuid', $uuid)->delete();
        Alumni_education::where('uuid', $uuid)->delete();
        Alumni_employment::where('uuid', $uuid)->delete();
        return $this->resolve();
    }

    public function get($uuid)
    {
        $alumni = Alumni::where('uuid', $uuid)->get();
        return $alumni;
    }

    public function total()
    {
        $total = Alumni::all()->count();
        return $total;
    }

    public function archive()
    {
        return view('admin.member.archive');
    }

    public function profile($uuid)
    {
        $alumni = Alumni::where('uuid', $uuid)->first();
        $jobs = Alumni_employment::where('uuid', $uuid)->orderBy('created_at')->get();
        $educations = Alumni_education::where('uuid', $uuid)->get();
        // dd($jobs);
        return view('admin.member.profile', ['alumni' => $alumni, 'educations' => $educations, 'jobs' => $jobs]);
    }

    public function sendUpdate($uuid)
    {
        $alumni = Alumni::where('uuid', $uuid)->first();

        try {
            Mail::to($alumni->email)->send(new UpdateForm($alumni));
        } catch (Exception $e) {
            return $this->reject();
        }

        return $this->resolve();
    }
}
