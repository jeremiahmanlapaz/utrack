<?php

namespace App\Http\Controllers;

use App\Applicant;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

use App\Mail\ApplicationVerified;
use App\Mail\RejectApplication;
use App\Payment;

class ApplicantController extends Controller
{
    public function index()
    {
        return view('admin.applicants.index');
    }

    public function archive()
    {
        return view('admin.applicants.archive');
    }

    public function all()
    {
        return Applicant::all();
    }

    public function applicants()
    {
        return Applicant::where('application_status', 'applicant')->orderBy('created_at')->get();
    }

    public function check($uuid)
    {
        $applicant = DB::table('applicants')
            ->select(
                'applicants.uuid as applicant_uuid',
                'applicants.first_name as applicant_first_name',
                'applicants.middle_name as applicant_middle_name',
                'applicants.last_name as applicant_last_name',
                'applicants.degree as applicant_degree',
                'applicants.college as applicant_college',
                'applicants.course as applicant_course',
                'applicants.year_graduated as applicant_year_graduated',
                'graduates.first_name as graduate_first_name',
                'graduates.middle_name as graduate_middle_name',
                'graduates.last_name as graduate_last_name',
                'graduates.degree as graduate_degree',
                'graduates.college as graduate_college',
                'graduates.course as graduate_course',
                'graduates.year_graduated as graduate_year_graduated'
            )
            ->join('graduates', function ($join) {
                $join->on('graduates.first_name', 'LIKE', 'applicants.first_name')
                    ->on('graduates.middle_name', 'LIKE', 'applicants.middle_name')
                    ->on('graduates.last_name', 'LIKE', 'applicants.last_name');
            })
            ->where('applicants.uuid', $uuid)
            ->get();
        return response()->json($applicant);
    }

    public function confirmPayment($uuid)
    {
        $applicant = Applicant::where('uuid', $uuid)->get();
        return $applicant;
    }

    public function delete($uuid)
    {
        $applicant = Applicant::where('uuid', $uuid)->first();
        $applicant->application_status = 'deleted';
        $applicant->save();
        $applicant->delete();
        return $this->resolve();
    }

    public function get($uuid)
    {
        $applicant = Applicant::where('uuid', $uuid)->get();
        return $applicant;
    }

    public function verified()
    {
        $verified = Applicant::where('applicant_status', 'confirmation')->get();
        return $verified;
    }

    public function total()
    {
        $applicants = Applicant::count();
        return $applicants;
    }

    public function trashed()
    {
        return Applicant::onlyTrashed()->get();
    }

    public function rejectApplication($uuid)
    {
        $applicant = Applicant::where('uuid', $uuid)->first();
        try {
            Mail::to($applicant->email)->send(new RejectApplication($applicant));
        } catch (\Exception $e) {
            return $e;
        }
        $applicant->application_status = 'rejected';
        $applicant->save();
        $applicant->delete();
        return $this->resolve();
    }

    public function restore($uuid)
    {
        $applicant = Applicant::onlyTrashed()->where('uuid', $uuid)->first();
        $applicant->application_status = 'applicant';
        $applicant->save();
        $applicant->restore();
        return $this->resolve();
    }

    public function revoke($uuid)
    {
        $applicant = Applicant::where('uuid', $uuid)->first();
        if ($applicant) {
            $applicant->status = 'applicant';
            $applicant->touch();
            $applicant->save();
            return $this->resolve();
        }
        return $this->reject();
    }

    public function verifyApplicant($uuid)
    {
        $applicant = Applicant::where('uuid', $uuid)->first();
        if (!$applicant) {
            return back()->with(['message' => 'Applicant Not Found', 'class' => 'danger']);
        }
        $payment = new Payment();
        $payment->uuid = $applicant->uuid;
        $payment->alumni_id_fee = 50;
        switch ($applicant->degree) {
            case 'Associate':
                $payment->membership_fee = 50;
                break;
            case 'Diploma':
                $payment->membership_fee = 100;
                break;
            case 'Bachelor':
                $payment->membership_fee = 150;
                break;
            case 'Master':
                $payment->membership_fee = 200;
                break;
            case 'Doctoral':
                $payment->membership_fee = 250;
            default:
                $payment->membership_fee = 50;
                break;
        }
        $payment->total_amount = $payment->membership_fee + $payment->alumni_id_fee;
        $applicant->application_status = 'verified';
        try {
            Mail::to($applicant->email)->send(new ApplicationVerified($applicant, $payment));
        } catch (\Exception $e) {
            return $e;
        }
        $applicant->touch();
        $payment->save();
        return $this->resolve();
    }
}
