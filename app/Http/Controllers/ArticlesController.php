<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

use App\Articles;
use App\Traits\UploadTrait;

class ArticlesController extends Controller
{
    use UploadTrait;

    public function index(Request $request)
    {
        return view('admin.articles.index');
    }

    public function all()
    {
        $articles = Articles::all();
        return $articles;
    }

    public function paginate()
    {
        $articles = Articles::paginate(5);
        return $articles;
    }

    public function showCreate()
    {
        return view('admin.articles.create');
    }

    public function create(Request $request)
    {
        $request->validate([
            'cover' =>  'image|mimes:jpeg,png,jpg,gif|required',
            'title' => 'required|string|min:5|max:200',
            'body' => 'required'
        ]);
        $article = $request->all();
        if ($request->hasFile('cover')) {
            $image = $request->file('cover');
            $img_name = Str::slug(Str::before($image->getClientOriginalName(), '.'), '-') . '-Cover';
            $folder = 'uploads/images/';
            $filePath =  $folder . $img_name . '.' . $image->getClientOriginalExtension();
            $article['cover'] = $filePath;
            $this->uploadOne($image, $folder, 'public', $img_name);
        }
        if (Articles::create($article)) {
            return back()->with(['message' => 'Article Created', 'class' => 'success']);
        } else {
            return back()->with(['message' => 'Article Create Failed!', 'class' => 'danger']);
        }
    }

    public function showEdit($id)
    {
        if ($article = Articles::find($id)) {
            return view('admin.articles.edit', ['article' => $article]);
        } else {
            return back()->with(['message' => 'Article Not Found!', 'class' => 'danger']);
        }
    }

    public function edit(Request $request, $id)
    {
        if ($article = Articles::find($id)) {
            $article->update($request->all());
            return back()->with(['message' => 'Article Updated!', 'class' => 'success']);
        }
    }

    public function delete($id)
    {
        if ($article = Articles::find($id)) {
            $url = 'http://' . request()->getHost() . '/';
            $file = Str::after($article->cover, $url);
            $this->delete($file);
            $article->delete();
            return back()->with(['message' => 'Article Deleted', 'class' => 'warning']);
        } else {
            return back()->with(['message' => 'Article Not Found', 'class' => 'danger']);
        }
    }

    public function send($id)
    {
        $alumni = DB::table('alumni')->where('id', 1)->get()->first();
        $article = Articles::find($id);
        // $mail = Mail::to($alumni->email)->send(new LatestArticle($alumni, $article));
        // if (Mail::failures()) {
        // $emails = Mail::failures();
        // return back()->with('emails', $emails);
        // }
        // return redirect()->back()->with(['message' => 'Article Send Succes!', 'class' => 'success']);
    }
}
