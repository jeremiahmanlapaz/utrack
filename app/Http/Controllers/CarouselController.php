<?php

namespace App\Http\Controllers;

use App\Carousel;

use App\Traits\UploadTrait;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

class CarouselController extends Controller
{
    use UploadTrait;

    public function get()
    {
        $carousel = Carousel::all()->values();
        return $carousel;
    }

    public function showCreateCarousel()
    {
        return view('admin.content.carousel.create');
    }

    public function create(Request $request)
    {

        $request->validate([
            'image' => 'image|mimes:jpeg,png,jpg,gif|max:5000|required',
            'title' => 'string|nullable',
            'header' => 'string|nullable|max:191',
            'description' => 'string|nullable|max:191'
        ]);
        $carousel = $request->all();
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $img_name = Str::slug(Str::before($image->getClientOriginalName(), '.'), '-') . '-Carousel';
            $folder = 'uploads/carousel/';
            $filePath =  $folder . $img_name . '.' . $image->getClientOriginalExtension();
            $carousel['image'] = $filePath;
            Carousel::create($carousel);
            $this->uploadOne($image, $folder, 'public', $img_name);
            return back()->with(['message' => 'Carousel Added!', 'class' => 'success']);
        }
    }

    public function edit($id)
    {
        $carousel = Carousel::find($id);
        if ($carousel) {
            return view('admin.content.carousel.edit', ['carousel' => $carousel]);
        }
        return back()->with(['message' => 'Carousel Not Found!', 'class' => 'danger']);
    }

    public function delete($id)
    {
        $carousel = Carousel::find($id);
        if ($carousel) {
            $file = $carousel->image;
            $this->delete($file, 'public');
            $carousel->delete();
            return back()->with(['message' => 'Carousel Deleted!', 'class' => 'warning']);
        }
    }

    public function restore($id)
    {
        Carousel::onlyTrashed()->find($id)->restore();
        return $this->resolve();
    }

    public function trashed()
    {
        return Carousel::onlyTrashed()->get();
    }
}
