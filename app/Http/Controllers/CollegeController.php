<?php

namespace App\Http\Controllers;

use App\College;
use App\Course;
use Illuminate\Http\Request;

class CollegeController extends Controller
{
    public function index()
    {
        return view('admin.college.index');
    }

    public function all()
    {
        $data = College::orderBy('name')->get();

        return $data;
    }

    public function create(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'code' => 'required'
        ]);
        if (College::create($request->except('_token')))
            return back()->with(['message' => 'Create Success', 'class' => 'success']);
        else {
            return back()->with(['message' => 'Create Failed', 'class' => 'danger']);
        }
    }

    public function get($code)
    {
        $data = College::where('code', $code);
        return $data;
    }

    public function getCourse($code)
    {
        $data = Course::where('college', $code)->get();
        return $data;
    }

    public function course($code)
    {
        return view('admin.college.course', ['code' => $code]);
    }

    public function createCourse(Request $request, $code)
    {
        $request->validate([
            'name' => 'required',
            'degree' => 'required'
        ]);

        $data = $request->except('_token');
        $data['college'] = $code;
        // dd($data);
        if (Course::create($data)) {
            return back()->with(['message' => 'Create Success', 'class' => 'success']);
        } else {
            return back()->with(['message' => 'Create Failed', 'class' => 'danger']);
        }
    }

    public function deleteCourse($id)
    {
        $data = Course::find($id)->delete();
        $data->delete();
        return $this->resolve();
    }

    public function editCourse(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'degree' => 'required'
        ]);

        $data = Course::find($id)->update($request->all());
        if ($data) {
            return back()->with(['message' => 'Update Successful', 'class' => 'success']);
        } else {
            return back()->with(['message' => 'Update Failed', 'class' => 'danger']);
        }
    }
}
