<?php

namespace App\Http\Controllers;

use App\Company;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    public function index()
    {
        return view('admin.company.index');
    }

    public function all()
    {
        $companies = Company::all();
        return $companies;
    }

    public function showCreate()
    {
        return view('admin.company.create');
    }

    public function create(Request $request)
    {
        $request->validate([
            'company_name' => 'required|unique:companies',
            'logo' => 'nullable|mimes:jpeg,bmp,png,gif,svg|max:',
            'industry' => 'required',
            'url' => 'string|nullable'
        ]);
        if (Company::create($request)) {
            return back()->with(['message' => 'Company Created', 'class' => 'success']);
        } else {
            return back()->with(['message' => 'Company Error', 'class' => 'danger']);
        }
    }

    public function showEdit($id)
    {

        if ($company = Company::find($id)) {
            return view('admin.company.edit', ['company' => $company]);
        } else {
            return back()->with(['message' => 'Company Not Found', 'class' => 'danger']);
        }
    }

    public function edit(Request $request, $id)
    {
        $request->validate([
            'company_name' => 'required|unique:companies',
            'logo' => 'nullable|mimetypes: ',
            'industry' => 'required',
            'url' => 'string|nullable'
        ]);
        if (Company::where('id', $id)->update($request->except('_token'))) {
            return back()->with(['message' => 'Company Updated', 'class' => 'success']);
        } else {
            return back()->with(['message' => 'Company Update Failed', 'class' => 'danger']);
        }
    }

    public function delete($id)
    {
        Company::find($id)->delete();
        return $this->resolve();
    }
}
