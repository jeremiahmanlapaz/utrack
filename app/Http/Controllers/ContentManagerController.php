<?php

namespace App\Http\Controllers;

class ContentManagerController extends Controller
{
    public function index()
    {
        return view('admin.content.index');
    }

    public function archive()
    {
        return view('admin.content.archive');
    }
}
