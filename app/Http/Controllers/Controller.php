<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function resolve($statusCode = 200, $message = 'Success', $data = [])
    {
        if($data == [] || $data == null) {
            return response()->json([
                'message' => $message
            ], $statusCode);
        } else {
            return response()->json([
                'message' => $message,
                'data' => $data,
            ], $statusCode);
        }
    }
    protected function reject($statusCode = 400, $message = 'Reject',  $data = [])
    {
        if($data == [] || $data == null) {
            return response()->json([
                'message' => $message
            ], $statusCode);
        } else {
            return response()->json([
                'message' => $message,
                'data' => $data,
            ], $statusCode);
        }
    }

    function getRandomTimestamps($backwardDays = -800)
	{
		$backwardCreatedDays = rand($backwardDays, 0);
		$backwardUpdatedDays = rand($backwardCreatedDays + 1, 0);

		return [
			'created_at' => Carbon::now()->addDays($backwardCreatedDays)->addMinutes(rand(0,
				60 * 23))->addSeconds(rand(0, 60)),
			'updated_at' => Carbon::now()->addDays($backwardUpdatedDays)->addMinutes(rand(0,
				60 * 23))->addSeconds(rand(0, 60))
		];
	}
}
