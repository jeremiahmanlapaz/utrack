<?php

namespace App\Http\Controllers;

use Globe\Connect\Oauth;

class GcashController extends Controller
{

    const key = "p9pBhxL4MkfjgiBBq4c4oyfEx9A8hzo7";
    const secret = "5f94fb0accb0e08f23262af802a76c850220fc40df9e33efec7071aa1efa00ef";
    const code = "eHyLMMqSzaMMRSeL7nMs6aR9BunXBqGUEMznKseRadquM6zb6fbgLkMuLdRkxC4jyARIegA4dSd7q67hALKp5fx7RzMuxkAKquREGqeUdyz4kf6RzBpCjqBEoC7kTRLyEX7cB8GCdXzBeCnRz7Lf5zGy7Ux4A5Ru6ARL7uKxKp4fakqxph9zAMrS7zyeBIpLRMKCdpLq4uydzogfa8arbuBMzd8sL6B5RUnMRXKu6p7Ras7pM6oSg7MjrSgB5ezH";

    public function __constructor(){
        $oauth = new Oauth(self::key, self::secret);

        // get redirect url
        echo $oauth->getRedirectUrl();

        // // redirect to dialog and process the code then ...

        // get access token
        echo $oauth->setCode(self::code);

        return $oauth;
    }

    public function getToken(){
        return response()->json(new GcashController());
    }
}

