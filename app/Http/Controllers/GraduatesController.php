<?php

namespace App\Http\Controllers;

use App\College;
use App\Graduate;

use App\Imports\GraduatesImport;
use App\Exports\GraduatesExport;

use Carbon\Carbon;

use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;

class GraduatesController extends Controller
{
    public function index()
    {
        return view('admin.graduates.index', ['colleges' => College::all()->sortBy('name')]);
    }

    public function import(Request $request)
    {
        $request->validate([
            // 'file' => 'mimetypes: application/vnd.ms-excel, text/csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet|required'
            'file' => 'mimes:xlsx,csv,xls,xlm,xla,xlc,xlt,xlw|required'
        ]);
        $data = Excel::import(new GraduatesImport, request()->file('file'));
        // dd($data);
        if ($data) {
            return back()->with(['message' => 'Import Success', 'class' => 'success']);
        } else {
            return back()->with(['message' => 'Import Failed', 'class' => 'danger']);
        }
    }

    public function export()
    {
        return Excel::download(new GraduatesExport, 'graduates-' . Carbon::now() . '.xlsx');
    }

    public function insert(Request $request)
    {
        $request->validate([
            'graduate.*' => 'required'
        ]);

        $graduate = Graduate::where([
            'first_name' => $request->input('graduate.first_name'),
            'middle_name' => $request->input('graduate.middle_name'),
            'last_name' => $request->input('graduate.last_name')
        ])->get();

        if ($graduate->isNotEmpty()) {
            return back()->with(['message' => 'graduated already exists', 'class' => 'danger']);
        }

        if (Graduate::create($request->get('graduate'))) {
            return back()->with(['message' => 'Graduate added', 'class' => 'success']);
        } else {
            return back()->with(['message' => 'Graduate add failed', 'class' => 'danger']);
        }
    }

    public function all()
    {
        return Graduate::all();
    }
}
