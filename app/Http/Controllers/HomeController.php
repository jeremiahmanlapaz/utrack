<?php

namespace App\Http\Controllers;

use App\Alumni;
use App\Alumni_education;
use App\Alumni_employment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

use App\Traits\UploadTrait;

use App\College;
use App\Feedback;
use App\Payment;
use App\Company;
use App\Job_post;
use App\Partner;
use App\Applicant;
use App\Articles;
use App\Course;
use App\FirstJob;
use App\Suggestion;
use App\Industry;
use App\JobTitle;
use App\Mail\PaymentReceived;
use App\Mail\RegistrationComplete;
use App\Mail\UpdateForm;

use Exception;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    use UploadTrait;

    public function index()
    {
        $articles = Articles::paginate(5);
        return view('home.index', ['articles' => $articles]);
    }

    public function news(Request $request)
    {
        $s = $request->input('s');
        $articles = Articles::latest()
            ->search($s)
            ->paginate(5);

        return view('home.articles.index', compact('articles', 's'));
    }

    public function articleSingle($article_id)
    {
        if ($article = Articles::find($article_id)) {
            return view('home.articles.single', ['article' => $article]);
        } else {
            return redirect()->route('news')->with(['message' => 'Article Not Found!', 'class' => 'danger']);
        }
    }

    public function about()
    {
        return view('home.about');
    }

    public function contact()
    {
        return view('home.contact');
    }

    public function colleges()
    {
        return view('home.colleges');
    }

    public function members()
    {
        return view('home.membership.membership');
    }

    public function benefits()
    {
        return view('home.membership.benefits');
    }

    public function partners()
    {
        $partners = Partner::all();
        $categories = [
            'Clothing',
            'Restaurant and Bars',
            'Tours and Travels',
            'Services',
            'Gadgets and Technology',
            'Others'
        ];

        return view('home.membership.partners', ['partners' => $partners, 'categories' => $categories]);
    }

    public function faqs()
    {
        return view('home.services.faqs');
    }

    public function job()
    {
        return view('home.services.jobs', ['companies' => Company::paginate(5)]);
    }

    public function jobSingle($company_name)
    {
        if ($company = Company::where('company_name', $company_name)->first()) {
            $jobs = Job_post::where('company_id', $company->id)->get();
            return view('home.services.job-single', ['company' => $company, 'jobs' => $jobs]);
        } else {
            return back()->with(['message' => 'Company Not Found', 'class' => 'error']);
        }
    }

    public function showRegistration()
    {
        $colleges = College::all();
        $course_list = Course::all();
        $job_title = JobTitle::all();
        $industries = Industry::orderBy('name', 'asc')->get();
        return view('registration.index', ['colleges' => $colleges, 'course_list' => $course_list, 'job_title' => $job_title, 'industries' => $industries]);
    }

    public function register(Request $request)
    {
        $request->validate([
            'applicant.*'              => 'required',
            'applicant.email'          => 'email|unique:applicants,email',
            'applicant.image'          => 'image|mimes:jpeg,png,jpg,gif|required',
            'applicant.contact_number' => 'nullable||numeric',
            'education.*'              => 'required',
            'employment.*'             => 'required',
            'first_job.*'              => 'required'
        ]);

        $applicant = collect($request->applicant)->merge($request->education)->merge($request->employment)->toArray();
        $applicant['uuid'] = (string) Str::uuid();
        $firstJob = $request->get('first_job');

        if ($request->hasFile('applicant.image')) {
            $image = $request->file('applicant.image');
            $img_name = Str::slug(Str::before($applicant['uuid'], '.'), '-') . '-image';
            $folder = 'uploads/images/';
            $filePath =  $folder . $img_name . '.' . $image->getClientOriginalExtension();
            $applicant['image'] = $filePath;
            $this->uploadOne($image, $folder, 'public', $img_name);
        }

        if ($applicant['status'] == "no/never employed") {
            $applicant['reason'] = implode(", ", (array) $applicant['reason']);
        } else {
            $firstJob['uuid']              = $applicant['uuid'];
            $firstJob['reason_accepting']  = implode(", ", (array) $firstJob['reason_accepting']);
            $firstJob['competency_skills'] = implode(", ", (array) $firstJob["competency_skills"]);
        }

        try {
            Mail::to($applicant['email'])->send(new RegistrationComplete($request->applicant));
        } catch (\Exception $e) {
            return back()->with(['message' => 'No Internet Connection', 'class' => 'danger'])->withInput();
        }

        if (!(Applicant::create($applicant))) {
            return back()->with(['message' => 'Registration Failed', 'class' => 'danger']);
        }

        if ($request->get('suggestion')) {
            $suggestion['suggestion'] = $request->get('suggestion');
            $suggestion['uuid']       = $applicant['uuid'];
            Suggestion::create($suggestion);
        }

        if ($firstJob) {
            FirstJob::create($firstJob);
        }

        return back()->with(['message' => 'Application complete, please check your email.', 'class' => 'success']);
    }

    public function complete()
    {
        return view('registration.complete');
    }

    public function getCourse($college = null)
    {
        $course = Course::where('college', $college)->select('*')->get();
        return $course;
    }

    public function showPayment($uuid)
    {
        $applicant = Applicant::where('uuid', $uuid)->first();
        if ($applicant)
            return view('registration.payment');
        else
            return redirect()->route('home');
    }

    public function payment(Request $request, $uuid)
    {
        $request->validate([
            'proof_of_payment' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:4000',
            'payment_method' => 'required',
            'amount_paid' => 'required',
            'delivery_method' => 'required',
            'delivery_address' => 'required_unless:delivery_method,Pick up at CAA office(UMak)'
        ]);

        if ($request->hasFile('proof_of_payment')) {
            $image = $request->file('proof_of_payment');
            $img_name = $uuid . '-proof_of_payment';
            $folder = 'uploads/payments/';
            $filePath = $folder . $img_name . '.' . $image->getClientOriginalExtension();
        }

        $user = Applicant::where('uuid', $uuid)->get()->first();
        $payment = Payment::where('uuid', $uuid)->get()->first();

        $payment->proof_of_payment = $filePath;
        $payment->amount_paid = $request->amount_paid;
        $payment->payment_method = $request->payment_method;
        switch ($request->delivery_method) {
            case 'Pick up at CAA office(UMak)':
                $payment->delivery_fee = '0';
                $payment->delivery_method = $request->delivery_method;
                break;
            case 'Deliver within NCR':
                $payment->delivery_fee = '120';
                $payment->delivery_address = $request->delivery_address;
                $payment->delivery_method = $request->delivery_method;
                break;
            case 'Deliver within Luzon area(North and South)':
                $payment->delivery_fee = '120';
                $payment->delivery_address = $request->delivery_address;
                $payment->delivery_method = $request->delivery_method;
                break;
            case 'Deliver within Visayas':
                $payment->delivery_fee = '160';
                $payment->delivery_address = $request->delivery_address;
                $payment->delivery_method = $request->delivery_method;
                break;
            case 'Deliver within Mindanao':
                $payment->delivery_fee = '160';
                $payment->delivery_address = $request->delivery_address;
                $payment->delivery_method = $request->delivery_method;
                break;
        }

        $payment->total_amount += $payment->delivery_fee;
        $payment->status = 'confirmation';
        $payment->is_paid = '1';
        try {
            Mail::to($user->email)->send(new PaymentReceived($user));
        } catch (Exception $e) {
            return back()->with(['message' => 'No Internet Access', 'class' => 'danger']);
        }
        $this->uploadOne($image, $folder, 'public', $img_name);
        $payment->save();
        return redirect()
            ->route('register.payment.complete', ['uuid' => $payment->uuid, 'email' => $user->email]);
    }

    public function completePayment($uuid, $email)
    {
        $payment = Payment::where('uuid', $uuid)->first();
        if ($payment)
            return view('registration.payment_complete', ['email' => $email]);
        else
            return redirect()->route('home');
    }

    public function showUpdate()
    {
        return view('update.show');
    }

    public function sendUpdateForm(Request $request)
    {
        $request->validate([
            'email' => 'required'
        ]);
        $email = $request->email;
        $alumni = Alumni::where('email', html_entity_decode($email))->first();
        if ($alumni) {
            try {
                Mail::to($alumni->email)->send(new UpdateForm($alumni));
            } catch (Exception $e) {
                return back()->with(['message' => 'No Internet Connection', 'class' => 'danger']);
            }
            return back()->with(['message' => 'Link send to ' . $email, 'class' => 'success']);
        } else {
            return back()->with(['message' => 'Member Not Found', 'class' => 'danger']);
        }
    }

    public function updateForm($uuid)
    {
        $alumni = Alumni::where('uuid', $uuid)->first();
        $reason = DB::table('reasons')->select('reason')->get();
        $colleges = College::all();
        $industries = Industry::all();
        return view('update.index', [
            'alumni' => $alumni,
            'colleges' => $colleges,
            'industries' => $industries,
            'reasons' => $reason
        ]);
    }

    public function update($uuid, Request $request)
    {
        $alumniData = $request->applicant;
        $jobData = $request->employment;
        $jobData['is_first'] = false;
        $jobData['uuid'] = $uuid;
        if (Alumni::where('uuid', $uuid)->first()->update($alumniData) && Alumni_employment::create($jobData)) {
            return redirect()->route('update')->with(['message' => 'Update Submitted, Thank you for updating', 'class' => 'success']);
        } else {
            return back()->with(['message' => 'Update error', 'class' => 'danger']);
        }
    }

    public function feedback(Request $request)
    {
        $feedback = Feedback::create($request->all());
        if ($feedback) {
            return  back()->with(['message' => 'Thank you for your Feedback!', 'class' => 'success']);
        }
    }
}
