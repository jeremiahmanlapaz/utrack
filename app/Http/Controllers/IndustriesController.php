<?php

namespace App\Http\Controllers;

use App\Industry;
use App\JobTitle;
use Illuminate\Contracts\Queue\Job;
use Illuminate\Http\Request;

class IndustriesController extends Controller
{
    public function industry()
    {
        return view('admin.industries.industry');
    }

    public function occupation()
    {
        return view('admin.industries.occupation');
    }

    public function getOccupation()
    {
        $data = JobTitle::all();
        return $data;
    }

    public function getIndustry()
    {
        $data = Industry::all();
        return $data;
    }

    public function addOccupation(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'industry' => 'required'
        ]);

        JobTitle::create($request->except('_token'));

        return back()->with(['message' => 'Occupation Added', 'class' => 'success']);
    }

    public function addIndustry(Request $request)
    {
        $request->validate([
            'name' => 'required'
        ]);
        Industry::create($request->except('_token'));
        return back()->with(['message' => 'Industry Added', 'class' => 'success']);
    }

    public function deleteOccupation($id)
    {
        JobTitle::find($id)->delete();
        return $this->resolve();
    }

    public function deleteIndustry($id)
    {
        Industry::find($id)->delete();
        return $this->resolve();
    }
}
