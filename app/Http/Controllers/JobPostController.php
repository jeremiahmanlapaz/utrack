<?php

namespace App\Http\Controllers;

use App\Job_post;
use App\Company;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class JobPostController extends Controller
{
    public function index($company_id)
    {
        return view('admin.job.index', ['company_id' => $company_id]);
    }

    public function showCreate($company_id)
    {
        return view('admin.job.create');
    }

    public function create(Request $request, $company_id)
    {
        $request->validate([
            'position' => 'required',
            'salary' => 'required',
            'description' => 'required',
            'requirements' => 'required',
            'end_date' => 'required',
            'url' => 'nullable'
        ]);
        $data = $request->except('_token');
        $data['company_id'] = $company_id;
        if (Job_post::create($data)) {
            return redirect()->route('company')->with(['message' => 'Create Success', 'class' => 'success']);
        } else {
            return redirect()->route('company')->with(['message' => 'Create Failed', 'class' => 'danger']);
        }
    }

    public function all()
    {
        $jobs = Job_post::all();
        return $jobs;
    }

    public function get($company_id)
    {
        $job = DB::table('job_posts')
            ->where('company_id', $company_id)
            ->get();
        return $job;
    }

    public function delete($id)
    {
        Job_post::find($id)->delete();
        return $this->resolve();
    }
}
