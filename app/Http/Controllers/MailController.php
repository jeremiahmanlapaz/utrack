<?php

namespace App\Http\Controllers;

use BeyondCode\Mailbox\Facades\Mailbox;
use BeyondCode\Mailbox\InboundEmail;

class MailController extends Controller
{
    public function index(){
        return view('admin.mails.index');
    }

    public function compose(){
        return view('admin.mails.compose');
    }

    public function receiveEmail(){
        Mailbox::from('utrack@gmail.com', function(InboundEmail $email){
            return response($email);
        });
    }
    
}
