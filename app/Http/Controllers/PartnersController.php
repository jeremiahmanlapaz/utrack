<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

use App\Partner;
use App\Traits\UploadTrait;

class PartnersController extends Controller
{
    use UploadTrait;

    public function index()
    {
        $partners = Partner::all();
        return view('admin.partners.index', ['partners' => $partners]);
    }

    public function showCreate()
    {
        return view('admin.partners.create');
    }

    public function create(Request $request)
    {
        $request->validate([
            'logo' =>  'image|mimes:jpeg,png,jpg,gif',
            'name' => 'required',
            'description' => 'required',
            'category' => 'required',
            'validity_date' => 'required'
        ]);
        $partner = $request->all();
        if($request->hasFile('logo')){
            $image = $request->file('logo');
            $img_name = Str::slug($request->name, '-') . '-logo';
            $folder = 'uploads/partners/';
            $filePath =  $folder . $img_name . '.' . $image->getClientOriginalExtension();
            $partner['logo'] =  $filePath;
            $this->uploadOne($image, $folder, 'public', $img_name);
        }
        Partner::create($partner);
        return back()->with(['message' => 'Partner Created', 'class' => 'success']);
    }

    public function showEdit($id)
    {
        $partner = Partner::find($id);
        return view('admin.partners.edit', ['partner' => $partner]);
    }

    public function edit(Request $request, $id)
    {
        $request->validate(['']);

        Partner::find($id)->update($request->all());

        return back()->with(['message' => 'Partner Updated', 'class' => 'success']);
    }

    public function all()
    {
        $partners = Partner::all();
        return $partners;
    }
}
