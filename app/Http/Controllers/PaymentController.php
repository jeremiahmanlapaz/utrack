<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

use App\Payment;
use App\Applicant;
use App\Alumni;
use App\Alumni_education;
use App\Alumni_employment;
use App\Mail\PaymentComplete;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class PaymentController extends Controller
{
    public function index()
    {
        return view('admin.payment.index');
    }

    public function all()
    {
        $payments = DB::table('payments')
            ->select(
                'applicants.first_name',
                'applicants.middle_name',
                'applicants.last_name',
                'payments.*'
            )
            ->join('applicants', 'applicants.uuid', '=', 'payments.uuid')
            ->where('payments.status', '!=', 'paid')
            ->get();
        return $payments;
    }

    public function confirm($uuid)
    {
        $payment = Payment::where('uuid', $uuid)->first();
        if (!$payment) {
            return $this->reject();
        }
        $payment->status = "paid";
        $payment->touch();
        $payment->save();
        return $this->resolve();
    }

    public function get($uuid)
    {
        $payment = Payment::where('uuid', $uuid)->get();
        return $payment;
    }

    public function showPaid()
    {
        return view('admin.payment.paid');
    }

    public function paid()
    {
        $payments = DB::table('payments')
            ->select(
                'applicants.first_name',
                'applicants.middle_name',
                'applicants.last_name',
                'payments.*'
            )
            ->join('applicants', 'applicants.uuid', '=', 'payments.uuid')
            ->where('payments.status', 'paid')
            ->get();
        return $payments;
    }

    public function showUpdate($id)
    {
        $payment = Payment::find($id);
        return view('admin.payment.update', ['payment' => $payment]);
    }

    public function update($uuid)
    {
        $payment = Payment::where('uuid', $uuid)->first();
        $applicant = Applicant::where('uuid', $uuid)->first()->toArray();
        $applicant['is_first'] = true;
        try {
            Mail::to($applicant['email'])->send(new PaymentComplete($applicant));
        } catch (Exception $e) {
            return back()->with(['message' => 'No Internet Connection', 'class' => 'danger']);
        }
        $payment->status = 'paid';
        Alumni::create($applicant);
        Alumni_education::create($applicant);
        Alumni_employment::create($applicant);
        $payment->save();

        return redirect()->route('payment')->with(['message' => 'Payment Updated to Paid!', 'class' => 'success']);
    }

    public function rejectPayment(Request $request)
    {
        $payment = Payment::where('uuid', $request->get('uuid'))->first();
        // dd($payment);
        $payment->status = 'rejected';
        $payment->save();
        return redirect()->route('payment');
    }
}
