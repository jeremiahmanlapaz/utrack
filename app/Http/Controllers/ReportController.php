<?php

namespace App\Http\Controllers;

use App\College;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    public function index()
    {
        $college = College::all();
        return view('admin.reports.index', ['colleges' => $college]);
    }

    public function show(Request $request)
    {
        $year = $request->get('year');
        $college = $request->get('college');
        $type = $request->get('type');

        $data = collect([
            'year' => $year,
            'college' => $college,
            'industries' => $this->countIndustry($college, $year),
            'employment_rate' => $this->employmentRate($college, $year),
            'place_of_work' => $this->placeOfWork($college, $year),
            'job_level' => $this->jobLevel($college, $year),
            'reasons' => $this->reasons($college, $year),
            'job_title' => $this->jobTitleIndustry($college, $year)
        ]);
        // dd($data);
        if ($type == 'chart') {
            return view('admin.reports.show', ['data' => $data]);
        } else {
            return view('admin.reports.table', ['data' => $data]);
        }
    }

    public function countIndustry($college, $year)
    {
        $data = DB::table('alumni_employments')
            ->select('alumni_employments.industry as category', DB::raw('COUNT(alumni_employments.industry) as value'))
            ->join('alumni_educations', 'alumni_educations.uuid', '=', 'alumni_employments.uuid')
            ->where('alumni_educations.college', '=', $college)
            ->where('alumni_educations.year_graduated', '=', $year)
            ->where('alumni_employments.status', '!=', 'no/never employed')
            ->where('alumni_employments.is_first', true)
            ->groupBy('alumni_employments.industry')
            ->get();
        return $data;
    }

    public function employmentRate($college, $year)
    {
        $data = DB::table('alumni_employments')
            ->select('alumni_employments.status as category', DB::raw('count(status) as value'))
            ->join('alumni_educations', 'alumni_educations.uuid', '=', 'alumni_employments.uuid')
            ->where('alumni_educations.college', '=', $college)
            ->where('alumni_educations.year_graduated', '=', $year)
            ->where('alumni_employments.is_first', true)
            ->groupBy('alumni_employments.status')
            ->get();
        return $data;
    }

    public function placeOfWork($college, $year)
    {
        $data = DB::table('alumni_employments')
            ->select('alumni_employments.place_of_work as category', DB::raw('count(*) as value'))
            ->join('alumni_educations', 'alumni_educations.uuid', '=', 'alumni_employments.uuid')
            ->where('alumni_educations.college', $college)
            ->where('alumni_educations.year_graduated', $year)
            ->where('alumni_employments.is_first', true)
            ->whereNotNull('alumni_employments.place_of_work')
            ->groupBy('alumni_employments.place_of_work')
            ->get();
        return $data;
    }

    public function jobLevel($college, $year)
    {
        $data = DB::table('alumni_employments')
            ->select('alumni_employments.job_level as category', DB::raw('count(*) as value'))
            ->join('alumni_educations', 'alumni_educations.uuid', '=', 'alumni_employments.uuid')
            ->where('alumni_educations.college', $college)
            ->where('alumni_educations.year_graduated', $year)
            ->where('alumni_employments.is_first', true)
            ->whereNotNull('alumni_employments.job_level')
            ->groupBy('alumni_employments.job_level')
            ->get();
        return $data;
    }

    public function reasons($college, $year)
    {
        $data = DB::table('alumni_employments')
            ->select('alumni_employments.reason as category', DB::raw('count(*) as value'))
            ->join('alumni_educations', 'alumni_educations.uuid', '=', 'alumni_employments.uuid')
            ->where('alumni_educations.college', $college)
            ->where('alumni_educations.year_graduated', $year)
            ->whereNotNull('alumni_employments.reason')
            ->groupBy('alumni_employments.reason')
            ->get();
        return $data;
    }

    public function jobTitleIndustry($college, $year)
    {
        $data = DB::table('alumni_employments')
            ->select(DB::raw('count(alumni_employments.occupation) as value'), 'job_title.industry as category')
            ->join('alumni_educations', 'alumni_educations.uuid', '=', 'alumni_employments.uuid')
            ->join('job_title', 'job_title.title', '=', 'alumni_employments.occupation')
            ->where('alumni_educations.college', $college)
            ->where('alumni_educations.year_graduated', $year)
            ->whereNotNull('alumni_employments.occupation')
            ->groupBy('job_title.industry')
            ->get();
        return $data;
    }

    public function registrationRate($year = 2019)
    {
        $data = DB::table('applicants')
            ->select(DB::raw('MONTH(created_at) as category'), DB::raw('count(MONTH(created_at)) as value'))
            ->whereRaw('YEAR(created_at) = ' . $year)
            ->groupBy(DB::raw('MONTH(created_at)'))
            ->get();

        foreach ($data as $key) {
            $key->category = Carbon::createFromFormat('m', $key->category)->format('F');
        }
        return $data;
    }
}
