<?php

namespace App\Http\Controllers;

use App\Alumni;

class SMSController extends Controller
{
    public $shortcode = 8586;
    public $access_token = "ebDTniHubm216FzawMs7sXr6-wrjomAVKSx4aR23woQ";

    public function sendCustomSMS($address, $message)
    {
        $this->sendSMS($address, $message);
    }

    public function sendUpdateNotification($uuid)
    {
        $alumni = Alumni::where('uuid', $uuid)->get()->first();

        if (!$alumni) {
            return $this->reject(400, 'reject', $alumni);
        }

        $address = $alumni->contact_number;

        $message = "Hey it's been since you update with us, visit http://web.utrack/members/update to update your profile. Thank you!";

        return $this->sendSMS($address, $message);
    }

    public function sendSMS($address, $message)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://devapi.globelabs.com.ph/smsmessaging/v1/outbound/" . $this->shortcode . "/requests?access_token=" . $this->access_token,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{\"outboundSMSMessageRequest\": { \"senderAddress\": \"" . $this->shortcode . "\", \"outboundSMSTextMessage\": {\"message\": \"" . $message . "\"}, \"address\": \"" . $address . "\" } }",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json"
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            return $this->reject();
        } else {
            return $this->resolve();
        }
    }
}
