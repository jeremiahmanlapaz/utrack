<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\College;
use App\Alumni;
use App\Alumni_education;
use App\Alumni_employment;
use App\Graduate;
use App\Payment;
use App\Company;
use App\Job_post;
use App\Articles;
use App\Partner;
use App\Applicant;
use App\Course;
use App\JobTitle;
use App\Mail\ApplicationVerified;

class TestingController extends Controller
{
    //FOR TESTING AND EVALUATION ONLY

    public function index()
    {
        return view('test.index');
    }

    public function massAddColleges(Request $request)
    {
        $colleges = $request->all();
        foreach ($colleges as $college) {
            $create = College::updateOrCreate($college);
            if (!$create) {
                return $this->reject($create);
            }
        }
        return $this->resolve();
    }

    public function massAddCompanies(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        foreach ($data as $company) {
            $create = Company::create($company);
            if ($create) {
                foreach ($company['job'] as $job) {
                    $job['company_id'] = $create['id'];
                    Job_post::create($job);
                }
            }
        }
        return $this->resolve();
    }

    public function massAddPartners(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        foreach ($data as $partner) {
            $create = Partner::create($partner);
        }
        return $this->resolve();
        dd($data);
    }

    public function massAddArticles(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        foreach ($data as $article) {
            $create = Articles::create($article);
        }
        return $this->resolve();
    }

    public function massAddCourse(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        foreach ($data as $courseList) {
            foreach ($courseList['course'] as $course) {
                $insert['college'] =  $courseList['college'];
                $insert['name'] = $course['name'];
                $insert['degree'] = $course['degree'];
                Course::create($insert);
            }
        }
        return $this->resolve();
    }

    public function massAddJobTitle(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        foreach ($data as $jobTitle) {
            JobTitle::create($jobTitle);
        }
        return $this->resolve();
    }

    public function testMail()
    {
        return view('test.mail');
    }

    public function previewMail()
    {
        return (new ApplicationVerified(Applicant::find(1), Payment::find(1)))->render();
        // return (new LatestArticle())->render();
    }

    public function testAlert()
    {
        return redirect()->back()->with(['message' => 'alert test!', 'class' => 'success']);
    }

    public function truncate($table)
    {
        switch ($table) {
            case 'all':
                Alumni::truncate();
                Alumni_education::truncate();
                Alumni_employment::truncate();
                Graduate::truncate();
                Applicant::truncate();
                Payment::truncate();
                break;
            case 'articles':
                Articles::truncate();
                break;
            default:
                return $this->reject();
        }
        return $this->resolve();
    }
}
