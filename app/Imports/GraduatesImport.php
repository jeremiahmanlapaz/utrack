<?php

namespace App\Imports;

use App\Graduate;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class GraduatesImport implements ToModel, WithHeadingRow
{
    public function model(array $row)
    {
        if (!isset($row['first_name'])) {
            return null;
        } else {
            return new Graduate([
                'first_name' => $row['first_name'],
                'middle_name' => $row['middle_name'],
                'last_name' => $row['last_name'],
                'college' => $row['college'],
                'course' => $row['course'],
                'degree' => $row['degree'],
                'year_graduated' => $row['year_graduated'],
            ]);
        }
    }

    public function HeadingRow(): int
    {
        return 1;
    }
}
