<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job_post extends Model
{

    protected $table = 'job_posts';

    protected $fillable = ['company_id', 'position', 'salary', 'description', 'requirements', 'post_date', 'end_date', 'url'];

    protected $hidden = ['created_at', 'updated_at'];
}
