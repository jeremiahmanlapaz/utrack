<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Article extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $user, $article;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    // public function __construct($user, $article)
    // {
    //     $this->user = $user;
    //     $this->article = $article;
    // }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // return $this->subject('Latest Article from UTrack')->view('mail.article', ['user' => $this->user, 'article' => $this->article]);
        return $this->subject('Latest Article from UTrack')->view('mail.article');
    }
}
