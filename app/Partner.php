<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Partner extends Model
{
    use SoftDeletes;

    protected $table = 'partners';

    protected $fillable = [
        'name',
        'logo',
        'description',
        'posted_date',
        'validity_date',
        'category'
    ];

    protected $hidden = ['created_at', 'updated_at'];
}
