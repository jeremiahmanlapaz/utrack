<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Payment extends Model
{
    use SoftDeletes;

    protected $table = 'payments';

    protected $fillable = [
        'uuid',
        'delivery_fee',
        'delivery_address',
        'delivery_method',
        'membership_fee',
        'alumni_id_fee',
        'payment_method',
        'status',
        'total_amount',
        'amount_paid',
        'is_paid',
        'updated_at',
        'created_at'
    ];
}
