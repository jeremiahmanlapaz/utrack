<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Auth;

use App\College;
use App\Company;
use App\Carousel;
use App\Partner;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Using Closure based composers...
        View::composer(['home.registration.index', 'home.update.index'], function ($view) {
            $view->with('colleges', College::all());
        });

        View::composer('home.membership.partners', function ($view) {
            $categories = [
                'Clothing',
                'Restaurant and Bars',
                'Tours and Travels',
                'Services',
                'Gadgets and Technology',
                'Others'
            ];
            $view->with(['partners' => Partner::all(), 'categories' => $categories]);
        });

        view()->composer('layouts.master', function ($view) {
            $view->with([
                'companies' => Company::all()->take(5),
                'carousel' => Carousel::all()
            ]);
        });

        view()->composer('layouts.admin_master', function ($view) {
            $view->with('admin', Auth::user());
        });
    }
}
