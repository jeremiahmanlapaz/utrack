<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Suggestion extends Model
{
   protected $table = 'suggestions';

   protected $fillable = ['uuid', 'suggestion'];
   
   protected $hidden = ['created_at', 'updated_at'];
}
