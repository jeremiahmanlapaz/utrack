<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

use App\Alumni;
use App\Alumni_education;
use App\Alumni_employment;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

$factory->define(Alumni::class, function (Faker $faker) {
    return [
        'uuid' => Str::uuid(),
        'first_name' => $faker->firstName(),
        'middle_name' => $faker->lastName(),
        'last_name' => $faker->lastName(),
        'email' => $faker->unique()->safeEmail,
        'birth_date' => mt_rand(Carbon::now()->subWeeks(2)->getTimestamp(), Carbon::now()->getTimestamp()),
        'contact_number' => $faker->tollFreePhoneNumber(),
        'address' => $faker->address(),
        'civil_status' => Arr::random(['Single', 'Married', 'Divorce', 'Single Parent']),
        'gender' => Arr::random(['Male', 'Female']),
        'status' => 'Applicant'
    ];
});