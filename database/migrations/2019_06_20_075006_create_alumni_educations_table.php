<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlumniEducationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alumni_educations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('uuid');
            $table->string('degree');
            $table->string('college')->nullable();
            $table->string('course')->nullable();
            $table->string('year_graduated');
            $table->boolean('is_first');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alumni_educations');
    }
}
