<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('uuid');
            $table->string('delivery_method')->nullable()->default('pick-up');
            $table->string('delivery_fee')->nullable()->default(0);
            $table->string('delivery_address')->nullable();
            $table->string('membership_fee')->nullable();
            $table->string('alumni_id_fee')->default(50);
            $table->string('payment_method')->nullable();
            $table->string('total_amount')->nullable();
            $table->string('amount_paid')->nullable();
            $table->string('proof_of_payment')->nullable();
            $table->string('is_paid')->nullable();
            $table->string('status')->default('pending');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
