<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFirstJobTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('first_job', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('uuid');
            $table->string('is_first_job');
            $table->text('reason_accepting')->nullable();
            $table->string('how_long_did_take');
            $table->text('competency_skills')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('first_job');
    }
}
