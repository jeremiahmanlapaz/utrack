<?php

use App\Course;
use App\Industry;

use Carbon\Carbon;

use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class ApplicantsDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $college = [
            'CCS', 'CBFS', 'COE',
            'COAHS', 'CHK', 'CAL',
            'COS', 'CGPP', 'CMLI',
            'CTM', 'CTHM', 'CCSCE'
        ];

        $status = [
            'regular',
            'contractual',
            'temporary',
            'self employed',
            'no/never employed'
        ];

        $jobLevel = [
            'Rank or clerical',
            'Professional, Supervisory or Technical',
            'Managerial or Executive',
            'Self-employed'
        ];

        $reason = [
            'Advance or further study',
            'No job opportunity',
            'Family concern and decided not to find a job',
            'Did not look for a job',
            'Health-related reason(s)',
            'Lack of work experience',
            'other reason(s)'
        ];

        $industry = Industry::all();
        $max = 50;
        for ($i = 0; $i < $max; $i++) {
            $applicant = [];
            $applicant['uuid']           = (string) Str::uuid();
            $applicant['created_at']     = mt_rand(
                Carbon::now()->subYear()->getTimestamp(),
                Carbon::now()->subMonth()->getTimestamp()
            );
            $applicant['updated_at']     = $applicant['created_at'];
            $applicant['gender']         = Arr::random(['male', 'female']);
            $applicant['first_name']     = $faker->firstName($applicant['gender']);
            $applicant['middle_name']    = $faker->lastName();
            $applicant['last_name']      = $faker->lastName();
            $applicant['email']          = $applicant['first_name'] . '.' . $applicant['last_name'] . '@example.com';
            $applicant['birth_date']     = $this->randomDate();
            $applicant['contact_number'] = $faker->tollFreePhoneNumber();
            $applicant['address']        = $faker->address();
            $applicant['civil_status']   = Arr::random(['Single', 'Married', 'Separated', 'Widowed']);

            $applicant['college']        = 'CCS';
            // $applicant['college']        = Arr::random($college);
            $applicant['course']         = $this->getCourseName($applicant['college']);
            $applicant['degree']         = $this->getCourseDegree($applicant['college']);
            $applicant['year_graduated'] = 2019;

            $applicant['status']         = Arr::random($status);
            if ($applicant['status'] != 'no/never employed') {
                $applicant['industry']       = $industry->random()->name;
                $applicant['company_name']   = $faker->company();
                $applicant['occupation']     = $faker->jobTitle();
                $applicant['job_level']      = Arr::random($jobLevel);
                $applicant['place_of_work']  = Arr::random(['Local', 'Abroad']);
            } else {
                $applicant['reason']         = Arr::random($reason);
            }

            $graduate['first_name']     = $applicant['first_name'];
            $graduate['middle_name']    = $applicant['middle_name'];
            $graduate['last_name']      = $applicant['last_name'];
            $graduate['college']        = $applicant['college'];
            $graduate['course']         = $applicant['course'];
            $graduate['degree']         = $applicant['degree'];
            $graduate['year_graduated'] = $applicant['year_graduated'];

            App\Applicant::create($applicant);
            if ($i % 2 == 0) {
                App\Graduate::create($graduate);
            }
        }
    }

    public function getCourseName($college)
    {
        $course = Course::where('college', $college)->get()->toArray();
        $course = Arr::random($course);
        return $course['name'];
    }

    public function getCourseDegree($college)
    {
        $course = Course::where('college', $college)->get()->toArray();
        $course = Arr::random($course);
        return $course['degree'];
    }

    public function randomDate()
    {
        $date = mt_rand(Carbon::now()->subYears(20)->getTimestamp(), Carbon::now()->subYears(10)->getTimestamp());
        return Carbon::createFromTimestamp($date)->format('Y-m-d H:i:s');
    }
}
