<?php

use Illuminate\Database\Seeder;

use App\Articles;

class ArticlesDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        $faker = Faker\Factory::create();
        $max = 10;
        for ($i = 0; $i < $max; $i++) {
            $article['title'] = $faker->sentence();
            $article['body'] = $faker->realText();
            $article['posted_by'] = 'Admin';
            $article['cover'] = "https://picsum.photos/1200/400?random=" . rand(0, 50);
            Articles::create($article);
        }
    }
}
