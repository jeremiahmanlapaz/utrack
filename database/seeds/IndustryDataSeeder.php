<?php

use Illuminate\Database\Seeder;
use App\Industry;

class IndustryDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $industries = [
            'Manufacturing',
            'Finance / Banking',
            'Management',
            'Transportation',
            'Retail',
            'Infomation Technology / Computer Science',
            'Advertising',
            'Insurance',
            'Trade / Investment',
            'Sales / Marketing',
            'Telecommunication',
            'Publishing',
            'Food Industry',
            'Consultancy',
            'Wholesale',
            'Accounting',
            'Research & Development',
            'Logistics',
            'E-Commerce'
        ];

        foreach ($industries as $industry) {
            Industry::create(['name' => $industry]);
        }
    }
}
