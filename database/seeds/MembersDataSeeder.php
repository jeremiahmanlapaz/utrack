<?php

use App\Course;
use App\JobTitle;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;


class MembersDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $max = 50;
        $year = 2019;
        $college = 'CCSCE';
        // $college = [
        //     'CCS', 'CBFS', 'COE',
        //     'COAHS', 'CHK', 'CAL',
        //     'COS', 'CGPP', 'CMLI',
        //     'CTM', 'CTHM', 'CCSCE'
        // ];

        $status = [
            'regular',
            'contractual',
            'temporary',
            'self employed',
            'no/never employed'
        ];

        $jobLevel = [
            'Rank or clerical',
            'Professional, Supervisory or Technical',
            'Managerial or Executive',
            'Self-employed'
        ];

        $reason = [
            'Advance or further study',
            'No job opportunity',
            'Family concern and decided not to find a job',
            'Did not look for a job',
            'Health-related reason(s)',
            'Lack of work experience',
            'other reason(s)'
        ];

        $industry = App\Industry::all();

        $startDate = Carbon::createFromFormat('Y-m-d H:i:s', $year . '-01-01 00:00:00');

        $endDate = Carbon::createFromFormat('Y-m-d H:i:s', $startDate)->addMonths(12);

        if ($max > 50) {
            $jobTitle = JobTitle::where('industry', 'Information Technology')->get();
        } else {
            $jobTitle = JobTitle::where('industry', '!=', 'Information Technology')->get();
        }

        for ($i = 0; $i < $max; $i++) {
            $user['uuid']                = (string) Str::uuid();
            $user['gender']              = Arr::random(['male', 'female']);
            $user['first_name']          = $faker->firstName($user['gender']);
            $user['middle_name']         = $faker->lastName();
            $user['last_name']           = $faker->lastName();
            $user['email']               = $user['first_name'] . ' ' . $user['last_name'] . '@example.mail';
            $user['birth_date']          = mt_rand(Carbon::now()->subWeeks(2)->getTimestamp(), Carbon::now()->getTimestamp());
            // $user['contact_number']      = $faker->tollFreePhoneNumber();
            $user['contact_number']      = "09276387225";
            $user['address']             = $faker->address();
            $user['civil_status']        = Arr::random(['Single', 'Married', 'Separated', 'Widowed']);
            $user['gender']              = Str::title($user['gender']);
            $user['status']              = 'member';
            $user['created_at']          = mt_rand($startDate->getTimestamp(), $endDate->getTimestamp());
            $user['updated_at']          = $user['created_at'];

            $education['uuid']           = $user['uuid'];
            $education['college']        = $college;
            $education['course']         = $this->getCourseName($education['college']);
            $education['degree']         = $this->getCourseDegree($education['college']);
            $education['year_graduated'] = $year;
            $education['is_first']       = true;
            $education['created_at']     = $user['created_at'];
            $education['updated_at']     = $user['updated_at'];

            $job = [];

            $job['uuid']                 = $user['uuid'];
            $job['status']               = Arr::random($status);
            if ($job['status'] != 'no/never employed') {
                $job['industry']         = $industry->random()->name;
                $job['company_name']     = $faker->company();
                $job['occupation']       = $jobTitle->random()->title;
                $job['job_level']        = Arr::random($jobLevel);
                $job['place_of_work']    = Arr::random(['Local', 'Abroad']);
                $job['is_first']         = true;
            } else {
                $job['reason']           = Arr::random($reason);
            }
            $job['created_at']     = $user['created_at'];
            $job['updated_at']     = $user['updated_at'];

            $graduate['first_name']      = $user['first_name'];
            $graduate['middle_name']     = $user['middle_name'];
            $graduate['last_name']       = $user['last_name'];
            $graduate['gender']          = $user['gender'];
            $graduate['college']         = $education['college'];
            $graduate['course']          = $education['course'];
            $graduate['degree']          = $education['degree'];
            $graduate['year_graduated']  = $education['year_graduated'];

            $payment['uuid'] = $user['uuid'];
            $payment['alumni_id_fee'] = 50;
            switch ($education['degree']) {
                case 'Associate':
                    $payment['membership_fee'] = 100;
                    break;
                case 'Bachelor':
                    $payment['membership_fee'] = 150;
                    break;
                case 'Master':
                    $payment['membership_fee'] = 200;
                    break;
                case 'Doctorate':
                    $payment['membership_fee'] = 250;
                    break;
                default:
                    $payment['membership_fee'] = 50;
                    break;
            }

            $payment['delivery_method'] = Arr::random([
                'pick-up',
                'deliver with Luzon',
                'deliver with Visayas',
                'deliver with Mindanao',
            ]);
            switch ($payment['delivery_method']) {
                case 'deliver with Luzon':
                    $payment['delivery_fee'] =  120;
                    break;
                case 'deliver with Visayas':
                    $payment['delivery_fee'] =  160;
                    break;
                case 'deliver with Mindanao':
                    $payment['delivery_fee'] =  160;
                    break;
                default:
                    $payment['delivery_fee'] =  0;
                    break;
            }
            if ($payment['delivery_method'] != 'pick-up') {
                $payment['delivery_address'] = $faker->address();
            }
            $payment['total_amount'] = $payment['membership_fee'] + $payment['alumni_id_fee'] + $payment['delivery_fee'];
            $payment['amount_paid'] =  $payment['total_amount'];
            $payment['payment_method'] = Arr::random(['COOP', 'Bank']);
            $payment['status'] = 'paid';

            $applicant = collect($user)->merge($education)->merge($job);
            $applicant = $applicant->except('is_first')->toArray();
            $applicant['application_status'] = 'verified';

            App\Applicant::create($applicant);
            App\Payment::create($payment);
            App\Alumni::create($user);
            App\Alumni_education::create($education);
            App\Alumni_employment::create($job);
            App\Graduate::create($graduate);
        }
    }

    public function getCourseName($college)
    {
        $course = Course::where('college', $college)->get()->toArray();
        $course = Arr::random($course);
        return $course['name'];
    }

    public function getCourseDegree($college)
    {
        $course = Course::where('college', $college)->get()->toArray();
        $course = Arr::random($course);
        return $course['degree'];
    }
}
