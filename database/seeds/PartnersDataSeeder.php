<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;

class PartnersDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $max = 20;

        $category = [
            'Clothing',
            'Restaurant and Bars',
            'Tours and Travels',
            'Services',
            'Gadgets and Technology',
            'Others'            
        ];


        for ($i = 0; $i < $max; $i++) {
            $partners['name'] = $faker->company();
            $partners['logo'] = 'images/200x200.png';
            $partners['description'] = $faker->word(50);
            $partners['category'] = Arr::random($category);
            $partners['posted_date'] = Carbon::now()->format('F d, Y');
            $date = mt_rand(Carbon::now()->addDay()->getTimestamp(), Carbon::now()->addWeeks(2)->getTimestamp());
            $partners['validity_date'] = Carbon::createFromTimestamp($date)->format('F d, Y');
            App\Partner::create($partners);
        }
    }
}
