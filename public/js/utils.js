function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

var color = ['#845EC2',
    '#D65DB1',
    '#FF6F91',
    '#FF9671',
    '#FFC75F',
    '#F9F871',
    '#02D1DC',
    '#28DECD',
    '#5DE9B7',
    '#90F29C',
    '#C4F783',
    '#F9F871',
    '#02DC17',
    '#00C369',
    '#00A68D',
    '#008794',
    '#006680',
    '#2F4858'
]

let today = new Date();
let cur_date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
let cur_datetime = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate() + '-' + today.getTime();
