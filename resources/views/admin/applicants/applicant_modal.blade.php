<!-- Applicant Modal -->
<div class="modal fade" id="applicantModal" tabindex="-1" role="dialog" aria-labelledby="applicantModal"
    aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-body" id="#modal-body">
                <div class="alert alert-success" role="alert">
                    Match Found
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <h4>Applicant</h4>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">First Name:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" data-name="applicant_first_name"
                                    placeholder="First name" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Middle Name:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" data-name="applicant_middle_name"
                                    placeholder="Middle name" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="alum_lname" class="col-sm-2 col-form-label">Last Name:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" data-name="applicant_last_name"
                                    placeholder="Last name" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Degree:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" data-name="applicant_degree"
                                    placeholder="Degree" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">College:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" data-name="applicant_college"
                                    placeholder="College" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Course:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" data-name="applicant_course"
                                    placeholder="Course" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Year Graduated:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" data-name="applicant_year_graduated"
                                    placeholder="Year Graduated" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h4>Graduate List</h4>
                        <div class="form-group row">
                            <label for="grad_fname" class="col-sm-2 col-form-label">First Name:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" data-name="graduate_first_name"
                                    placeholder="First name" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="grad_mname" class="col-sm-2 col-form-label">Middle Name:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" data-name="graduate_middle_name"
                                    placeholder="Middle name" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="grad_lname" class="col-sm-2 col-form-label">Last Name:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" data-name="graduate_last_name"
                                    placeholder="Last name" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="grad_degree" class="col-sm-2 col-form-label">Degree:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" data-name="graduate_degree" placeholder="Degree"
                                    readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="grad_college" class="col-sm-2 col-form-label">College:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" data-name="graduate_college"
                                    placeholder="College" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="grad_course" class="col-sm-2 col-form-label">Course:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" data-name="graduate_course" placeholder="Course"
                                    readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="grad_year_graduated" class="col-sm-2 col-form-label">Year Graduated:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" data-name="graduate_year_graduated"
                                    placeholder="Year Graduated" readonly>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" name="confirmBtn" class="btn btn-success" data-id>Verify
                    Applicant</button>
            </div>
        </div>
    </div>
</div>