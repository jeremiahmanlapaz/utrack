@extends('layouts.admin_master')

@section('title', 'Archive')

@section('links')
<link rel="stylesheet" href="{{ asset('vendor/DataTables/datatables.min.css') }}">
@endsection

@section('content')
<div class="row">
    <div class="col-md-12 mb-4">
        <a class="btn btn-primary p-3" href="{{route('applicants')}}">
            <h6 class="m-0 font-weight-bold text-light"><i class="fa fa-chevron-left"></i> {{__('Back')}}</h6>
        </a>
    </div>
    <div class="col-md-12">
        <div class="card shadow mb-4">
            <a href="#collapseBody" class="d-block card-header py-3 bg-danger" data-toggle="collapse" role="button"
                aria-expanded="true" aria-controls="collapseBody">
                <h6 class="m-0 font-weight-bold text-light">{{__('Applicant / Archive')}}</h6>
            </a>
            <div class="collapse show" id="collapseBody">
                <div class="card-body">
                    <div class="table-responsive w-100">
                        <table class="table table-hover" id="archive_table" style="width:100%">
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="{{ asset('vendor/DataTables/datatables.min.js') }}"></script>
<script>
    $(document).ready(function(){
        let archive_table = $('#archive_table').DataTable({
            ajax: {
                url: "{{route('applicant.trashed')}}",
                dataSrc: ''
            },
            columns: [
                {data: 'first_name', title: 'First name'},
                {data: 'middle_name', title: 'Middle name'},
                {data: 'last_name', title: 'Last name'},
                {data: 'deleted_at', title: 'Archived at'},
                {
                    data: 'application_status',
                    title: 'Status',
                    render: function(data){
                        return '<span class="badge badge-pill badge-danger">' + data + '</span>';
                    }
                },
                {
                    data: null,
                    title: 'Action', 
                    render: function(data) {
                        return '<button class="btn btn-sm btn-warning" data-id=' + data.uuid + ' name=restore>Restore</button>';
                    }
                }
            ]
        });

        $(document).on('click', 'button[name=restore]', function() {
            let id = $(this).data('id');
            let url =  "{{route('applicant.restore', ['uuid' => ':id'])}}";
            $.get({
                url: url.replace(':id', id),
                success: function(data, textStatus, jqXHR){
                    archive_table.ajax.reload();
                    console.log(data);
                    customAlert('Applicant Restore', 'warning');
                }
            });
        });
    });
</script>
@endsection