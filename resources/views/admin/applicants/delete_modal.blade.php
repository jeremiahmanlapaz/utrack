<!-- Delete Modal -->
@section('delete-modal')
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="deleteModalLabel">Not Found</h3>
            </div>
            <div class="modal-body">
                <strong>Applicant not found in graduate list.</strong>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger" name="rejectBtn" data-id>Archive</button>
            </div>
        </div>
    </div>
</div>
@show