@extends('layouts.admin_master')

@section('title', 'Applicants')

@section('links')
{{-- Custom styles for this page --}}
<link rel="stylesheet" href="{{ asset('vendor/DataTables/datatables.min.css') }}">
@endsection

@section('content')
<div class="row">
    <div class="col-md-12 mb-4">
        <a class="btn btn-danger p-3 mr-3" href="{{route('applicant.archive')}}">
            <h6 class="m-0 font-weight-bold text-light">{{__('Archive')}}
                <i class="fa fa-chevron-right"></i></h6>
        </a>
        <a class="btn btn-warning p-3  mr-3" href="{{route('payment')}}">
            <h6 class="m-0 font-weight-bold text-light">{{__('Payment')}}
                <i class="fa fa-chevron-right"></i></h6>
        </a>
    </div>
    <div class="col-md-12">
        <div class="card shadow mb-4">
            <a href="#collapseBody" class="d-block card-header py-3" data-toggle="collapse" role="button"
                aria-expanded="true" aria-controls="collapseBody">
                <h6 class="m-0 font-weight-bold text-primary">{{__('Applicant / Verification')}}</h6>
            </a>
            <div class="collapse show" id="collapseBody">
                <div class="card-body">
                    <table class="table table-hover" id="verification_table" style="width:100%">
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@include('admin.applicants.applicant_modal')
@include('admin.applicants.delete_modal')
@include('admin.applicants.view_modal')
@endsection

@section('js')
{{-- Page level plugins --}}
<script src="{{ asset('vendor/DataTables/datatables.min.js') }}"></script>
<script>
    $(document).ready(function () {
        let verification_table = $('#verification_table').on('error.dt', function (e, settings, techNote, message) {
            // verification_table.ajax.reload();
            console.log(e);
        }).DataTable({
            ajax: {
                url: "{{route('applicant.applicants')}}",
                dataSrc: ''
            },
            columns: [
                {
                    data: 'first_name',
                    title: 'First Name'
                },
                {
                    data: 'middle_name',
                    title: 'Middle Name'
                },
                {
                    data: 'last_name',
                    title: 'Last Name'
                },
                {
                    data: 'email',
                    title: 'Email',
                    orderable: false
                },
                {
                    data: 'application_status',
                    title: 'Status',
                    render: function (data) {
                        return '<span class="badge badge-pill badge-warning">' + data + '</span>'
                    }
                },
                {
                    data: 'created_at',
                    title: 'Application Date',
                    render: function (data, type, row) {
                        var date = new Date(data);
                        return moment(date).format("MMMM DD, YYYY");
                    }
                },
                {
                    data: null,
                    title: 'Actions',
                    render: function (data, type, row) {
                        return ' <button type="button" class="mx-1 btn btn-success btn-sm" name="verifyBtn" data-id="' + data.uuid + '"><i class="fas fa-check"></i> Verify</button>' +
                            '<button class="mx-1 btn btn-info btn-sm" name="viewBtn" data-id="' + data.uuid + '"><i class="fas fa-eye"></i> View</button>' +
                            ' <button type="button" class="mx-1 btn btn-danger btn-sm" name="deleteBtn" data-id="' + data.uuid + '"><i class="fas fa-trash"></i> Archive</button>';
                    },
                    width: '25%',
                }
            ], 
            order: [[ 5, "asc" ]],
            dom: 'lBfrtip',
            buttons: [{
                text: '<i class="fas fa-sync-alt"></i> Reload table',
                action: function () {
                    verification_table.ajax.reload();
                },
                className: 'btn btn-success rounded-pill mx-1',
            }],
            scrollX: true,
            scrollCollapse: true
        });
        $(document).on('click', 'button[name=verifyBtn]', function () {
            let id = $(this).data('id');
            let url = "{{route('applicant.check', ['id' => ':id'])}}";
            $.get({
                url: url.replace(':id', id),
                success: function (data, status, jqXHR) {
                    if (jQuery.isEmptyObject(data) == false) {
                        setApplicantModal(data)
                    } else {
                        deleteModal(id)
                    }
                },
                error: function (jqXHR, status, e) {
                    customAlert(e, 'danger');
                }
            });
        }).on('click', 'button[name=viewBtn]', function () {
            let id = $(this).data('id')
            let url = "{{route('applicant.get', ['id' => ':id'])}}";
            $.ajax({
                url: url.replace(':id', id),
                type: 'get',
                success: function (data) {
                    setViewModal(data)
                },
            })
        }).on('click', '[name=confirmBtn]', function () {
            let id = $(this).data('id');
            let url = "{{route('applicant.verify', ['id' => ':id'])}}";
            $.ajax({
                url: url.replace(':id', id),
                type: 'get',
                success: function () {
                    verification_table.ajax.reload();
                    customAlert('Applicant Verified', 'success');
                    $('#applicantModal').modal('hide');
                }
            })
        }).on('click', 'button[name=deleteBtn]', function () {
            alertify.confirm('Proceed to Archive?', () => {
                let id = $(this).data('id');
                let url = "{{route('applicant.delete', ['id' => ':id'])}} ";
                $.ajax({
                    url: url.replace(':id', id),
                    success: function(){
                        verification_table.ajax.reload();
                        customAlert('Applicant Archive', 'danger');
                    }
                })
            }, (e) => {
                return; 
            });
        }).on('click', '[name=rejectBtn]', function (){
            let id = $(this).data('id');
            let url = "{{route('applicant.reject', ['id' => ':id'])}}";
            $.ajax({
                url: url.replace(':id', id),
                success: function(){
                    verification_table.ajax.reload();
                    $('#deleteModal').modal('hide');
                    customAlert('Applicant Rejected!', 'danger');
                },error: function(){
                    customAlert('Server Error', 'danger');
                }
            })
        });

        function setViewModal(data) {
            $('#viewModal .modal-body input').each(function (i, element) {
                var dataName = $(element).data('name');
                $(element).val(data[0][dataName]);
            })
            $('#viewModal').modal('show');
        }

        function setApplicantModal(data) {
            $('#applicantModal .modal-body input').each(function (i, element) {
                var dataName = $(element).data('name');
                $(element).val(data[0][dataName]);
            })
            $('[name=confirmBtn]').data('id', data[0]['applicant_uuid']);
            $('#applicantModal').modal('show');
        }

        function deleteModal(id) {
            $('[name=rejectBtn]').data('id', id);
            $('#deleteModal').modal('show');
        }
    });
</script>
@endsection