@extends('layouts.admin_master')

@section('title', 'Applicants')

@section('links')
{{-- Custom styles for this page --}}
<link rel="stylesheet" href="{{ asset('vendor/DataTables/datatables.min.css') }}">
@endsection


@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card shadow mb-4">
            <a href="#collapseBody2" class="d-block card-header py-3" data-toggle="collapse" role="button"
                aria-expanded="true" aria-controls="collapseBody2">
                <h6 class="m-0 font-weight-bold text-primary">{{__('Applicant / Payment Confirmation')}}</h6>
            </a>
            <div class="collapse show" id="collapseBody2">
                <div class="card-body">
                    <div class="table-responsive w-100">
                        <table class="table table-hover" id="verified_table" style="width:100%">
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="{{ asset('vendor/DataTables/datatabels.min.js') }}"></script>
<script>
    $(document).ready(function () {
        let verified_table = $('#verified_table').on('error.dt', function (e, settings, techNote, message) {
            verified_table.ajax.reload();
        }).DataTable({
            ajax: {
                url: "{{route('applicant.verified')}}",
                method: 'get',
                dataSrc: ''
            },
            columns: [{
                    data: '',
                    render: function (data, type, row, meta) {
                        return meta.row + 1;
                    },
                    title: ''
                },
                {
                    data: 'first_name',
                    title: 'First name'
                },
                {
                    data: 'middle_name',
                    title: 'Middle name'
                },
                {
                    data: 'last_name',
                    title: 'Last name'
                },
                {
                    data: 'status',
                    title: 'Status',
                    render: function (data, type, row, meta) {
                        return '<span class="badge badge-pill badge-success">' + data + '</span>'
                    }
                },
                {
                    data: 'updated_at',
                    title: 'Verified at',
                    render: function (data, type, row, meta) {
                        let date = new Date(data);
                        return moment(date).format("MMMM DD, YYYY");
                    }
                },
                {
                    data: null,
                    title: 'action',
                    render: function (data, type, row, meta) {
                        return '<button class="btn btn-warning btn-sm mx-1" name="confirmPaymentBtn" data-id="' + data.uuid + '">Confirm </button>' +
                            '<button class="btn btn-sm btn-danger mx-1" name="deleteBtn" data-id="' + data.uuid + '">Delete</button>'
                    }
                }
            ],
            dom: 'lBfrtip',
            buttons: [{
                text: '<i class="fas fa-sync-alt"></i> Reload table',
                action: function () {
                    verified_table.ajax.reload();
                },
                className: 'btn btn-success rounded-pill mx-1',
            }],
            scrollX: true,
            scrollCollapse: true,
            ordering: false
        });
    }).on('click', 'button[name=deleteBtn]', function () {
        let id = $(this).data('id');
        let url = "{{route('applicant.revoke', ['id' => ':id'])}} ";
        $.ajax({
            url: url.replace(':id', id),
            success: function (data, textStatus, jqXHR) {
                verified_table.ajax.reload();
                verification_table.ajax.reload();
                customAlert('Alumni Delete!', 'danger');
            }
        })
    }).on('click', 'button[name=confirmPaymentBtn]', function () {
        var id = $(this).data('id');
        $.ajax({
            url: window.location.href + '/confirm_payment/' + id,
            success: function (data, textStatus, jqXHR) {
                console.log([data]);
                customAlert('Payment Confirm!', 'success');
            }
        });
    });
</script>
@endsection