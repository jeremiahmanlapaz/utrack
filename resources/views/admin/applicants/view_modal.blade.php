<!-- View Modal -->
<div class="modal fade" id="viewModal" tabindex="-1" role="dialog" aria-labelledby="viewModal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title text-dark strong">{{__('Applicant Information')}}</h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="alum_firstName" class="col-form-label">{{__('First Name:')}}</label>
                            <input type="text" class="form-control" data-name="first_name" placeholder="First Name"
                                readonly>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="alum_middleName" class="col-form-label">{{__('Middle Name:')}}</label>
                            <input type="text" class="form-control" data-name="middle_name" placeholder="Middle Name"
                                readonly>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="alum_lastName" class="col-form-label">{{__('Last Name:')}}</label>
                            <input type="text" class="form-control" data-name="last_name" placeholder="Last Name"
                                readonly>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="alum_birthDate" class="col-form-label">{{__('Birth Date:')}}</label>
                            <input type="text" class="form-control" data-name="birth_date" placeholder="Birth Date"
                                readonly>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="alum_gender" class="col-form-label">{{__('Gender:')}}</label>
                            <input type="text" class="form-control" data-name="gender" placeholder="Gender" readonly>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="alum_email" class="col-form-label">{{__('Email:')}}</label>
                            <input type="text" class="form-control" data-name="email" placeholder="Email" readonly>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="alum_degree" class="col-form-label">{{__('Degree:')}}</label>
                            <input type="text" class="form-control" data-name="degree" placeholder="Degree" readonly>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="alum_college" class="col-form-label">{{__('College:')}}</label>
                            <input type="text" class="form-control" data-name="college" placeholder="College" readonly>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="alum_year_graduated" class="col-form-label">{{__('Year Graduated:')}}</label>
                            <input type="text" class="form-control" data-name="year_graduated"
                                placeholder="Year Graduated" readonly>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="alum_course" class="col-form-label">{{__('Course:')}}</label>
                            <input type="text" class="form-control" data-name="course" placeholder="Course" readonly>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
            </div>
        </div>
    </div>
</div>