@extends('layouts.admin_master')

@section('title', 'Articles')

@section('style')
<style>
    .ck-editor__editable_inline {
        height: 100%;
        min-height: 500px;
    }

    #img {
        width: 1200px;
        max-height: 300px;
    }
</style>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">{{__('Create Article')}}</h6>
            </div>
            <div class="card-body">
                <form method="POST" action="{{ route('articles.showCreate') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label>{{__('Cover Photo')}} <small>Note: recommended image resolution size 1200 x
                                400</small></label>
                        <div class="custom-file">
                            <input type="file" name="cover"
                                class="custom-file-input @error('cover') is-invalid @enderror" id="custom-file"
                                accept="image/*">
                            <label class="custom-file-label" for="custom-file">{{__('Choose Cover Image')}}</label>
                            @error('cover')
                            <div class="invalid-feedback">
                                {{$message}}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <img class="img-fluid" id="img" src="https://via.placeholder.com/1200x300" alt="Image Preview"
                        hidden>
                    <div class="form-group">
                        <label for="title">{{__('Title')}}</label>
                        <input name="title" type="text" class="form-control  @error('title') is-invalid @enderror"
                            id="title" placeholder="Title" value="{{old('title')}}">
                        @error('title')
                        <div class="invalid-feedback">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="body">{{__('Body')}}</label>
                        <textarea name="body" class="textarea" id="body" placeholder="Place some text here">
                            {{old('body')}}
                        </textarea>
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-primary mb-2">{{__('Create Article')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="{{ asset('js/bootstrap/bs-custom-file-input.js') }}"></script>
@include('ckfinder::setup')
<script src="{{ asset('vendor/ckeditor5-build-classic/ckeditor.js') }}"></script>
<script type="text/javascript">
    bsCustomFileInput.init()

        $('[name=cover]').change(function(){
            var url = this.value;
            
            var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
            if (this.files && this.files[0]&& (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#img').attr('src', e.target.result).attr('hidden', false);
                }
                reader.readAsDataURL(this.files[0]);
            } else{
                $('#img').attr('src', 'https://via.placeholder.com/1200x300');
            } 
        });
        
    //CKEditor5 Init()
        ClassicEditor
            .create( document.querySelector( '#body' ), {
                toolbar: ["undo", 
                "redo", 
                "bold", 
                "italic", 
                "blockQuote", 
                "heading", 
                "indent", 
                "outdent", 
                "link", 
                "numberedList", 
                "bulletedList", 
                "insertTable", 
                "tableColumn", 
                "tableRow", 
                "mergeTableCells"]
            } )
            .then( editor => {
                console.log( Array.from( editor.ui.componentFactory.names() ) )
            } )
            .catch( err => {
                console.error( err.stack );
            } );
</script>
@endsection