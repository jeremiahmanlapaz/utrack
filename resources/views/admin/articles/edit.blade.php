@extends('layouts.admin_master')

@section('title', 'Articles')

@section('style')
<style>
    .ck-editor__editable_inline {
        height: 100%;
        min-height: 500px;
    }
</style>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">
                    <small></small>
                </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body pad">
                <form method="POST" action="{{ route('articles.showEdit', ['id'=>$article['id']]) }}"
                    enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label>Change Cover Photo</label>
                        <div class="custom-file">
                            <input type="file" name="cover_image"
                                class="custom-file-input @error('cover_image') is-invalid @enderror" id="custom-file">
                            <label class="custom-file-label" for="custom-file">{{__('Update Cover Photo')}}</label>
                            @error('cover_image')
                            <div class="invalid-feedback">
                                {{$message}}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="title">{{__('Title')}}</label>
                        <input name="title" type="text" class="form-control" id="title" placeholder="Title"
                            value="{{$article['title']}}">
                    </div>
                    <div class="form-group">
                        <label for="body">{{__('Editor')}}</label>
                        <textarea name="body" class="textarea" id="body" placeholder="Place some text here">
                            {{$article['body']}}
                        </textarea>
                    </div>
                    <button type="submit" class="btn btn-warning mb-2">{{__('Update Article')}}</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="{{ asset('js/bootstrap/bs-custom-file-input.js') }}"></script>
<script src="{{ asset('vendor/ckeditor5-build-classic/ckeditor.js') }}"></script>
@include('ckfinder::setup')
<script type="text/javascript">
    bsCustomFileInput.init()

    // ClassicEditor
	// 	.create( document.querySelector( '#body' ), { 
    //         ckfinder: {
    //             // Upload the images to the server using the CKFinder QuickUpload command.
    //             uploadUrl: '{{ route('ckfinder_connector') }}?command=QuickUpload&type=Images&responseType=json'
    //         }
	// 	} )
	// 	.then( editor => {
    //         console.log( Array.from( editor.ui.componentFactory.names() ) )
	// 	} )
	// 	.catch( err => {
	// 		console.error( err.stack );
    // 	} );
    
    ClassicEditor
    .create( document.querySelector( '#body' ), {
        toolbar: [
            "undo",
            "redo",
            "bold",
            "italic",
            "blockQuote",
            "heading",
            "indent",
            "outdent",
            "link",
            "numberedList",
            "bulletedList",
            "insertTable",
            "tableColumn",
            "tableRow",
            "mergeTableCells"
        ];
    } )
    .then( editor => {
        // console.log( Array.from( editor.ui.componentFactory.names() ) )
    } )
    .catch( err => {
        console.error( err.stack );
    } );
</script>
@endsection