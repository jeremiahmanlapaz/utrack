@extends('layouts.admin_master')

@section('title', 'Articles')

@section('links')
<!-- Custom styles for this page -->
<link rel="stylesheet" href="{{ asset('vendor/DataTables/datatables.min.css') }}">
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card mb-4">
            <div class="card-header">
                {{__('Articles')}}
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover" id="table">
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="modal-delete">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{__('Delete Article')}}</h5>
            </div>
            <div class="modal-body">
                <p>{{__('Are you sure you want to delete this? ')}}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Cancel')}}</button>
                <a href="#"><button type="button" name="submit"
                        class="btn btn-danger">{{__('Confirm Delete')}}</button></a>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<!-- Page level plugins -->
<script src="{{ asset('vendor/DataTables/datatables.min.js') }}"></script>
<script>
    $(document).ready(function(){
        var table =  $('#table')
        .on('error.dt', function (e, settings, techNote, message) {
            table.ajax.reload();
        })
        .DataTable({
            ajax: {
                url: '{{route("articles.all")}}',
                method: 'get',
                dataSrc: '',
            },
            columns: [
                {data: 'id', visible: false},
                {data: 'title', title: 'Title'},
                {data: 'posted_by', title: 'Posted by'},
                {
                    data: 'created_at', 
                    title: 'Date created',
                    render(data) {
                        let date = new Date(data);
                        return moment(date).format("MMMM DD, YYYY");
                    }
                },
                {
                    data: null , 
                    render: function ( data, type, row ) {
                        let edit = " {{route('articles.showEdit', ['id' => ':id'])}} ";
                        return '<a class="mx-1 btn btn-warning btn-sm" href=' + edit.replace(':id', data.id) + ' title=Edit><i class="fas fa-pencil-alt"></i> Edit</a>' + 
                               '<button type=button class="mx-1 btn btn-danger btn-sm" name=delete title=View data-id=' + data.id + '><i class="fa fa-trash"></i> Archive</button>';
                    },
                    orderable: false
                }
            ],
            rowId: 'id',
            dom: 'lBfrtip',
            buttons: [
                {
                    text: '<i class="fas fa-sync-alt"></i> Reload table',
                    action: function () {
                        table.ajax.reload();
                    },
                    className: 'btn btn-success rounded-pill mx-1',
                },
                {
                    text: '<i class="fas fa-plus"></i> Create Article',
                    action: function () {
                        window.location.href = "{{ route('articles.showCreate') }}"
                    },
                    className: 'btn btn-success rounded-pill mx-1'
                }
            ],
            scrollX: true,
            scrollCollapse: true,
            autoWidth: false
        });

        $(document).on('click', '[name=delete]', function(){
            $('#modal-delete a').attr('href', window.location.href + '/delete/' + $(this).data('id'));
            $('#modal-delete').modal('toggle');
        })
    });
</script>
@endsection