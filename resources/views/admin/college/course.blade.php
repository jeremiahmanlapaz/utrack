@extends('layouts.admin_master')

@section('title', 'College')

@section('links')
<link rel="stylesheet" href="{{ asset('vendor/DataTables/datatables.min.css') }}">
@endsection

@section('content')
<div class="row">
    <div class="col-md-12 mb-4">
        <a class="btn btn-success p-3 mr-3" href="{{route('college')}}">
            <h6 class="m-0 font-weight-bold text-light">{{__('College')}}
                <i class="fa fa-chevron-left"></i></h6>
        </a>
    </div>
    <div class="col-md-12">
        <!-- Basic Card Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">
                    {{__(' Courses / ' . $code)}}
                </h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover" id="table"></table>
                </div>
            </div>
        </div>
    </div>
</div>

@include('admin.college.course_edit')

@endsection

@section('js')
<script src="{{ asset('vendor/DataTables/datatables.min.js') }}"></script>
<script>
    $(document).ready(function(){
        let url = "{{route('college.get.courses', ['code' => $code])}}";
        let table = $('#table')
        .on('error.dt', function (e, settings, techNote, message) {
            table.ajax.reload();
        }).DataTable({
            ajax: {
              url: url,
              dataSrc: ''  
            },
            columns: [
                {data: 'name', title: 'Name'},
                {data: 'degree', title: 'Degree'},
                {
                    data: null, 
                    title: 'Actions',
                    render: function (data){
                        return '<button class="btn btn-warning btn-sm mx-1" name="edit" data-id="' + data.id + '" data-name="' + data.name + '" data-degree=' + data.degree + '> <i class="fas fa-pencil-alt"></i> Edit</button>' + 
                        '<button class="btn btn-danger btn-sm mx-1" name="delete" data-id=' + data.id + '> <i class="fas fa-trash"></i> Remove</button>';
                    }
                }
            ],
            dom: 'lBfrtip',
            buttons: [
                {
                    text: '<i class="fas fa-plus"></i> Add Course',
                    className: 'btn btn-success rounded-pill mx-1',
                    action: function (){
                        $('#edit_form').attr('action', "{{route('college.course.create', ['code' => $code])}}");
                        $('#edit_name').val('');
                        $('#edit_degree').val('Associate');
                        $('#edit').modal('show');
                    }
                }
            ]
        });

        $(document).on('click', 'button[name=edit]', function () {
            let name = $(this).data('name');
            let degree = $(this).data('degree');
            let id = $(this).data('id');
            let url = "{{route('college.course.edit', ['id' => ':id'])}}";
            $('#edit').modal('show');
            $('#edit_form').attr('action', url.replace(':id', id));
            $('#edit_name').val(name);
            $('#edit_degree').val(degree);
        }).on('click', 'button[name=delete]', function () {
            let id = $(this).data('id');
            let url = "{{route('college.course.delete', ['id' => ':id'])}}";
            $.get({
                url: url.replace(':id', id),
                success: function(data) {
                    console.log(data);
                    customAlerts(data.message, 'success');
                    table.ajax.reload();
                }
            });
        });
    });
</script>
@endsection