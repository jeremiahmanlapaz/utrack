@extends('layouts.admin_master')

@section('title', 'College')

@section('links')
<link rel="stylesheet" href="{{ asset('vendor/DataTables/datatables.min.css') }}">
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <!-- Basic Card Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">{{__('College / ')}}</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover" id="table"></table>
                </div>
            </div>
        </div>
    </div>
</div>

@include('admin.college.college_edit')
@endsection

@section('js')
<script src="{{ asset('vendor/DataTables/datatables.min.js') }}"></script>
<script>
    $(document).ready(function(){
        let table = $('#table')
        .on('error.dt', function (e, settings, techNote, message) {
                table.ajax.reload();
        }).DataTable({
            ajax: {
                url: '{{route("college.all")}}',
                dataSrc: ''
            },
            columns: [
                {data: 'name', title: 'Name'},
                {data: 'code', title: 'Code'},
                {
                    data: null, 
                    title: 'Actions',
                    render: function (data){
                        let url = '{{route("college.course", ["code" => ":code"])}}';
                        return '<a class="btn btn-info btn-sm mx-1" href="' + url.replace(':code', data.code) + '"> <i class="fas fa-eye"></i> View Courses</a>' + 
                        '<button class="btn btn-warning btn-sm mx-1"> <i class="fas fa-pencil-alt"></i> Edit</button>';
                    }
                }
            ],
            dom: 'lBfrtip',
            buttons: [
                {
                    text: '<i class="fas fa-plus"></i> Add College',
                    className: 'btn btn-success rounded-pill mx-1',
                    action: function (){
                        $('#edit').modal('show');
                    }
                }
            ]
        })
    });
</script>
@endsection