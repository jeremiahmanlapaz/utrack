@extends('layouts.admin_master')

@section('title', 'Job Posting')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">{{__('Add Company')}}</h6>
            </div>
            <div class="card-body">
                <form action="" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="company_name">{{_('Company Name')}}</label>
                        <input type="text" name="company_name"
                            class="form-control @error('company_name') is-invalid @enderror" id="company_name" value="{{$company->company_name}}">
                        @error('company_name')
                        <div class="invalid-feedback">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>{{__('Company Logo')}}</label>
                        <div class="custom-file">
                            <input type="file" name="logo"
                                class="custom-file-input @error('logo') is-invalid @enderror" id="custom-file">
                            <label class="custom-file-label" for="custom-file">{{__('Change Company Logo')}}</label>
                            @error('logo')
                            <div class="invalid-feedback">
                                {{$message}}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="industry">{{_('Company Industry')}}</label>
                        <input type="text" name="industry" class="form-control @error('industry') is-invalid @enderror"
                            id="industry" value="{{$company->industry}}">
                        @error('industry')
                        <div class="invalid-feedback">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="url">{{_('Company URL')}}</label>
                        <input type="text" name="url" class="form-control @error('url') is-invalid @enderror"
                            id="url" value="{{$company->url}}">
                        @error('url')
                        <div class="invalid-feedback">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                    <div class="row">
                        <div class="mx-auto">
                            <button type="submit" class="btn btn-warning"> {{__('Save')}}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="{{ asset('js/bootstrap/bs-custom-file-input.js') }}"></script>
<script>
    bsCustomFileInput.init()
</script>
@endsection