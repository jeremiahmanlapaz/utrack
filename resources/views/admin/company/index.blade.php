@extends('layouts.admin_master')

@section('title', 'Job Posting')

@section('links')
<!-- Custom styles for this page -->
<link rel="stylesheet" href="{{ asset('vendor/DataTables/datatables.min.css') }}">
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <!-- Basic Card Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">{{__('Job')}}</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover" id="table"></table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="delete-modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{__('Delete Company')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>{{__('Are you sure you want to delete this company?')}}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Cancel')}}</button>
                <a href="#"><button type="button" class="btn btn-danger">{{__('Confirm Delete')}}</button></a>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<!-- Page level plugins -->
<script src="{{ asset('vendor/DataTables/datatables.min.js') }}"></script>
<script>
    $(document).ready(function () { 
       var table = $('#table')
        .on('error.dt', function (e, settings, techNote, message) {
                table.ajax.reload();
        })
        .DataTable({
            ajax: {
                'url': '{{route("company.all")}}',
                'method': 'get',
                'dataSrc': '',
            },
            columns: [
                {'data': 'id', 'visible': false},
                {'data': 'company_name', 'title': 'Company Name'},
                {'data': 'industry', 'title': 'Industry'},
                {'data': 'id', 'title': 'Position Available'},
                {
                    'data': null, 
                    render: function(data, type, row){
                        let viewCompany = null;
                        let editCompany = "{{route('company.showEdit', ['id' => ':id'])}}";
                        let jobList = "{{route('job', ['company_id' => ':id'])}}";

                        return '<a href=' + viewCompany + ' class="mr-2 btn btn-info btn-sm text-white" name=view><i class="fas fa-eye"></i> <span>View</span></a>' +
                        '<a href=' + editCompany.replace(':id', data.id) +' class="mr-2 btn btn-warning btn-sm"><i class="fa fa-pencil-alt"></i> <span>Edit</span></a>'+
                        '<a href=' + jobList.replace(':id', data.id) + ' class="mr-2 btn btn-success btn-sm"><i class="fa fa-plus"></i> <span>Job List</span></a>' + 
                        '<button class="mr-2 btn btn-danger btn-sm" name=delete data-id=' + data.id + '><i class="fa fa-trash"></i> <span>Delete</span></button>';
                    },
                    'orderable': false
                }
            ],
            rowId: 'id',
            dom: 'lBfrtip',
            buttons: [
                {
                    text: '<i class="fas fa-plus"></i> Add Company',
                    action: function () {
                        window.location.href = "{{route('company.showCreate')}}"
                    },
                    className: 'btn btn-success rounded-pill mx-1'
                },
                {
                    text: '<i class="fas fa-sync-alt"></i> Reload table',
                    action: function () {
                        table.ajax.reload();
                    },
                    className: 'btn btn-success rounded-pill mx-1',
                },
            ]
        });
     });

     $(document).on('click', '[name=delete]', function(){
        var id = $(this).data('id');  
        $('#delete-modal .modal-footer a').attr('href', window.location.protocol + '//' + window.location.host + '/admin/company/delete/' + id);
        $('#delete-modal').modal('toggle');
    });
</script>
@endsection