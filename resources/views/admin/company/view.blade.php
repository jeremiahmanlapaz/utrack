@extends('layouts.admin_master')

@section('content')
<div class="row">
    <div class="col-md-12">
        <!-- Basic Card Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">{{__('View Company')}}</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover" id="table"></table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<!-- Page level plugins -->
<script src="{{ asset('vendor/DataTables/datatables.min.js') }}"></script>
<script>
    var table = $('#table')
    .on('error.dt', function (e, settings, techNote, message) {
                table.ajax.reload();
    }).DataTable({
        ajax: {
            'url': '{{url("admin/company/view")}}',
            'method': 'get',
            'dataSrc': '',
        },
    })
</script>
@endsection