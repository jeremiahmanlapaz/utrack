@extends('layouts.admin_master')

@section('title', 'Content - Archive')

@section('links')
<link rel="stylesheet" href="{{ asset('vendor/DataTables/datatables.min.css') }}">
@endsection

@section('content')
<div class="row">
    <div class="col-md-12 mb-4">
        <a class="btn btn-primary p-3" href="{{route('content')}}">
            <h6 class="m-0 font-weight-bold text-light"><i class="fa fa-chevron-left"></i> {{__('Back')}}</h6>
        </a>
    </div>
    <div class="col-md-12">
        <div class="card shadow mb-4">
            <a href="#collapseBody" class="d-block card-header py-3 bg-danger" data-toggle="collapse" role="button"
                aria-expanded="true" aria-controls="collapseBody">
                <h6 class="m-0 font-weight-bold text-light">{{__('Carousel / Archive')}}</h6>
            </a>
            <div class="collapse show" id="collapseBody">
                <div class="card-body">
                    <div class="table-responsive w-100">
                        <table class="table table-hover" id="table" style="width:100%">
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="{{ asset('vendor/DataTables/datatables.min.js') }}"></script>
<script>
    $(document).ready(function(){
        let table = $('#table').DataTable({
            ajax: {
                url: "{{route('carousel.trashed')}}",
                dataSrc: ''
            },
            columns: [
                {data: 'id', title: "ID", visible:false},
                {data: 'title', title: "Title"},
                {data: 'heading',title: "Header"},
                {data: 'description', title: "Description"},
                {
                    data: null,
                    title: 'Action', 
                    render: function(data) {
                        return '<button class="btn btn-sm btn-warning" data-id=' + data.id + ' name=restore>Restore</button>';
                    }
                }
            ]
        });

        $(document).on('click', 'button[name=restore]', function() {
            let id = $(this).data('id');
            let url = "{{route('carousel.restore', ['id' => ':id'])}}";
            $.get({
                url: url.replace(':id', id),
                success: function(data, textStatus, jqXHR){
                    table.ajax.reload();
                    customAlert('Carousel Restored!', 'warning');
                }
            });
        });
    });
</script>
@endsection