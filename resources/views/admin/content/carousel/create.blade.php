@extends('layouts.admin_master')

@section('title', 'Content')

@section('style')
<style>
    #img {
        width: 1200px;
        max-height: 300px;
    }
</style>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card shadow mb-4">
            <div class="card-header">
                {{__('Home Carousel')}}
            </div>
            <div class="card-body">
                <form action="" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label>{{__('Carousel Photo')}}</label>
                        <div class="custom-file">
                            <input type="file" name="image"
                                class="custom-file-input @error('image') is-invalid @enderror" id="custom-file"
                                accept="image/*">
                            <label class="custom-file-label" for="custom-file">{{__('Choose Carousel Image')}}</label>
                            @error('image')
                            <div class="invalid-feedback">
                                {{$message}}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <img class="img-fluid" id="img" src="{{ asset('images/1200x300.png') }}" alt="Image Preview">
                    <div class="form-group">
                        <label for="title">{{__('Title')}}</label>
                        <input name="title" type="text" class="form-control  @error('title') is-invalid @enderror"
                            id="title" value="{{old('title')}}">
                        @error('title')
                        <div class="invalid-feedback">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="header">{{__('Header')}}</label>
                        <input name="header" type="text" class="form-control  @error('header') is-invalid @enderror"
                            id="header" value="{{old('header')}}">
                        @error('header')
                        <div class="invalid-feedback">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="description">{{__('Description')}}</label>
                        <input name="description" type="text"
                            class="form-control  @error('description') is-invalid @enderror" id="description"
                            value="{{old('description')}}">
                        @error('description')
                        <div class="invalid-feedback">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                    <div class="row">
                        <div class="mx-auto">
                            <button type="submit" class="btn btn-success btn-sm">{{strtoupper(_('create'))}}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="{{ asset('js/bootstrap/bs-custom-file-input.js') }}"></script>
<script>
    bsCustomFileInput.init()

    $('[name=image]').change(function(){
        var url = this.value;
        
        var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
        if (this.files && this.files[0]&& (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#img').attr('src', e.target.result).attr('hidden', false);
            }
            reader.readAsDataURL(this.files[0]);
        }
        else{
            $('#img').attr('src', '{{ asset("images/1200x300.png") }}');
        }
    })
</script>
@endsection