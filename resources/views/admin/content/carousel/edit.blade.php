@extends('layouts.admin_master')

@section('title', 'Content')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card shadow mb-4">
            <div class="card-header">
                {{__('Edit Carousel')}}
            </div>
            <div class="card-body">
                <form action="" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label>{{__('Carousel Photo')}}</label>
                        <div class="custom-file">
                            <input type="file" name="image"
                                class="custom-file-input @error('image') is-invalid @enderror" id="custom-file">
                            <label class="custom-file-label" for="custom-file">{{__('Choose Carousel Image')}}</label>
                            @error('image')
                            <div class="invalid-feedback">
                                {{$message}}
                            </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="header">{{__('Header')}}</label>
                        <input name="header" type="text" class="form-control  @error('header') is-invalid @enderror"
                            id="header" placeholder="Header" value="{{old('header')}}">
                        @error('header')
                        <div class="invalid-feedback">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="description">{{__('Description')}}</label>
                        <input name="description" type="text"
                            class="form-control  @error('description') is-invalid @enderror" id="description"
                            placeholder="description" value="{{old('description')}}">
                        @error('description')
                        <div class="invalid-feedback">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                    <div class="row">
                        <div class="mx-auto">
                            <button type="submit" class="btn btn-success">{{strtoupper(_('create'))}}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection