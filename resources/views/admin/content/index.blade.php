@extends('layouts.admin_master')

@section('title', 'Content')

@section('links')
<!-- Custom styles for this page -->
<link rel="stylesheet" href="{{ asset('vendor/DataTables/datatables.min.css') }}">
@endsection

@section('content')
<div class="row">
    <div class="col-md-12 mb-3">
        <a class="btn btn-danger p-3 mr-3" href="{{route('content.archive')}}">
            <h6 class="m-0 font-weight-bold text-light">{{__('Archive')}}
                <i class="fa fa-chevron-right"></i>
            </h6>
        </a>
    </div>
    <div class="col-md-12 mb-3">
        <div class="card shadow mb-4">
            <div class="card-header">
                <h6 class="m-0 font-weight-bold text-primary">{{ "Carousel Table" }}</h6>
            </div>
            <div class="card-body">
                <table class="table table-hover" id="table" style="width: 100%"></table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<!-- Page level plugins -->
<script src="{{ asset('vendor/DataTables/datatables.min.js') }}"></script>
<script>
    var table = $('#table')
        .on('error.dt', function (e, settings, techNote, message) {
            table.ajax.reload();
        })
        .DataTable({
            ajax: {
                url: "{{route('carousel.get')}}",
                dataSrc: '',
            },
            columns: [
                {data: 'id', title: "ID", visible:false},
                {data: 'title', title: "Title"},
                {data: 'heading',title: "Header"},
                {data: 'description', title: "Description"},
                {
                    data: null,
                    render: function(data, type, row, meta){
                        return '<button name=edit class="btn btn-warning btn-sm" data-id=' + data.id + '><i class="fas fa-pencil-alt"></i> Edit</button>' +
                                '<button name=delete class="mx-2 btn btn-danger btn-sm" data-id=' + data.id + '><i class="fas fa-trash"></i> Archive</button>';
                    },
                    orderable: false
                },
            ],
            rowId: 'id',
            dom: 'lBfrtip',
            buttons: [
                {
                    text: '<i class="fas fa-plus"></i> Add Carousel Image',
                    action: function () {
                        window.location.href = "{{route('carousel.showCreate')}}"
                    },
                    className: 'btn btn-success rounded-pill mx-1'
                },
                {
                    text: '<i class="fas fa-sync-alt"></i> Reload table',
                    action: function () {
                        table.ajax.reload();
                    },
                    className: 'btn btn-success rounded-pill mx-1',
                }
            ]
        })

        $(document).on('click', '[name=delete]', function (){
            var id = $(this).data('id');
            let url = "{{route('carousel.delete', ['id' => 'id'])}}"
            $.ajax({
                url: url.replace('id', id),
                success: function(data, textStatus, jqXHR){
                    console.log(jqXHR)
                },
                error: function(jqXHR, textStatus, errorThrown){
                    console.log(jqXHR)
                },
                complete: function(jqXHR, textStatus){
                    console.log(jqXHR)
                    table.ajax.reload();
                }
            })
        }).on('click', '[name=edit]', function () {
            let id = $(this).data('id');
            let url = "{{route('carousel.edit', ['id' => ':id'])}}";
            window.location.href = url.replace(':id', id);
        });
</script>
@endsection