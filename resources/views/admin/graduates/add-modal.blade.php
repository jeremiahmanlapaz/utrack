@section('add-modal')
<div class="modal" tabindex="-1" role="dialog" id="modal-add">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add Graduates</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" id="add-form">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>First Name:</label>
                                <input type="text" name="graduate[first_name]"
                                    class="form-control @error('graduate.first_name') is-invalid @enderror"
                                    value="{{old("graduate.first_name")}}">
                                @error('graduate.first_name')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Middle Name:</label>
                                <input type="text" name="graduate[middle_name]"
                                    class="form-control @error('graduate.middle_name') is-invalid @enderror"
                                    value="{{old("graduate.middle_name")}}">
                                @error('graduate.middle_name')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Last Name:</label>
                                <input type="text" name="graduate[last_name]"
                                    class="form-control @error('graduate.last_name') is-invalid @enderror"
                                    value="{{old("graduate.last_name")}}">
                                @error('graduate.last_name')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Degree:</label>
                                <select name="graduate[degree]"
                                    class="form-control @error('graduate.degree') is-invalid @enderror">
                                    <option value="Associate">Associate</option>
                                    <option value="Diploma">Diploma</option>
                                    <option value="Bachelor">Bachelor</option>
                                    <option value="Master's">Master's</option>
                                    <option value="Doctoral">Doctoral</option>
                                </select>
                                @error('graduate.degree')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>College:</label>
                                <select name="graduate[college]"
                                    class="form-control @error('graduate.college') is-invalid @enderror">
                                    @foreach ($colleges as $college)
                                    <option value="{{$college->code}}">{{$college->name}}</option>
                                    @endforeach
                                </select>
                                @error('graduate.college')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Year Graduated:</label>
                                <input type="text" name="graduate[year_graduated]"
                                    class="form-control @error('graduate.year_graduated') is-invalid @enderror"
                                    value="{{old("graduate.year_graduated")}}">
                                @error('graduate.year_graduated')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label>Course:</label>
                                <select name="graduate[course]"
                                    class="form-control @error('graduate.course') is-invalid @enderror">
                                </select>
                                @error('graduate.course')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Create</button>
                </div>
            </form>
        </div>
    </div>
</div>
@show