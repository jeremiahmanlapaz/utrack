{{-- Import Modal --}}

<div class="modal" tabindex="-1" role="dialog" id="modal-import">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{__('Import Graduate List')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="{{ route('graduates.import') }}" enctype="multipart/form-data">
                <div class="modal-body">
                    @csrf
                    <div class="custom-file">
                        <input type="file" name="file" class="custom-file-input @error('file') is-invalid @enderror"
                            id="custom-file">
                        <label class="custom-file-label" for="custom-file">{{__('Upload Excel File')}}</label>
                        <small>Note: Graduate list must have the same format with exported file.</small>
                        @error('file')
                        <div class="invalid-feedback">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">{{__('Import File')}}</button>
                </div>
            </form>
        </div>
    </div>
</div>