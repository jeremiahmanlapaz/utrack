@extends('layouts.admin_master')

@section('title', 'Graduate List')

@section('links')
<!-- Custom styles for this page -->
<link rel="stylesheet" href="{{ asset('vendor/DataTables/datatables.min.css') }}">
@endsection

@section('content')
<!-- Basic Card Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">{{__('Graduate List')}}</h6>
    </div>
    <div class="card-body">
        @if (session('error'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong>{{session('error')}}</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif
        <div class="table-responsive">
            <table class="table table-hover" id="table">
            </table>
        </div>
    </div>
</div>
@include('admin.graduates.import-modal')
@include('admin.graduates.add-modal')
@endsection

@section('js')
<!-- Page level plugins -->
<script src="{{ asset('js/bootstrap/bs-custom-file-input.js') }}"></script>
<script src="{{ asset('vendor/DataTables/datatables.min.js') }}"></script>
@if ($errors->has('file'))
<script>
    $('#modal-import').modal('show');
</script>
@elseif($errors->has('graduate.*'))
<script>
    $('#modal-add').modal('show');
</script>
@endif
<script>
    bsCustomFileInput.init()
    $(document).ready(function(){
        $('[name=graduate\\[college\\]]').on('change', function () {
            var val = $(this).val();
            let url = "{{route('register.get.course', ['college' => ':college'])}}";
            $('[name=graduate\\[course\\]]').children().remove();
            $.ajax({
                url: url.replace(':college', val) ,
                success: function(data, textStatus, jqXHR){
                    $.each(data, function (index, val) { 
                        $('[name=graduate\\[course\\]]').append($('<option>').val(val.name).text(val.name));           
                    });
                }
            })
        });

        $.fn.dataTable.ext.errMode = 'none';
        let table = $('#table').on('error.dt', function (e, settings, techNote, message) {
            // table.ajax.reload();
        }).DataTable({
            ajax: {
                url: "{{route('graduate.all')}}",
                dataSrc: '',
            },
            columns: [ 
                {data: 'id' , visible: false},
                {data: 'first_name' , title: 'First Name'},
                {data: 'middle_name' , title: 'Middle Name'},
                {data: 'last_name' , title: 'Last Name'},
                {data: 'degree' , title: 'Degree'},
                {data: 'college' , title: 'College'},
                {data: 'course' , title: 'Course'},
                {data: 'year_graduated' , title: 'Year Graduated'},
            ],
            order: [[7, 'desc'], [1, 'asc']],
            rowId: 'id',
            dom: 'lBfrtip',
            buttons: [
                {
                    text: '<i class="fas fa-file-import"></i> Import',
                    action: function(){
                        $('#modal-import').modal();
                    },
                    className: 'btn btn-success rounded-pill mx-1'
                },
                
                {
                    text: '<i class="fas fa-plus"></i> Add Graduates',
                    action: function(){
                        $('#modal-add').modal();
                    },
                    className: 'btn btn-success rounded-pill mx-1'
                },
                { 
                    extend: 'excelHtml5', 
                    text: '<i class="fas fa-save"></i> Save as Excel',
                    className: 'btn btn-success rounded-pill mx-1',
                    filename: 'Graduate-list-'+cur_datetime
                },
                { 
                    extend: 'pdfHtml5', 
                    text: '<i class="fas fa-save"></i> Save as PDF',
                    className: 'btn btn-success rounded-pill mx-1',
                    filename: 'Graduate list'+cur_datetime
                },
                {
                    text: '<i class="fas fa-sync-alt"></i> Reload table',
                    action: function () {
                        table.ajax.reload();
                    },
                    className: 'btn btn-success rounded-pill mx-1',
                },
            ],
        });
    });
</script>
@endsection