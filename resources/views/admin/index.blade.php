@extends('layouts.admin_master')

@section('title', 'Dashboard')

@section('links')
<!-- Custom styles for this page -->
<link rel="stylesheet" href="{{ asset('vendor/DataTables/datatables.min.css') }}">
@endsection

@section('content')
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
</div>
<div class="row">
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-success shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1"><a
                                href="{{ route('applicants') }}">{{__('Pending Applicants')}}</a></div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800 text-center">{{ $applicants }}</div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-portrait fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-warning shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1"><a
                                href="{{ route('payment') }}">{{__('Pending Payment')}}</a></div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800 text-center">{{ $payments }}</div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-portrait fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-danger shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1"><a
                                href="{{ route('members') }}">{{__('Members')}}</a></div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800 text-center">{{ $alumni }}</div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-user-check fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1"><a
                                href="{{ route('articles') }}">{{__('Articles')}}</a></div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800 text-center">{{ $articles }}</div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-newspaper fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 mb-4">
        <div class="card border-primary shadow">
            <div class="card-header">
                {{__('Registration Rate')}}
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group form-inline">
                            <label>Select Year</label>
                            <select type="text" class="form-control mx-3" name="year">
                                @for ($i = 2019; $i >= 2012; $i--)
                                <option value="{{ $i }}">{{ $i }}</option>
                                @endfor
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div id="registrationChart" style="width: 100%; height: 600px;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 mb-4">
        <div class="card border-primary shadow">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-6">
                        {{__('Applicant Table')}}
                    </div>
                    <div class="col-md-6">
                        <div class="float-right">
                            <a href="{{ route('applicants') }}" class="btn btn-success btn-sm">Go to Applicants</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover" id="applicantTable"></table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<!-- Page level plugins -->
<script src="{{ asset('vendor/DataTables/datatables.min.js') }}"></script>
<script src="{{ asset('vendor/amcharts4/core.js') }}"></script>
<script src="{{ asset('vendor/amcharts4/charts.js') }}"></script>
<script src="{{ asset('vendor/amcharts4/themes/animated.js') }}"></script>
<script>
    $(document).ready(function(){
        let table = initTable($('#applicantTable'))
        let url = "{{route('reports.registration')}}";
        ajaxCall(url);
        $('[name=year]').on('change', function() {
            let year = $(this).children(':selected').val();
            let url = "{{route('reports.registration', ['year' => ':year'])}}";
            ajaxCall(url.replace(':year', year), year) 
        });

    });

    function ajaxCall(url, year) {
        $.get({
            url: url,
            success: function(data){
                console.log(data);
                initChart(data, year);
            },
            error: function () {
                console.log("Ajax error");
            }
        });
    }

    function initChart(data, year = 2019){
        let chart = am4core.create('registrationChart', am4charts.XYChart);
        let label = chart.titles.create();
        let xAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        let yAxis = chart.yAxes.push(new am4charts.ValueAxis());

        chart.data = data;
        // chart.scrollbarX = new am4core.Scrollbar();

        xAxis.title.text = 'Month';
        xAxis.dataFields.category = 'category'
        xAxis.renderer.grid.template.location = 0;
        xAxis.renderer.minGridDistance = 50;

        yAxis.title.text = 'Number of Applicants';

        let series = chart.series.push(new am4charts.LineSeries());
        series.dataFields.valueY = 'value';
        series.dataFields.categoryX = 'category';
        // series.stroke = am4core.color('#000');
        series.strokeWidth = 3;

        let bullet = series.bullets.push(new am4charts.CircleBullet());
        bullet.tooltipText = "[bold]{category}: {value}[/]";

        chart.exporting.menu = new am4core.ExportMenu();

        label.text = "Registration Rate of " + year;
    }

    function initTable(table){
        table.on('error.dt', function (e, settings, techNote, message) {
                // table.ajax.reload();
        }).DataTable({
            ajax: {
                url: "{{route('applicant.applicants')}}",
                method: 'get',
                dataSrc: ''
            },
            columns: [
                { 
                    data: null,
                    render: function(data, type, row, meta){
                        return meta.row + 1;
                    }
                },
                {
                    data: null,
                    title: 'Applicant Name',
                    render: function (data, type, row, meta) {
                        if (data != null){
                            return data.first_name + ' ' + data.last_name;
                        }
                    }
                },
                {
                    data: 'created_at',
                    title: 'Application Date',
                    render: function (data, type, row, meta) {
                        let date = new Date(data);
                        return moment(date).format("MMMM DD, YYYY");
                    }
                },
            ],
            // order: [1, 'asc'],
            dom: 'p',
            pageLength: 10
        });
    }
</script>
@endsection