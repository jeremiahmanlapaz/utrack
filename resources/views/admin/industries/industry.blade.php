@extends('layouts.admin_master')

@section('title', 'Industries')

@section('links')
<link rel="stylesheet" href="{{ asset('vendor/DataTables/datatables.min.css') }}">
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <!-- Basic Card Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">{{__('Industries / ')}}</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover" id="table"></table>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal" tabindex="-1" role="dialog" id="create-modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{__('Add Industry')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('industry.industries.create') }}" method="post">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label>{{__('Name')}}</label>
                        <input type="text" name="name" class="form-control @error('name') is-invalid @enderror">
                        @error('name')
                        <div class="invalid-feedback">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Cancel')}}</button>
                    <button type="submit" class="btn btn-success">{{__('Add Industry')}}</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="{{ asset('vendor/DataTables/datatables.min.js') }}"></script>
<script>
    $(document).ready(function () {
        let table = $('#table').on('error.dt', function (e, settings, techNote, message) {
                table.ajax.reload();
        }).DataTable({
            ajax: {
                url: "{{route('industry.industries.get')}}",
                dataSrc: ''
            },
            columns: [
                {data: 'name', title: 'Industry'},
                {
                    data: null, title: '',
                    render: function (data) {
                        return '<button class="btn btn-sm btn-danger" name=deleteBtn data-id=' + data.id + '><i class="fa fa-trash-alt"></i> Delete</button>'
                    }
                }
            ],
            dom: 'lBfrtip',
            buttons: [
                {
                    text: '<i class="fas fa-sync-alt"></i> Reload table',
                    action: function () {
                        table.ajax.reload();
                    },
                    className: 'btn btn-success rounded-pill mx-1',
                },
                {
                    text: '<i class="fa fa-plus"></i> Add Industry',
                    action: function () {
                        $('#create-modal').modal('toggle');
                    },
                    className: 'btn btn-success rounded-pill mx-1',
                },
            ]
        });
        $(document).on('click', '[name=deleteBtn]', function () {
            let id = $(this).data('id');
            let url = "{{route('industry.industries.delete', ['id' => ':id'])}}";
            $.get({
                url: url.replace(':id', id),
                success: function (data) {
                    customAlert('Industry Removed', 'success');
                    table.ajax.reload();
                }
            })
        });
    });
</script>
@endsection