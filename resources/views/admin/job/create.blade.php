@extends('layouts.admin_master')

@section('title', 'Job Posting')

@section('links')
<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap/bootstrap-datepicker.css') }}">
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">{{__('Add Job Post')}}</h6>
            </div>
            <div class="card-body">
                <form method="post">
                    @csrf
                    <div class="form-group">
                        <label for="position">{{_('Position')}}</label>
                        <input type="text" name="position" class="form-control @error('position') is-invalid @enderror"
                            id="position">
                        @error('position')
                        <div class="invalid-feedback">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="salary">{{_('Salary')}}</label>
                        <input type="text" name="salary" class="form-control @error('salary') is-invalid @enderror"
                            id="salary">
                        @error('salary')
                        <div class="invalid-feedback">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="description">{{_('Description')}}</label>
                        <input type="text" name="description"
                            class="form-control @error('description') is-invalid @enderror" id="description">
                        @error('description')
                        <div class="invalid-feedback">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="requirements">{{_('Requirements')}}</label>
                        <input type="text" name="requirements"
                            class="form-control @error('requirements') is-invalid @enderror" id="requirements">
                        @error('requirements')
                        <div class="invalid-feedback">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="end_date">{{_('End Date')}}</label>
                        <input type="text" name="end_date"
                            class="form-control date-picker @error('end_date') is-invalid @enderror" id="end_date">
                        @error('end_date')
                        <div class="invalid-feedback">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="url">{{_('URL')}}</label>
                        <input type="text" name="url" class="form-control @error('url') is-invalid @enderror" id="url">
                        @error('url')
                        <div class="invalid-feedback">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                    <div class="row">
                        <div class="mx-auto">
                            <button class="btn btn-success" type="submit">Add Job</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="{{ asset('js/bootstrap/bootstrap-datepicker.js') }}"></script>
<script>
    $(document).ready(function(){
        var nowDate = new Date();
        var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
        $('.date-picker').datepicker({ 
            startDate: today 
        });
    })
</script>
@endsection