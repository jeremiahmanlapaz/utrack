@extends('layouts.admin_master')

@section('title', 'Job Posting')

@section('links')
<!-- Custom styles for this page -->
<link rel="stylesheet" href="{{ asset('vendor/DataTables/datatables.min.css') }}">
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <!-- Basic Card Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">{{__('Job List / ')}}</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover" id="table"></table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<!-- Page level plugins -->
<script src="{{ asset('vendor/DataTables/datatables.min.js') }}"></script>
<script>
    $(document).ready(function () { 
        let url = "{{route('job.get', ['company_id' => $company_id])}}";
       var table = $('#table')
       .on('error.dt', function (e, settings, techNote, message) {
            table.ajax.reload();
        })
        .DataTable({
            ajax: {
                url: url,
                method: 'get',
                dataSrc: '',
            },
            columns: [
                {data: 'position', title: 'Position Available'},
                {data: 'post_date', title: 'Post Date'},
                {data: 'end_date', title: 'End Date'},
                {
                    data: null, 
                    render: function(data, type, row){
                        return '<button class="mr-2 btn btn-warning btn-sm" name=edit data-id=' + data.id + '><i class="fa fa-pencil-alt"></i> <span>Edit</span></button>' +
                        '<button class="mr-2 btn btn-danger btn-sm" name=delete data-id=' + data.id + '><i class="fa fa-trash-alt"></i> <span>Delete</span></button>'

                    },
                    orderable: false
                }
            ],
            rowId: 'id',
            dom: 'lBfrtip',
            buttons: [
                {
                    text: '<i class="fas fa-sync-alt"></i> Reload table',
                    action: function () {
                        table.ajax.reload();
                    },
                    className: 'btn btn-success rounded-pill mx-1',
                }, 
                {
                    text: '<i class="fas fa-plus"></i> Add Job',
                    action: function () {
                        table.ajax.reload();
                    },
                    className: 'btn btn-success rounded-pill mx-1',
                }

            ]
        });

        $(document).on('click', '[name=edit]', function (){
            let id = $(this).data('id');
            console.log(id);
        }).on('click', '[name=delete]', function (){
            let id = $(this).data('id');
            let url = "{{route('job.delete', ['id' => ':id'])}}";
            $.get({
                url: url.replace(':id', id),
                success(data) {
                    if(data.message == "Success") {
                        customAlert("Delete Success", 'warning')
                    }
                    table.ajax.reload();
                },
            })
        });
     });
</script>
@endsection