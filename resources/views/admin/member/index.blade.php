@extends('layouts.admin_master')

@section('title', 'Members')

@section('links')
<!-- Custom styles for this page -->
<link rel="stylesheet" href="{{ asset('vendor/DataTables/datatables.min.css') }}">
@endsection

@section('content')
<div class="row">
    <div class="col-md-12 mb-4">
        <a class="btn btn-danger p-3" href="{{route('members.archive')}}">
            <h6 class="m-0 font-weight-bold text-light">{{__('Archive')}}
                <i class="fa fa-chevron-right"></i></h6>
        </a>
    </div>
    <div class="col-md-12">
        <div class="card shadow mb-4">
            <a href="#collapseBody" class="d-block card-header py-3" data-toggle="collapse" role="button"
                aria-expanded="true" aria-controls="collapseBody">
                <h6 class="m-0 font-weight-bold text-primary">{{__('Members')}}</h6>
            </a>
            <div class="collapse show" id="collapseBody">
                <div class="card-body">
                    <table class="table table-hover" id="table" style="width: 100%;">
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<!-- Page level plugins -->
<script src="{{ asset('vendor/DataTables/datatables.min.js') }}"></script>
<script>
    $(document).ready(function(){
        var table = $('#table').on('error.dt', function (e, settings, techNote, message) {
            table.ajax.reload();
        })
        .DataTable({ 
            ajax: {
                url: "{{route('members.all')}}",
                method: 'get',
                dataSrc: ''
            },
            columns: [
                {
                    data: null,
                    orderable: false,
                    render: function(data, row, type, meta){
                        return meta.row + 1;
                    },
                },
                {data: 'first_name', title: 'First Name'},
                {data: 'middle_name', title: 'Middle Name'},
                {data: 'last_name', title: 'Last Name'},
                {
                    data: 'updated_at',
                    title: 'Last Update',
                    render: function(data){
                        let date = new Date(data);
                        return moment(date).format("MMMM DD, YYYY");
                    } 
                },
                {
                    data: null, 
                    title: 'actions',
                    orderable: false,
                    render: function(data){
                        if(data != null){
                            let v = "{{ route('members.profile', ['id' => ':id']) }}";
                            let d = "{{ route('members.delete', ['id' => ':id']) }}";
                            let u = "{{ route('members.sendUpdate', ['id' => ':id']) }}";
                            let s = "{{route('sms.update', ['id' => ':id'])}}";
                            let id = data.uuid;
                            return '<a class="btn btn-primary btn-sm mx-1" name=viewBtn href=' + v.replace(':id', id) + '><i class="fas fa-user"></i> Profile</a>'
                                // + '<button class="btn btn-danger btn-sm mx-1" name=deleteBtn" data-url=' + d.replace(':id', id) + '><i class="fas fa-trash-alt"></i> Archive</button>'
                                + '<button class="btn btn-success btn-sm mx-1" name="updateBtn" data-url=' + u.replace(':id', id) + '><i class="fas fa-envelope"></i> Send Update</button>'
                                + '<button class="btn btn-success btn-sm mx-1" name="smsBtn" data-url=' + s.replace(':id', id) + '><i class="fas fa-bell"></i> Send SMS</button>'
                        } 
                    }
                }
            ],
            rowId: 'id',
            dom: 'lBfrtip',
            buttons: [
                {
                    text: 'Reload table',
                    action: function () {
                        table.ajax.reload();
                    },
                    className: 'btn btn-success'
                }
            ],
            order: false
        });
            
        $(document).on('click', 'button[name=deleteBtn]', function (e) {
            let url = $(this).data('url');
            $.ajax({
                url: url,
                success: function (data, textStatus, jqXHR) {
                    console.log(data);
                    table.ajax.reload();
                },
                error: function (jqXHR, textStatus, error) {
                    console.log(error);
                }
            })
        }).on('click', '[name=updateBtn]', function (){ 
            let url = $(this).data('url');
            $.get({
                url: url,
                success: function (data) {
                    if(data.message == "Success"){
                        customAlert('Update Link Send', 'success');
                    }
                }
            });
        }).on('click', '[name=smsBtn]', function () {
            let url = $(this).data('url'); 
            $.get({ 
                url: url,
                success: function (data) {
                    console.log(data);
                },
                erro: function (data){
                    console.log(data);
                }
            });
        });
    });
</script>
@endsection