@extends('layouts.admin_master')

@section('title', 'Profile')

@section('content')
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Profile</h6>
    </div>
    <div class="card-body">
        <div class="container py-4 my-2">
            <div class="row">
                <div class="col-md-4 pr-md-5">
                    <img class="w-100 rounded border" src="{{ asset($alumni->image) }}" />
                    <div class="pt-4 mt-2">
                        <section class="mb-4 pb-1">
                            <h3 class="h6 font-weight-light text-secondary text-uppercase">Work Experiences</h3>
                            <div class="work-experience pt-2">
                                @foreach ($jobs as $job)
                                <div class="work mb-4">
                                    <strong
                                        class="h5 d-block text-secondary font-weight-bold mb-1">{{$job->company_name}}</strong>
                                    <strong class="h6 d-block text-warning mb-1">{{$job->occupation}}</strong>
                                </div>
                                @endforeach
                            </div>
                        </section>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="d-flex align-items-center">
                        <h2 class="font-weight-bold m-0">
                            {{$alumni->first_name.' '.$alumni->middle_name.' '.$alumni->last_name}}
                        </h2>
                    </div>
                    <section class="mt-4">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab"
                                    aria-controls="home" aria-selected="true">
                                    About
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content py-4" id="myTabContent">
                            <div class="tab-pane py-3 fade show active" id="home" role="tabpanel"
                                aria-labelledby="home-tab">
                                <h6 class="text-uppercase font-weight-light text-secondary">
                                    Contact Information
                                </h6>
                                <dl class="row mt-4 mb-4 pb-3">
                                    <dt class="col-sm-3">Phone</dt>
                                    <dd class="col-sm-9">{{$alumni->contact_number}}</dd>

                                    <dt class="col-sm-3">Home address</dt>
                                    <dd class="col-sm-9">
                                        <address class="mb-0">
                                            {{$alumni->address}}
                                        </address>
                                    </dd>

                                    <dt class="col-sm-3">Email address</dt>
                                    <dd class="col-sm-9">
                                        <a href="mailto:{{$alumni->email}}">{{$alumni->email}}</a>
                                    </dd>
                                </dl>

                                <h6 class="text-uppercase font-weight-light text-secondary">
                                    Basic Information
                                </h6>
                                <dl class="row mt-4 mb-4 pb-3">
                                    <dt class="col-sm-3">Birthday</dt>
                                    <dd class="col-sm-9">{{$alumni->birth_date}}</dd>

                                    <dt class="col-sm-3">Gender</dt>
                                    <dd class="col-sm-9">{{$alumni->gender}}</dd>
                                </dl>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection