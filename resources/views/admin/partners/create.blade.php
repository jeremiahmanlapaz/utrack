@extends('layouts.admin_master')

@section('title', 'Partners')

@section('links')
<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap/bootstrap-datepicker.css') }}">
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Partner Create</h6>
            </div>
            <div class="card-body">
                <form action=" {{ route('partners.create') }} " method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label>{{__('Partner Logo')}} <small>Note: recommended image resolution size 200 x
                                        200</small></label>
                                <div class="custom-file">
                                    <input type="file" name="logo"
                                        class="custom-file-input @error('logo') is-invalid @enderror" id="custom-file"
                                        accept="image/*">
                                    <label class="custom-file-label"
                                        for="custom-file">{{__('Choose Partner logo')}}</label>
                                    @error('logo')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group row">
                                <label class="col-md-2" for="name">{{_('Name')}}</label>
                                <div class="col-md-10">
                                    <input type="text" name="name"
                                        class="form-control @error('name') is-invalid @enderror" id="name">
                                    @error('name')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group row">
                                <label class="col-md-2" for="description">{{_('Description')}}</label>
                                <div class="col-md-10">
                                    <input type="text" name="description"
                                        class="form-control @error('description') is-invalid @enderror"
                                        id="description">
                                    @error('description')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group row">
                                <label class="col-md-4" for="category">{{_('Category')}}</label>
                                <div class="col-md-8">
                                    <select name="category" class="form-control @error('category') is-invalid @enderror"
                                        id="category">
                                        <option value="Clothing">Clothing</option>
                                        <option value="Restaurant and Bars">Restaurant and Bars</option>
                                        <option value="Tour and Travels">Tour and Travels</option>
                                        <option value="Services">Services</option>
                                        <option value="Gadgets and Technology">Gadgets and Technology</option>
                                        <option value="Others">Others</option>
                                    </select>
                                    @error('category')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-md-3" for="validity_date">{{_('Validity Date')}}</label>
                                <div class="col-md-5">
                                    <input type="text" name="validity_date"
                                        class="form-control datepicker @error('validity_date') is-invalid @enderror"
                                        id="validity_date">
                                    @error('validity_date')
                                    <div class="invalid-feedback">
                                        {{$message}}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-center">
                        <button class="btn btn-sm btn-success" type="submit">Create Partner</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="{{ asset('js/bootstrap/bs-custom-file-input.js') }}"></script>
<script src="{{ asset('js/bootstrap/bootstrap-datepicker.js') }}"></script>
<script>
    bsCustomFileInput.init()
    $('.datepicker').datepicker();
</script>
@endsection