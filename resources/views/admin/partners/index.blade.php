@extends('layouts.admin_master')

@section('title', 'Partners')

@section('style')
<!-- Custom styles for this page -->
<link rel="stylesheet" href="{{ asset('vendor/DataTables/datatables.min.css') }}">
@endsection

@section('content')
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">{{__('Partners')}}</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-hovered" id="table"></table>
        </div>
    </div>
</div>
@endsection

@section('js')
<!-- Page level plugins -->
<script src="{{ asset('vendor/DataTables/datatables.min.js') }}"></script>
<script>
    var table = $('#table')
        .on('error.dt', function (e, settings, techNote, message) {
            table.ajax.reload();
        })
        .DataTable({
            ajax: {
                url: "{{ route('partners.all') }}",
                dataSrc: '',
            },
            columns: [
                {data: 'name', title: 'Name'},
                {data: 'category', title: 'Category'},
                {data: 'posted_date', title: 'Posted Date'},
                {data: 'validity_date', title: 'Validity Date'},
                {
                    data: 'description', 
                    title: 'Description',
                    render: (data) => {
                        return data.substring(1, 30) + '...';
                    }
                },
                {
                    data: null,
                    render: function(data){
                        let id = data.id;
                        let e = " {{ route('partners.showEdit', ['id' => ':id']) }}";
                        let d = " {{ route('partners.delete', ['id' => ':id']) }}";
                        return '<a class="mx-1 btn btn-warning btn-sm" href='+ e.replace(':id', id) +'><i class="fas fa-pencil-alt"></i> <span>Edit</span></a>' + 
                        '<button class="mx-1 btn btn-danger btn-sm" data-url='+ d.replace(':id', id) +'><i class="fas fa-trash"></i> <span>Delete</span></button>';
                    }
                }
            ],
            dom: 'lBfrtip',
            buttons: [
                {
                    text: '<i class="fas fa-plus"></i> Create Partner',
                    action: function () {
                        window.location.href = " {{ route('partners.showCreate')}} "
                    },
                    className: 'btn btn-success rounded-pill mx-1'
                },
                {
                    text: '<i class="fas fa-sync-alt"></i> Reload table',
                    action: function () {
                        table.ajax.reload();
                    },
                    className: 'btn btn-success rounded-pill mx-1',
                }
            ],
    });

</script>
@endsection