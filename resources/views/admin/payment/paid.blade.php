@extends('layouts.admin_master')

@section('title', 'Payments - Paid')


@section('links')
<!-- Custom styles for this page -->
<link rel="stylesheet" href="{{ asset('vendor/DataTables/datatables.min.css') }}">
@endsection

@section('content')
<div class="row">
    <div class="col-md-12 mb-3">
        <a class="btn btn-primary p-3  mr-3" href="{{route('payment')}}">
            <h6 class="m-0 font-weight-bold text-light">
                <i class="fa fa-chevron-left"></i> {{__('Back')}}
            </h6>
        </a>
    </div>
    <div class="col-md-12">
        <div class="card shadow mb-4">
            <a href="#collapseBody" class="d-block card-header py-3" data-toggle="collapse" role="button"
                aria-expanded="true" aria-controls="collapseBody">
                <h6 class="m-0 font-weight-bold text-primary">{{__('Payment / Paid')}}</h6>
            </a>
            <div class="collapse show" id="collapseBody">
                <div class="card-body">
                    <table class="table table-hover" id="table" style="width:100%">
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="{{ asset('vendor/DataTables/datatables.min.js') }}"></script>
<script>
    $(document).ready(function (){
        let table = $('#table')
        .on('error.dt', function (e, settings, techNote, message) {
                // table.ajax.reload();
        }).DataTable({
            ajax: {
                url: "{{route('payment.paid')}}",
                dataSrc: ''
            },
            columns:[
                {
                    data: null,
                    title: 'Applicant Name',
                    render: function (data) {
                        return data.first_name + ' ' + data.middle_name + ' ' + data.last_name;
                    }
                },
                {data: 'delivery_method', title: 'Shipping Option'},
                {data: 'delivery_fee', title: 'Delivery fee'},
                {data: 'membership_fee', title: 'Membership fee'},
                {
                    data: 'amount_paid',
                    title: 'Amount Paid', 
                    orderable: false,
                    render: function(data){
                        if(data == null||data == 0){
                            return 0;
                        }else{
                            return 'Php ' + data;
                        }
                    }
                },
                {data: 'total_amount', title: 'Total Amount'},
                {
                    data: 'status',
                    title: 'Status', 
                    render:function(data){
                        if(data != 'reject'){
                            return '<small class="badge badge-pill badge-warning">' + data + '</small>'
                        }else{
                            return '<small class="badge badge-pill badge-danger">' + data + '</small>'
                        }
                    }
                },
                {
                    data: 'created_at',
                    title: 'Approved Date',
                    render: function(data){
                        let date = new Date(data);
                        return moment(date).format("MMMM DD, YYYY");
                    }
                },
            ],
            dom: 'lBfrtip',
            buttons: [{
                text: '<i class="fas fa-sync-alt"></i> Reload table',
                action: function () {
                    table.ajax.reload();
                },
                className: 'btn btn-success rounded-pill mx-1',
            }],
            scrollX: true,
            responsive: true
        });
    });
</script>
@endsection