@extends('layouts.admin_master')

@section('title', 'Payment - Confirmation')

@section('links')
<link type="text/css" rel="stylesheet" href="{{ asset('vendor/lightgallery.js-master/src/css/lightgallery.css') }}">
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                Payment Details
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div id="lightgallery">
                            <a href="{{ asset($payment->proof_of_payment) }}">
                                <p>Proof of payment</p>
                                <img class="img-fluid rounded" src="{{ asset($payment->proof_of_payment) }}"
                                    alt="Proof of payment" width="100%" height="auto">
                            </a>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered">
                                <tr>
                                    <td>Shipping Option: </td>
                                    <td>{{$payment->delivery_method}} </td>
                                </tr>
                                <tr>
                                    <td>Payment Method: </td>
                                    <td>{{$payment->payment_method}}</td>
                                </tr>
                                <tr>
                                    <td>Delivery Fee: </td>
                                    <td>{{$payment->delivery_fee}}</td>
                                </tr>
                                <tr>
                                    <td>Delivery Address: </td>
                                    <td>{{$payment->delivery_address}}</td>
                                </tr>
                                <tr>
                                    <td>Membership Fee: </td>
                                    <td>{{$payment->membership_fee}}</td>
                                </tr>
                                <tr>
                                    <td>Alumni ID Fee: </td>
                                    <td>{{$payment->alumni_id_fee}}</td>
                                </tr>
                                <tr>
                                    <td>Total Amount: </td>
                                    <td>{{$payment->total_amount}}</td>
                                </tr>
                                <tr>
                                    <td>Amount Paid:</td>
                                    <td>{{$payment->amount_paid}}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="text-center">
                            <button class="btn btn-success" id='updateBtn' data-id="{{$payment->uuid}}">
                                <i class="fa fa-check"></i> Update to Paid
                            </button>
                            <button class="btn btn-danger" id="rejectBtn" data-id="{{$payment->uuid}}">
                                <i class="fa fa-times"></i> Reject Payment
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="{{ asset('js/bootstrap/bs-custom-file-input.js') }}"></script>
<script src="{{ asset('vendor/lightgallery.js-master/dist/js/lightgallery.js') }}"></script>
<script>
    bsCustomFileInput.init()

    $(document).ready(function () {
        lightGallery(document.getElementById('lightgallery'), {
            download: false,
            counter: false,
            thumbnail: true
        });
    }).on('click', '#updateBtn', function() {
        let id = $(this).data('id');
        let url = "{{route('payment.update', ['id' => ':id'])}}";
        window.location.href = url.replace(':id', id);
    }).on('click', '#rejectBtn', function () {
        let id = $(this).data('id');
        let url = "{{route('payment.reject', ['uuid' => 'zxc'])}}";

        console.log([url.replace('zxc', id), id]);
        window.location.href = url.replace('zxc', id);
    });
</script>
@endsection