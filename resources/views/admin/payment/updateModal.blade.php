<!--- Modal --->
<div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="updateModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title text-center" id="updateModalLabel">{{__('Update')}}</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="Updateform" method="POST">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 mb-2">
                            <img class="img-fluid" src="{{ asset('images/200x200.png') }}" id="pop_url">
                        </div>
                        <div class="col-md-12 mb-2">
                            <input type="number" name="amount_paid"
                                class="form-control @error('amount_paid') is-invalid @enderror" placeholder="Amount"
                                value="{{old('amount_paid')}}">
                            @error('amount_paid')
                            <div class="invalid-feedback">
                                {{$message}}
                            </div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="pull-right">
                        <button type="submit" class="btn btn-success">Update to Paid</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>