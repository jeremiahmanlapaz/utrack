@extends('layouts.admin_master')

@section('title', 'Push Notification')

@section('content')
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Push Notification</h6>
    </div>
    <div class="card-body">
        <button class="btn btn-success">Send Notification</button>
    </div>
</div>
@endsection