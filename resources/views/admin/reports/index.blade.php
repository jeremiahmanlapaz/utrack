@extends('layouts.admin_master')

@section('title', 'Reports')

@section('links')
<link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/select2/css/select2-bootstrap.css') }}">
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card shadow mb-4">
            <div class="card-header">
                <p class="m-0 font-weight-bolder text-primary">Generate Reports</p>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12 col-lg-12">
                        <form action="{{ route('reports.show') }}" method="GET">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{{__('College')}}</label>
                                        <select name="college"
                                            class="form-control @error('college') is-invalid @enderror">
                                            @foreach ($colleges as $college)
                                            <option value="{{ $college->code }}"> {{ $college->name }} </option>
                                            @endforeach
                                        </select>
                                        @error('college')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>{{__('Year Graduated')}}</label>
                                        <select name="year" class="form-control @error('year') is-invalid @enderror">
                                            @for ($i = 2019; $i >= 2012; $i--)
                                            <option value="{{ $i }}">{{ $i }}</option>
                                            @endfor
                                        </select>
                                        @error('year')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>{{__('Type')}}</label>
                                        <select name="type" class="form-control @error('type') is-invalid @enderror">
                                            <option value="chart">Chart</option>
                                            <option value="table">Table</option>
                                        </select>
                                        @error('type')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-2 text-center mt-4">
                                    <button type="submit" class="btn btn-success btn-sm">
                                        <i class="fa fa-print"></i>
                                        Generate
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="{{ asset('vendor/select2/js/select2.js') }}"></script>
<script>
    $(document).ready(function () {
        $('select').select2({
            theme: 'bootstrap'
        });
    });
</script>
@endsection