<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Print Reports</title>
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{ asset('admin/bower_components/bootstrap/dist/css/bootstrap.min.css') }} ">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('admin/bower_components/font-awesome/css/font-awesome.min.css') }} ">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('admin/bower_components/Ionicons/css/ionicons.min.css') }} ">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('admin/dist/css/AdminLTE.min.css') }} ">
</head>
<style>
    @media print{
        #canvas{
            vertical-align: middle;
            min-width: 700px;
            max-height: 250px;
            width: 100%!important;
        }
    }
</style>
<body onload="window.print()">
    <div class="wrapper">
        <!-- Main content -->
        <section class="invoice">
            <!-- title row -->
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="page-header text-center">
                        <img src="{{ asset('images/icon.png') }}" alt="logo" width="100px" height="100px"> 
                        <b>Center For Alumni Affairs</b>
                    </h2>
                </div>
                <!-- /.col -->
            </div>
            <!-- info row -->
            <div class="row invoice-info">
                <div class="col-sm-12 invoice-col">
                    <p class="text-center">
                    <img id="canvas" class="img-fluid" src="{{$chart}}" alt="chart">
                </p>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

            <!-- Table row -->
            <div class="row">
                <div class="col-xs-6 table-responsive">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>College</th>
                                <th>Employed</th>
                                <th>Unemployed</th>
                            </tr>
                        </thead>
                        <tbody>
                           @foreach ($data['employment'] as $item)
                               <tr>
                                   <td>{{$item->college}}</td>
                                   <td>{{$item->employed}}</td>
                                   <td>{{$item->unemployed}}</td>
                               </tr>
                           @endforeach
                           <tr>
                                <td>Total</td>
                                <td>{{$data['totalEmployed']}}</td>
                                <td>{{$data['totalUnemployed']}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-xs-12">
                    {{-- <p>The total graduates of <strong>{{$year}}</strong> is <strong>{{$data['totalGraduates']}}</strong>, Out of that only <strong>{{$data['totalMembers']}}</strong>.</p> --}}
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
</body>
</html>