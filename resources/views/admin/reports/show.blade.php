@extends('layouts.admin_master')

@section('title', 'Reports')

@section('links')
<!-- Custom styles for this page -->
<link rel="stylesheet" href="{{ asset('vendor/DataTables/datatables.min.css') }}">
@endsection

@section('content')
<div class="row">
    <div class="col-md-6">
        <div class="card shadow mb-4">
            <div class="card-header">
                <h6 class="m-0 font-weight-bold text-primary">{{__('Reports / Employment status Chart')}}</h6>
            </div>
            <div class="card-body">
                <div id="employmentChart" style="height: 500px; width: 100%;"></div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card shadow mb-4">
            <div class="card-header">
                <h6 class="m-0 font-weight-bold text-primary">{{__('Reports / Job Level Chart')}}</h6>
            </div>
            <div class="card-body">
                <div id="jobLevelChart" style="height: 500px; width: 100%;"></div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="card shadow mb-4">
            <div class="card-header">
                <h6 class="m-0 font-weight-bold text-primary">{{__('Reports / Occupation Industry')}}</h6>
            </div>
            <div class="card-body">
                <div id="jobTitleChart" style="height: 800px; width: 100%;"></div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="card shadow mb-4">
            <a href="#collapseBody" class="d-block card-header py-3" data-toggle="collapse" role="button"
                aria-expanded="true" aria-controls="collapseBody">
                <h6 class="m-0 font-weight-bold text-primary">{{__('Reports / Industry Chart')}}</h6>
            </a>
            <div class="collapse show" id="collapseBody">
                <div class="card-body">
                    <div id="industryChart" style="height: 1000px; width: 100%;"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card shadow mb-4">
            <div class="card-header">
                <h6 class="m-0 font-weight-bold text-primary">{{__('Reports / Place of Work Chart')}}</h6>
            </div>
            <div class="card-body">
                <div id="placeOfWorkChart" style="height: 500px; width: 100%;"></div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card shadow mb-4">
            <div class="card-header">
                <h6 class="m-0 font-weight-bold text-primary">{{__('Reports / Reasons Chart')}}</h6>
            </div>
            <div class="card-body">
                <div id="reasonsChart" style="height: 500px; width: 100%;"></div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<!-- Page level plugins -->
<script src="{{ asset('vendor/DataTables/datatables.min.js') }}"></script>
<script src="{{ asset('vendor/amcharts4/core.js') }}"></script>
<script src="{{ asset('vendor/amcharts4/charts.js') }}"></script>
<script src="{{ asset('vendor/amcharts4/themes/animated.js') }}"></script>
<script src="{{ asset('vendor/amcharts4/plugins/sliceGrouper.js') }}"></script>
<script>
    $(document).ready(function(){
        let data = $.parseJSON( JSON.stringify( {!! $data !!} ) );
        console.log(data);
        am4core.useTheme(am4themes_animated);
        initColumnChart(data['industries'], "Company Industry of {{$data['college']}} Alumni Graduate Year of {{$data['year']}}", 'industryChart')
        initPieChart(data['employment_rate'], "Employment Status of {{$data['college']}} Alumni Graduate Year of {{$data['year']}}", 'employmentChart')
        initPieChart(data['job_level'], "Job Level of Employed {{$data['college']}} Alumni Graduate Year of  {{$data['year']}}", 'jobLevelChart')
        initPieChart(data['place_of_work'], "Place of Work of Employed {{$data['college']}} Alumni Graduate Year of  {{$data['year']}}", 'placeOfWorkChart')
        initPieChart(data['reasons'], "Reasons of Unemployed {{$data['college']}} Alumni Graduate Year of {{$data['year']}}", 'reasonsChart')
        initPieChart(data['job_title'], "Occupation Industry {{$data['college']}} Alumni Graduate Year of {{$data['year']}}", 'jobTitleChart')
    });

    function initPieChart(data, title, div) {
        let chart = am4core.create(div, "PieChart");
        let series = chart.series.push(new am4charts.PieSeries());
        let label = chart.titles.create();

        chart.data = data;
        chart.legend = new am4charts.Legend(); // For legends
        chart.exporting.menu = new am4core.ExportMenu();
        chart.exporting.menu.align = "left";

        series.dataFields.value = "value";
        series.dataFields.category = "category";
        series.labels.template.paddingTop = 0;
        series.labels.template.paddingBottom = 0;
        series.labels.template.fontSize = 12;

        label.text = title;

        let grouper = series.plugins.push(new am4plugins_sliceGrouper.SliceGrouper());
        grouper.threshold = 5;
        grouper.groupName = "Others";
        grouper.clickBehavior =  "Zoom";
    }

    function initColumnChart(data, title, div){
        let chart = am4core.create(div, am4charts.XYChart);
        let label = chart.titles.create();
        let xAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        let yAxis = chart.yAxes.push(new am4charts.ValueAxis());

        chart.data = data;
        chart.scrollbarX = new am4core.Scrollbar();

        xAxis.title.text = 'Month';
        xAxis.dataFields.category = 'category'
        xAxis.renderer.grid.template.location = 0;
        xAxis.renderer.minGridDistance = 50;

        yAxis.title.text = 'Number of Applicants';

        let series = chart.series.push(new am4charts.ColumnSeries());
        series.columns.template.tooltipText = "Category: {categoryX}\nValue: {valueY}";
        series.dataFields.valueY = 'value';
        series.dataFields.categoryX = 'category';

        chart.exporting.menu = new am4core.ExportMenu();

        label.text = title;
    }

</script>
@endsection