@extends('layouts.admin_master')

@section('title', 'Reports')

@section('links')
<!-- Custom styles for this page -->
<link rel="stylesheet" href="{{ asset('vendor/DataTables/datatables.min.css') }}">
@endsection

@section('content')
<div class="row">
    <div class="col-md-6">
        <div class="card shadow mb-4">
            <div class="card-header">
                <h6 class="m-0 font-weight-bold text-primary">{{__('Reports / Employment status Table')}}</h6>
            </div>
            <div class="card-body">
                <div style="width: 100%;">
                    <table id="employmentTable" class="table table-bordered"></table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card shadow mb-4">
            <div class="card-header">
                <h6 class="m-0 font-weight-bold text-primary">{{__('Reports / Job Level Table')}}</h6>
            </div>
            <div class="card-body">
                <div style="width: 100%;">
                    <table id="jobLevelTable" class="table table-bordered"></table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card shadow mb-4">
            <div class="card-header">
                <h6 class="m-0 font-weight-bold text-primary">{{__('Reports / Occupation Industry')}}</h6>
            </div>
            <div class="card-body">
                <div style="width: 100%;">
                    <table id="occupationTable" class="table table-bordered"></table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card shadow mb-4">
            <a href="#collapseBody" class="d-block card-header py-3" data-toggle="collapse" role="button"
                aria-expanded="true" aria-controls="collapseBody">
                <h6 class="m-0 font-weight-bold text-primary">{{__('Reports / Industry Table')}}</h6>
            </a>
            <div class="collapse show" id="collapseBody">
                <div class="card-body">
                    <div style="width: 100%;">
                        <table id="industryTable" class="table table-bordered"></table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card shadow mb-4">
            <div class="card-header">
                <h6 class="m-0 font-weight-bold text-primary">{{__('Reports / Place of Work Table')}}</h6>
            </div>
            <div class="card-body">
                <div style="width: 100%;">
                    <table id="placeOfWorkTable" class="table table-bordered"></table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card shadow mb-4">
            <div class="card-header">
                <h6 class="m-0 font-weight-bold text-primary">{{__('Reports / Reasons Table')}}</h6>
            </div>
            <div class="card-body">
                <div style="width: 100%;">
                    <table id="reasonsTable" class="table table-bordered"></table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<!-- Page level plugins -->
<script src="{{ asset('vendor/DataTables/datatables.min.js') }}"></script>
<script>
    $(document).ready(function(){
        let data = $.parseJSON( JSON.stringify( {!! $data !!} ) );
        initTable('#employmentTable', data['employment_rate']);
        initTable('#jobLevelTable', data['job_level']);
        initTable('#occupationTable', data['job_title']);
        initTable('#industryTable', data['industries']);
        initTable('#placeOfWorkTable', data['place_of_work']);
        initTable('#reasonsTable', data['reasons']);
    });

    function initTable(table, data) {
        let dataTable = $(table).DataTable({
            data: data,
            columns: [
                {data: 'category', title: 'Category'},
                {data: 'value', title: 'Value'},
            ],
            dom: 'B',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]

        })
    }

</script>
@endsection