@extends('layouts.app')

@section('title', 'Register')

@section('links')
<link rel="stylesheet" href="{{ asset('css/bootstrap/bootstrap-datepicker.css') }}">
@endsection

@section('content')
<div class="card o-hidden border-0 shadow-lg my-5">
    <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
            <div class="col-lg-5 d-none d-lg-block bg-register-image"></div>
            <div class="col-lg-7">
                <div class="p-5">
                    <div class="text-center">
                        <h1 class="h4 text-gray-900 mb-4">{{__('Create an Account!')}}</h1>
                    </div>
                    <form class="user" method="POST" action="{{ route('register') }}">
                        @csrf
                        <div class="form-group row">
                            <div class="col-sm-12 mb-3 mb-sm-0">
                                <input type="text" name="first_name" class="form-control form-control-user"
                                    value="{{ old('first_name') }}" id="first_name" placeholder="First Name">
                                @error('first_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <input type="text" name="middle_name" class="form-control form-control-user"
                                    value="{{ old('middle_name') }}" id="middle_name" placeholder="Middle Name">
                                @error('middle_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <input type="text" name="last_name" class="form-control form-control-user"
                                    value="{{ old('last_name') }}" id="Last_name" placeholder="Last Name">
                                @error('last_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <input type="text" name="birth_date" class="form-control form-control-user date-picker"
                                    value="{{ old('birth_date') }}" id="birth_date" placeholder="Birth Date">
                                @error('birth_date')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="email" name="email" class="form-control form-control-user"
                                value="{{ old('email') }}" id="email" placeholder="Email Address">
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6 mb-3 mb-sm-0">
                                <input type="password" name="password" class="form-control form-control-user"
                                    id="password" placeholder="Password" autocomplete="current-password">
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="col-sm-6">
                                <input type="password" name="password_confirmation"
                                    class="form-control form-control-user" id="password2" placeholder="Repeat Password">
                                @error('password2')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary btn-user btn-block">
                            {{__('Register Account')}}
                        </button>

                    </form>
                    <hr>
                    @if (Route::has('password.request'))
                    <div class="text-center">
                        <a class="small" href="{{ route('password.request') }}">{{ __('Forgot Your Password?') }}</a>
                    </div>
                    @endif
                    <div class="text-center">
                        <a class="small" href="{{ route('login') }}">{{__('Already have an account? Login!')}}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="{{ asset('js/bootstrap/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('js/moment.js') }}"></script>
<script>
    $('.date-picker').datepicker();
</script>
@endsection