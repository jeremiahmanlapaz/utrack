@extends('layouts.master')

@section('title', 'About')

@section('content')
<div class="row pb-2">
    <div class="col-md-12">
        <img src="{{ asset('images/Umak2.jpg') }}" alt="" class="img-fluid" style="height: 400px; width: 100%">
        <h3 class="bg-custom p-3 mt-2 text-center rounded">Our Story</h3>
        <p class="text-justify text-indent">
            This office aims to keep University of Makati alumni connected to campus via electronic and printed
            communication, chapter and regional activities, mentoring, outreach to new students, and social
            activities.
        </p>
        <h5 class="bg-custom p-3 text-center rounded mt-5"><strong>GET INVOLVED</strong></h5>
        <p class="text-justify text-indent">
            For years, the university has invested in all of us. Let’s work together and develop mechanisms to give
            back and share the benefits our education gave us.
        </p>
        <h5 class="bg-custom p-3 text-center rounded mt-5"><strong>ATTEND OUR ALUMNI ACTIVITIES</strong></h5>
        <p class="text-justify text-indent">
            The Center for Alumni Affairs in close coordination with the Association of UMak Alumni, Inc. ,
            regularly hold activities and events within the UMak campus. Join the activities and meet your fellow
            alumni!
        </p>
        <h5 class="bg-custom p-3 text-center rounded mt-5"><strong>KNOWLEDGE-SHARING</strong></h5>
        <p class="text-justify text-indent">
            UMak takes pride in its innovative practice of direct interface between the academe and the public and
            private sector. Tell us about new trends in your workplace and help our academic centers enhance our
            curriculum and programs.
        </p>
        <h5 class="bg-custom p-3 text-center rounded mt-5"><strong>JOIN OUR EXTENSION PROGRAMS</strong></h5>
        <p class="text-justify text-indent">
            Participate in on-going community-based activities by lending your skills, time and resources. Giving
            back to the Makati community is one of our primary focus as we engage alumni. Join our medical missions,
            scholarship programs and other meaningful initiatives which aim to contribute to the development of
            communities within and outside Makati.
        </p>
        <h5 class="bg-custom p-3 text-center rounded mt-5"><strong>INTERNSHIP AND EMPLOYMENT OPPORTUNITIES</strong></h5>
        <p class="text-justify text-indent">
            Help our current students and graduates connect with the best and most relevant programs for internship
            and employment. Tell us if your company is looking for interns or if they are hiring and provide UMak
            students with the opportunity to apply.
        </p>
        <h5 class="bg-custom p-3 text-center rounded mt-5"><strong>SHARE YOUR STORIES</strong></h5>
        <p class="text-justify text-indent">
            Tell us about your life after college. Help us inspire our students and the Makati community about how
            education remains to be one of the most powerful means for individual progress.
            HELP US GET IN TOUCH WITH OTHER ALUMNI
            Assist the Center for Alumni Affairs to reach as many alumni as we can. Encourage fellow alumni to
            register here.
        </p>
        <h5 class="bg-custom p-3 text-center rounded mt-5"><strong>SPONSOR A STUDENT</strong></h5>
        <p class="text-justify text-indent">
            The University of Makati continues to provide educational opportunities that pose very little financial
            burden on Makatizens and non-Makati residents. However, many students still grapple with daily needs on
            transportation, food, school supplies, textbooks, projects and the like. Sponsor a student’s need and
            share the benefits you’ve gained with the education provided by our beloved university and city
            government.
        </p>
        <h5 class="bg-custom p-3 text-center rounded mt-5"><strong>THINK OF OTHER WAYS FOR ENGAGEMENT AND
                PARTICIPATION</strong></h5>
        <p class="text-justify text-indent">
            One of the objectives of the Center for Alumni Affairs is to develop meaningful ways for alumni to give
            back to the university that was our second home for years and to develop a strong and vibrant alumni
            community. Share your thoughts on how you want to participate as an alumni and let’s work together to
            ensure that our ties to the university continue to strengthen through the years.
        </p>
    </div>
</div>

@endsection