@extends('layouts.master')

@section('title', 'News and Events')

@section('content')
<!-- Page Heading -->
<h1 class="my-4">{{__('News & Articles')}}</h1>
<form action="{{ route('news') }}" method="GET">
    {{csrf_field()}}
    <div class="input-group">
        <input type="text" class="form-control" name="s" placeholder="Search for..." value="{{ isset($s) ? $s : '' }}">
        <span class="input-group-btn">
            <button class="btn btn-secondary" type="submit">Search</button>
        </span>
    </div>
</form>
<hr>
<small>
    {{__('The Association of UMak Alumni, Inc. (AUAI) is an independent association dedicated to the promotion of
        the welfare of the University by cultivating a mutually beneficial relationship between UMak and its
        growing worldwide community of alumni.')}}
</small>
<hr>

@if ($articles->count() > 0)
@foreach ($articles as $article)
<!-- Project One -->
<div class="row">
    <div class="col-md-4">
        <a href="{{ route('news.single', ['id' => $article->id]) }}">
            <img class="img-fluid img-responsive rounded mb-3 mb-md-0"
                src="{{substr($article->cover,0,0) == '/' ? asset($article->cover):$article->cover }}" alt=""
                class="img-responsive" width="400px">
        </a>
    </div>
    <div class="col-md-8">
        <h3><a href="{{ route('news.single', ['id' => $article->id]) }}">{{$article->title}}</a> </h3>
        <p>{{substr(strip_tags($article->body), 0, 150)}}{{strlen($article->body) > 50 ? '...':''}}</p>
        <a class="btn btn-primary" href="{{ route('news.single', ['id' => $article->id]) }}">{{__('Read More')}}</a>
    </div>
</div>
<hr>
@endforeach
{{$articles->appends(['s' => $s])->links()}}
<hr>
@else
<h1>No Articles</h1>
@endif
@if(isset($details))
<p>The search results for your query <b> {{ $query }} </b> are:</p>
<div class="row">
    @foreach($details as $post)
    <div class="col-md-4">
        <div class="card" style="margin-bottom: 10px">
            <div class="card-block">
                <h3 class="card-title">{{ $post->title }}</h3>
                <p>{{substr(strip_tags($post->body), 0, 150)}}{{strlen($post->body) > 50 ? '...':''}}</p>
                <a class="btn btn-primary"
                    href="{{ route('news.single', ['id' => $article->id]) }}">{{__('Read More')}}</a>
            </div>
        </div>
    </div>
    @endforeach
</div>
@endif

@endsection