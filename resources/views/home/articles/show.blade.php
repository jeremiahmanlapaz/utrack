@extends('layouts.master')

@section('title', 'News and Events')

@section('content')

<div class="row">
    <div class="col-md-4">
        @foreach($posts as $post)
        <div class="card" style="margin-bottom: 10px">
            <div class="card-block">
                <h3 class="card-title">{{ $post->title }}</h3>
                <p>{{substr(strip_tags($post->body), 0, 150)}}{{strlen($post->body) > 50 ? '...':''}}</p>
                <a class="btn btn-primary"
                    href="{{ route('news.single', ['id' => $article->id]) }}">{{__('Read More')}}</a>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection