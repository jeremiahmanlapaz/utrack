@extends('layouts.master')

@section('title', 'News and Events')

@section('style')
<style type="text/css">
    .cover {
        max-height: 500px;
        width: 100%;
    }
</style>
@endsection

@section('content')
<div class="row">

    <!-- Post Content Column -->
    <div class="col-lg-12">

        <!-- Title -->
        <h1 class="mt-4">{{$article->title}}</h1>

        <!-- Author -->
        <small class="lead">
            by {{$article->posted_by}}
        </small>

        <hr>

        <!-- Date/Time -->
        <p>Posted on {{ $article->created_at->format('F d, Y g:i A') }}</p>

        <hr>

        <!-- Preview Image -->
        @if ($article->cover)
        <div class="text-center">
            <img class="img-fluid rounded cover" src="{{ asset( $article->cover ) }}" alt="cover_image">
        </div>
        @else
        <img class="img-fluid rounded" src="http://placehold.it/1200x400" alt="cover_image">
        @endif

        <hr>

        <!-- Post Content -->
        {!! $article->body !!}

        <hr>

        <!-- Single Comment -->
        <div id="fb-root"></div>
        <script async defer crossorigin="anonymous"
            src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v5.0&appId=454139695117961&autoLogAppEvents=1">
        </script>
        <div class="fb-comments" data-href="https://developers.facebook.com/docs/plugins/comments#configurator"
            data-width="100%" data-numposts="5"></div>

    </div>

</div>
<!-- /.row -->

@endsection