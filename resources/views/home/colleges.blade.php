@extends('layouts.master')

@section('title', 'College')

@section('style')
<style>
</style>
@endsection

@section('content')
<!-- Jumbotron Start-->
<div class="text-center banner">

    <div class="container">
        <div class="row">

        </div>
    </div>
</div>
<!-- Jumbotron Ended-->

<!-- services section Start -->
<section id="blog">
    <div class="container-fluid">
        <div class="row">
            <h4>COLLEGES</h4>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" data-aos="fade-up" data-aos-easing="linear"
                data-aos-duration="500">
                <a href="blog_single.html"><img src="{{ asset('home/images/ctm.jpg') }} " class="img-responsive"></a>
                <div class="inner-contant"></div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" data-aos="fade-up" data-aos-easing="linear"
                data-aos-duration="500" data-aos-delay="500">
                <a href="blog_single.html"><img src="{{ asset('home/images/coe.png') }} " class="img-responsive"></a>
                <div class="inner-contant"></div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" data-aos="fade-up" data-aos-easing="linear"
                data-aos-duration="500" data-aos-delay="1000">
                <a href="blog_single.html"><img src="{{ asset('home/images/cos.jpg') }} " class="img-responsive"></a>
                <div class="inner-contant"></div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" data-aos="fade-up" data-aos-easing="linear"
                data-aos-duration="500" data-aos-delay="1500">
                <a href="blog_single.html"><img src="{{ asset('home/images/cthm.png') }} " class="img-responsive"></a>
                <div class="inner-contant"></div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" data-aos="fade-up" data-aos-easing="linear"
                data-aos-duration="500">
                <a href="blog_single.html"><img src="{{ asset('home/images/cal.jpg') }} " class="img-responsive"></a>
                <div class="inner-contant"></div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" data-aos="fade-up" data-aos-easing="linear"
                data-aos-duration="500" data-aos-delay="500">
                <a href="blog_single.html"><img src="{{ asset('home/images/cbfs.png') }} " class="img-responsive"></a>
                <div class="inner-contant"></div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" data-aos="fade-up" data-aos-easing="linear"
                data-aos-duration="500" data-aos-delay="1000">
                <a href="blog_single.html"><img src="{{ asset('home/images/ccaps.jpg') }} " class="img-responsive"></a>
                <div class="inner-contant"></div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" data-aos="fade-up" data-aos-easing="linear"
                data-aos-duration="500" data-aos-delay="1500">
                <a href="blog_single.html"><img src="{{ asset('home/images/ccgp.png') }} " class="img-responsive"></a>
                <div class="inner-contant"></div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" data-aos="fade-up" data-aos-easing="linear"
                data-aos-duration="500">
                <a href="blog_single.html"><img src="{{ asset('home/images/ccs.jpg') }} " class="img-responsive"></a>
                <div class="inner-contant"></div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" data-aos="fade-up" data-aos-easing="linear"
                data-aos-duration="500" data-aos-delay="500">
                <a href="blog_single.html"><img src="{{ asset('home/images/ccse.jpg') }} " class="img-responsive"></a>
                <div class="inner-contant"></div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" data-aos="fade-up" data-aos-easing="linear"
                data-aos-duration="500" data-aos-delay="1000">
                <a href="blog_single.html"><img src="{{ asset('home/images/cmli.png') }} " class="img-responsive"></a>
                <div class="inner-contant">

                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" data-aos="fade-up" data-aos-easing="linear"
                data-aos-duration="500" data-aos-delay="1500">
                <a href="blog_single.html"><img src="{{ asset('home/images/coahs.jpg') }} " class="img-responsive"></a>
                <div class="inner-contant"></div>
            </div>

        </div>
</section>
<!-- services section Ended -->
@endsection