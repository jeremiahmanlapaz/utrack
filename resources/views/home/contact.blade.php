@extends('layouts.master')

@section('title', 'Contact')

@section('content')
<div class="row pb-2">
  <div class="col-md-12">
    <style>
      #map {
        height: 400px;
        /* The height is 400 pixels */
        width: 100%;
        /* The width is the width of the web page */
      }
    </style>
    </head>

    <body>
      <h3>Maps</h3>
      <!--The div element for the map -->
      <div id="map"></div>
      <script>
        // Initialize and add the map
        function initMap() {
          // The location of Umak
          let umak = {lat: 14.564024, lng: 121.055666};
          // The map, centered at Umak
          let map = new google.maps.Map(
              document.getElementById('map'), {zoom: 15, center: umak});
          // The marker, positioned at Umak
          let marker = new google.maps.Marker({position: umak, map: map});
        }
      </script>
      <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCy-8HbkBW2x57xky0JHXfJVszb_GPwc8o&callback=initMap">
      </script>
  </div>
</div>
@endsection