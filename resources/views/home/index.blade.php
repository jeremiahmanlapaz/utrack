@extends('layouts.master')

@section('title', 'Home')

@section('content')
@if ($articles->count() > 0)
@foreach ($articles as $article)
<div class="row pb-2">
	<div class="col-md-12">
		<a href="{{ route('news.single', ['id'=> $article->id ]) }}">
			<h3>{{$article->title}}</h3>
			<img class="img-fluid rounded mb-3 mb-md-0"
				src="{{ $article->cover ? $article->cover:'http://placehold.it/1200x400'}}" alt="Cover Photo"
				style="width: 700px; max-height: 300px;">
		</a>
	</div>
	<div class="col-md-12">
		@if ($article->sub_heading)
		<h4>{{$article->sub_heading}}</h4>
		@else
		<br>
		@endif
		<p>{{ substr(strip_tags($article->body), 0, 200) }} {{ strlen($article->body) > 200 ? '...' : '' }}</p>
		<a class="btn btn-info" href="{{ route('news.single', ['id'=> $article->id ]) }}">Read more</a>
	</div>
</div>
<hr>
@endforeach
{{$articles->links()}}
@else
<div class="text-center">
	<h2>No Articles</h2>
</div>
@endif

@endsection