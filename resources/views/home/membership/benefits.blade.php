@extends('layouts.master')

@section('title', 'Benefits')

@section('content')
    <h2><b>MEMBER'S PRIVILEGES AND BENEFITS</b></h2>
    <ol>
        <li>Employability/Job referral and Entreprenuership assistance.</li>
        <li>Inclusion in the on-line directory in the university Alumni Website.</li>
        <li>Participation in University and AUAI-sponsored seminars, trainings, job fairs, fora, workshops, alumni events and programs.</li>
        <li>Others as may be announced.</li>
    </ol>
    <p>Please contact the Center for Alumni Affairs or the Association of Umak Alumni, Inc. (AUAI).</p>
    <p>Log on to Umak Alumni website: <strong>www.umak.edu.ph</strong></p>
    <p><strong>Email Address:</strong> alumni@umak.edu.ph</p>
@endsection