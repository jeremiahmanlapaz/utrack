@extends('layouts.master')

@section('title', 'Membership')

@section('style')
<style>
    #content {
        font-size: 14px;
    }
</style>
@endsection

@section('content')
<div class="row" id="content">
    <div class="row">
        <div class="col-md-12">
            <h1 class="text-strong">Membership</h1>
            <div class="text-center">
                <img src="{{ asset('images/id-sample.jpg') }}" class="img-fluid shadow border border-primary rounded"
                    width="50%" height="auto">
            </div>
        </div>
        <div class="col-md-12 mt-4">
            <h3>A. Walk in &frasl; New Application</h3>
            <ol>
                <li>
                    <p> Proceed to the UMak Coop Office at G/F Academic Bldg. 3 and pay the corresponding
                        amounts listed below. The amount of the alumni regular membership fee varies on the educational
                        degree
                        attained by the alumni from the University based from Section 2.2, Article IV of the Association
                        of
                        UMak Alumni's
                        Constitution and By-Laws which are as follows:
                    </p>
                    <div class="table-responsive">
                        <table class="table table-bordered" id="rates_table">
                            <thead class="thead-blue">
                                <th>Program</th>
                                <th>Membership Fee</th>
                                <th>Alumni ID fee</th>
                                <th>Total Amount</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Doctorate</td>
                                    <td>&#8369;250</td>
                                    <td>&#8369;50</td>
                                    <td>&#8369;300</td>
                                </tr>
                                <tr>
                                    <td>Masters</td>
                                    <td>&#8369;200</td>
                                    <td>&#8369;50</td>
                                    <td>&#8369;250</td>
                                </tr>
                                <tr>
                                    <td>Bachelor</td>
                                    <td>&#8369;150</td>
                                    <td>&#8369;50</td>
                                    <td>&#8369;200</td>
                                </tr>
                                <tr>
                                    <td>Associate</td>
                                    <td>&#8369;100</td>
                                    <td>&#8369;50</td>
                                    <td>&#8369;150</td>
                                </tr>
                                <tr>
                                    <td>THS</td>
                                    <td>&#8369;50</td>
                                    <td>&#8369;50</td>
                                    <td>&#8369;100</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </li>
                <li>
                    <p>
                        Proceed to the Center for Alumni Affairs Office 4th Floor Admin Bldg. and present
                        your official receipt together with accomplished Alumni Update Form.
                    </p>
                </li>
                <li>
                    <p>
                        Proceed to the ID Card Section, and have your photo taken. Receive your new UMak
                        Alumni ID card
                    </p>
                </li>
            </ol>
            <h3>B. Online Application</h3>
            <ol>
                <li>
                    <p>Accomplish the alumni ID card applicaation form. Send us a message thru this Facebook page and
                        we'll
                        send you
                        the form.
                        Please print all necessary entries in the form. Submit via e-mail to umak.alumni@umak.edu.ph or
                        alumni@umak.edu.ph together with
                        any valid ID(Company ID, Government ID or Passport) or your old student ID.
                    </p>
                </li>
                <li>
                    <p>Wait for the outcome of the status verification</p>
                </li>
                <li>
                    <p>Upon approval, please deposit payment at any Bank of the Philippine Islands branch using the
                        information
                        below:</p>
                    <p>&bull; <b>Account Number: 3643 018199</b></p>
                    <p>&bull; <b>Account Name: Association of UMak Alumni, Inc.</b></p>
                    <p>&bull; <b>Account Type: Savings</b></p>
                </li>
                <li>
                    <p>Email a clear iamge of the deposit/payment slip, 2x2 picture with white background.</p>
                </li>
                <li>
                    <p>Please leave a note if you wish to have you Alumni ID picked up or via delivery.</p>
                    <br>
                    <p><strong>For pick-up:</strong></p>
                    <ul style="list-style-type:disc;">
                        <li>
                            <p>Proceed to the Center for Alumni Affairs Office (4th Floor, Administrative Bldg). The
                                office
                                is open
                                from Monday to Friday, 8:00 AM - 5:00 PM</p>
                        </li>
                        <li>
                            <p>If to be picked-up by a representative, an authorization letter is required. Both you and
                                your
                                representative must present a valid company,
                                government ID or passport. both signatures must appear on the authorization letter.</p>
                        </li>
                    </ul>
                    <br>
                    <p>
                        <strong>
                            For delivery: Please see applicable rates. This should be added on top of your payment
                            for
                            the Alumni
                            Membership fee and ID.
                        </strong>
                    </p>
                    <div class="row">
                        <div class="mx-auto">
                            <div class="col-md-12">
                                <table class="table table-bordered" id="delivery_table">
                                    <thead class="thead-blue">
                                        <th>Location</th>
                                        <th>Fee</th>
                                    </thead>
                                    <tr>
                                        <td>NCR</td>
                                        <td>&#8369; 120</td>
                                    </tr>
                                    <tr>
                                        <td>Luzon(North and South Luzon Areas)</td>
                                        <td>&#8369; 120</td>
                                    </tr>
                                    <tr>
                                        <td>Visayas</td>
                                        <td>&#8369; 160</td>
                                    </tr>
                                    <tr>
                                        <td>Mindanao</td>
                                        <td>&#8369; 160</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <br>
                    <p>
                        <small>
                            CAA and AUAI will process your application and will send your Alumni ID Card within 10
                            working
                            days.
                        </small>
                    </p>
                </li>
            </ol>
            <h3>C. Lost &frasl; Replacement.</h3>
            <ol>
                <li>
                    <p>Proceed to the UMak Coop Office and pay the amount of Fifty Pesos(&#8369; 50) for the
                        reprinting of you alumni ID</p>
                </li>
                <li>
                    <p>Proceed to the Center for Alumni Affairs and present the official receipt.</p>
                </li>
                <li>
                    <p>Receive your new alumni ID card</p>
                </li>
            </ol>
        </div>
    </div>
    <div class="col-md-12 text-center">
        <a href="{{ route('register.show') }}" class="btn btn-info px-5 py-3">Apply Now</a>
    </div>
</div>
@endsection