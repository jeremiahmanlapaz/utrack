@extends('layouts.master')

@section('title', 'Partners')

@section('content')
<div class="row mb-4">
    <div class="col-md-12 text-center">
        <div class="btn-group" role="group" aria-label="Basic example">
            <button type="button" name="categoryBtn" class="btn btn-primary mx-1 active" data-filter="*">Show
                All</button>
            @foreach ($categories as $category)
            <button type="button" name="categoryBtn" class="btn btn-primary mx-1"
                data-filter=".{{ Str::slug($category, '-') }}">{{ $category }}</button>
            @endforeach
        </div>
    </div>
</div>
<div class="grid row">
    @foreach ($partners as $partner)
    <div class="grid-item col-md-4 mb-4 {{Str::slug($partner->category, '-')}} ">
        <div class="card border-0 shadow" data-category="{{Str::slug($partner->category, '-')}}">
            <img src=" {{ asset($partner->logo) }} " class="card-img-top" alt="Partners Logo">
            <div class="card-body text-center">
                <h5 class="card-title mb-0" title="{{$partner->name}}">
                    {{ substr(strip_tags($partner->name), 0, 20)}}{{strlen($partner->name) > 20 ? '...':'' }}</h5>
                <div class="card-text text-black-75">{{$partner->category}}</div>
                <div class="card-text text-muted">{{"Valid Until: " . $partner->validity_date}}</div>
                <a href="#" class="stretched-link"></a>
            </div>
        </div>
    </div>
    @endforeach
    <div class="container-fluid text-center" id="no-partners" hidden>
        <h2>No partners found</h2>
    </div>
</div>
@endsection

@section('js')
<script src=" {{ asset('js/isotopes.js') }} "></script>
<script>
    let $grid = $('.grid').isotope({
            itemSelector: '.grid-item',
            layoutMode: 'masonry'
        });

    $('[name=categoryBtn]').on('click', function (e) {
        let selected = $(this);
        let category = selected.data('filter');
        $('[name=categoryBtn].active').removeClass('active');
        selected.addClass('active');
        $grid.isotope({
            filter: category
        });
    });

    $grid.on( 'layoutComplete', function( event, laidOutItems ) {
        if(laidOutItems.length > 0){
            $('#no-partners').attr('hidden', true);
        }else{
            $('#no-partners').attr('hidden', false);        
        }
    });
</script>
@endsection