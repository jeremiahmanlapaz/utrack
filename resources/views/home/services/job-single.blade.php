@extends('layouts.master')

@section('title', 'Services - Job Opportunity')

@section('content')
<div class="row">
    <div class="col-md-3">
        <h3>{{$company->name}}</h3>
        <a href="{{$company->url}}" class="stretched-link">
            <img class="img-fluid" src="{{ asset($company->logo) }}" alt="Logo">
        </a>
    </div>
    <div class="col-md-8">
        <ul class="list-group list-group-flush">
            @foreach ($jobs as $job)
            <li class="list-group-item">
                <h3>{{Str::title($job->position)}}</h3>
                <h5>Description</h5>
                <p>{{$job->description}}</p>
                <h5>Requirements</h5>
                <p>{{$job->requirements}}</p>
            </li>
            @endforeach
        </ul>

    </div>
</div>
@endsection