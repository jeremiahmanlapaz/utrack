@extends('layouts.master')

@section('title', 'Services - Job Opportunity')

@section('content')
<div class="job-opportunity">
    <div class="my-4">
        <h4 class="">Job Opportunity</h4>
        <small>
            Available Job opening at these companies.
        </small>
    </div>
    @foreach ($companies as $company)
        <div class="row">
            <div class="col-md-3">
                <a href="{{ route('jobs.single', ['company_name' => $company->company_name]) }}">
                    <img class="img-fluid rounded mb-3 mb-md-0" src="{{ asset('images/200x200.png') }}" alt="">
                </a>
            </div>
            <div class="col">
                <a href="{{ route('jobs.single', ['company_name' => $company->company_name]) }}"><h4 class="mt-3">{{$company->company_name}}</h4></a>
                <p><small>Posted Date: {{date('M d, Y', strtotime($company->created_at))}}</small></p>
                <a class="btn btn-primary" href="{{ route('jobs.single', ['company_name' => $company->company_name]) }}">See available positions</a>
            </div>
        </div>
        <hr>
    @endforeach
    {{$companies->links()}}
</div>
@endsection