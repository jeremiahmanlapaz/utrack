<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="University of Makati Alumni Tracking System">
        <meta name="author" content="MayaNinja">

        <title>Admin - @yield('title')</title>

        <link rel="icon" href="{{ asset('images/icon.png') }}">
        <!-- Custom fonts for this template-->
        <link href="{{ asset('vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
        <link
            href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
            rel="stylesheet">

        <!-- Custom styles for this template-->
        <link href="{{ asset('css/sb-admin-2.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('vendor/PACE/themes/blue/pace-theme-corner-indicator.css') }}">
        <link rel="stylesheet" href="{{ asset('vendor/alertify/css/alertify.min.css') }}">
        <link rel="stylesheet" href="{{ asset('vendor/alertify/css/themes/bootstrap.min.css') }}">
        @yield('links')
    </head>

    @yield('style')

    <body class="hold-transition sidebar-toggled" id="page-top">

        <!-- Page Wrapper -->
        <div id="wrapper">

            <!-- Sidebar -->
            <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion d-print-none toggled"
                id="accordionSidebar">

                <!-- Sidebar - Brand -->
                <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ route('admin') }}">
                    <div class="sidebar-brand-icon">
                        <img src="{{ asset('images/icon.png') }}" alt="Logo" style="max-width: 50px;">
                    </div>
                    <div class="sidebar-brand-text mx-3">{{__('UTrack')}} <sup></sup></div>
                </a>

                <hr class="sidebar-divider my-0">

                <!-- Heading -->
                <div class="sidebar-heading">
                    Dashboard
                </div>

                <!-- Divider -->
                <hr class="sidebar-divider my-0">

                <!-- Nav Item - Dashboard -->
                <li class="nav-item {{Request::is('admin/home') ? 'active':''}}">
                    <a class="nav-link" href="{{ route('admin') }}">
                        <i class="fas fa-fw fa-tachometer-alt"></i>
                        <span>{{__('Dashboard')}}</span>
                    </a>
                </li>

                <hr class="sidebar-divider my-0">

                <!-- Heading -->
                <div class="sidebar-heading">
                    Admin
                </div>

                <!-- Nav Item - Members -->
                <li class="nav-item {{Request::is('admin/members*') ? 'active':''}}">
                    <a class="nav-link" href="{{ route('members') }}">
                        <i class="fas fa-fw fa-user-check"></i>
                        <span>{{__('Members')}}</span>
                    </a>
                </li>

                <!-- Nav Item - Applicants -->
                <li class="nav-item {{Request::is('admin/applicants*') ? 'active':''}}">
                    <a class="nav-link" href="{{ route('applicants') }}">
                        <i class="fas fa-fw fa-portrait"></i>
                        <span>{{__('Applicants')}}</span>
                    </a>
                </li>

                <!-- Nav Item - Payments -->
                <li class="nav-item {{Request::is('admin/payment*') ? 'active':''}}">
                    <a class="nav-link" href="{{ route('payment') }}">
                        <i class="fas fa-fw fa-money-bill"></i>
                        <span>{{__('Payments')}}</span>
                    </a>
                </li>

                <!-- Nav Item - Graduate List -->
                <li class="nav-item {{Request::is('admin/graduates') ? 'active':''}}">
                    <a class="nav-link" href="{{ route('graduate') }}">
                        <i class="fas fa-fw fa-user-graduate"></i>
                        <span>{{__('Graduate List')}}</span>
                    </a>
                </li>

                <!-- Nav Item - Graduate List -->
                <li class="nav-item {{Request::is('admin/college*') ? 'active':''}}">
                    <a class="nav-link" href="{{ route('college') }}">
                        <i class="fas fa-fw fa-university"></i>
                        <span>{{__('College')}}</span>
                    </a>
                </li>

                <!-- Nav Item - Reports -->
                <li class="nav-item {{Request::is('admin/reports*') ? 'active':''}}">
                    <a class="nav-link" href="{{ route('reports') }}">
                        <i class="fas fa-fw fa-chart-bar"></i>
                        <span>{{__('Reports')}}</span>
                    </a>
                </li>
                <!-- Divider -->
                <hr class="sidebar-divider my-0">

                <!-- Heading -->
                <div class="sidebar-heading">
                    Content Management
                </div>

                <!-- Nav Item - Content -->
                <li class="nav-item {{Request::is('admin/content*') ? 'active':''}}">
                    <a class="nav-link" href="{{ route('content') }}">
                        <i class="fas fa-fw fa-home"></i>
                        <span>{{__('Content Manager')}}</span>
                    </a>
                </li>

                <!-- Nav Item - Articles -->
                <li class="nav-item {{Request::is('admin/articles*') ? 'active' : ''}}">
                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseArticles"
                        aria-expanded="true" aria-controls="collapseArticles">
                        <i class="fas fa-fw fa-newspaper"></i>
                        <span>{{__('Articles')}}</span>
                    </a>
                    <div id="collapseArticles" class="collapse {{Request::is('admin/articles*') ? 'show' : ''}}"
                        aria-labelledby="headingArticles" data-parent="#accordionSidebar">
                        <div class="bg-white py-2 collapse-inner rounded">
                            <a class="collapse-item " href="{{ route('articles') }}">{{__('All')}}</a>
                            <a class="collapse-item " href="{{ route('articles.showCreate') }}">{{__('Create')}}</a>
                        </div>
                    </div>
                </li>

                <!-- Nav Item - Partners -->
                <li class="nav-item {{Request::is('admin/partners*') ? 'active':''}}">
                    <a class="nav-link" href="{{ route('partners.admin') }}">
                        <i class="fas fa-fw fa-handshake"></i>
                        <span>{{__('Partners')}}</span>
                    </a>
                </li>

                <!-- Nav Item - Push Notification -->
                {{-- <li class="nav-item {{Request::is('admin/push-notification*') ? 'active':''}}">
                <a class="nav-link" href="{{ route('push-notif') }}">
                    <i class="fas fa-fw fa-bell"></i>
                    <span>{{__('Push Notification')}}</span>
                </a>
                </li> --}}

                <!-- Nav Item - Job Posting -->
                <li class="nav-item {{Request::is('admin/job_post*') ? 'active' : ''}}">
                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseJobPosting"
                        aria-expanded="true" aria-controls="collapseJobPosting">
                        <i class="fas fa-fw fa-briefcase"></i>
                        <span>{{__('Job Posting')}}</span>
                    </a>
                    <div id="collapseJobPosting" class="collapse" aria-labelledby="headingJobposting"
                        data-parent="#accordionSidebar">
                        <div class="bg-white py-2 collapse-inner rounded">
                            <div class="collapse-divider"></div>
                            <h6 class="collapse-header">{{__('Companies')}}:</h6>
                            <a class="collapse-item" href="{{ route('company') }}">{{__('All Company')}}</a>
                            <a class="collapse-item" href="{{ route('company.showCreate') }}">{{__('Add Company')}}</a>
                            <div class="collapse-divider"></div>
                            {{-- <h6 class="collapse-header">{{__('Job Post')}}:</h6> --}}
                            {{-- <a class="collapse-item {{Request::is('admin/job_post') ? 'active' : ''}}"
                            href="{{ route('job') }}">{{__('All Job')}}</a> --}}
                            {{-- <a class="collapse-item {{Request::is('admin/job_post/add') ? 'active' : ''}}"
                            href="{{ route('job.showCreate') }}">{{__('Add Job')}}</a> --}}
                        </div>
                    </div>
                </li>

                <!-- Nav Item - Job Posting -->
                <li class="nav-item {{Request::is('admin/industries*') ? 'active' : ''}}">
                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseIndustry"
                        aria-expanded="true" aria-controls="collapseIndustry">
                        <i class="fas fa-fw fa-industry"></i>
                        <span>{{__('Industries')}}</span>
                    </a>
                    <div id="collapseIndustry" class="collapse " aria-labelledby="headingIndustry"
                        data-parent="#accordionSidebar">
                        <div class="bg-white py-2 collapse-inner rounded">
                            <div class="collapse-divider"></div>
                            <h6 class="collapse-header">{{__('Industry')}}:</h6>
                            <a class="collapse-item" href="{{ route('industry.occupation') }}">{{__('Occupation')}}</a>
                            <a class="collapse-item" href="{{ route('industry.industries') }}">{{__('Company')}}</a>
                        </div>
                    </div>
                </li>

                <!-- Divider -->
                <hr class="sidebar-divider">

                <!-- Heading -->
                <div class="sidebar-heading">
                    {{__('Testing')}}
                </div>

                <!-- Nav Item - Test -->
                <li class="nav-item {{Request::is('admin/test*') ? 'active' : ''}}">
                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTest"
                        aria-expanded="true" aria-controls="collapseTest">
                        <i class="fas fa-fw fa-cog"></i>
                        <span>{{__('Testing')}}</span>
                    </a>
                    <div id="collapseTest" class="collapse" aria-labelledby="headingTest"
                        data-parent="#accordionSidebar">
                        <div class="bg-white py-2 collapse-inner rounded">
                            <a class="collapse-item" href="{{ route('test') }}">{{__('Imports')}}</a>
                            <a class="collapse-item" href="{{ route('test.mail') }}">{{__('Mailing')}}</a>
                        </div>
                    </div>
                </li>

                <!-- Divider -->
                <hr class="sidebar-divider d-none d-md-block">

                <!-- Sidebar Toggler (Sidebar) -->
                <div class="text-center d-none d-md-inline">
                    <button class="rounded-circle border-0" id="sidebarToggle"></button>
                </div>

            </ul>
            <!-- End of Sidebar -->

            <!-- Content Wrapper -->
            <div id="content-wrapper" class="d-flex flex-column">

                <!-- Main Content -->
                <div id="content">

                    <!-- Topbar -->
                    <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top d-print-none shadow">

                        <!-- Sidebar Toggle (Topbar) -->
                        <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                            <i class="fa fa-bars"></i>
                        </button>

                        <!-- Topbar Navbar -->
                        <ul class="navbar-nav ml-auto">

                            <div class="topbar-divider d-none d-sm-block"></div>

                            <!-- Nav Item - User Information -->
                            <li class="nav-item dropdown no-arrow">
                                <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span
                                        class="mr-2 d-none d-lg-inline text-gray-600 small">{{Auth::user()->first_name.' '.Auth::user()->last_name}}</span>
                                    <img class="img-profile rounded-circle"
                                        src="{{Auth::user()->profile_img ? Auth::user()->profile_img : asset('images/avatar.jpg') }}">
                                </a>
                                <!-- Dropdown - User Information -->
                                <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                                    aria-labelledby="userDropdown">
                                    <a class="dropdown-item" href="#">
                                        <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                                        {{__('Profile')}}
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="#" data-toggle="modal"
                                            data-target="#logoutModal">
                                            <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                            {{__('Logout')}}
                                        </a>
                                </div>
                            </li>

                        </ul>

                    </nav>
                    <!-- End of Topbar -->

                    <!-- Begin Page Content -->
                    <div class="container-fluid">
                        @yield('content')
                    </div>
                    <!-- /.container-fluid -->

                </div>
                <!-- End of Main Content -->

                <!-- Footer -->
                <footer class="sticky-footer bg-white d-print-none">
                    <div class="container my-auto">
                        <div class="copyright text-center my-auto">
                            <span>Copyright &copy; UTRACK ADMIN 2019</span>
                        </div>
                    </div>
                </footer>
                <!-- End of Footer -->

            </div>
            <!-- End of Content Wrapper -->

        </div>
        <!-- End of Page Wrapper -->

        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fas fa-angle-up"></i>
        </a>

        <!-- Logout Modal-->
        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">{{__('Cancel')}}</button>
                        <form action="{{ route('logout') }}" method="post">
                            {{ csrf_field() }}
                            <button class="btn btn-primary" type="submit">{{__('Logout')}}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Bootstrap core JavaScript-->
        <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
        <script src="{{ asset('js/moment.js') }}"></script>
        <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
        <!-- Core plugin JavaScript-->
        <script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>
        <!-- Custom scripts for all pages-->
        <script src="{{ asset('js/sb-admin-2.js') }}"></script>
        <script src="{{ asset('vendor/PACE/pace.js') }}"></script>
        <script src="{{ asset('vendor/alertify/alertify.min.js') }}"></script>
        <script>
            // To make Pace works on Ajax calls
            $(document).ajaxStart(function () {
                Pace.restart()
            })
            function customAlert(message, type) {
                switch (type) {
                    case 'success':
                        alertify.success(message);
                        break;
                    case 'warning':
                        alertify.warning(message);
                    break;
                    case 'danger':
                        alertify.error(message);
                    break;
                    default:
                        alertify.message(message);
                        break;
                }
            } 
        </script>
        @if (session()->has('message') && session()->has('class'))
        <script>
            var message = "{{ session()->get('message') }}";
            var type = "{{ session()->get('class') }}";
                customAlert(message, type);
        </script>
        @endif
        <script src="{{ asset('js/utils.js') }}"></script>
        @yield('js')

    </body>

</html>