<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>UTRACK - @yield('title')</title>
        <link rel="icon" href="{{ asset('images/icon.png') }}">
        <link rel="stylesheet" href="{{ asset('css/bootstrap/bootstrap.css') }}">
        <link rel="stylesheet" href="{{ asset('css/custom.css') }}">

        <link rel="stylesheet" href="{{ asset('vendor/fontawesome-free/css/all.min.css') }}">
        @yield('links')

    </head>

    @yield('style')

    <body>
        @if (Request::is('/'))
        <div id="fb-root"></div>
        <script async defer crossorigin="anonymous"
            src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v4.0&appId=454139695117961&autoLogAppEvents=1">
        </script>
        @endif
        <div class="container-fluid text-center py-1" style="background-color: white;">
            <a href="{{ route('home') }}">
                <img class="img-fluid logo" src="{{ asset('images/umak_caa_header.png') }}" alt="Logo">
            </a>
        </div>
        <header>
            <nav class="navbar navbar-expand-lg navbar-dark bg-custom" data-toggle="sticky-onscroll">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar"
                    aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse justify-content-center" id="navbar">
                    <ul class="navbar-nav">
                        <li class="nav-item {{ Request::is('/') ? 'active': ''}}">
                            <a class="nav-link" href="{{ route('home') }}">{{__('HOME')}}</a>
                        </li>
                        <li
                            class="nav-item dropdown {{ Request::is('members*') || Request::is('register*') ? 'active': ''}}">
                            <a class="nav-link dropdown-toggle" role="button" href="#" id="dropdownCAA"
                                data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">{{__('MEMBERSHIP')}}</a>
                            <div class="dropdown-menu" aria-labelledby="dropdownCAA">
                                <a class="dropdown-item {{ Request::is('members/benefits') ? 'active' : '' }}"
                                    href="{{ route('benefits') }}">{{__('BENEFITS')}}</a>
                                <a class="dropdown-item {{ Request::is('members/partners') ? 'active' : '' }}"
                                    href="{{ route('partners') }}">{{__('PARTNERS')}}</a>
                                <a class="dropdown-item {{ Request::is('members') ? 'active' : '' }}"
                                    href="{{ route('membership') }}">{{__('APPLY NOW!')}}</a>
                                <a class="dropdown-item {{ Request::is('members/update') ? 'active' : '' }}"
                                    href="{{ route('update') }}">{{__('UPDATE FORM')}}</a>
                            </div>
                        </li>
                        <li
                            class="nav-item dropdown {{ Request::is('about*')||Request::is('contact*') ? 'active': ''}}">
                            <a class="nav-link dropdown-toggle" role="button" href="#" id="dropdownCAA"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{__('CAA')}}</a>
                            <div class="dropdown-menu" aria-labelledby="dropdownCAA">
                                <a class="dropdown-item {{ Request::is('about') ? 'active' : '' }}"
                                    href="{{ route('about') }}">{{__('ABOUT US')}}</a>
                                <a class="dropdown-item {{ Request::is('contact*') ? 'active' : '' }}"
                                    href="{{ route('contact') }}">{{__('CONTACT US')}}</a>
                            </div>
                        </li>
                        <li class="nav-item {{ Request::is('news*') ? 'active': ''}}">
                            <a class="nav-link" href="{{ route('news') }}">{{__('NEWS AND EVENTS')}}</a>
                        </li>
                        <li
                            class="nav-item dropdown {{ Request::is('services*') || Request::is('job-opportunity*')? 'active': ''}}">
                            <a class="nav-link dropdown-toggle" role="button" href="{{ route('home') }}"
                                id="dropdownSERVICES" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">{{__('SERVICES')}}</a>
                            <div class="dropdown-menu" aria-labelledby="dropdownSERVICES">
                                <a class="dropdown-item {{ Request::is('services/job-opportunity') ? 'active' : '' }}"
                                    href="{{ route('jobs') }}">{{__('JOB OPPORTUNITY')}}</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>

        @if (url()->current() == route('home'))
        <div id="carousel" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                @foreach ($carousel as $item => $values)
                <li data-target="#carousel" data-slide-to="{{$item}}" class="{{$item == 0 ? 'active':''}}"></li>
                @endforeach
            </ol>
            <div class="carousel-inner" role="listbox">
                @foreach ($carousel as $item)
                <!-- Slides - Set the background image for this slide in the line below -->
                <div class="carousel-item {{ $item == $carousel->first() ?'active':'' }}">
                    <img src="{{ asset($item->image) }}" alt="Carousel Image" width="100%" height="100%"
                        data-title="{{ $item->title }}">
                    <div class="carousel-caption d-none d-md-block">
                        <h2 class="display-4">{{ $item->header }}</h2>
                        <p class="lead">{{ $item->description }}</p>
                    </div>
                </div>
                @endforeach
            </div>
            <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        @endif
        <div class="container mt-4 content-wrapper">
            <div class="row">
                @if (Request::is('/'))
                <div class="col-md-8">
                    @yield('content')
                </div>
                <div class="col-md-4">
                    <p class="h4">Like Our Facebook Page</p>
                    <div class="fb-page" data-href="https://www.facebook.com/pg/umakalumniaffairs/"
                        data-tabs="timeline, events, messages, jobs" data-width="" data-height=""
                        data-small-header="false" data-adapt-container-width="true" data-hide-cover="false"
                        data-show-facepile="true">
                        <blockquote cite="https://www.facebook.com/pg/umakalumniaffairs/" class="fb-xfbml-parse-ignore">
                            <a href="https://www.facebook.com/pg/umakalumniaffairs/">University of Makati Center for
                                Alumni Affairs</a>
                        </blockquote>
                    </div>
                </div>
                @else
                <div class="col-md-12">
                    @yield('content')
                </div>
                @endif
            </div>
        </div>

        <!-- Footer -->
        <footer class="page-footer font-small unique-color-dark mt-2">

            <div style="background-color: #122964;">
                <div class="container">

                    <!-- Grid row-->
                    <div class="row py-4 d-flex align-items-center">

                        <!-- Grid column -->
                        <div class="col-md-6 col-lg-5 text-center text-md-left mb-4 mb-md-0">
                            <h6 class="mb-0" style="color:white">Get connected with us on social networks!</h6>
                        </div>
                        <!-- Grid column -->
                        <!-- Grid column -->
                        <div class="col-md-6 col-lg-7 text-center text-md-right">
                            <!-- Facebook -->
                            <a class="fb-ic" href="https://www.facebook.com/umakalumniaffairs/" target="_blank">
                                <i class="fab fa-facebook-f white-text mr-4" style="color:white"> </i>
                            </a>
                            <!-- Twitter -->
                            <a class="tw-ic" href="https://twitter.com/UMakPH" target="_blank">
                                <i class="fab fa-twitter white-text mr-4" style="color:white"> </i>
                            </a>
                            <!--Linkedin -->
                            <a class="li-ic" href="https://www.linkedin.com/school/university-of-makati/"
                                target="_blank">
                                <i class="fab fa-linkedin-in white-text mr-4" style="color:white"> </i>
                            </a>
                            <!--Instagram-->
                            <a class="ins-ic" href="https://www.instagram.com/umakph/" target="_blank">
                                <i class="fab fa-instagram white-text" style="color:white"> </i>
                            </a>
                        </div>
                        <!-- Grid column -->
                    </div>
                    <!-- Grid row-->
                </div>
            </div>

            <!-- Footer Links -->
            <div class="container text-center text-md-left mt-5">

                <!-- Grid row -->
                <div class="row mt-3 d-flex">

                    <!-- Grid column -->
                    <div class="col-lg-4 col-md-12 col-sm-12 aligncenter mx-auto mb-4">

                        <img src="{{asset('/images/UMak-logo.png')}}" width="100px;">
                        <img src="{{asset('/images/AUAI-logo.jpg')}}" width="100px;">
                        <img src="{{asset('/images/CAA-logo.png')}}" width="100px;">


                    </div>
                    <!-- Grid column -->

                    <!-- Grid column -->
                    <div class="col-lg-4 col-md-12 col-sm-12 aligncenter mx-auto mb-4">

                        <p>
                            <i class="fas fa-home mr-3"></i> University of Makati
                            <br>JP.Rizal St. West Rembo,
                            <br>Makati City. Philippines
                        </p>
                        <p>
                            <i class="fas fa-envelope mr-3"></i> caa.utrack@gmail.com
                        </p>

                    </div>

                    <div class="col-lg-4 col-md-12 col-sm-12 aligncenter mx-auto mb-4">
                        <p>
                            <i class="fas fa-phone mr-3"></i> + 02 883 1870
                        </p>
                        <br>
                        <p>
                            <i class="fas fa-print mr-3"></i> + 02 883 1870
                        </p>
                    </div>
                    <!-- Grid column -->

                </div>
                <!-- Grid row -->

            </div>
            <!-- Footer Links -->

            <!-- Copyright -->
            <div class="footer-copyright text-center py-3">&copy;2020:
                <a href="/"> UTRACK</a>
            </div>
            <!-- Copyright -->

        </footer>
        <!-- Footer -->

        <script src="{{ asset('vendor/jquery/jquery.js') }}"></script>
        <script src="{{ asset('js/bootstrap/bootstrap.js') }}"></script>
        <script src="{{ asset('js/sticky_navbar.js') }}"></script>
        <script src="{{ asset('js/hover_dropdown.js') }}"></script>
        @yield('js')
    </body>

</html>