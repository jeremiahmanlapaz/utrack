@extends('mail.master')

@section('table')
<table class="text-center" border="1px solid black">
    <thead>
        <th>Location</th>
        <th>Fee</th>
    </thead>
    <tbody>
        <tr>
            <td>NCR</td>
            <td>150</td>
        </tr>
        <tr>
            <td>Luzon</td>
            <td>150</td>
        </tr>
        <tr>
            <td>Visayas</td>
            <td>200</td>
        </tr>
        <tr>
            <td>Mindanao</td>
            <td>250</td>
        </tr>
    </tbody>
</table>
@endsection

@section('table2')
<table class="text-center" border="1px solid black">
    <thead>
        <th>Payment</th>
        <th>Amount</th>
    </thead>
    <tbody>
        <tr>
            <td>Alumni ID</td>
            <td>{{$payment->alumni_id_fee}}</td>
        </tr>
        <tr>
            <td>Membership Fee{{'('.$user->degree.')'}} </td>
            <td>{{$payment->membership_fee}}</td>
        </tr>
        <tr>
            <td>Total</td>
            <td>{{$payment->total_amount}}</td>
        </tr>
    </tbody>
</table>
@endsection

@section('content')
<tr>
    <td class="content">
        <h1 class="text-center">Application Verified!</h1>
        <h2>Hi {{$user->first_name}},</h2>
        <p>
            Congratulations! Your application is now verified. You may now proceed with your payment through the
            following:
        </p>

        @yield('table2')

        <br>
        <ol style="margin-left: 40px;">
            <li>Deposit to any Bank of the Philippines Islands branch.
                <ul style="margin-left: 40px; list-style-type:disc;">
                    <li>Account Number: 3643 018199</li>
                    <li>Account Name: Association of UMak Alumni, Inc.</li>
                    <li>Account Type: Savings</li>
                </ul>
            </li>
            <li>University of Makati Employees Multi-Purpose Cooperative (COOP)</li>
        </ol>

        <p>
            For delivery: Please see applicable rates. This should be added on top of your payment for the Alumni ID
            and Membership fee.
        </p>

        @yield('table')

        <p>Once settled, Click the link to upload your proof of payment. </p>

        <div class="text-center">
            <a class="button" href="{{route('register.payment', ['id' => $user->uuid])}}">
                Click this upload your receipt.
            </a>
        </div>

        <br>

        <p>
            By the way, if you want to get the latest updates and articles to UMak Alumni,
            visit <a href="{{ route('home') }}">UTRACK</a>.
        </p>
        <p><em>– Center for Alumni Affairs</em></p>
    </td>
</tr>
@endsection