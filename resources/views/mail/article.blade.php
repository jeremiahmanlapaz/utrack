@extends('mail.master')

@section('content')
<tr>
    <td class="content">

        <h2>Hi</h2>

        <p>Have you heard the latest news on us, Click the link below to read this</p>

        <table>
            <tr>
                <td align="center">
                    <p>
                        <a href="#" class="button">Click Me! This is not a virus :)</a>
                    </p>
                </td>
            </tr>
        </table>

        <p>By the way, if you want to get the latest updates and articles to our alumni, visit <a
                href="http://{{request()->getHost()}}">UTRACK</a>.</p>

        <p><em>– Center for Alumni Affairs</em></p>

    </td>
</tr>
@endsection