@extends('mail.master')

@section('content')

<tr>
   <td class="content">

      <h2>Hi {{$applicant->first_name}},</h2>

      <p>Sorry for the long wait, We just confirmed your membership application, thank you for applying to us, The
         Center of Alumni Affairs.
         Kindly click the button below to verify your email address.</p>

      <table>
         <tr>
            <td align="center">
               <p>
                  <a href="{{route('applicants.verify.email', ['id' => $applicant->uuid])}}" class="button">Verify
                     Email</a>
               </p>
            </td>
         </tr>
      </table>

      <p>By the way, if you want to get the latest updates and articles to our alumni, visit <a
            href="{{route('home')}}">UTRACK</a>.</p>

      <p><em>– Center for Alumni Affairs</em></p>

   </td>
</tr>
@endsection