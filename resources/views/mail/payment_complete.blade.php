@extends('mail.master')

@section('content')
<tr>
    <td class="content">

        <h2>Hi {{ $user['first_name'] }}</h2>

        <p>
            We confirmed your payment,
            your are now an official member of Alumni Association of University of makati.
            If your shipping option is Pick-up at CAA office, you may pick your ID after 5 working days.
            else, please wait for the parcel to be delivered at the address you input.
        </p>

        <p><em>– Center for Alumni Affairs</em></p>

    </td>
</tr>
@endsection