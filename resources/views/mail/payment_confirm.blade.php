@extends('mail.master')

@section('content')
<tr>
    <td class="content">

        <h2>Hi {{$applicant->first_name}},</h2>

        <p>We've confirmed your payment, to finish your application please kindly filled the necessary information in
            the link below.</p>

        <table>
            <tr>
                <td align="center">
                    <p>
                        <a href="{{ route( 'register.show', ['uuid' => $applicant->uuid] ) }}" class="button">Alumni
                            Tracker Form</a>
                    </p>
                </td>
            </tr>
        </table>

        <p>By the way, if you want to get the latest updates and articles to our alumni, visit <a
                href="{{route('home')}}">UTRACK</a>.</p>
        <p><em>– Center for Alumni Affairs</em></p>
    </td>
</tr>
@endsection