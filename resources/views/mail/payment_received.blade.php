@extends('mail.master')

@section('content')
<tr>
    <td class="content">

        <h2>Hi {{ $user->first_name }}</h2>

        <p>We have received your payment. please wait while we verify your payment. thank you.</p>

        <p><em>– Center for Alumni Affairs</em></p>

    </td>
</tr>
@endsection