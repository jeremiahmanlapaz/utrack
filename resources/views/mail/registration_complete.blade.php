@extends('mail.master')

@section('content')
<tr>
   <td class="content">

      <h2>Hi {{$user['first_name']}},</h2>

      <p>Thank you for applying to us, Center of Alumni Affairs. </p>
      <p>We need to review your application before you proceed to payment.</p>
      <p>We'll process your application with 7 working days.</p>

      <p>By the way, if you want to get the latest updates and articles to our alumni, visit <a
            href="{{route('news')}}">UTRACK</a>.
      </p>

      <p><em>– Center for Alumni Affairs</em></p>

   </td>
</tr>
@endsection