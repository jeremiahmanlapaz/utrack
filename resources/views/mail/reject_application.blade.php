@extends('mail.master')

@section('content')
<tr>
    <td class="content">
        <h2>Hi {{$applicant->first_name}}</h2>
        <p>Your Application was rejected, we did not find you on our alumni list. sorry.</p>
        <p><em>– Center for Alumni Affairs</em></p>
    </td>
</tr>
@endsection