@extends('mail.master')

@section('content')
<tr>
   <td class="content">

      <h2>Hi {{$alumni->first_name}},</h2>

      <p>Click the button below to redirect to update form. </p>
      <a href="{{ route('update.show', ['uuid' => $alumni->uuid]) }}" class="button text-center">Click Me </a>
      <p>By the way, if you want to get the latest updates and articles to our alumni, visit <a
            href="{{route('home')}}">UTRACK</a>.</p>

      <p><em>– Center for Alumni Affairs</em></p>

   </td>
</tr>
@endsection