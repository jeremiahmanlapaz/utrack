@extends('mail.master')

@section('content')
<tr>
    <td class="content">
        <h1 class="text-center">Thank you for applying!</h1>
        <h2>Hi {{$applicant['first_name']}},</h2>

        <p>We've received your application.</p>
        <p>We'll notify you.</p>

        <p>By the way, if you want to get the latest updates and articles to UMak Alumni, visit <a
                href="http://{{request()->getHost()}}">UTRACK</a>.</p>
        <p><em>– Center for Alumni Affairs</em></p>
    </td>
</tr>
@endsection