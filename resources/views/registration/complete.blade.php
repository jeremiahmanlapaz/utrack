@extends('layouts.master')

@section('title', 'Registration Complete')

@section('content')
@if (!isset($email))
<script type="text/javascript">
    window.location = "{{ url('/register') }}"
</script>

@else
<div class="jumbotron text-center">
    <i class="fas fa-envelope fa-5x fa-fw"></i>
    <h3>Thank You!</h3>
    <p class="text-muted">We'll verify your application, please wait for an update on your email
        <strong>{{ $email }}</strong></p>
    <hr>
</div>
@endif
@endsection