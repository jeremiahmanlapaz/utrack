@extends('layouts.master')

@section('title', 'Registration')

@section('links')
<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap/bootstrap-datepicker.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('vendor/SmartWizard/dist/css/smart_wizard.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('vendor/SmartWizard/dist/css/smart_wizard_theme_dots.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('vendor/select2/css/select2.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('vendor/select2/css/select2-bootstrap.css') }}">
@endsection

@section('style')
<style>
    input[type="radio"] {
        -webkit-appearance: checkbox;
        /* Chrome, Safari, Opera */
        -moz-appearance: checkbox;
        /* Firefox */
        -ms-appearance: checkbox;
        /* not currently supported */
    }

    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    .datepicker-dropdown {
        z-index: 999px !important;
    }
</style>
@endsection

@section('content')
<div class="container">
    <form action="{{route('register.create')}} " method="post" enctype="multipart/form-data">
        @csrf
        @if (session()->has('message'))
        <div class="alert {{'alert-' . session()->get('class')}} alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                <span class="sr-only">Close</span>
            </button>
            {{session()->get('message')}}
        </div>
        @endif
        <div class="m-3" id="smartwizard">
            <ul class="mb-5">
                <li><a href="#step-1">{{__('General Information')}}</a></li>
                <li><a href="#step-2">{{__('Graduation Details')}}</a></li>
                <li><a href="#step-3">{{__('Employment Details')}}</a></li>
                <li><a href="#step-4">{{__('Upload Image')}}</a></li>
            </ul>
            <div class="mt-5" id="container" style="height: 400px">
                <div id="step-1">
                    @include('registration.step-1')
                </div>
                <div id="step-2">
                    @include('registration.step-2')
                </div>
                <div id="step-3">
                    @include('registration.step-3')
                </div>
                <div id="step-4">
                    @include('registration.step-4')
                </div>
            </div> <!-- mt-5 -->
        </div> <!-- smartwizard -->
    </form> <!-- form -->
</div> <!-- container -->
@endsection

@section('js')
<script src="{{ asset('js/bootstrap/bs-custom-file-input.js') }}"></script>
<script src="{{ asset('vendor/SmartWizard/dist/js/jquery.smartWizard.js') }}"></script>
<script src="{{ asset('vendor/select2/js/select2.js') }}"></script>
<script src="{{ asset('js/bootstrap/bootstrap-datepicker.js') }}"></script>
<script>
    bsCustomFileInput.init()
    //Document ready
$(document).ready(function () {

// Toolbar extra buttons
let btnFinish = $('<button></button>')
    .text('Submit')
    .addClass('btn btn-success sw-btn-finish')
    .attr('type', 'submit')
    .hide();

$('#smartwizard').smartWizard({
    theme: 'dots',
    trasitionEffect: "fade",
    keyNavigation: false,
    showStepURLhash: false,
    toolbarSettings: {
        toolbarButtonPosition: "center",
        toolbarExtraButtons: [btnFinish]
    }
}).on("showStep", function (e, anchorObject, stepNumber, stepDirection) {
    showFinishBtn();
});
showFinishBtn();

$('.datepicker').datepicker({
    zIndexOffset: '999'
});

var college = $('[name=education\\[college\\]]');
getCourse(college);

college.on('change', function () {
    getCourse($(this));
});

employed($("[name=employment\\[status\\]]:checked"));

//test CheckBox set true
// $(document).find('input[type=checkbox]').prop('checked', true);


$('.select2').select2({
    theme: 'bootstrap',
    width: null,
    height: '60px',
    containerCss: {'height': '38px' },
}).on('change', (e) => {
    // console.log($(e.target).val());
});

$('.select3').select2({
    theme: 'bootstrap',
    tags: true,
    height: '60px',
    containerCss: {'height': '38px' },
}).on('change', (e) => {
    // console.log($(e.target).val());
});

$('.datepickeryear').datepicker({
    format: "yyyy",
    viewMode: "years",
    minViewMode: "years",
    startDate: new Date(1970, 1, 1),
    endDate: new Date(2020, 1, 1),
    zIndexOffset: 999
});

$("[name=employment\\[status\\]]").change(function () {
    employed($(this));
});

$('[name=education\\[course\\]]').change(function () {
    let degree = $(this).children("option:selected").data('degree');
    let inputDegree = $('[name=education\\[degree\\]]');
    inputDegree.val(degree);
});

function showFinishBtn() {
    if ($('button.sw-btn-next').hasClass('disabled')) {
        $('.sw-btn-finish').show(); // show the button extra only in the last page
    } else {
        $('.sw-btn-finish').hide();
    }
}

function employed(el) {
    if ($(el).val() == 'no/never employed') {
        $('#employment_employed').attr('hidden', true);
        $('#employment_reason').removeAttr('hidden');
        $('#container').css('min-height', '150px');
        setInputs($('#employment_reason'), $('#employment_employed'));
    } else {
        $('#employment_reason').attr('hidden', true);
        $('#employment_employed').removeAttr('hidden');
        $('#container').css('height', 'auto');
        setInputs($('#employment_employed'), $('#employment_reason'));
    }
}

function setInputs(enable, disable) {
    $(enable).find('input, select').removeAttr('disabled');
    $(disable).find('input, select').prop('disabled', true);
}

function getCourse(e) {
    let course = $('[name=education\\[course\\]]')
    course.children().remove();
    let val = $(e).val();
    $.get({
        url: window.location.protocol + '//' + window.location.host + '/register/getCourse/' + val,
        success: function (data, Textstatus, jqXHR) {
            if (data.length > 0) {
                $.each(data, function (index, val) {
                    let opt = $('<option>');
                    course.append(opt.text(val.name).val(val.name).data("degree", val.degree));
                });
            } else {
                course.append($('<option>').text("").val(""));
            }
        },
        error: function (jqHXR, textStatus, error) {
            console.error(error);
        }
    });
}

});
</script>
@endsection