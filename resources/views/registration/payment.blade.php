@extends('layouts.master')

@section('title', 'Registration Payment Update')

@section('links')
<link rel="stylesheet" href="{{ asset('vendor/viewerjs/dist/viewer.css') }}">
@endsection

@section('content')
<br>
<h2>Payment Update</h2>
<small>Please refer payment to <a target="_blank" href="{{route('membership')}}/#rates_table">rates</a> & <a
        target="_blank" href="{{route('membership')}}/#delivery_table">delivery rates</a> </small>
<div class="row" style="min-height: 500px;">
    <div class="col-md-12">
        <form method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label>{{__('Upload Proof of Payment')}}</label>
                            <div class="custom-file">
                                <input type="file" name="proof_of_payment"
                                    class="custom-file-input @error('proof_of_payment') is-invalid @enderror"
                                    id="custom-file" accept="image/*">
                                <label class="custom-file-label" for="custom-file"></label>
                                @error('proof_of_payment')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                                @enderror
                            </div>
                            <small class="form-text text-muted">Upload your Proof of Payment.</small>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="payment_method">Payment method</label>
                        <select type="text" class="form-control @error('payment_method') is-invalid @enderror"
                            name="payment_method" id="payment_method" aria-describedby="addressHelp">
                            <option value="Bank Deposit"
                                {{ old('payment_method') == 'Bank Deposit' ? 'selected' : '' }}>Bank
                                Deposit</option>
                            <option value="COOP">University of Makati Employees Multi-Purpose Cooperative (COOP)
                            </option>
                        </select>
                        <small id="addressHelp" class="form-text text-muted">Select the mode of payment you
                            used.</small>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Amount Paid</label>
                        <input type="number" name="amount_paid"
                            class="form-control @error('amount_paid') is-invalid @enderror"
                            value="{{old('amount_paid')}}">
                        <small class="form-text text-muted">Enter total amount paid.</small>
                        @error('amount_paid')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="shipping_option">Shipping option</label>
                        <select class="form-control" name="delivery_method">
                            <option value="Pick up at CAA office(UMak)"
                                {{ old('delivery_method') == 'Pick up at CAA office(UMak)' ? 'selected' : '' }}>Pick up
                                at CAA office(UMak)</option>
                            <option value="Deliver within NCR"
                                {{ old('delivery_method') == 'Deliver within NCR' ? 'selected' : '' }}>Deliver within
                                NCR - &#8369; 120</option>
                            <option value="Deliver within Luzon(North and South Luzon Areas)"
                                {{ old('delivery_method') == 'Deliver within Luzon(North and South Luzon Areas)' ? 'selected' : '' }}>
                                Deliver within Luzon(North and South Luzon Areas)&#8369; 120</option>
                            <option value="Deliver within Visayas"
                                {{ old('delivery_method') == 'Deliver within Visayas' ? 'selected' : '' }}>Deliver
                                within Visayas - &#8369; 160</option>
                            <option value="Deliver within Mindanao"
                                {{ old('delivery_method') == 'Deliver within Mindanao' ? 'selected' : '' }}>Deliver
                                within Mindanao - &#8369; 120</option>
                        </select>
                        <small class="form-text text-muted">The delivery rates must added on your top of your payment
                            before uploading.</small>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="delivery_address">Shipping Address</label>
                        <input type="text" class="form-control @error('delivery_address') is-invalid @enderror"
                            name="delivery_address" id="delivery_address" aria-describedby="addressHelp"
                            placeholder="Enter Address" value="{{ old('delivery_address') }}"
                            {{ old('delivery_address') ? '': 'disabled'}}>
                        <small id="addressHelp" class="form-text text-muted">.</small>
                        @error('delivery_address')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-success">Submit</button>
        </form>
    </div>
</div>
@endsection

@section('js')
<script src="{{ asset('vendor/viewerjs/dist/viewer.js') }}"></script>
<script src="{{ asset('js/bootstrap/bs-custom-file-input.js') }}"></script>
<script>
    bsCustomFileInput.init()

    $('[name=delivery_method]').change(function(){
        var selected = $(this).children('option:selected').val();
        if(selected == 'Pick up at CAA office(UMak)'){
            $('#delivery_address').prop('disabled', true);
        }else{
            $('#delivery_address').prop('disabled', false);            
        }
    })
</script>
@endsection