@extends('layouts.master')

@section('title', 'Payment Received')

@section('content')
<div class="jumbotron text-center">
    <i class="fas fa-envelope fa-5x fa-fw"></i>
    <h3>Thank You!</h3>
    <p class="text-muted">
        We'll verify your payment,
        Please check your email <strong>{{ $email }}</strong>
    </p>
    <hr>
    <div class="text-center">
        <a class="btn btn-success" href="{{route('home')}}">Return Home</a>
    </div>
</div>
@endsection