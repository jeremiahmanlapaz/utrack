@section('step2')
{{-- 12. ATTAINMENT --}}
<p>{{__('Education Attainment')}}</p>
<hr>
<div class="row">
    <div class="col-md-3">
        <div class="form-group">
            <label>{{__('College')}}</label>
            <select class="form-control select2 @error('education.college') is-invalid @enderror"
                name="education[college]">
                @foreach ($colleges as $college)
                <option value="{{$college->code}}" {{old('education.college') == $college->code ? 'selected' : ''}}>
                    {{$college->name}}
                </option>
                @endforeach
            </select>
            @error('education.college')
            <div class="invalid-feedback">
                {{$message}}
            </div>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>{{__('Course/Specialization')}}</label>
            <select class="form-control select2 @error('education.course') is-invalid @enderror"
                name="education[course]">
                <option value=""></option>
            </select>
            @error('education.course')
            <div class="invalid-feedback">
                {{$message}}
            </div>
            @enderror
        </div>
    </div>
    <input type="hidden" name="education[degree]"
        value="{{old('education.degree') ? old('education.degree') : 'Bachelor'}}">
    <div class="col-md-3">
        <div class="form-group">
            <label>{{__('Year Graduated')}}</label>
            <input type="text"
                class="form-control datepickeryear @error('education.year_graduated') is-invalid @enderror"
                name="education[year_graduated]" value="{{ old('education.year_graduated') }}">
            @error('education.year_graduated')
            <div class="invalid-feedback">
                {{$message}}
            </div>
            @enderror
        </div>
    </div>
</div>
@show