@section('step4')
{{-- 13. EMPLOYMENT STATUS --}}
<fieldset class="form-group">
    <div class="row">
        <label class=" col-md-4">{{ __('Present Employment Status') }}</label>
        <div class="col-md-8">
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="employment[status]" value="regular"
                    {{old('employment[status]') == 'regular'?'checked':''}} checked>
                <label class="form-check-label">{{__('Regular')}}</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="employment[status]" value="temporary"
                    {{old('employment[status]') == 'temporary'?'checked':''}}>
                <label class="form-check-label">{{__('Temporary')}}</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="employment[status]" value="contractual"
                    {{old('employment[status]') == 'contractual'?'checked':''}}>
                <label class="form-check-label">{{__('Contractual')}}</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="employment[status]" value="self employed"
                    {{old('employment[status]') == 'self employed'?'checked':''}}>
                <label class="form-check-label">{{__('Self-employed')}}</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="employment[status]" value="no/never employed"
                    {{old('employment[status]') == 'no/never employed'?'checked':''}}>
                <label class="form-check-label">{{__('No / Never-employed')}}</label>
            </div>
        </div>
    </div>
</fieldset>
{{-- 17. REASONS --}}
<div class="row" id="employment_reason" hidden>
    <div class="col-md-12">
        <p>
            {{__('Please state reason(s) why you are not yet employed. you may check more than one answer.')}}
        </p>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="employment[reason][]"
                            value="Advance or further study">
                        <label class="form-check-label">
                            {{__('Advance or further study')}}
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="employment[reason][]"
                            value="No job opportunity">
                        <label class="form-check-label">
                            {{__('No job opportunity')}}
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="employment[reason][]"
                            value="Family concern and decided not to find a job">
                        <label class="form-check-label">
                            {{__('Family concern and decided not to find a job')}}
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="employment[reason][]"
                            value="Did not look for a job">
                        <label class="form-check-label">
                            {{__('Did not look for a job')}}
                        </label>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="employment[reason][]"
                            value="Health-related reason(s)">
                        <label class="form-check-label">
                            {{__('Health-related reason(s)')}}
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="employment[reason][]"
                            value="Lack of work experience">
                        <label class="form-check-label">
                            {{__('Lack of work experience')}}
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="employment[reason][]"
                            value="other reason(s)">
                        <label class="form-check-label">
                            {{__('other reason(s)')}}
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="employment_employed">
    {{-- 19. OCCUPATION --}}
    <div class="form-group row">
        <label class="col-sm-4 col-form-label">{{ __('Present Occupation') }}</label>
        <div class="col-md-8">
            <select class="form-control select3 @error('employment.occupation') is-invalid @enderror"
                name="employment[occupation]">
                @foreach ($job_title as $title)
                <option value="{{$title->title}}">{{$title->title}}</option>
                @endforeach
            </select>
            @error('employment.occupation')
            <div class="invalid-feedback">
                {{$message}}
            </div>
            @enderror
        </div>
    </div>
    {{-- 19. COMPANY NAME --}}
    <div class="form-group row">
        <label class="col-sm-4 col-form-label">{{ __('Company Name') }}</label>
        <div class="col-md-8">
            <input type="text" class="form-control @error('employment.company_name') is-invalid @enderror"
                name="employment[company_name]" value="{{ old('employment.company_name') }}">
            @error('employment.company_name')
            <div class="invalid-feedback">
                {{$message}}
            </div>
            @enderror
        </div>
    </div>
    {{-- 20. Industries --}}
    <div class="form-group row">
        <label class="col-sm-4 col-form-label">{{ __('Company Industry') }}</label>
        <div class="col-md-8">
            <select name="employment[industry]"
                class="form-control select2 @error('employment.industry') is-invalid @enderror">
                @foreach ($industries as $industry)
                <option value="{{ $industry->name }}" {{old('employment[industry]' == $industry->name?'selected':'')}}>
                    {{ $industry->name }}</option>
                @endforeach
            </select>
            @error('employment.industry')
            <div class="invalid-feedback">
                {{$message}}
            </div>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label class="col-md-4 col-form-label">{{__('Job Level')}}</label>
        <div class="col-md-8">
            <select class="form-control select2" name="employment[job_level]">
                <option value="Rank or clerical" {{old('employment.job_level') == 'Rank or clerical'?'selected':''}}>
                    {{__('Rank or clerical')}}</option>
                <option value="Professional, Technical or Supervisory"
                    {{old('employment.job_level') == 'Professional, Technical or Supervisory'?'selected':''}}>
                    {{__('Professional, Technical or Supervisory')}}
                </option>
                <option value="Managerial or Executive"
                    {{old('employment.job_level') == 'Managerial or Executive'?'selected':''}}>
                    {{__('Managerial or Executive')}}</option>
                <option value="Self-employed" {{old('employment.job_level') == 'Self-employed'?'selected':''}}>
                    {{__('Self-employed')}}</option>
            </select>
        </div>
    </div>
    {{-- 21. PLACE OF WORK --}}
    <fieldset class="form-group">
        <div class="row">
            <label class="col-md-4">{{ __('Place of work') }}</label>
            <div class="col-md-8">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="employment[place_of_work]" value="local"
                        {{old('employment[pow]') == 'local'?'checked':''}} checked>
                    <label class="form-check-label">{{__('Local')}}</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="employment[place_of_work]" value="abroad"
                        {{old('employment[pow]') == 'abroad'?'checked':''}}>
                    <label class="form-check-label">{{__('Abroad')}}</label>
                </div>
            </div>
        </div>
    </fieldset>
    <hr>
    {{-- 22. FIRST JOB --}}
    <fieldset class="form-group">
        <div class="row">
            <label class=" col-md-4">{{__('is this your first job after college') }}</label>
            <div class="col-md-8">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="first_job[is_first_job]" value="yes"
                        {{old('first_job[is_first_job]') == 'yes'?'checked':''}} checked>
                    <label class="form-check-label">{{__('Yes')}}</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="first_job[is_first_job]" value="no"
                        {{old('first_job[is_first_job]') == 'no'?'checked':''}}>
                    <label class="form-check-label">{{__('No')}}</label>
                </div>
            </div>
        </div>
    </fieldset>
    {{-- 23. REASONS FOR accepting --}}
    <div class="row">
        <div class="col-md-12">
            <p>
                {{__('What are your reason(s) for accepting the job? You may check more than one answer.')}}
            </p>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="first_job[reason_accepting][]"
                                value="Salaries and benefits">
                            <label class="form-check-label">
                                {{__('Salaries and benefits')}}
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="first_job[reason_accepting][]"
                                value="Carrer challenge">
                            <label class="form-check-label">
                                {{__('Carrer challenge')}}
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="first_job[reason_accepting][]"
                                value="Related to special skill">
                            <label class="form-check-label">
                                {{__('Related to special skill')}}
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="first_job[reason_accepting][]"
                                value="Related to course or program of study">
                            <label class="form-check-label">
                                {{__('Related to course or program of study')}}
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="first_job[reason_accepting][]"
                                value="Proximity to residence">
                            <label class="form-check-label">
                                {{__('Proximity to residence')}}
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="first_job[reason_accepting][]"
                                value="Peer influence">
                            <label class="form-check-label">
                                {{__('Peer influence')}}
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="first_job[reason_accepting][]"
                                value="Family influence">
                            <label class="form-check-label">
                                {{__('Family influence')}}
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="first_job[reason_accepting][]"
                                value="Other reason(s)">
                            <label class="form-check-label">
                                {{__('Other reason(s)')}}
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- 29. NO. JOB LENGTH --}}
    <div class="row">
        <div class="col-md-12">
            <p>
                {{__('How long did it take you to land your first job?')}}
            </p>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="first_job[how_long_did_take]"
                                value="Less than a month" checked>
                            <label class="form-check-label">
                                {{__('Less than a month')}}
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="first_job[how_long_did_take]"
                                value="1 to 3 months">
                            <label class="form-check-label">
                                {{__('1 to 3 months')}}
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="first_job[how_long_did_take]"
                                value="3 to 6 months">
                            <label class="form-check-label">
                                {{__('3 to 6 months')}}
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="first_job[how_long_did_take]"
                            value="7 to 11 months">
                        <label class="form-check-label">
                            {{__('7 to 11 months')}}
                        </label>
                    </div>
                    <div class="form-group">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="first_job[how_long_did_take]"
                                value="1 year to less than 2 years">
                            <label class="form-check-label">
                                {{__('1 year to less than 2 years')}}
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="first_job[how_long_did_take]"
                                value="2 year to less than 3 years">
                            <label class="form-check-label">
                                {{__('2 year to less than 3 years')}}
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr>
    {{-- 33. IF yes. --}}
    <div class="row">
        <div class="col-md-12">
            <p>
                {{__('what competencies learned in college did you find very useful in your first job? You may
                check more than one answer')}}
            </p>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="first_job[competency_skills][]"
                                value="Communication Skills">
                            <label class="form-check-label">
                                {{__('Communication Skills')}}
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="first_job[competency_skills][]"
                                value="Human Relations Skills">
                            <label class="form-check-label">
                                {{__('Human Relations Skills')}}
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="first_job[competency_skills][]"
                                value="Entrepreneurial Skills">
                            <label class="form-check-label">
                                {{__('Entrepreneurial Skills')}}
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="first_job[competency_skills][]"
                                value="Information Technology Skills">
                            <label class="form-check-label">
                                {{__('Information Technology Skills')}}
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="first_job[competency_skills][]"
                                value="Problem-solving Skills">
                            <label class="form-check-label">
                                {{__('Problem-solving Skills')}}
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="first_job[competency_skills][]"
                                value="Critical Thinking Skills">
                            <label class="form-check-label">
                                {{__('Critical Thinking Skills')}}
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="first_job[competency_skills][]"
                                value="Other Skills">
                            <label class="form-check-label">
                                {{__('Other Skills')}}
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- 34. SUGGESTION --}}
<div class="row">
    <div class="col-md-12">
        <p>{{__('Suggestions to further improve your course curriculum')}}</p>
        <textarea class="form-control" name="suggestion" cols="20" rows="5"></textarea>
    </div>
</div>
@show