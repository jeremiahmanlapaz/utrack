@section('step-4')
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label>{{__('I.D. Photo')}}</label>
            <div class="custom-file">
                <input type="file" name="applicant[image]"
                    class="custom-file-input @error('applicant.image') is-invalid @enderror" id="custom-file"
                    accept="image/*">
                <label class="custom-file-label" for="custom-file">{{__('Choose Cover Image')}}</label>
                @error('applicant.image')
                <div class="invalid-feedback">
                    {{$message}}
                </div>
                @enderror
            </div>
        </div>
    </div>
</div>
@show