@extends('layouts.admin_master')

@section('title')
{{__('Imports')}}
@endsection

@section('content-header')
@endsection

@section('content')
<div class="row">
    <div class="col-md-6">
        <div class="card mb-4">
            <div class="card-header">
                {{__('Mass Add College')}}
            </div>
            <div class="card-body">
                <form action="javascript:void(0)" class="form" data-url="{{ route('test.colleges') }}">
                    @csrf
                    <div class="form-group">
                        <label for="inputFile"> JSON</label>
                        <input type="file" name="inputFile" id="inputFile" accept="*.json">
                    </div>
                    <input class="btn btn-success btn-md" type="submit" id="submit">
                </form>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="card mb-4">
            <div class="card-header">
                {{__('Mass Add Job Title')}}
            </div>
            <div class="card-body">
                <form action="javascript:void(0)" class="form" data-url="{{ route('test.jobtitle') }}">
                    @csrf
                    <div class="form-group">
                        <label for="inputFile"> JSON</label>
                        <input type="file" name="inputFile" id="inputFile" accept="*.json">
                    </div>
                    <input class="btn btn-success btn-md" type="submit" id="submit">
                </form>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="card mb-4">
            <div class="card-header">
                {{__('Mass Add Companies')}}
            </div>
            <div class="card-body">
                <form action="javascript:void(0)" class="form" data-url="{{ route('test.companies') }}">
                    @csrf
                    <div class="form-group">
                        <label for="inputFile"> JSON</label>
                        <input type="file" name="inputFile" id="inputFile" accept="*.json">
                    </div>
                    <input class="btn btn-success btn-md" type="submit" id="submit">
                </form>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="card mb-4">
            <div class="card-header">
                {{__('Mass Add Articles')}}
            </div>
            <div class="card-body">
                <form action="javascript:void(0)" class="form" data-url="{{ route('test.articles') }}">
                    @csrf
                    <div class="form-group">
                        <label for="inputFile"> JSON</label>
                        <input type="file" name="inputFile" id="inputFile" accept="*.json">
                    </div>
                    <input class="btn btn-success btn-md" type="submit" id="submit">
                </form>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="card mb-4">
            <div class="card-header">
                {{__('Mass Add Course')}}
            </div>
            <div class="card-body">
                <form action="javascript:void(0)" class="form" data-url="{{ route('test.course') }}">
                    @csrf
                    <div class="form-group">
                        <label for="inputFile"> JSON</label>
                        <input type="file" name="inputFile" id="inputFile" accept="*.json">
                    </div>
                    <input class="btn btn-success btn-md" type="submit" id="submit">
                </form>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="card mb-4">
            <div class="card-header">
                {{__('Test Alert')}}
            </div>
            <div class="card-body">
                <a href="{{ route('test.alert') }}" class="btn btn-success"><i class="fa fa-bell"></i> Test Alert</a>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="card mb-4">
            <div class="card-header text-white  bg-danger">
                {{__('Truncate Table')}}
            </div>
            <div class="card-body">
                <button name="truncate" data-url="{{ route('test.truncate', ['table' => 'all']) }}"
                    class="btn btn-danger mb-2"><i class="fa fa-times"></i> Truncate Members, Applicants, Graduates, &
                    Payments</button>
                <button name="truncate" data-url="{{ route('test.truncate', ['table' => 'articles']) }}"
                    class="btn btn-danger mb-2"><i class="fa fa-times"></i> Truncate Articles</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
<script>
    $(document).ready(function(){
        var json;
            $('[name=inputFile]').change(function(event){
                console.log("file Loaded");
                var reader = new FileReader();
                reader.readAsText(event.target.files[0]);
                reader.onload = function(event){
                    json = JSON.parse(event.target.result);
                };
                // console.log(reader);
            });

            $('.form').submit(function(event){
                var url = $(this).data('url');
                $.post({
                    url: url,
                    data: JSON.stringify(json),
                    contentType: 'application/json',
                    processData: false,
                    dataType: 'json',
                    headers: {
                        'X-CSRF-TOKEN': $('[name=_token]').val()
                    },
                    beforeSend: function(jqXHR, plainObject){
                        // console.log("beforeSend: %O", plainObject);
                    },
                    success: function(jqXHR, status, response){
                        console.log(status + ": %o", jqXHR);
                        customAlert(jqXHR.message, 'success');
                    },
                    error: function(jqXHR, textStatus, errorThrown){
                        console.log("Failed: %O", jqXHR);
                        console.log("AJAX error: " + errorThrown);
                        customAlert(jqXHR.message, 'danger');
                    }
                });
            });

            $('button[name=truncate]').click(function (e) { 
                let url = $(this).data('url');
                $.get({
                    url: url,
                    success: function (jqXHR, status, response) {
                        console.log(status + ": %o", jqXHR);
                        customAlert(jqXHR.message, 'success');
                    },
                    error: function (jqXHR, textStatus, errorThrown){
                        console.log("Failed: %O", jqXHR);
                        console.log("AJAX error: " + errorThrown);
                        customAlert(jqXHR.message, 'danger');
                    }
                })    
            });
        });
</script>
@endsection