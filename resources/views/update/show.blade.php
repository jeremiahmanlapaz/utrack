@extends('layouts.master')

@section('title', 'Update')

@section('content')
<div class="row pb-2">
    @if (session()->has('message'))
    <div class="col-md-12">
        <div class="alert alert-{{session()->get('class')}} alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                <span class="sr-only">Close</span>
            </button>
            <strong>{{session()->get('message')}}!</strong>
        </div>
    </div>
    @endif
    <div class="col-md-12">
        <form method="post">
            @csrf
            <div class="form-group">
                <label>Email Address</label>
                <input type="email" class="form-control @error('email') is-invalid @enderror" name="email">
                <small>Enter Registered Email Address</small>
                @error('email')
                <div class="invalid-feedback">
                    {{$message}}
                </div>
                @enderror
            </div>
            <div class="form-group text-centered">
                <button class="btn btn-success">Send Request</button>
            </div>
        </form>
    </div>
</div>


@endsection

@section('js')

@endsection