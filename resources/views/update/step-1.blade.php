@section('step-1')
{{-- 1. NAME --}}
<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label for="first_name">{{ __('First Name') }}</label>
            <input type="text" class="form-control @error('applicant.first_name') is-invalid @enderror" id="first_name"
                name="applicant[first_name]" value="{{ $alumni->first_name }}">
            @error('applicant.first_name')
            <div class="invalid-feedback">
                {{$message}}
            </div>
            @enderror
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="middle_name">{{ __('Middle Name (n/a if not applicable)') }}</label>
            <input type="text" class="form-control @error('applicant.middle_name') is-invalid @enderror"
                id="middle_name" name="applicant[middle_name]" value="{{ $alumni->middle_name }}">
            @error('applicant.middle_name')
            <div class="invalid-feedback">
                {{$message}}
            </div>
            @enderror
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="last_name">{{ __('Last Name') }}</label>
            <input type="text" class="form-control @error('applicant.last_name') is-invalid @enderror" id="last_name"
                name="applicant[last_name]" value="{{ $alumni->last_name }}">
            @error('applicant.last_name')
            <div class="invalid-feedback">
                {{$message}}
            </div>
            @enderror
        </div>
    </div>
</div>
{{-- 2. ADDRESS --}}
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label for="address">{{ __('Permanent Address') }}</label>
            <input type="text" class="form-control @error('applicant.address') is-invalid @enderror" id="address"
                name="applicant[address]" value="{{ $alumni->address }}">
            @error('applicant.address')
            <div class="invalid-feedback">
                {{$message}}
            </div>
            @enderror
        </div>
    </div>
</div>
{{-- 3. EMAIL --}}
<div class="row">
    <div class="col-md-8">
        <div class="form-group">
            <label for="email">{{ __('Email Address') }}</label>
            <input type="text" class="form-control @error('applicant.email') is-invalid @enderror" id="email"
                name="applicant[email]" value="{{ $alumni->email }}">
            @error('applicant.email')
            <div class="invalid-feedback">
                {{$message}}
            </div>
            @enderror
        </div>
    </div>
</div>
{{-- 4 & 5. BIRTHDATE & CONTACT NUMBER --}}
<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label for="birth_date">{{ __('Birth Date') }}</label>
            <input type="text" class="form-control datepicker @error('applicant.birth_date') is-invalid @enderror"
                name="applicant[birth_date]" value="{{ date('m/d/Y', $alumni->birth_date) }}" placeholder="mm/dd/yyy">
            @error('applicant.birth_date')
            <div class="invalid-feedback">
                {{$message}}
            </div>
            @enderror
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="contact_number">{{ __('Contact Number') }}</label>
            <input type="number" class="form-control @error('applicant.contact_number') is-invalid @enderror"
                id="contact_number" name="applicant[contact_number]" value="{{ $alumni->contact_number }}">
            @error('applicant.contact_number')
            <div class="invalid-feedback">
                {{$message}}
            </div>
            @enderror
        </div>
    </div>
</div>
{{-- 6. CIVIL STATUS --}}
<fieldset class="form-group">
    <div class="row">
        <label class=" col-md-2">{{ __('Civil Status') }}</label>
        <div class="col-md-8">
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="applicant[civil_status]" value="Single"
                    {{ $alumni->civil_status == 'Single'?'checked':''}}>
                <label class="form-check-label">{{__('Single')}}</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="applicant[civil_status]" value="Married"
                    {{ $alumni->civil_status == 'Married'?'checked':''}}>
                <label class="form-check-label">{{__('Married')}}</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="applicant[civil_status]" value="Widowed"
                    {{ $alumni->civil_status == 'Widowed'?'checked':''}}>
                <label class="form-check-label">{{__('Widowed')}}</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="applicant[civil_status]" value="Separated"
                    {{ $alumni->civil_status == 'Separated'?'checked':''}}>
                <label class="form-check-label">{{__('Separated')}}</label>
            </div>
        </div>
    </div>
</fieldset>
{{-- 7. GENDER --}}
<fieldset class="form-group">
    <div class="row">
        <label class=" col-md-2">{{__('Gender')}}</label>
        <div class="col-md-10">
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="applicant[gender]" value="Male"
                    {{ $alumni->gender == 'male'?'checked':''}} checked>
                <label class="form-check-label">{{__('Male')}}</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="applicant[gender]" value="Female"
                    {{ $alumni->gender == 'female'?'checked':''}}>
                <label class="form-check-label">{{__('Female')}}</label>
            </div>
        </div>
    </div>
</fieldset>
@show