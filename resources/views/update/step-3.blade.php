@section('step4')
{{-- 13. EMPLOYMENT STATUS --}}
<fieldset class="form-group">
    <div class="row">
        <label class=" col-md-4">{{ __('Present Employment Status') }}</label>
        <div class="col-md-8">
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="employment[status]" value="regular"
                    {{ old('employment.status') == 'regular'?'checked':''}} checked>
                <label class="form-check-label">{{__('Regular')}}</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="employment[status]" value="temporary"
                    {{ old('employment.status') == 'temporary'?'checked':''}}>
                <label class="form-check-label">{{__('Temporary')}}</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="employment[status]" value="contractual"
                    {{ old('employment.status') == 'contractual'?'checked':''}}>
                <label class="form-check-label">{{__('Contractual')}}</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="employment[status]" value="self employed"
                    {{ old('employment.status') == 'self employed'?'checked':''}}>
                <label class="form-check-label">{{__('Self-employed')}}</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="employment[status]" value="no/never employed"
                    {{ old('employment.status') == 'no/never employed'?'checked':''}}>
                <label class="form-check-label">{{__('No / Never-employed')}}</label>
            </div>
        </div>
    </div>
</fieldset>
{{-- 17. REASONS --}}
<div class="row" id="employment_reason" hidden>
    <div class="col-md-12">
        <p>
            {{__('Please state reason(s) why you are not yet employed. you may check more than one answer.')}}
        </p>
        <div class="row">
            <div class="col-md-6">
                @foreach ($reasons as $reason)
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="employment[reason][]"
                        value="{{ $reason->reason }}">
                    <label class="form-check-label">
                        {{ $reason->reason }}
                    </label>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
<div id="employment_employed">
    {{-- 19. OCCUPATION --}}
    <div class="form-group row">
        <label class="col-sm-4 col-form-label">{{ __('Present Occupation') }}</label>
        <div class="col-md-8">
            <input type="text" class="form-control @error('employment.occupation') is-invalid @enderror"
                name="employment[occupation]" value="{{ old('employment.occupation') }}">
            @error('employment.occupation')
            <div class="invalid-feedback">
                {{$message}}
            </div>
            @enderror
        </div>
    </div>
    {{-- 19. COMPANY NAME --}}
    <div class="form-group row">
        <label class="col-sm-4 col-form-label">{{ __('Company Name') }}</label>
        <div class="col-md-8">
            <input type="text" class="form-control @error('employment.company_name') is-invalid @enderror"
                name="employment[company_name]" value="{{ old('employment.company_name') }}">
            @error('employment.company_name')
            <div class="invalid-feedback">
                {{$message}}
            </div>
            @enderror
        </div>
    </div>
    {{-- 20. Industries --}}
    <div class="form-group row">
        <label class="col-sm-4 col-form-label">{{ __('Company Industry') }}</label>
        <div class="col-md-8">
            <select name="employment[industry]" class="form-control @error('employment.industry') is-invalid @enderror">
                @foreach ($industries as $industry)
                <option value="{{ $industry->name }}" {{ old('employment.industry') == $industry->name?'selected':''}}>
                    {{ $industry->name }}</option>
                @endforeach
            </select>
            @error('employment.industry')
            <div class="invalid-feedback">
                {{$message}}
            </div>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label class="col-md-4 col-form-label">{{__('Job Level')}}</label>
        <div class="col-md-8">
            <select class="form-control" name="employment[job_level]">
                <option value="Rank or clerical" {{ old('employment.job_level') == 'Rank or clerical'?'selected':''}}>
                    {{__('Rank or clerical')}}</option>
                <option value="Professional, Technical or Supervisory"
                    {{ old('employment.job_level') == 'Professional, Technical or Supervisory'?'selected':''}}>
                    {{__('Professional, Technical or Supervisory')}}
                </option>
                <option value="Managerial or Executive"
                    {{ old('employment.job_level') == 'Managerial or Executive'?'selected':''}}>
                    {{__('Managerial or Executive')}}</option>
                <option value="Self-employed" {{ old('employment.job_level') == 'Self-employed'?'selected':''}}>
                    {{__('Self-employed')}}</option>
            </select>
        </div>
    </div>
    {{-- 21. PLACE OF WORK --}}
    <fieldset class="form-group">
        <div class="row">
            <label class="col-md-4">{{ __('Place of work') }}</label>
            <div class="col-md-8">
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="employment[place_of_work]" value="local"
                        {{ old('employment.place_of_work') == 'local'?'checked':''}} checked>
                    <label class="form-check-label">{{__('Local')}}</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="employment[place_of_work]" value="abroad"
                        {{ old('employment.place_of_work') == 'abroad'?'checked':''}}>
                    <label class="form-check-label">{{__('Abroad')}}</label>
                </div>
            </div>
        </div>
    </fieldset>
    <hr>
</div>
@show