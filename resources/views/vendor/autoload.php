// composer autoload
require __DIR__ . '/vendor/autoload.php';

// if you are not using composer
// require_once 'path/to/algolia/folder/autoload.php';

$client = Algolia\AlgoliaSearch\SearchClient::create(
  '4GO5DJ6FF1',
  'd0df8b5ec6547680389d36f1a0b33aae'
);

$index = $client->initIndex('your_index_name');
