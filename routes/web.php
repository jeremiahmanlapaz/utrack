<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'admin'], function () {

    Auth::routes();

    Route::get('/auth', 'AdminController@getAuth');

    Route::group(['middleware' => ['auth']], function () {
        //Index
        Route::get('/dashboard', 'AdminController@index')->name('admin');
        //Applicants
        Route::group(['prefix' => 'applicants'], function () {

            Route::get('/', 'ApplicantController@index')->name('applicants');

            Route::get('/archive', 'ApplicantController@archive')->name('applicant.archive');

            Route::get('/all', 'ApplicantController@all')->name('applicant.all');

            Route::get('/applicants', 'ApplicantController@applicants')->name('applicant.applicants');

            Route::get('/check/{uuid}', 'ApplicantController@check')->name('applicant.check');

            Route::get('/confirm_payment/{uuid}', 'ApplicantController@confirmPayment')->name('applicant.confirmPayment');

            Route::get('/delete/{uuid}', 'ApplicantController@delete')->name('applicant.delete');

            Route::get('/get/{uuid}', 'ApplicantController@get')->name('applicant.get');

            Route::get('/total', 'ApplicantController@total')->name('applicant.total');

            Route::get('/trashed', 'ApplicantController@trashed')->name('applicant.trashed');

            route::get('/reject/{uuid}', 'ApplicantController@rejectApplication')->name('applicant.reject');

            Route::get('/revoke/{uuid}', 'ApplicantController@revoke')->name('applicant.revoke');

            Route::get('/restore/{uuid}', 'ApplicantController@restore')->name('applicant.restore');

            Route::get('/verified', 'ApplicantController@verified')->name('applicant.verified');

            Route::get('/verify/{uuid}', 'ApplicantController@verifyApplicant')->name('applicant.verify');
        });
        //Content
        Route::group(['prefix' => 'content'], function () {

            Route::get('/', 'ContentManagerController@index')->name('content');

            Route::get('/archive', 'ContentManagerController@archive')->name('content.archive');

            Route::group(['prefix' => 'carousel'], function () {

                Route::get('/get', 'CarouselController@get')->name('carousel.get');

                Route::get('/create', 'CarouselController@showCreateCarousel')->name('carousel.showCreate');

                Route::post('/create', 'CarouselController@create')->name('carousel.create');

                Route::get('/delete/{id}', 'CarouselController@delete')->name('carousel.delete');

                Route::get('/edit/{id}', 'CarouselController@edit')->name('carousel.edit');

                Route::get('/restore/{id}', 'CarouselController@restore')->name('carousel.restore');

                Route::get('/trashed', 'CarouselController@trashed')->name('carousel.trashed');
            });
        });
        //Members
        Route::group(['prefix' => 'members'], function () {

            Route::get('/', 'AlumniController@index')->name('members');

            Route::get('/all', 'AlumniController@all')->name('members.all');

            Route::get('/archive', 'AlumniController@archive')->name('members.archive');

            Route::get('/delete/{uuid}', 'AlumniController@delete')->name('members.delete');

            Route::get('/get/{uuid}', 'AlumniController@get')->name('members.get');

            Route::get('/total', 'AlumniController@total')->name('members.total');

            Route::get('/profile/{uuid}', 'AlumniController@profile')->name('members.profile');

            Route::get('/sendUpdate/{uuid}', 'AlumniController@sendUpdate')->name('members.sendUpdate');
        });
        //Payments
        Route::group(['prefix' => 'payment'], function () {

            Route::get('/', 'PaymentController@index')->name('payment');

            Route::get('/all', 'PaymentController@all')->name('payment.all');

            Route::get('/get/{id}', 'PaymentController@get')->name('payment.get');

            Route::get('/showPaid', 'PaymentController@showPaid')->name('payment.showPaid');

            Route::get('/paid', 'PaymentController@paid')->name('payment.paid');

            Route::get('/confirm/{uuid}',  'PaymentController@showUpdate')->name('payment.showUpdate');

            Route::get('/confirm/update/{uuid}', 'PaymentController@update')->name('payment.update');

            Route::get('/reject', 'PaymentController@rejectPayment')->name('payment.reject');
        });
        //Reports
        Route::group(['prefix' => 'reports'], function () {

            Route::get('/', 'ReportController@index')->name('reports');

            Route::get('/generate/{college?}/{year_graduated?}/{type?}', 'ReportController@show')->name('reports.show');

            Route::get('/registration/{year?}', 'ReportController@registrationRate')->name('reports.registration');

            Route::get('/alumni/list', 'ReportController@getList')->name('reports.alumni.list');

            Route::group(['prefix' => 'print'], function () {
            });
        });

        //Articles
        Route::group(['prefix' => 'articles'], function () {

            Route::get('/', 'ArticlesController@index')->name('articles');

            Route::get('/paginate', 'ArticlesController@paginate')->name('articles.paginate');

            Route::get('/all', 'ArticlesController@all')->name('articles.all');

            Route::get('/create', 'ArticlesController@showCreate')->name('articles.showCreate');

            Route::post('/create', 'ArticlesController@create')->name('articles.create');

            Route::get('/edit/{id}', 'ArticlesController@showEdit')->name('articles.showEdit');

            Route::post('/edit/{id}', 'ArticlesController@edit')->name('articles.edit');

            Route::get('/delete/{id}', 'ArticlesController@delete')->name('articles.delete');

            Route::get('/send/{id}', 'ArticlesController@send')->name('articles.send');
        });
        //Partners
        Route::group(['prefix' => 'partners'], function () {

            Route::get('/', 'PartnersController@index')->name('partners.admin');

            Route::get('/create', 'PartnersController@showCreate')->name('partners.showCreate');

            Route::post('/create', 'PartnersController@create')->name('partners.create');

            Route::get('/edit/{id}', 'PartnersController@showEdit')->name('partners.showEdit');

            Route::post('/edit/{id}', 'PartnersController@edit')->name('partners.edit');

            Route::get('/delete', 'PartnersController@delete')->name('partners.delete');

            Route::get('/all', 'PartnersController@all')->name('partners.all');
        });
        //Push-Notification
        Route::group(['prefix' => 'push-notification'], function () {

            Route::get('/', 'PushNotificationController@index')->name('push-notif');
        });
        //Job Posting
        Route::group(['prefix' => 'job_post'], function () {

            Route::get('/{company_id}', 'JobPostController@index')->name('job');

            Route::get('/all', 'JobPostController@all')->name('job.all');

            Route::get('/get/{company_id}', 'JobPostController@get')->name('job.get');

            Route::get('/create/{company_id}', 'JobPostController@showCreate')->name('job.showCreate');

            Route::post('/create/{company_id}', 'JobPostController@addJobPost')->name('job.create');

            Route::get('/edit/{id}', 'JobPostController@showEdit')->name('job.showEdit');

            Route::post('/edit/{id}', 'JobPostController@edit')->name('job.edit');

            Route::get('/delete/{id}', 'JobPostController@delete')->name('job.delete');
        });
        //Company
        Route::group(['prefix' => 'company'], function () {

            Route::get('/', 'CompanyController@index')->name('company');

            Route::get('/all', 'CompanyController@all')->name('company.all');

            Route::get('/create', 'CompanyController@showCreate')->name('company.showCreate');

            Route::post('/create', 'CompanyController@create')->name('company.create');

            Route::get('/edit/{id}', 'CompanyController@showEdit')->name('company.showEdit');

            Route::post('/edit/{id}', 'CompanyController@edit')->name('company.edit');

            Route::get('/delete/{id}', 'CompanyController@delete')->name('company.delete');
        });
        //College
        Route::group(['prefix' => 'college'], function () {

            Route::get('/', 'CollegeController@index')->name('college');

            Route::get('/all', 'CollegeController@all')->name('college.all');

            Route::post('/create', 'CollegeController@create')->name('college.create');

            Route::get('/get/{code}', 'CollegeController@get')->name('college.get');

            Route::get('/get/course/{code}', 'CollegeController@getCourse')->name('college.get.courses');

            Route::get('/course/{code}', 'CollegeController@course')->name('college.course');

            Route::post('/course/create/{code}', 'CollegeController@createCourse')->name('college.course.create');

            Route::get('/course/delete/{id}', 'CollegeController@deleteCourse')->name('college.course.delete');

            Route::post('/course/edit/{id}', 'CollegeController@editCourse')->name('college.course.edit');
        });
        //industries
        Route::group(['prefix' => 'industries'], function () {

            Route::get('/occupation', 'IndustriesController@occupation')->name('industry.occupation');

            Route::get('/industry', 'IndustriesController@industry')->name('industry.industries');

            Route::get('/get/occupation', 'IndustriesController@getOccupation')->name('industry.occupation.get');

            Route::get('/get/industry', 'IndustriesController@getIndustry')->name('industry.industries.get');

            Route::post('/create/industry', 'IndustriesController@addIndustry')->name('industry.industries.create');

            Route::post('/create/occupation', 'IndustriesController@addOccupation')->name('industry.occupation.create');

            Route::get('/delete/industry/{id}', 'IndustriesController@deleteIndustry')->name('industry.industries.delete');

            Route::get('/delete/occupation/{id}', 'IndustriesController@deleteOccupation')->name('industry.occupation.delete');
        });
        //Mails
        Route::group(['prefix' => 'mails'], function () {

            Route::get('/', 'MailController@index')->name('mails.index');

            Route::get('/compose', 'MailController@compose')->name('mails.compose');

            Route::get('/receive', 'MailController@receiveEmail');
        });
        //SMS
        Route::group(['prefix' => 'sms'], function () {

            Route::get('/update/{uuid}', 'SMSController@sendUpdateNotification')->name('sms.update');

            Route::post('/custom/{uuid}', 'SMSController@sendCustomSMS')->name('sms.custom');
        });
        //Graduate List
        Route::group(['prefix' => 'graduates'], function () {

            Route::get('/', 'GraduatesController@index')->name('graduate');

            Route::post('/', 'GraduatesController@insert')->name('graduate.create');

            Route::get('/all', 'GraduatesController@all')->name('graduate.all');

            Route::get('/export', 'GraduatesController@export')->name('graduates.export');

            Route::post('/import', 'GraduatesController@import')->name('graduates.import');
        });
        //Testing
        Route::group(['prefix' => 'test'], function () {

            Route::get('/', 'TestingController@index')->name('test');

            Route::post('/add/colleges', 'TestingController@massAddColleges')->name('test.colleges');

            Route::post('/add/companies', 'TestingController@massAddCompanies')->name('test.companies');

            Route::post('/add/articles', 'TestingController@massAddArticles')->name('test.articles');

            Route::post('/add/partners', 'TestingController@massAddPartners')->name('test.partners');

            Route::post('/add/course', 'TestingController@massAddCourse')->name('test.course');

            Route::post('/add/job_title', 'TestingController@massAddJobTitle')->name('test.jobtitle');

            Route::get('/mail', 'TestingController@testMail')->name('test.mail');

            Route::get('/mail/preview', 'TestingController@previewMail')->name('test.mail.preview');

            Route::get('/alert', 'TestingController@testAlert')->name('test.alert');

            Route::get('/truncate/{table}', 'TestingController@truncate')->name('test.truncate');
        });
    });
});

Route::get('/', 'HomeController@index')->name('home');

Route::get('/about-us', 'HomeController@about')->name('about');

Route::get('/contact', 'HomeController@contact')->name('contact');

Route::get('/colleges', 'HomeController@colleges')->name('colleges');

Route::group(['prefix' => 'news'], function () {

    Route::get('/', 'HomeController@news')->name('news');

    Route::get('/{article_id}', 'HomeController@articleSingle')->name('news.single');
});

Route::post('/feedback', 'HomeController@feedback')->name('feedback');

Route::group(['prefix' => 'register'], function () {

    Route::get('/', 'HomeController@showRegistration')->name('register.show');

    Route::post('/', 'HomeController@register')->name('register.create');

    Route::get('/complete', 'HomeController@complete')->name('register.complete');

    Route::get('/payment/{uuid}', 'HomeController@showPayment')->name('register.payment');

    Route::post('/payment/{uuid}', 'HomeController@payment');

    Route::get('/payment/{uuid}/complete/{email}', 'HomeController@completePayment')->name('register.payment.complete');

    Route::get('/getCourse/{college?}', 'HomeController@getCourse')->name('register.get.course');
});

Route::group(['prefix' => 'members'], function () {

    Route::get('/', 'HomeController@members')->name('membership');

    Route::get('/benefits', 'HomeController@benefits')->name('benefits');

    Route::get('/partners', 'HomeController@partners')->name('partners');

    Route::group(['prefix' => 'update'], function () {

        Route::get('/', 'HomeController@showUpdate')->name('update');

        Route::post('/', 'HomeController@sendUpdateForm');
    });
});

Route::group(['prefix' => 'update'], function () {

    Route::get('/{uuid}', 'HomeController@updateForm')->name('update.show');

    Route::post('/{uuid}', 'HomeController@update');
});

Route::group(['prefix' => 'job-opportunity'], function () {

    Route::get('/', 'HomeController@job')->name('jobs');

    Route::get('/{company_name}', 'HomeController@jobSingle')->name('jobs.single');
});
