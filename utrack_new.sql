-- phpMyAdmin SQL Dump
-- version 4.4.15.9
-- https://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 12, 2020 at 03:07 AM
-- Server version: 5.6.37
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `utrack_new`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` bigint(20) unsigned NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `middle_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birth_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile_img` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `api_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `first_name`, `middle_name`, `last_name`, `birth_date`, `email`, `profile_img`, `email_verified_at`, `password`, `api_token`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Jeremiah', 'Bernales', 'Manlapaz', '10/03/1997', 'jeremiahmanlapaz@gmail.com', NULL, NULL, '$2y$10$FPdMOFBHP.hJhy44kbJSXOqXJQVgbmu/jFjmNJhk2LPpyvh7Eqx9.', NULL, 'D9fxdNCCZMDMjMEOspYhSBW7oAdSPKVdeQ3jYyT7fW6KYY1c2ZPDItrshPVn', '2019-08-07 02:52:08', '2019-08-07 02:52:08');

-- --------------------------------------------------------

--
-- Table structure for table `alumni`
--

CREATE TABLE IF NOT EXISTS `alumni` (
  `id` bigint(20) unsigned NOT NULL,
  `uuid` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `middle_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birth_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `civil_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `contact_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `alumni`
--

INSERT INTO `alumni` (`id`, `uuid`, `first_name`, `middle_name`, `last_name`, `email`, `birth_date`, `gender`, `address`, `status`, `civil_status`, `email_verified_at`, `contact_number`, `image`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '7e56571d-1e68-4723-a3bf-c1236c732438', 'Jenifer', 'Schuppe', 'Reynolds', 'Jenifer Reynolds@example.mail', '1579662612', 'Female', '6258 Della Squares Apt. 437\nAnsleytown, NJ 84603-1320', 'member', 'Married', NULL, '09276387225', NULL, NULL, '2019-10-03 11:54:11', '2019-10-03 11:54:11'),
(2, '8a72c3ce-2d5b-4785-9793-b23c77f2be3b', 'Duncan', 'Marks', 'Batz', 'Duncan Batz@example.mail', '1579808017', 'Male', '5532 Tyree Glens\nBoylechester, AL 81051', 'member', 'Separated', NULL, '09276387225', NULL, NULL, '2019-08-22 11:30:57', '2019-08-22 11:30:57'),
(3, '9b5f20b3-6268-4219-81dc-6c94df37521c', 'Cecile', 'Ruecker', 'Hermiston', 'Cecile Hermiston@example.mail', '1580511180', 'Female', '71311 Hodkiewicz Stream Apt. 479\nBlickbury, AZ 46728-4196', 'member', 'Single', NULL, '09276387225', NULL, NULL, '2019-06-08 13:35:04', '2019-06-08 13:35:04'),
(4, 'b6cfaa5d-be87-4e95-9b0e-f102ad48d171', 'Adelle', 'Dare', 'Blick', 'Adelle Blick@example.mail', '1580225794', 'Female', '420 Larkin Well\nSouth Fletcherport, WV 00547-8674', 'member', 'Married', NULL, '09276387225', NULL, NULL, '2019-07-07 20:53:40', '2019-07-07 20:53:40'),
(5, '3ffb0369-078b-4d8b-8d8a-3a0a922cdf67', 'Deon', 'Upton', 'Rau', 'Deon Rau@example.mail', '1579923083', 'Male', '756 Ivy Causeway\nNew Lilliefort, RI 36445', 'member', 'Separated', NULL, '09276387225', NULL, NULL, '2019-04-28 06:17:43', '2019-04-28 06:17:43'),
(6, '31accb2a-9587-45de-9fce-255b208800a7', 'Nettie', 'Schinner', 'Heidenreich', 'Nettie Heidenreich@example.mail', '1579923713', 'Female', '31064 Beatty Orchard\nSouth Shaunland, NM 88520-9001', 'member', 'Separated', NULL, '09276387225', NULL, NULL, '2019-06-27 19:56:50', '2019-06-27 19:56:50'),
(7, '12193e7e-7b0d-46f1-ac73-e1f94aca4f7e', 'Brenna', 'Hickle', 'Steuber', 'Brenna Steuber@example.mail', '1579777034', 'Female', '47055 Stark Branch Suite 438\nEast Madilyn, IL 79457', 'member', 'Single', NULL, '09276387225', NULL, NULL, '2019-03-19 01:39:02', '2019-03-19 01:39:02'),
(8, 'd09414c5-2f26-4439-b923-9a7b5e58f65e', 'Jo', 'Ruecker', 'Glover', 'Jo Glover@example.mail', '1580009807', 'Male', '53784 Kamille Centers Suite 617\nAdamsburgh, MN 55879', 'member', 'Married', NULL, '09276387225', NULL, NULL, '2019-11-10 01:24:00', '2019-11-10 01:24:00'),
(9, '90e459e3-3464-4f81-9767-0c1842eeaa12', 'Yvette', 'Wunsch', 'Schimmel', 'Yvette Schimmel@example.mail', '1580556053', 'Female', '650 Malinda Village Suite 544\nWendellshire, WV 93474-8893', 'member', 'Single', NULL, '09276387225', NULL, NULL, '2019-03-04 13:22:09', '2019-03-04 13:22:09'),
(10, '316eacd8-4ac8-4467-a617-425161d5fc56', 'Clarabelle', 'Mitchell', 'Champlin', 'Clarabelle Champlin@example.mail', '1579718335', 'Female', '508 Alden Shore Suite 882\nEast Christop, ND 04152', 'member', 'Widowed', NULL, '09276387225', NULL, NULL, '2019-07-24 04:27:59', '2019-07-24 04:27:59'),
(11, '846ba039-041e-4466-9e15-577eaa04d9a0', 'Cara', 'Rippin', 'Kemmer', 'Cara Kemmer@example.mail', '1579662396', 'Female', '313 Daugherty Ville Suite 196\nNew Abigale, WI 63179', 'member', 'Single', NULL, '09276387225', NULL, NULL, '2019-03-14 14:51:37', '2019-03-14 14:51:37'),
(12, '3e126d7b-7d44-44be-a99c-f911230e0d58', 'Cali', 'Hermann', 'Price', 'Cali Price@example.mail', '1580094955', 'Female', '58182 Jovan Road\nLake Chynaton, ID 58478', 'member', 'Single', NULL, '09276387225', NULL, NULL, '2019-03-31 07:21:55', '2019-03-31 07:21:55'),
(13, 'b81982d5-4dae-41ff-99c2-644b6dddabe2', 'Tremayne', 'Smith', 'Howe', 'Tremayne Howe@example.mail', '1579686848', 'Male', '27731 Lempi Points Suite 790\nNew Brenden, DC 88652', 'member', 'Widowed', NULL, '09276387225', NULL, NULL, '2019-08-01 14:03:55', '2019-08-01 14:03:55'),
(14, 'b4afe0ea-a67f-448a-ad6d-319740d6841b', 'Jairo', 'Lubowitz', 'McDermott', 'Jairo McDermott@example.mail', '1580511638', 'Male', '4492 Ozella Island\nNorth Calebport, GA 80678-0342', 'member', 'Married', NULL, '09276387225', NULL, NULL, '2019-11-07 18:39:28', '2019-11-07 18:39:28'),
(15, '924c7c3a-5300-4817-9fc5-83229c9a1047', 'Shawna', 'Fritsch', 'Thiel', 'Shawna Thiel@example.mail', '1580358316', 'Female', '5609 Princess Mill Suite 385\nEast Assuntamouth, MT 72392-6246', 'member', 'Single', NULL, '09276387225', NULL, NULL, '2019-11-18 07:05:32', '2019-11-18 07:05:32'),
(16, 'c0747ecc-b98a-4be2-ae9e-d59933983ee6', 'Danny', 'Muller', 'Mayer', 'Danny Mayer@example.mail', '1580451967', 'Male', '75216 McDermott Ports Suite 998\nRaheemside, WV 31222-6459', 'member', 'Single', NULL, '09276387225', NULL, NULL, '2019-10-27 03:08:25', '2019-10-27 03:08:25'),
(17, '5139ebd5-8c61-4417-955c-53aef9a62d9d', 'Anya', 'Frami', 'O''Reilly', 'Anya O''Reilly@example.mail', '1579924289', 'Female', '323 Von Parkway\nKoeppview, GA 74432-8864', 'member', 'Widowed', NULL, '09276387225', NULL, NULL, '2019-03-25 20:55:12', '2019-03-25 20:55:12'),
(18, '9f69512d-8e83-4db4-ab56-b65eb1d47b18', 'Dejon', 'Legros', 'Hagenes', 'Dejon Hagenes@example.mail', '1580095375', 'Male', '74328 Glover Neck\nPort Nannie, FL 68060-8447', 'member', 'Separated', NULL, '09276387225', NULL, NULL, '2019-03-20 09:23:52', '2019-03-20 09:23:52'),
(19, '8d15373f-4953-4deb-91c6-7138d7a81d40', 'Freida', 'Kris', 'Jacobson', 'Freida Jacobson@example.mail', '1579801967', 'Female', '8739 Halvorson Parks\nDurganchester, FL 02541-1369', 'member', 'Single', NULL, '09276387225', NULL, NULL, '2019-01-08 01:54:24', '2019-01-08 01:54:24'),
(20, '2bffad8a-c9e8-4e96-a3ac-b56933f2582c', 'Logan', 'King', 'Gutkowski', 'Logan Gutkowski@example.mail', '1580659126', 'Male', '468 Kyleigh Rest Suite 208\nLydafurt, CA 29531-8331', 'member', 'Married', NULL, '09276387225', NULL, NULL, '2019-12-21 12:40:09', '2019-12-21 12:40:09'),
(21, '7f856c00-7290-4e34-8af5-6e4262bba532', 'Myra', 'Cormier', 'Gislason', 'Myra Gislason@example.mail', '1579770931', 'Female', '5863 Lemke Crossroad Suite 256\nNew Therese, OH 06842', 'member', 'Married', NULL, '09276387225', NULL, NULL, '2019-12-26 18:25:43', '2019-12-26 18:25:43'),
(22, '2f26ee46-95af-4c6a-97a3-71000e121867', 'Bonnie', 'Hartmann', 'Kohler', 'Bonnie Kohler@example.mail', '1580360775', 'Female', '853 Carolyn Bypass\nMagnusfurt, IA 94421', 'member', 'Single', NULL, '09276387225', NULL, NULL, '2019-12-26 12:43:27', '2019-12-26 12:43:27'),
(23, '2fb7fa14-bde6-4031-b680-513212e7f479', 'Tessie', 'Ziemann', 'Barrows', 'Tessie Barrows@example.mail', '1579810376', 'Female', '591 Miller Cliff\nDashawntown, AR 25851', 'member', 'Separated', NULL, '09276387225', NULL, NULL, '2019-08-20 11:11:12', '2019-08-20 11:11:12'),
(24, '515988d6-178e-4f6a-9ab4-59251d6b095a', 'Ed', 'Watsica', 'Schneider', 'Ed Schneider@example.mail', '1580428115', 'Male', '77823 Reece Prairie\nLanetown, NY 68843', 'member', 'Married', NULL, '09276387225', NULL, NULL, '2019-04-30 13:28:51', '2019-04-30 13:28:51'),
(25, 'ec58c01e-7069-4348-baac-454862cf1873', 'Lupe', 'Howell', 'Shanahan', 'Lupe Shanahan@example.mail', '1580633686', 'Female', '7778 Kling Circles\nYundtton, VA 64982', 'member', 'Separated', NULL, '09276387225', NULL, NULL, '2019-08-18 15:57:49', '2019-08-18 15:57:49'),
(26, '3b6f4771-e84b-472e-b8d4-4fd82c0eb686', 'Kelsi', 'Hane', 'Hermiston', 'Kelsi Hermiston@example.mail', '1579810315', 'Female', '16401 Spinka Meadow Suite 104\nBechtelarberg, IA 22624-0949', 'member', 'Single', NULL, '09276387225', NULL, NULL, '2019-06-27 17:18:00', '2019-06-27 17:18:00'),
(27, '0bbdd886-f782-4053-b9bf-60d2acd711f7', 'Natalia', 'Simonis', 'Stracke', 'Natalia Stracke@example.mail', '1579697758', 'Female', '1497 Hegmann Skyway\nEast Van, FL 48690', 'member', 'Separated', NULL, '09276387225', NULL, NULL, '2019-02-19 18:31:51', '2019-02-19 18:31:51'),
(28, '228fb3ca-5449-40ba-8211-530b6e73f484', 'Nash', 'Osinski', 'Champlin', 'Nash Champlin@example.mail', '1580074177', 'Male', '687 Krystal Skyway Suite 803\nMullerview, IL 80321-8019', 'member', 'Widowed', NULL, '09276387225', NULL, NULL, '2019-12-05 04:42:01', '2019-12-05 04:42:01'),
(29, 'e9a48bdf-bdb3-4c2d-97c0-74abb64064b0', 'Jaiden', 'Beahan', 'Renner', 'Jaiden Renner@example.mail', '1580139834', 'Male', '554 Zulauf Ferry Suite 906\nDaphneyport, VA 94780-3286', 'member', 'Single', NULL, '09276387225', NULL, NULL, '2019-12-05 08:24:55', '2019-12-05 08:24:55'),
(30, 'e062b6c9-1763-428c-93ef-36c7f2a49309', 'Tristin', 'Mann', 'Funk', 'Tristin Funk@example.mail', '1580159085', 'Male', '354 Lakin Prairie\nLake Waltonshire, RI 63047-6323', 'member', 'Separated', NULL, '09276387225', NULL, NULL, '2019-07-12 20:28:37', '2019-07-12 20:28:37'),
(31, 'ea690755-380a-4fda-9511-aa1b1df4f8ac', 'Kristy', 'Schumm', 'Heaney', 'Kristy Heaney@example.mail', '1580554840', 'Female', '475 Mitchell Flat Apt. 822\nEast Alessandroland, VA 70607', 'member', 'Separated', NULL, '09276387225', NULL, NULL, '2019-10-30 00:10:47', '2019-10-30 00:10:47'),
(32, '9c0c2bf9-1ddf-43e0-b714-33ee2827c617', 'Flossie', 'Yost', 'Bode', 'Flossie Bode@example.mail', '1580556165', 'Female', '1198 Humberto Crest\nBoyleland, AK 97417-0830', 'member', 'Single', NULL, '09276387225', NULL, NULL, '2019-07-25 11:05:17', '2019-07-25 11:05:17'),
(33, '97a9e580-8763-47f0-93d2-f6d28b07a1e2', 'Devin', 'Dach', 'Haag', 'Devin Haag@example.mail', '1579580210', 'Male', '85375 Corkery Flat Suite 149\nLake Shemar, VT 02838-4731', 'member', 'Widowed', NULL, '09276387225', NULL, NULL, '2019-08-08 12:46:02', '2019-08-08 12:46:02'),
(34, 'f41b6b4d-5f3f-4ce1-8a01-a76f1d8f226e', 'Myrl', 'Kilback', 'Kub', 'Myrl Kub@example.mail', '1579602033', 'Male', '24313 Rodrick Light Apt. 561\nBillybury, CA 57645', 'member', 'Separated', NULL, '09276387225', NULL, NULL, '2019-06-26 05:15:38', '2019-06-26 05:15:38'),
(35, 'e9d6f559-4633-4863-a31a-d6f2b7b02d5a', 'Cristina', 'Greenfelder', 'Ruecker', 'Cristina Ruecker@example.mail', '1580063356', 'Male', '97840 Luettgen Lane Suite 072\nMullerside, ID 67333', 'member', 'Separated', NULL, '09276387225', NULL, NULL, '2019-12-25 09:48:08', '2019-12-25 09:48:08'),
(36, 'f352f64c-9aa5-4e30-a28b-40bdd96bea86', 'Seamus', 'Wehner', 'Abbott', 'Seamus Abbott@example.mail', '1580183265', 'Male', '7187 Verdie Stream Suite 879\nEast Adella, OR 93930', 'member', 'Single', NULL, '09276387225', NULL, NULL, '2019-06-06 22:01:26', '2019-06-06 22:01:26'),
(37, '71fd555e-b151-4573-bd0f-e8c6c4abf9c2', 'Dasia', 'Russel', 'Casper', 'Dasia Casper@example.mail', '1580540044', 'Female', '9082 Berge Turnpike\nDibbertchester, SC 05475', 'member', 'Widowed', NULL, '09276387225', NULL, NULL, '2019-09-22 10:32:23', '2019-09-22 10:32:23'),
(38, 'd8f26f81-dcbf-48b3-bee6-6a71d1126f25', 'Alisa', 'Collier', 'Miller', 'Alisa Miller@example.mail', '1580438575', 'Female', '15372 Hassan Ports\nPort Dorcashaven, CO 45764-9322', 'member', 'Single', NULL, '09276387225', NULL, NULL, '2019-01-31 10:53:40', '2019-01-31 10:53:40'),
(39, 'a383d7f0-0184-48f3-9d64-670c94a9a7e5', 'Ursula', 'Rippin', 'Ullrich', 'Ursula Ullrich@example.mail', '1580535394', 'Female', '777 Merl Extension Apt. 760\nWest Diego, WI 81872-2591', 'member', 'Married', NULL, '09276387225', NULL, NULL, '2019-07-30 11:01:40', '2019-07-30 11:01:40'),
(40, 'fac03282-06fb-4d24-8131-cea2d10b706e', 'Daryl', 'Powlowski', 'Langosh', 'Daryl Langosh@example.mail', '1580526922', 'Male', '60938 Bartholome Meadow Apt. 505\nLake Guidoport, VT 29879', 'member', 'Single', NULL, '09276387225', NULL, NULL, '2019-04-30 05:29:48', '2019-04-30 05:29:48'),
(41, 'd206e6cb-faf4-4d87-a8da-5af4cdb1b710', 'Jonathan', 'Marquardt', 'Mohr', 'Jonathan Mohr@example.mail', '1579691824', 'Male', '73302 Marquardt Square Apt. 766\nBoyleshire, IA 48249', 'member', 'Single', NULL, '09276387225', NULL, NULL, '2019-01-26 07:21:13', '2019-01-26 07:21:13'),
(42, '62acd4a1-073b-433e-85f1-929c755f69b0', 'Name', 'Nikolaus', 'Halvorson', 'Name Halvorson@example.mail', '1580275878', 'Female', '4833 Will Village\nWest Darrickberg, FL 79270', 'member', 'Single', NULL, '09276387225', NULL, NULL, '2019-06-07 23:14:40', '2019-06-07 23:14:40'),
(43, '775d44df-ce67-4056-94c0-f486ed255afb', 'Bernice', 'Kovacek', 'Zulauf', 'Bernice Zulauf@example.mail', '1580164130', 'Female', '88406 Wilkinson Terrace Apt. 268\nBlicktown, AK 41227', 'member', 'Separated', NULL, '09276387225', NULL, NULL, '2019-06-04 06:25:32', '2019-06-04 06:25:32'),
(44, '3b9050e5-940c-4cff-815f-7751dbb7f540', 'Irwin', 'Mayer', 'Labadie', 'Irwin Labadie@example.mail', '1579901803', 'Male', '67511 Carter Turnpike\nSouth Letha, AK 00828', 'member', 'Married', NULL, '09276387225', NULL, NULL, '2019-05-15 14:40:29', '2019-05-15 14:40:29'),
(45, 'fc56cc36-987d-41fb-8885-5e928399eb3f', 'Jayce', 'Torp', 'Nader', 'Jayce Nader@example.mail', '1579692401', 'Male', '1262 London Brooks Suite 379\nNew Justynmouth, WY 62869', 'member', 'Separated', NULL, '09276387225', NULL, NULL, '2019-09-04 01:14:39', '2019-09-04 01:14:39'),
(46, '9dd08520-4936-4904-a403-56cfdbde1c10', 'Dessie', 'Witting', 'Lebsack', 'Dessie Lebsack@example.mail', '1580655798', 'Female', '42304 Irma View\nSporerhaven, DE 81473', 'member', 'Single', NULL, '09276387225', NULL, NULL, '2019-05-03 21:01:51', '2019-05-03 21:01:51'),
(47, '83f07d7a-af86-4707-b3e0-3ec014b6d696', 'Faye', 'Macejkovic', 'Heidenreich', 'Faye Heidenreich@example.mail', '1579558746', 'Female', '4006 Schulist Plains\nPort Yasmeenland, KY 22099', 'member', 'Widowed', NULL, '09276387225', NULL, NULL, '2019-08-26 06:21:55', '2019-08-26 06:21:55'),
(48, '846fde34-d3df-4229-a5cd-f8e418ee9edd', 'Olin', 'Abernathy', 'Harris', 'Olin Harris@example.mail', '1579951031', 'Male', '56736 Easter View\nLake Tessiemouth, WV 07369', 'member', 'Married', NULL, '09276387225', NULL, NULL, '2019-12-11 21:51:26', '2019-12-11 21:51:26'),
(49, '6cad82cd-a6ba-45dc-8359-0d577c7e2336', 'Joey', 'Marvin', 'Lesch', 'Joey Lesch@example.mail', '1579624429', 'Male', '6929 Mosciski Passage\nJoannyview, MD 42894', 'member', 'Widowed', NULL, '09276387225', NULL, NULL, '2019-05-13 11:13:36', '2019-05-13 11:13:36'),
(50, '4c2e13c8-2869-4e45-9b60-a46585df7de9', 'Stefan', 'Ortiz', 'Schimmel', 'Stefan Schimmel@example.mail', '1580162030', 'Male', '938 Harmon Shore Apt. 643\nPort Maya, RI 53159', 'member', 'Separated', NULL, '09276387225', NULL, NULL, '2019-01-17 18:01:25', '2019-01-17 18:01:25'),
(51, '7112e28d-575d-4b61-bd7d-59f9117e122b', 'Meghan', 'Crooks', 'Cummings', 'Meghan Cummings@example.mail', '1580485019', 'Female', '84055 Dayna Throughway\nNorth Allystad, VT 39954-4438', 'member', 'Separated', NULL, '09276387225', NULL, NULL, '2019-05-01 04:11:19', '2019-05-01 04:11:19'),
(52, '507fdbd8-618b-4797-8a9d-42cee307dcfe', 'Katrina', 'Brown', 'Auer', 'Katrina Auer@example.mail', '1580673926', 'Female', '89843 Wiegand Place\nSouth Stella, MN 64225-4473', 'member', 'Married', NULL, '09276387225', NULL, NULL, '2019-05-05 07:37:11', '2019-05-05 07:37:11'),
(53, '822798d7-cee0-44f9-9144-f6e101938c0c', 'Stuart', 'Schultz', 'Schultz', 'Stuart Schultz@example.mail', '1579709285', 'Male', '17162 Wilton Drive\nNorth Valentinport, FL 87972', 'member', 'Married', NULL, '09276387225', NULL, NULL, '2019-05-22 19:42:55', '2019-05-22 19:42:55'),
(54, '2a1ff151-20b4-42a2-8643-ba3263920836', 'Adella', 'Cummings', 'Deckow', 'Adella Deckow@example.mail', '1579734118', 'Female', '4446 Alexzander Fall Apt. 939\nLloydfort, CO 58546-4829', 'member', 'Married', NULL, '09276387225', NULL, NULL, '2019-10-18 21:18:45', '2019-10-18 21:18:45'),
(55, '71517d7c-0e0a-4c66-a668-5f91e5eed799', 'Adonis', 'Barton', 'Veum', 'Adonis Veum@example.mail', '1579580332', 'Male', '58829 Nader Shores\nFrederickbury, NY 69553-4969', 'member', 'Married', NULL, '09276387225', NULL, NULL, '2019-05-26 09:06:05', '2019-05-26 09:06:05'),
(56, '5f44e824-bd22-4753-8fb3-01faf6eb5ced', 'Jazmyn', 'Oberbrunner', 'Schoen', 'Jazmyn Schoen@example.mail', '1580301389', 'Female', '6703 Ashtyn Streets\nBerneiceside, PA 04975', 'member', 'Married', NULL, '09276387225', NULL, NULL, '2019-06-12 09:37:20', '2019-06-12 09:37:20'),
(57, '63c47d39-b7b7-42a0-b066-a984315c1da4', 'Camilla', 'Schumm', 'Jast', 'Camilla Jast@example.mail', '1580082654', 'Female', '700 Smith Inlet Apt. 497\nDietrichville, MS 17889', 'member', 'Widowed', NULL, '09276387225', NULL, NULL, '2019-01-20 16:06:01', '2019-01-20 16:06:01'),
(58, '07439ee8-2d5f-46bb-a675-28fdfcbaedc4', 'Rex', 'Gutmann', 'Adams', 'Rex Adams@example.mail', '1580197512', 'Male', '521 Freida Meadow Suite 820\nSchuppeside, MO 27592-2409', 'member', 'Married', NULL, '09276387225', NULL, NULL, '2019-07-27 09:41:34', '2019-07-27 09:41:34'),
(59, 'b9b8ed58-5e9f-44b4-89d4-9e3d021a42a5', 'Wyman', 'Thompson', 'Cummings', 'Wyman Cummings@example.mail', '1580380707', 'Male', '1106 Elyssa Loaf\nNorth Yeseniaburgh, AZ 06604', 'member', 'Widowed', NULL, '09276387225', NULL, NULL, '2019-09-19 20:33:31', '2019-09-19 20:33:31'),
(60, 'd0e52cfa-5804-4a43-88e6-b24337953d0b', 'Logan', 'Kohler', 'Kris', 'Logan Kris@example.mail', '1579580808', 'Male', '5210 Nikolas Forest\nWest Eunaview, VA 42825', 'member', 'Separated', NULL, '09276387225', NULL, NULL, '2019-02-06 02:46:24', '2019-02-06 02:46:24'),
(61, 'a550923d-1708-401e-b8ed-c8300a1dc10d', 'Fabiola', 'Sawayn', 'Klein', 'Fabiola Klein@example.mail', '1579667558', 'Female', '67163 Dietrich Underpass\nPercyland, WA 20197', 'member', 'Widowed', NULL, '09276387225', NULL, NULL, '2019-03-08 17:05:04', '2019-03-08 17:05:04'),
(62, '62ed9569-3046-4aee-b6e9-20d8a77702e6', 'Laurence', 'Barton', 'Kirlin', 'Laurence Kirlin@example.mail', '1579639089', 'Female', '365 Hoppe Glen Suite 230\nNorth Melody, NH 85654-8839', 'member', 'Single', NULL, '09276387225', NULL, NULL, '2019-07-22 12:02:52', '2019-07-22 12:02:52'),
(63, '34ab9432-8f9c-40a1-af39-b59a7281dacf', 'Destiny', 'Lebsack', 'Sawayn', 'Destiny Sawayn@example.mail', '1580114226', 'Female', '2497 Murray Summit Apt. 841\nSouth Zechariahmouth, SD 79252', 'member', 'Single', NULL, '09276387225', NULL, NULL, '2019-06-11 10:33:27', '2019-06-11 10:33:27'),
(64, 'f593079b-afcb-4d0e-a053-a123b3617014', 'Pamela', 'Fahey', 'Hirthe', 'Pamela Hirthe@example.mail', '1580599860', 'Female', '259 Bradtke Highway Suite 290\nNorth Vincent, KS 65313', 'member', 'Widowed', NULL, '09276387225', NULL, NULL, '2019-03-28 23:15:40', '2019-03-28 23:15:40'),
(65, '52adc49f-f453-45e5-a31f-6a61ee0b0107', 'Amparo', 'Will', 'Schowalter', 'Amparo Schowalter@example.mail', '1580629880', 'Male', '92471 Bailey Light\nElbertborough, NH 43015', 'member', 'Separated', NULL, '09276387225', NULL, NULL, '2019-06-14 11:18:12', '2019-06-14 11:18:12'),
(66, '334dcfb0-7d5e-4394-958d-f94ce6dcbebc', 'Iva', 'Johns', 'Hermann', 'Iva Hermann@example.mail', '1580431777', 'Female', '31891 Jazmyn Summit Suite 913\nEast Lorainehaven, MN 23418', 'member', 'Married', NULL, '09276387225', NULL, NULL, '2019-10-14 13:52:01', '2019-10-14 13:52:01'),
(67, 'becb249e-ac81-4a4e-934c-0011bd9137be', 'Marina', 'Kling', 'Marquardt', 'Marina Marquardt@example.mail', '1580497555', 'Female', '980 Lindgren Square\nNorth Johnathan, MD 24582-9604', 'member', 'Single', NULL, '09276387225', NULL, NULL, '2019-06-15 14:37:14', '2019-06-15 14:37:14'),
(68, 'd22c1f3f-cdb4-45c3-b1fd-711f18f7b96d', 'Stephanie', 'Kirlin', 'Flatley', 'Stephanie Flatley@example.mail', '1579745519', 'Female', '40898 Salvador Ranch\nSouth Wymanfurt, AL 26370', 'member', 'Single', NULL, '09276387225', NULL, NULL, '2019-06-23 16:08:54', '2019-06-23 16:08:54'),
(69, 'c63fb14d-8696-4067-a23a-79e2b9ff5b54', 'Neal', 'Boehm', 'Jakubowski', 'Neal Jakubowski@example.mail', '1580271741', 'Male', '42448 Watsica Centers\nEast Wilsontown, IA 24452', 'member', 'Separated', NULL, '09276387225', NULL, NULL, '2019-12-23 15:50:55', '2019-12-23 15:50:55'),
(70, 'e8cc9656-5943-45cd-bbab-7bf38644ac47', 'Jacklyn', 'Little', 'Skiles', 'Jacklyn Skiles@example.mail', '1580357865', 'Female', '820 Ziemann Crescent\nAlvertaland, IA 00265', 'member', 'Separated', NULL, '09276387225', NULL, NULL, '2019-08-05 08:39:05', '2019-08-05 08:39:05'),
(71, '4c13a383-1850-4f8e-a7e5-d36e8059ebe9', 'Reba', 'Crona', 'Bahringer', 'Reba Bahringer@example.mail', '1579912064', 'Female', '437 Oberbrunner Harbors Apt. 149\nChristiansenfort, IA 86590-4074', 'member', 'Married', NULL, '09276387225', NULL, NULL, '2019-08-25 06:16:52', '2019-08-25 06:16:52'),
(72, '95b885cb-b74c-4712-9ed6-bdd0b7d0e00c', 'Mathias', 'Parker', 'Nader', 'Mathias Nader@example.mail', '1580241616', 'Male', '9020 Kshlerin Club Apt. 031\nLake Alexandrinestad, SC 32337', 'member', 'Single', NULL, '09276387225', NULL, NULL, '2019-10-03 23:01:35', '2019-10-03 23:01:35'),
(73, '44ef75b7-7479-4a8e-8c31-7d7984eab6d8', 'Norbert', 'Bailey', 'Mueller', 'Norbert Mueller@example.mail', '1580433058', 'Male', '4482 Beier Plains\nPort Geovanniside, SD 88428', 'member', 'Single', NULL, '09276387225', NULL, NULL, '2019-03-23 00:12:28', '2019-03-23 00:12:28'),
(74, '5ba67b4c-92b0-44da-a606-e7e65c894b11', 'Mae', 'Armstrong', 'Conn', 'Mae Conn@example.mail', '1579854865', 'Female', '9231 Alva Road\nWest Erynport, NJ 52565', 'member', 'Single', NULL, '09276387225', NULL, NULL, '2019-08-02 09:32:43', '2019-08-02 09:32:43'),
(75, '1516869e-24d8-4329-89f9-815ed8a42e0f', 'Murray', 'Walter', 'Bergnaum', 'Murray Bergnaum@example.mail', '1580236409', 'Male', '18744 Liliane Cliff Suite 692\nAdolphuschester, NE 88980-0353', 'member', 'Widowed', NULL, '09276387225', NULL, NULL, '2019-12-13 21:42:31', '2019-12-13 21:42:31'),
(76, '5a9e5aa2-22fb-46b6-bad7-bc1a0e4eb3e0', 'Bernice', 'Orn', 'Will', 'Bernice Will@example.mail', '1579969842', 'Female', '8032 Gaylord Views Apt. 841\nGarettborough, HI 89851', 'member', 'Single', NULL, '09276387225', NULL, NULL, '2019-12-19 06:30:56', '2019-12-19 06:30:56'),
(77, 'b24dda16-0138-4e1e-b3fb-de0b63391981', 'Dale', 'McCullough', 'Beier', 'Dale Beier@example.mail', '1579810673', 'Male', '116 Blaze River Apt. 279\nNew Makennaville, IL 68651', 'member', 'Widowed', NULL, '09276387225', NULL, NULL, '2019-03-20 11:15:41', '2019-03-20 11:15:41'),
(78, '36b1efbf-2967-4e26-8786-c32bf176f8eb', 'Earline', 'Willms', 'Pacocha', 'Earline Pacocha@example.mail', '1579829021', 'Female', '76742 Schultz Plain\nLaviniamouth, NC 96043', 'member', 'Married', NULL, '09276387225', NULL, NULL, '2019-09-18 04:29:34', '2019-09-18 04:29:34'),
(79, '507b50a6-41e3-4e31-bdc5-6744f0febae8', 'Aglae', 'Maggio', 'Macejkovic', 'Aglae Macejkovic@example.mail', '1579833648', 'Female', '20855 Legros Estates Apt. 478\nVestaside, TN 43714-7037', 'member', 'Widowed', NULL, '09276387225', NULL, NULL, '2019-06-30 15:13:26', '2019-06-30 15:13:26'),
(80, '70e6e37c-a057-4a39-a5f9-07277d551fe2', 'Eugene', 'Hyatt', 'Kris', 'Eugene Kris@example.mail', '1579812736', 'Male', '508 Kaylin Avenue Suite 757\nPort Alejandrinville, NE 90748', 'member', 'Widowed', NULL, '09276387225', NULL, NULL, '2019-12-25 12:51:13', '2019-12-25 12:51:13'),
(81, 'f8a83dc1-5866-4b60-b2cd-77bd0cc94298', 'Dallas', 'Fritsch', 'Purdy', 'Dallas Purdy@example.mail', '1579557926', 'Male', '808 Olson Mission Apt. 824\nNew Janetbury, AR 02206', 'member', 'Widowed', NULL, '09276387225', NULL, NULL, '2019-12-29 04:31:18', '2019-12-29 04:31:18'),
(82, '7bdf0a6b-b731-485f-9dc4-d7f6c9f4e6d8', 'Ezequiel', 'Hodkiewicz', 'Dicki', 'Ezequiel Dicki@example.mail', '1580392726', 'Male', '493 Lilla Station\nPort Alexandrea, IL 72733-7598', 'member', 'Married', NULL, '09276387225', NULL, NULL, '2019-11-04 18:12:37', '2019-11-04 18:12:37'),
(83, '9be8d863-e6b9-4800-beae-68166baaf99c', 'Anibal', 'Effertz', 'Runolfsdottir', 'Anibal Runolfsdottir@example.mail', '1579694401', 'Male', '706 Lizzie Trafficway\nSouth Erickland, MI 51258', 'member', 'Widowed', NULL, '09276387225', NULL, NULL, '2019-11-08 02:38:06', '2019-11-08 02:38:06'),
(84, 'ca933acc-2e58-445b-8b7b-8324b74a4077', 'Javon', 'Rowe', 'Bogisich', 'Javon Bogisich@example.mail', '1580503840', 'Male', '81521 Paucek Ranch\nFunkton, MS 05467', 'member', 'Single', NULL, '09276387225', NULL, NULL, '2019-12-17 17:29:18', '2019-12-17 17:29:18'),
(85, '76318359-81fc-4b28-81be-a16e3ac00ed7', 'Christop', 'Klein', 'DuBuque', 'Christop DuBuque@example.mail', '1580308006', 'Male', '26599 Adeline Lights\nLake Guiseppe, NY 88782-7508', 'member', 'Separated', NULL, '09276387225', NULL, NULL, '2019-01-04 10:31:11', '2019-01-04 10:31:11'),
(86, '9f389bc3-eb4a-4008-bd90-16202e6ffe88', 'Sim', 'Weissnat', 'Mertz', 'Sim Mertz@example.mail', '1579959086', 'Male', '858 Jerrod Mall Apt. 687\nKohlerberg, MI 24131-9609', 'member', 'Widowed', NULL, '09276387225', NULL, NULL, '2019-12-17 12:34:14', '2019-12-17 12:34:14'),
(87, 'cddabc0e-3d35-431f-b281-2254f6d7d4bc', 'Jalen', 'Langosh', 'Cormier', 'Jalen Cormier@example.mail', '1579907709', 'Male', '2587 Jaqueline Canyon Suite 687\nLaurettaland, AZ 56247-9915', 'member', 'Married', NULL, '09276387225', NULL, NULL, '2019-08-12 12:02:08', '2019-08-12 12:02:08'),
(88, '7bce045d-1c17-448d-b2ea-b467f7081a9a', 'Edgar', 'Reynolds', 'Connelly', 'Edgar Connelly@example.mail', '1579619454', 'Male', '69448 Gutmann Path Suite 172\nElzaton, CO 55037-5952', 'member', 'Married', NULL, '09276387225', NULL, NULL, '2019-05-17 02:40:58', '2019-05-17 02:40:58'),
(89, 'e0b968d7-f221-440a-b1fc-34227e0bd250', 'Ryann', 'Mertz', 'Zboncak', 'Ryann Zboncak@example.mail', '1580658600', 'Male', '9709 Gwen Divide\nRogersshire, VT 18880', 'member', 'Separated', NULL, '09276387225', NULL, NULL, '2019-05-29 17:52:27', '2019-05-29 17:52:27'),
(90, 'e6229fe4-0924-4e93-b2f1-81b924a088a6', 'Abdullah', 'Luettgen', 'Rippin', 'Abdullah Rippin@example.mail', '1579810329', 'Male', '289 Schimmel Passage\nNorth Ransombury, NC 68058', 'member', 'Single', NULL, '09276387225', NULL, NULL, '2019-12-03 15:52:43', '2019-12-03 15:52:43'),
(91, '1f551f0e-4ce2-42d3-95ff-f61b67b2e664', 'Alfred', 'Murazik', 'Wiza', 'Alfred Wiza@example.mail', '1579860528', 'Male', '864 Unique Avenue Apt. 405\nNorth Christianaville, AZ 25455-7073', 'member', 'Single', NULL, '09276387225', NULL, NULL, '2019-02-14 06:45:53', '2019-02-14 06:45:53'),
(92, 'ade8f315-d50b-499f-aa42-f778b24d32fe', 'Finn', 'Jakubowski', 'Hane', 'Finn Hane@example.mail', '1580595604', 'Male', '660 Runolfsson Estate\nHarveymouth, OH 53611-4027', 'member', 'Single', NULL, '09276387225', NULL, NULL, '2019-07-12 00:32:40', '2019-07-12 00:32:40'),
(93, 'b4b6b7b0-c2a1-4eba-b219-10b3fedb7da4', 'Kendrick', 'Heathcote', 'Schmidt', 'Kendrick Schmidt@example.mail', '1579524557', 'Male', '8861 Toy Drive\nAshlystad, GA 15361', 'member', 'Widowed', NULL, '09276387225', NULL, NULL, '2019-07-26 19:36:12', '2019-07-26 19:36:12'),
(94, '3d748c0e-fe6e-476f-befb-3b20ff34983d', 'Roderick', 'Schultz', 'Flatley', 'Roderick Flatley@example.mail', '1580662532', 'Male', '2675 Runte Rue Apt. 189\nNorth Sybleburgh, AL 24605', 'member', 'Married', NULL, '09276387225', NULL, NULL, '2019-08-26 09:46:20', '2019-08-26 09:46:20'),
(95, '37cca6c7-e7ab-4a54-9272-a0460fb989a5', 'Veda', 'Nitzsche', 'Quigley', 'Veda Quigley@example.mail', '1580375533', 'Female', '9233 Loma Cove Apt. 603\nWest Lincolnmouth, ID 55245-5945', 'member', 'Separated', NULL, '09276387225', NULL, NULL, '2019-11-25 13:16:12', '2019-11-25 13:16:12'),
(96, '97646a0e-943a-4c70-b0b5-109bb0e2c01e', 'Lamont', 'Kerluke', 'Jakubowski', 'Lamont Jakubowski@example.mail', '1580670152', 'Male', '54831 Dooley Mill Suite 798\nAshtonview, WY 68094', 'member', 'Married', NULL, '09276387225', NULL, NULL, '2019-03-25 15:55:42', '2019-03-25 15:55:42'),
(97, 'fac2b109-b47a-4542-bcab-ceb1bdf909dd', 'Marion', 'Altenwerth', 'Bosco', 'Marion Bosco@example.mail', '1579747462', 'Female', '9283 Hamill Well Apt. 128\nHauckshire, RI 25495-2174', 'member', 'Married', NULL, '09276387225', NULL, NULL, '2019-06-17 00:56:23', '2019-06-17 00:56:23'),
(98, 'c9c973b6-3951-41c4-a08e-3cebe9bb9707', 'Stephania', 'Fahey', 'Erdman', 'Stephania Erdman@example.mail', '1580118330', 'Female', '1612 Klein Club\nNew Hilario, IL 86125', 'member', 'Separated', NULL, '09276387225', NULL, NULL, '2019-01-25 19:33:47', '2019-01-25 19:33:47'),
(99, 'eee4732e-2e37-4a49-b852-e70eb1d59b0a', 'Jaylen', 'Weimann', 'Huel', 'Jaylen Huel@example.mail', '1579536737', 'Male', '9704 Murphy Throughway Apt. 798\nTerryfurt, MD 02559', 'member', 'Separated', NULL, '09276387225', NULL, NULL, '2019-03-10 10:14:15', '2019-03-10 10:14:15'),
(100, '50c61260-3115-4fb8-b825-50de573b2ae2', 'Lea', 'Wyman', 'Swift', 'Lea Swift@example.mail', '1580505324', 'Female', '35559 Johnston Neck\nGradystad, CA 84285-1754', 'member', 'Separated', NULL, '09276387225', NULL, NULL, '2019-09-25 19:30:35', '2019-09-25 19:30:35');

-- --------------------------------------------------------

--
-- Table structure for table `alumni_educations`
--

CREATE TABLE IF NOT EXISTS `alumni_educations` (
  `id` bigint(20) unsigned NOT NULL,
  `uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `degree` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `college` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `course` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `year_graduated` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_first` tinyint(1) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `alumni_educations`
--

INSERT INTO `alumni_educations` (`id`, `uuid`, `degree`, `college`, `course`, `year_graduated`, `is_first`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '7e56571d-1e68-4723-a3bf-c1236c732438', 'Diploma', 'CCS', 'Diploma in Computer Network Administration', '2019', 1, NULL, '2019-10-03 11:54:11', '2019-10-03 11:54:11'),
(2, '8a72c3ce-2d5b-4785-9793-b23c77f2be3b', 'Bachelor', 'CCS', 'Diploma in Computer Network Administration', '2019', 1, NULL, '2019-08-22 11:30:57', '2019-08-22 11:30:57'),
(3, '9b5f20b3-6268-4219-81dc-6c94df37521c', 'Bachelor', 'CCS', 'Diploma in Application Development', '2019', 1, NULL, '2019-06-08 13:35:04', '2019-06-08 13:35:04'),
(4, 'b6cfaa5d-be87-4e95-9b0e-f102ad48d171', 'Diploma', 'CCS', 'Bachelor of Science in Computer Science Major in Application Development', '2019', 1, NULL, '2019-07-07 20:53:40', '2019-07-07 20:53:40'),
(5, '3ffb0369-078b-4d8b-8d8a-3a0a922cdf67', 'Bachelor', 'CCS', 'Diploma in Computer Network Administration', '2019', 1, NULL, '2019-04-28 06:17:43', '2019-04-28 06:17:43'),
(6, '31accb2a-9587-45de-9fce-255b208800a7', 'Diploma', 'CCS', 'Diploma in Application Development', '2019', 1, NULL, '2019-06-27 19:56:50', '2019-06-27 19:56:50'),
(7, '12193e7e-7b0d-46f1-ac73-e1f94aca4f7e', 'Diploma', 'CCS', 'Diploma in Computer Network Administration', '2019', 1, NULL, '2019-03-19 01:39:02', '2019-03-19 01:39:02'),
(8, 'd09414c5-2f26-4439-b923-9a7b5e58f65e', 'Bachelor', 'CCS', 'Diploma in Computer Network Administration', '2019', 1, NULL, '2019-11-10 01:24:00', '2019-11-10 01:24:00'),
(9, '90e459e3-3464-4f81-9767-0c1842eeaa12', 'Diploma', 'CCS', 'Bachelor of Science in Computer Science Major in Social Computing', '2019', 1, NULL, '2019-03-04 13:22:09', '2019-03-04 13:22:09'),
(10, '316eacd8-4ac8-4467-a617-425161d5fc56', 'Diploma', 'CCS', 'Diploma in Application Development', '2019', 1, NULL, '2019-07-24 04:27:59', '2019-07-24 04:27:59'),
(11, '846ba039-041e-4466-9e15-577eaa04d9a0', 'Diploma', 'CCS', 'Bachelor of Science in Computer Science Major in Application Development', '2019', 1, NULL, '2019-03-14 14:51:37', '2019-03-14 14:51:37'),
(12, '3e126d7b-7d44-44be-a99c-f911230e0d58', 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Application Development', '2019', 1, NULL, '2019-03-31 07:21:55', '2019-03-31 07:21:55'),
(13, 'b81982d5-4dae-41ff-99c2-644b6dddabe2', 'Bachelor', 'CCS', 'Bachelor of Science in Information Technology Major in Information and Network Security', '2019', 1, NULL, '2019-08-01 14:03:55', '2019-08-01 14:03:55'),
(14, 'b4afe0ea-a67f-448a-ad6d-319740d6841b', 'Bachelor', 'CCS', 'Bachelor of Science in Information Technology Major in Information and Network Security', '2019', 1, NULL, '2019-11-07 18:39:28', '2019-11-07 18:39:28'),
(15, '924c7c3a-5300-4817-9fc5-83229c9a1047', 'Bachelor', 'CCS', 'Diploma in Application Development', '2019', 1, NULL, '2019-11-18 07:05:32', '2019-11-18 07:05:32'),
(16, 'c0747ecc-b98a-4be2-ae9e-d59933983ee6', 'Bachelor', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', 1, NULL, '2019-10-27 03:08:25', '2019-10-27 03:08:25'),
(17, '5139ebd5-8c61-4417-955c-53aef9a62d9d', 'Diploma', 'CCS', 'Diploma in Computer Network Administration', '2019', 1, NULL, '2019-03-25 20:55:12', '2019-03-25 20:55:12'),
(18, '9f69512d-8e83-4db4-ab56-b65eb1d47b18', 'Diploma', 'CCS', 'Bachelor of Science in Computer Science Major in Social Computing', '2019', 1, NULL, '2019-03-20 09:23:52', '2019-03-20 09:23:52'),
(19, '8d15373f-4953-4deb-91c6-7138d7a81d40', 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Application Development', '2019', 1, NULL, '2019-01-08 01:54:24', '2019-01-08 01:54:24'),
(20, '2bffad8a-c9e8-4e96-a3ac-b56933f2582c', 'Diploma', 'CCS', 'Bachelor of Science in Computer Science Major in Application Development', '2019', 1, NULL, '2019-12-21 12:40:09', '2019-12-21 12:40:09'),
(21, '7f856c00-7290-4e34-8af5-6e4262bba532', 'Bachelor', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', 1, NULL, '2019-12-26 18:25:43', '2019-12-26 18:25:43'),
(22, '2f26ee46-95af-4c6a-97a3-71000e121867', 'Bachelor', 'CCS', 'Bachelor of Science in Information Technology Major in Information and Network Security', '2019', 1, NULL, '2019-12-26 12:43:27', '2019-12-26 12:43:27'),
(23, '2fb7fa14-bde6-4031-b680-513212e7f479', 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Social Computing', '2019', 1, NULL, '2019-08-20 11:11:12', '2019-08-20 11:11:12'),
(24, '515988d6-178e-4f6a-9ab4-59251d6b095a', 'Bachelor', 'CCS', 'Diploma in Application Development', '2019', 1, NULL, '2019-04-30 13:28:51', '2019-04-30 13:28:51'),
(25, 'ec58c01e-7069-4348-baac-454862cf1873', 'Bachelor', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', 1, NULL, '2019-08-18 15:57:49', '2019-08-18 15:57:49'),
(26, '3b6f4771-e84b-472e-b8d4-4fd82c0eb686', 'Bachelor', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', 1, NULL, '2019-06-27 17:18:00', '2019-06-27 17:18:00'),
(27, '0bbdd886-f782-4053-b9bf-60d2acd711f7', 'Diploma', 'CCS', 'Bachelor of Science in Computer Science Major in Application Development', '2019', 1, NULL, '2019-02-19 18:31:51', '2019-02-19 18:31:51'),
(28, '228fb3ca-5449-40ba-8211-530b6e73f484', 'Bachelor', 'CCS', 'Diploma in Computer Network Administration', '2019', 1, NULL, '2019-12-05 04:42:01', '2019-12-05 04:42:01'),
(29, 'e9a48bdf-bdb3-4c2d-97c0-74abb64064b0', 'Bachelor', 'CCS', 'Diploma in Computer Network Administration', '2019', 1, NULL, '2019-12-05 08:24:55', '2019-12-05 08:24:55'),
(30, 'e062b6c9-1763-428c-93ef-36c7f2a49309', 'Bachelor', 'CCS', 'Diploma in Computer Network Administration', '2019', 1, NULL, '2019-07-12 20:28:37', '2019-07-12 20:28:37'),
(31, 'ea690755-380a-4fda-9511-aa1b1df4f8ac', 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Social Computing', '2019', 1, NULL, '2019-10-30 00:10:47', '2019-10-30 00:10:47'),
(32, '9c0c2bf9-1ddf-43e0-b714-33ee2827c617', 'Bachelor', 'CCS', 'Bachelor of Science in Information Technology Major in Information and Network Security', '2019', 1, NULL, '2019-07-25 11:05:17', '2019-07-25 11:05:17'),
(33, '97a9e580-8763-47f0-93d2-f6d28b07a1e2', 'Bachelor', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', 1, NULL, '2019-08-08 12:46:02', '2019-08-08 12:46:02'),
(34, 'f41b6b4d-5f3f-4ce1-8a01-a76f1d8f226e', 'Diploma', 'CCS', 'Diploma in Application Development', '2019', 1, NULL, '2019-06-26 05:15:38', '2019-06-26 05:15:38'),
(35, 'e9d6f559-4633-4863-a31a-d6f2b7b02d5a', 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Social Computing', '2019', 1, NULL, '2019-12-25 09:48:08', '2019-12-25 09:48:08'),
(36, 'f352f64c-9aa5-4e30-a28b-40bdd96bea86', 'Bachelor', 'CCS', 'Diploma in Computer Network Administration', '2019', 1, NULL, '2019-06-06 22:01:26', '2019-06-06 22:01:26'),
(37, '71fd555e-b151-4573-bd0f-e8c6c4abf9c2', 'Diploma', 'CCS', 'Diploma in Application Development', '2019', 1, NULL, '2019-09-22 10:32:23', '2019-09-22 10:32:23'),
(38, 'd8f26f81-dcbf-48b3-bee6-6a71d1126f25', 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Social Computing', '2019', 1, NULL, '2019-01-31 10:53:40', '2019-01-31 10:53:40'),
(39, 'a383d7f0-0184-48f3-9d64-670c94a9a7e5', 'Diploma', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', 1, NULL, '2019-07-30 11:01:40', '2019-07-30 11:01:40'),
(40, 'fac03282-06fb-4d24-8131-cea2d10b706e', 'Bachelor', 'CCS', 'Diploma in Computer Network Administration', '2019', 1, NULL, '2019-04-30 05:29:48', '2019-04-30 05:29:48'),
(41, 'd206e6cb-faf4-4d87-a8da-5af4cdb1b710', 'Bachelor', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', 1, NULL, '2019-01-26 07:21:13', '2019-01-26 07:21:13'),
(42, '62acd4a1-073b-433e-85f1-929c755f69b0', 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Application Development', '2019', 1, NULL, '2019-06-07 23:14:40', '2019-06-07 23:14:40'),
(43, '775d44df-ce67-4056-94c0-f486ed255afb', 'Diploma', 'CCS', 'Diploma in Application Development', '2019', 1, NULL, '2019-06-04 06:25:32', '2019-06-04 06:25:32'),
(44, '3b9050e5-940c-4cff-815f-7751dbb7f540', 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Application Development', '2019', 1, NULL, '2019-05-15 14:40:29', '2019-05-15 14:40:29'),
(45, 'fc56cc36-987d-41fb-8885-5e928399eb3f', 'Bachelor', 'CCS', 'Diploma in Application Development', '2019', 1, NULL, '2019-09-04 01:14:39', '2019-09-04 01:14:39'),
(46, '9dd08520-4936-4904-a403-56cfdbde1c10', 'Diploma', 'CCS', 'Bachelor of Science in Computer Science Major in Application Development', '2019', 1, NULL, '2019-05-03 21:01:51', '2019-05-03 21:01:51'),
(47, '83f07d7a-af86-4707-b3e0-3ec014b6d696', 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Application Development', '2019', 1, NULL, '2019-08-26 06:21:55', '2019-08-26 06:21:55'),
(48, '846fde34-d3df-4229-a5cd-f8e418ee9edd', 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Social Computing', '2019', 1, NULL, '2019-12-11 21:51:26', '2019-12-11 21:51:26'),
(49, '6cad82cd-a6ba-45dc-8359-0d577c7e2336', 'Bachelor', 'CCS', 'Diploma in Computer Network Administration', '2019', 1, NULL, '2019-05-13 11:13:36', '2019-05-13 11:13:36'),
(50, '4c2e13c8-2869-4e45-9b60-a46585df7de9', 'Bachelor', 'CCS', 'Diploma in Application Development', '2019', 1, NULL, '2019-01-17 18:01:25', '2019-01-17 18:01:25'),
(51, '7112e28d-575d-4b61-bd7d-59f9117e122b', 'Bachelor', 'CCS', 'Diploma in Computer Network Administration', '2019', 1, NULL, '2019-05-01 04:11:19', '2019-05-01 04:11:19'),
(52, '507fdbd8-618b-4797-8a9d-42cee307dcfe', 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Application Development', '2019', 1, NULL, '2019-05-05 07:37:11', '2019-05-05 07:37:11'),
(53, '822798d7-cee0-44f9-9144-f6e101938c0c', 'Bachelor', 'CCS', 'Diploma in Application Development', '2019', 1, NULL, '2019-05-22 19:42:55', '2019-05-22 19:42:55'),
(54, '2a1ff151-20b4-42a2-8643-ba3263920836', 'Diploma', 'CCS', 'Bachelor of Science in Information Technology Major in Information and Network Security', '2019', 1, NULL, '2019-10-18 21:18:45', '2019-10-18 21:18:45'),
(55, '71517d7c-0e0a-4c66-a668-5f91e5eed799', 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Social Computing', '2019', 1, NULL, '2019-05-26 09:06:05', '2019-05-26 09:06:05'),
(56, '5f44e824-bd22-4753-8fb3-01faf6eb5ced', 'Bachelor', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', 1, NULL, '2019-06-12 09:37:20', '2019-06-12 09:37:20'),
(57, '63c47d39-b7b7-42a0-b066-a984315c1da4', 'Bachelor', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', 1, NULL, '2019-01-20 16:06:01', '2019-01-20 16:06:01'),
(58, '07439ee8-2d5f-46bb-a675-28fdfcbaedc4', 'Diploma', 'CCS', 'Bachelor of Science in Computer Science Major in Application Development', '2019', 1, NULL, '2019-07-27 09:41:34', '2019-07-27 09:41:34'),
(59, 'b9b8ed58-5e9f-44b4-89d4-9e3d021a42a5', 'Bachelor', 'CCS', 'Bachelor of Science in Information Technology Major in Information and Network Security', '2019', 1, NULL, '2019-09-19 20:33:31', '2019-09-19 20:33:31'),
(60, 'd0e52cfa-5804-4a43-88e6-b24337953d0b', 'Diploma', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', 1, NULL, '2019-02-06 02:46:24', '2019-02-06 02:46:24'),
(61, 'a550923d-1708-401e-b8ed-c8300a1dc10d', 'Bachelor', 'CCS', 'Bachelor of Science in Information Technology Major in Information and Network Security', '2019', 1, NULL, '2019-03-08 17:05:04', '2019-03-08 17:05:04'),
(62, '62ed9569-3046-4aee-b6e9-20d8a77702e6', 'Bachelor', 'CCS', 'Bachelor of Science in Information Technology Major in Information and Network Security', '2019', 1, NULL, '2019-07-22 12:02:52', '2019-07-22 12:02:52'),
(63, '34ab9432-8f9c-40a1-af39-b59a7281dacf', 'Bachelor', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', 1, NULL, '2019-06-11 10:33:27', '2019-06-11 10:33:27'),
(64, 'f593079b-afcb-4d0e-a053-a123b3617014', 'Diploma', 'CCS', 'Bachelor of Science in Computer Science Major in Application Development', '2019', 1, NULL, '2019-03-28 23:15:40', '2019-03-28 23:15:40'),
(65, '52adc49f-f453-45e5-a31f-6a61ee0b0107', 'Diploma', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', 1, NULL, '2019-06-14 11:18:12', '2019-06-14 11:18:12'),
(66, '334dcfb0-7d5e-4394-958d-f94ce6dcbebc', 'Bachelor', 'CCS', 'Diploma in Application Development', '2019', 1, NULL, '2019-10-14 13:52:01', '2019-10-14 13:52:01'),
(67, 'becb249e-ac81-4a4e-934c-0011bd9137be', 'Bachelor', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', 1, NULL, '2019-06-15 14:37:14', '2019-06-15 14:37:14'),
(68, 'd22c1f3f-cdb4-45c3-b1fd-711f18f7b96d', 'Diploma', 'CCS', 'Diploma in Application Development', '2019', 1, NULL, '2019-06-23 16:08:54', '2019-06-23 16:08:54'),
(69, 'c63fb14d-8696-4067-a23a-79e2b9ff5b54', 'Diploma', 'CCS', 'Diploma in Application Development', '2019', 1, NULL, '2019-12-23 15:50:55', '2019-12-23 15:50:55'),
(70, 'e8cc9656-5943-45cd-bbab-7bf38644ac47', 'Bachelor', 'CCS', 'Bachelor of Science in Information Technology Major in Information and Network Security', '2019', 1, NULL, '2019-08-05 08:39:05', '2019-08-05 08:39:05'),
(71, '4c13a383-1850-4f8e-a7e5-d36e8059ebe9', 'Bachelor', 'CCS', 'Diploma in Application Development', '2019', 1, NULL, '2019-08-25 06:16:52', '2019-08-25 06:16:52'),
(72, '95b885cb-b74c-4712-9ed6-bdd0b7d0e00c', 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Application Development', '2019', 1, NULL, '2019-10-03 23:01:35', '2019-10-03 23:01:35'),
(73, '44ef75b7-7479-4a8e-8c31-7d7984eab6d8', 'Bachelor', 'CCS', 'Diploma in Application Development', '2019', 1, NULL, '2019-03-23 00:12:28', '2019-03-23 00:12:28'),
(74, '5ba67b4c-92b0-44da-a606-e7e65c894b11', 'Diploma', 'CCS', 'Bachelor of Science in Information Technology Major in Information and Network Security', '2019', 1, NULL, '2019-08-02 09:32:43', '2019-08-02 09:32:43'),
(75, '1516869e-24d8-4329-89f9-815ed8a42e0f', 'Bachelor', 'CCS', 'Diploma in Computer Network Administration', '2019', 1, NULL, '2019-12-13 21:42:31', '2019-12-13 21:42:31'),
(76, '5a9e5aa2-22fb-46b6-bad7-bc1a0e4eb3e0', 'Bachelor', 'CCS', 'Diploma in Application Development', '2019', 1, NULL, '2019-12-19 06:30:56', '2019-12-19 06:30:56'),
(77, 'b24dda16-0138-4e1e-b3fb-de0b63391981', 'Bachelor', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', 1, NULL, '2019-03-20 11:15:41', '2019-03-20 11:15:41'),
(78, '36b1efbf-2967-4e26-8786-c32bf176f8eb', 'Diploma', 'CCS', 'Diploma in Application Development', '2019', 1, NULL, '2019-09-18 04:29:34', '2019-09-18 04:29:34'),
(79, '507b50a6-41e3-4e31-bdc5-6744f0febae8', 'Bachelor', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', 1, NULL, '2019-06-30 15:13:26', '2019-06-30 15:13:26'),
(80, '70e6e37c-a057-4a39-a5f9-07277d551fe2', 'Bachelor', 'CCS', 'Bachelor of Science in Information Technology Major in Information and Network Security', '2019', 1, NULL, '2019-12-25 12:51:13', '2019-12-25 12:51:13'),
(81, 'f8a83dc1-5866-4b60-b2cd-77bd0cc94298', 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Social Computing', '2019', 1, NULL, '2019-12-29 04:31:18', '2019-12-29 04:31:18'),
(82, '7bdf0a6b-b731-485f-9dc4-d7f6c9f4e6d8', 'Diploma', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', 1, NULL, '2019-11-04 18:12:37', '2019-11-04 18:12:37'),
(83, '9be8d863-e6b9-4800-beae-68166baaf99c', 'Bachelor', 'CCS', 'Bachelor of Science in Information Technology Major in Information and Network Security', '2019', 1, NULL, '2019-11-08 02:38:06', '2019-11-08 02:38:06'),
(84, 'ca933acc-2e58-445b-8b7b-8324b74a4077', 'Bachelor', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', 1, NULL, '2019-12-17 17:29:18', '2019-12-17 17:29:18'),
(85, '76318359-81fc-4b28-81be-a16e3ac00ed7', 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Application Development', '2019', 1, NULL, '2019-01-04 10:31:11', '2019-01-04 10:31:11'),
(86, '9f389bc3-eb4a-4008-bd90-16202e6ffe88', 'Bachelor', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', 1, NULL, '2019-12-17 12:34:14', '2019-12-17 12:34:14'),
(87, 'cddabc0e-3d35-431f-b281-2254f6d7d4bc', 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Social Computing', '2019', 1, NULL, '2019-08-12 12:02:08', '2019-08-12 12:02:08'),
(88, '7bce045d-1c17-448d-b2ea-b467f7081a9a', 'Bachelor', 'CCS', 'Diploma in Computer Network Administration', '2019', 1, NULL, '2019-05-17 02:40:58', '2019-05-17 02:40:58'),
(89, 'e0b968d7-f221-440a-b1fc-34227e0bd250', 'Bachelor', 'CCS', 'Diploma in Computer Network Administration', '2019', 1, NULL, '2019-05-29 17:52:27', '2019-05-29 17:52:27'),
(90, 'e6229fe4-0924-4e93-b2f1-81b924a088a6', 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Application Development', '2019', 1, NULL, '2019-12-03 15:52:43', '2019-12-03 15:52:43'),
(91, '1f551f0e-4ce2-42d3-95ff-f61b67b2e664', 'Diploma', 'CCS', 'Bachelor of Science in Information Technology Major in Information and Network Security', '2019', 1, NULL, '2019-02-14 06:45:53', '2019-02-14 06:45:53'),
(92, 'ade8f315-d50b-499f-aa42-f778b24d32fe', 'Bachelor', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', 1, NULL, '2019-07-12 00:32:40', '2019-07-12 00:32:40'),
(93, 'b4b6b7b0-c2a1-4eba-b219-10b3fedb7da4', 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Application Development', '2019', 1, NULL, '2019-07-26 19:36:12', '2019-07-26 19:36:12'),
(94, '3d748c0e-fe6e-476f-befb-3b20ff34983d', 'Diploma', 'CCS', 'Bachelor of Science in Computer Science Major in Social Computing', '2019', 1, NULL, '2019-08-26 09:46:20', '2019-08-26 09:46:20'),
(95, '37cca6c7-e7ab-4a54-9272-a0460fb989a5', 'Bachelor', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', 1, NULL, '2019-11-25 13:16:12', '2019-11-25 13:16:12'),
(96, '97646a0e-943a-4c70-b0b5-109bb0e2c01e', 'Bachelor', 'CCS', 'Diploma in Computer Network Administration', '2019', 1, NULL, '2019-03-25 15:55:42', '2019-03-25 15:55:42'),
(97, 'fac2b109-b47a-4542-bcab-ceb1bdf909dd', 'Diploma', 'CCS', 'Bachelor of Science in Information Technology Major in Information and Network Security', '2019', 1, NULL, '2019-06-17 00:56:23', '2019-06-17 00:56:23'),
(98, 'c9c973b6-3951-41c4-a08e-3cebe9bb9707', 'Diploma', 'CCS', 'Diploma in Application Development', '2019', 1, NULL, '2019-01-25 19:33:47', '2019-01-25 19:33:47'),
(99, 'eee4732e-2e37-4a49-b852-e70eb1d59b0a', 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Social Computing', '2019', 1, NULL, '2019-03-10 10:14:15', '2019-03-10 10:14:15'),
(100, '50c61260-3115-4fb8-b825-50de573b2ae2', 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Social Computing', '2019', 1, NULL, '2019-09-25 19:30:35', '2019-09-25 19:30:35');

-- --------------------------------------------------------

--
-- Table structure for table `alumni_employments`
--

CREATE TABLE IF NOT EXISTS `alumni_employments` (
  `id` bigint(20) unsigned NOT NULL,
  `uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `occupation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `industry` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `job_level` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `place_of_work` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_first` tinyint(1) DEFAULT NULL,
  `reason` text COLLATE utf8mb4_unicode_ci,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `alumni_employments`
--

INSERT INTO `alumni_employments` (`id`, `uuid`, `status`, `occupation`, `company_name`, `industry`, `job_level`, `place_of_work`, `is_first`, `reason`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '7e56571d-1e68-4723-a3bf-c1236c732438', 'self employed', 'Government Relations Manager', 'Romaguera, Wyman and Tillman', 'Other', 'Self-employed', 'Local', 1, NULL, NULL, '2019-10-03 11:54:11', '2019-10-03 11:54:11'),
(2, '8a72c3ce-2d5b-4785-9793-b23c77f2be3b', 'regular', 'Financial Analyst', 'Adams Group', 'Logistics', 'Rank or clerical', 'Local', 1, NULL, NULL, '2019-08-22 11:30:57', '2019-08-22 11:30:57'),
(3, '9b5f20b3-6268-4219-81dc-6c94df37521c', 'regular', 'Civil Enginner', 'Hill and Sons', 'Food Industry', 'Self-employed', 'Abroad', 1, NULL, NULL, '2019-06-08 13:35:04', '2019-06-08 13:35:04'),
(4, 'b6cfaa5d-be87-4e95-9b0e-f102ad48d171', 'contractual', 'Electrical Engineer', 'McKenzie-Stanton', 'Infomation Technology / Computer Science', 'Professional, Supervisory or Technical', 'Local', 1, NULL, NULL, '2019-07-07 20:53:40', '2019-07-07 20:53:40'),
(5, '3ffb0369-078b-4d8b-8d8a-3a0a922cdf67', 'self employed', 'Electrical Engineer', 'Hudson-Ledner', 'E-Commerce', 'Professional, Supervisory or Technical', 'Local', 1, NULL, NULL, '2019-04-28 06:17:43', '2019-04-28 06:17:43'),
(6, '31accb2a-9587-45de-9fce-255b208800a7', 'regular', 'Teacher', 'Nolan, Rice and Johnson', 'Trade / Investment', 'Self-employed', 'Local', 1, NULL, NULL, '2019-06-27 19:56:50', '2019-06-27 19:56:50'),
(7, '12193e7e-7b0d-46f1-ac73-e1f94aca4f7e', 'contractual', 'Truck Mechanic', 'Metz, Brakus and Streich', 'Advertising', 'Professional, Supervisory or Technical', 'Abroad', 1, NULL, NULL, '2019-03-19 01:39:02', '2019-03-19 01:39:02'),
(8, 'd09414c5-2f26-4439-b923-9a7b5e58f65e', 'self employed', 'Truck Mechanic', 'Schulist, Murphy and Breitenberg', 'Telecommunication', 'Professional, Supervisory or Technical', 'Local', 1, NULL, NULL, '2019-11-10 01:24:00', '2019-11-10 01:24:00'),
(9, '90e459e3-3464-4f81-9767-0c1842eeaa12', 'regular', 'Clinical Research Scientist', 'Durgan-Welch', 'Insurance', 'Managerial or Executive', 'Abroad', 1, NULL, NULL, '2019-03-04 13:22:09', '2019-03-04 13:22:09'),
(10, '316eacd8-4ac8-4467-a617-425161d5fc56', 'contractual', 'Electronics Engineer', 'Kuphal LLC', 'Research & Development', 'Professional, Supervisory or Technical', 'Abroad', 1, NULL, NULL, '2019-07-24 04:27:59', '2019-07-24 04:27:59'),
(11, '846ba039-041e-4466-9e15-577eaa04d9a0', 'contractual', 'Chemist', 'Gaylord Ltd', 'E-Commerce', 'Rank or clerical', 'Abroad', 1, NULL, NULL, '2019-03-14 14:51:37', '2019-03-14 14:51:37'),
(12, '3e126d7b-7d44-44be-a99c-f911230e0d58', 'temporary', 'Investor Relations Associate', 'Jaskolski Ltd', 'E-Commerce', 'Professional, Supervisory or Technical', 'Abroad', 1, NULL, NULL, '2019-03-31 07:21:55', '2019-03-31 07:21:55'),
(13, 'b81982d5-4dae-41ff-99c2-644b6dddabe2', 'temporary', 'Biomedical Enginner', 'Yundt, Morissette and Jacobs', 'Manufacturing', 'Managerial or Executive', 'Local', 1, NULL, NULL, '2019-08-01 14:03:55', '2019-08-01 14:03:55'),
(14, 'b4afe0ea-a67f-448a-ad6d-319740d6841b', 'temporary', 'Chemist', 'Parker Inc', 'Accounting', 'Rank or clerical', 'Local', 1, NULL, NULL, '2019-11-07 18:39:28', '2019-11-07 18:39:28'),
(15, '924c7c3a-5300-4817-9fc5-83229c9a1047', 'temporary', 'Actuary', 'Hirthe PLC', 'Management', 'Professional, Supervisory or Technical', 'Abroad', 1, NULL, NULL, '2019-11-18 07:05:32', '2019-11-18 07:05:32'),
(16, 'c0747ecc-b98a-4be2-ae9e-d59933983ee6', 'self employed', 'Medical Assistant', 'Reichert-Nienow', 'Retail', 'Managerial or Executive', 'Local', 1, NULL, NULL, '2019-10-27 03:08:25', '2019-10-27 03:08:25'),
(17, '5139ebd5-8c61-4417-955c-53aef9a62d9d', 'contractual', 'Diplomat', 'Prohaska-Barton', 'Research & Development', 'Self-employed', 'Abroad', 1, NULL, NULL, '2019-03-25 20:55:12', '2019-03-25 20:55:12'),
(18, '9f69512d-8e83-4db4-ab56-b65eb1d47b18', 'temporary', 'Chemist', 'Streich Ltd', 'Wholesale', 'Self-employed', 'Local', 1, NULL, NULL, '2019-03-20 09:23:52', '2019-03-20 09:23:52'),
(19, '8d15373f-4953-4deb-91c6-7138d7a81d40', 'self employed', 'Public Relations and Marketing Manager', 'Zulauf, Herzog and Emard', 'Trade / Investment', 'Managerial or Executive', 'Local', 1, NULL, NULL, '2019-01-08 01:54:24', '2019-01-08 01:54:24'),
(20, '2bffad8a-c9e8-4e96-a3ac-b56933f2582c', 'temporary', 'Travel Agent', 'Adams Ltd', 'Other', 'Professional, Supervisory or Technical', 'Abroad', 1, NULL, NULL, '2019-12-21 12:40:09', '2019-12-21 12:40:09'),
(21, '7f856c00-7290-4e34-8af5-6e4262bba532', 'contractual', 'Biophysics', 'Schmeler Inc', 'Accounting', 'Self-employed', 'Abroad', 1, NULL, NULL, '2019-12-26 18:25:43', '2019-12-26 18:25:43'),
(22, '2f26ee46-95af-4c6a-97a3-71000e121867', 'self employed', 'Medical Records and Health Information Technicians', 'Nikolaus, Brekke and Fritsch', 'Other', 'Rank or clerical', 'Local', 1, NULL, NULL, '2019-12-26 12:43:27', '2019-12-26 12:43:27'),
(23, '2fb7fa14-bde6-4031-b680-513212e7f479', 'self employed', 'Automotive Enginner', 'Morissette, Swaniawski and Schoen', 'Food Industry', 'Managerial or Executive', 'Local', 1, NULL, NULL, '2019-08-20 11:11:12', '2019-08-20 11:11:12'),
(24, '515988d6-178e-4f6a-9ab4-59251d6b095a', 'contractual', 'Chemist', 'Kutch, Bernier and Muller', 'Finance / Banking', 'Managerial or Executive', 'Abroad', 1, NULL, NULL, '2019-04-30 13:28:51', '2019-04-30 13:28:51'),
(25, 'ec58c01e-7069-4348-baac-454862cf1873', 'contractual', 'Intructors Position', 'Hauck Ltd', 'Wholesale', 'Managerial or Executive', 'Abroad', 1, NULL, NULL, '2019-08-18 15:57:49', '2019-08-18 15:57:49'),
(26, '3b6f4771-e84b-472e-b8d4-4fd82c0eb686', 'contractual', 'Biomedical Enginner', 'Schmidt-Robel', 'Research & Development', 'Managerial or Executive', 'Abroad', 1, NULL, NULL, '2019-06-27 17:18:00', '2019-06-27 17:18:00'),
(27, '0bbdd886-f782-4053-b9bf-60d2acd711f7', 'temporary', 'Travel Agent', 'Mosciski Ltd', 'Other', 'Rank or clerical', 'Abroad', 1, NULL, NULL, '2019-02-19 18:31:51', '2019-02-19 18:31:51'),
(28, '228fb3ca-5449-40ba-8211-530b6e73f484', 'regular', 'Electronics Engineer', 'Carter, Mayer and Ryan', 'Sales / Marketing', 'Rank or clerical', 'Local', 1, NULL, NULL, '2019-12-05 04:42:01', '2019-12-05 04:42:01'),
(29, 'e9a48bdf-bdb3-4c2d-97c0-74abb64064b0', 'temporary', 'Science Research Assistant', 'Osinski Inc', 'Infomation Technology / Computer Science', 'Professional, Supervisory or Technical', 'Local', 1, NULL, NULL, '2019-12-05 08:24:55', '2019-12-05 08:24:55'),
(30, 'e062b6c9-1763-428c-93ef-36c7f2a49309', 'self employed', 'Hotel Manager', 'Kassulke-Feeney', 'Management', 'Managerial or Executive', 'Local', 1, NULL, NULL, '2019-07-12 20:28:37', '2019-07-12 20:28:37'),
(31, 'ea690755-380a-4fda-9511-aa1b1df4f8ac', 'contractual', 'Travel Agent', 'Konopelski, Haley and Wintheiser', 'Finance / Banking', 'Self-employed', 'Abroad', 1, NULL, NULL, '2019-10-30 00:10:47', '2019-10-30 00:10:47'),
(32, '9c0c2bf9-1ddf-43e0-b714-33ee2827c617', 'contractual', 'Spa Manager', 'Gislason Inc', 'Manufacturing', 'Rank or clerical', 'Abroad', 1, NULL, NULL, '2019-07-25 11:05:17', '2019-07-25 11:05:17'),
(33, '97a9e580-8763-47f0-93d2-f6d28b07a1e2', 'contractual', 'Hotel Manager', 'Vandervort-Ebert', 'Research & Development', 'Professional, Supervisory or Technical', 'Local', 1, NULL, NULL, '2019-08-08 12:46:02', '2019-08-08 12:46:02'),
(34, 'f41b6b4d-5f3f-4ce1-8a01-a76f1d8f226e', 'regular', 'Public Relations and Marketing Manager', 'Johnston-Hoeger', 'Accounting', 'Professional, Supervisory or Technical', 'Abroad', 1, NULL, NULL, '2019-06-26 05:15:38', '2019-06-26 05:15:38'),
(35, 'e9d6f559-4633-4863-a31a-d6f2b7b02d5a', 'contractual', 'Librarian', 'Torphy-Terry', 'Other', 'Professional, Supervisory or Technical', 'Abroad', 1, NULL, NULL, '2019-12-25 09:48:08', '2019-12-25 09:48:08'),
(36, 'f352f64c-9aa5-4e30-a28b-40bdd96bea86', 'contractual', 'Investor Relations Associate', 'Feest, White and Greenholt', 'Wholesale', 'Rank or clerical', 'Abroad', 1, NULL, NULL, '2019-06-06 22:01:26', '2019-06-06 22:01:26'),
(37, '71fd555e-b151-4573-bd0f-e8c6c4abf9c2', 'regular', 'Scientist', 'Kessler-Kihn', 'Consultancy', 'Self-employed', 'Local', 1, NULL, NULL, '2019-09-22 10:32:23', '2019-09-22 10:32:23'),
(38, 'd8f26f81-dcbf-48b3-bee6-6a71d1126f25', 'regular', 'Public Relations and Marketing Manager', 'Hodkiewicz, Douglas and Robel', 'Other', 'Managerial or Executive', 'Local', 1, NULL, NULL, '2019-01-31 10:53:40', '2019-01-31 10:53:40'),
(39, 'a383d7f0-0184-48f3-9d64-670c94a9a7e5', 'self employed', 'Budget Analyst', 'Fahey Ltd', 'Sales / Marketing', 'Professional, Supervisory or Technical', 'Abroad', 1, NULL, NULL, '2019-07-30 11:01:40', '2019-07-30 11:01:40'),
(40, 'fac03282-06fb-4d24-8131-cea2d10b706e', 'temporary', 'Community Relations Manager', 'Denesik, Kshlerin and Hansen', 'Infomation Technology / Computer Science', 'Professional, Supervisory or Technical', 'Abroad', 1, NULL, NULL, '2019-04-30 05:29:48', '2019-04-30 05:29:48'),
(41, 'd206e6cb-faf4-4d87-a8da-5af4cdb1b710', 'temporary', 'Aerospace Enginner', 'O''Kon-Ruecker', 'Food Industry', 'Self-employed', 'Abroad', 1, NULL, NULL, '2019-01-26 07:21:13', '2019-01-26 07:21:13'),
(42, '62acd4a1-073b-433e-85f1-929c755f69b0', 'contractual', 'Aerospace Enginner', 'Lynch Inc', 'Research & Development', 'Managerial or Executive', 'Abroad', 1, NULL, NULL, '2019-06-07 23:14:40', '2019-06-07 23:14:40'),
(43, '775d44df-ce67-4056-94c0-f486ed255afb', 'regular', 'Intructors Position', 'Bashirian-Wiegand', 'Publishing', 'Professional, Supervisory or Technical', 'Local', 1, NULL, NULL, '2019-06-04 06:25:32', '2019-06-04 06:25:32'),
(44, '3b9050e5-940c-4cff-815f-7751dbb7f540', 'contractual', 'Automotive Mechanic', 'Terry, Roberts and Cartwright', 'Management', 'Self-employed', 'Abroad', 1, NULL, NULL, '2019-05-15 14:40:29', '2019-05-15 14:40:29'),
(45, 'fc56cc36-987d-41fb-8885-5e928399eb3f', 'regular', 'Automotive Mechanic', 'Fay, Kshlerin and Simonis', 'Retail', 'Rank or clerical', 'Abroad', 1, NULL, NULL, '2019-09-04 01:14:39', '2019-09-04 01:14:39'),
(46, '9dd08520-4936-4904-a403-56cfdbde1c10', 'self employed', 'Lawyer', 'Leuschke Inc', 'Management', 'Managerial or Executive', 'Abroad', 1, NULL, NULL, '2019-05-03 21:01:51', '2019-05-03 21:01:51'),
(47, '83f07d7a-af86-4707-b3e0-3ec014b6d696', 'self employed', 'Financial Planner', 'O''Connell Ltd', 'E-Commerce', 'Professional, Supervisory or Technical', 'Abroad', 1, NULL, NULL, '2019-08-26 06:21:55', '2019-08-26 06:21:55'),
(48, '846fde34-d3df-4229-a5cd-f8e418ee9edd', 'self employed', 'Biomedical Enginner', 'Krajcik Inc', 'Trade / Investment', 'Rank or clerical', 'Abroad', 1, NULL, NULL, '2019-12-11 21:51:26', '2019-12-11 21:51:26'),
(49, '6cad82cd-a6ba-45dc-8359-0d577c7e2336', 'self employed', 'Community Relations Manager', 'Wunsch LLC', 'Telecommunication', 'Professional, Supervisory or Technical', 'Abroad', 1, NULL, NULL, '2019-05-13 11:13:36', '2019-05-13 11:13:36'),
(50, '4c2e13c8-2869-4e45-9b60-a46585df7de9', 'regular', 'Biophysics', 'Koelpin-Bernier', 'Research & Development', 'Managerial or Executive', 'Abroad', 1, NULL, NULL, '2019-01-17 18:01:25', '2019-01-17 18:01:25'),
(51, '7112e28d-575d-4b61-bd7d-59f9117e122b', 'contractual', 'Software Developer', 'Wiza LLC', 'Trade / Investment', 'Self-employed', 'Local', 1, NULL, NULL, '2019-05-01 04:11:19', '2019-05-01 04:11:19'),
(52, '507fdbd8-618b-4797-8a9d-42cee307dcfe', 'contractual', 'Computer Network Architect', 'Koepp-Bernhard', 'E-Commerce', 'Professional, Supervisory or Technical', 'Abroad', 1, NULL, NULL, '2019-05-05 07:37:11', '2019-05-05 07:37:11'),
(53, '822798d7-cee0-44f9-9144-f6e101938c0c', 'regular', 'Database Administrator', 'Lesch-McCullough', 'Retail', 'Professional, Supervisory or Technical', 'Local', 1, NULL, NULL, '2019-05-22 19:42:55', '2019-05-22 19:42:55'),
(54, '2a1ff151-20b4-42a2-8643-ba3263920836', 'regular', 'Database Administrator', 'Monahan-Fisher', 'Advertising', 'Managerial or Executive', 'Abroad', 1, NULL, NULL, '2019-10-18 21:18:45', '2019-10-18 21:18:45'),
(55, '71517d7c-0e0a-4c66-a668-5f91e5eed799', 'contractual', 'Database Administrator', 'Koch, Koss and Block', 'Manufacturing', 'Self-employed', 'Abroad', 1, NULL, NULL, '2019-05-26 09:06:05', '2019-05-26 09:06:05'),
(56, '5f44e824-bd22-4753-8fb3-01faf6eb5ced', 'temporary', 'Database Administrator', 'Schimmel, Abshire and Haley', 'Wholesale', 'Rank or clerical', 'Local', 1, NULL, NULL, '2019-06-12 09:37:20', '2019-06-12 09:37:20'),
(57, '63c47d39-b7b7-42a0-b066-a984315c1da4', 'contractual', 'Software Developer', 'Miller-McClure', 'Logistics', 'Managerial or Executive', 'Local', 1, NULL, NULL, '2019-01-20 16:06:01', '2019-01-20 16:06:01'),
(58, '07439ee8-2d5f-46bb-a675-28fdfcbaedc4', 'contractual', 'Computer Network Architect', 'Gulgowski-McGlynn', 'Insurance', 'Rank or clerical', 'Abroad', 1, NULL, NULL, '2019-07-27 09:41:34', '2019-07-27 09:41:34'),
(59, 'b9b8ed58-5e9f-44b4-89d4-9e3d021a42a5', 'self employed', 'Computer System Analyst', 'Hudson, Roob and Thompson', 'Infomation Technology / Computer Science', 'Managerial or Executive', 'Local', 1, NULL, NULL, '2019-09-19 20:33:31', '2019-09-19 20:33:31'),
(60, 'd0e52cfa-5804-4a43-88e6-b24337953d0b', 'temporary', 'Database Administrator', 'Stokes-Harris', 'E-Commerce', 'Professional, Supervisory or Technical', 'Local', 1, NULL, NULL, '2019-02-06 02:46:24', '2019-02-06 02:46:24'),
(61, 'a550923d-1708-401e-b8ed-c8300a1dc10d', 'regular', 'Computer Hardware Engineer', 'Zulauf, VonRueden and Terry', 'Infomation Technology / Computer Science', 'Managerial or Executive', 'Abroad', 1, NULL, NULL, '2019-03-08 17:05:04', '2019-03-08 17:05:04'),
(62, '62ed9569-3046-4aee-b6e9-20d8a77702e6', 'regular', 'Software Developer', 'Funk PLC', 'Sales / Marketing', 'Professional, Supervisory or Technical', 'Local', 1, NULL, NULL, '2019-07-22 12:02:52', '2019-07-22 12:02:52'),
(63, '34ab9432-8f9c-40a1-af39-b59a7281dacf', 'regular', 'Database Administrator', 'Walter, Bartoletti and Mohr', 'Other', 'Self-employed', 'Abroad', 1, NULL, NULL, '2019-06-11 10:33:27', '2019-06-11 10:33:27'),
(64, 'f593079b-afcb-4d0e-a053-a123b3617014', 'temporary', 'Computer Hardware Engineer', 'Johnson, Waters and Hill', 'Research & Development', 'Rank or clerical', 'Abroad', 1, NULL, NULL, '2019-03-28 23:15:40', '2019-03-28 23:15:40'),
(65, '52adc49f-f453-45e5-a31f-6a61ee0b0107', 'regular', 'Database Administrator', 'Anderson Inc', 'Insurance', 'Professional, Supervisory or Technical', 'Local', 1, NULL, NULL, '2019-06-14 11:18:12', '2019-06-14 11:18:12'),
(66, '334dcfb0-7d5e-4394-958d-f94ce6dcbebc', 'contractual', 'Computer Network Architect', 'Champlin, Champlin and Mann', 'Trade / Investment', 'Managerial or Executive', 'Local', 1, NULL, NULL, '2019-10-14 13:52:01', '2019-10-14 13:52:01'),
(67, 'becb249e-ac81-4a4e-934c-0011bd9137be', 'self employed', 'Computer Hardware Engineer', 'Nikolaus-Steuber', 'Food Industry', 'Self-employed', 'Local', 1, NULL, NULL, '2019-06-15 14:37:14', '2019-06-15 14:37:14'),
(68, 'd22c1f3f-cdb4-45c3-b1fd-711f18f7b96d', 'contractual', 'Computer Network Architect', 'Christiansen LLC', 'Publishing', 'Professional, Supervisory or Technical', 'Abroad', 1, NULL, NULL, '2019-06-23 16:08:54', '2019-06-23 16:08:54'),
(69, 'c63fb14d-8696-4067-a23a-79e2b9ff5b54', 'regular', 'Computer Hardware Engineer', 'Predovic-Daugherty', 'Publishing', 'Self-employed', 'Local', 1, NULL, NULL, '2019-12-23 15:50:55', '2019-12-23 15:50:55'),
(70, 'e8cc9656-5943-45cd-bbab-7bf38644ac47', 'contractual', 'Computer Network Architect', 'Bruen-Ferry', 'Accounting', 'Rank or clerical', 'Local', 1, NULL, NULL, '2019-08-05 08:39:05', '2019-08-05 08:39:05'),
(71, '4c13a383-1850-4f8e-a7e5-d36e8059ebe9', 'contractual', 'Computer System Analyst', 'Deckow LLC', 'Advertising', 'Self-employed', 'Abroad', 1, NULL, NULL, '2019-08-25 06:16:52', '2019-08-25 06:16:52'),
(72, '95b885cb-b74c-4712-9ed6-bdd0b7d0e00c', 'self employed', 'Computer Hardware Engineer', 'Mosciski, Murphy and Hill', 'Finance / Banking', 'Rank or clerical', 'Local', 1, NULL, NULL, '2019-10-03 23:01:35', '2019-10-03 23:01:35'),
(73, '44ef75b7-7479-4a8e-8c31-7d7984eab6d8', 'temporary', 'Computer System Analyst', 'Beier-Huel', 'Advertising', 'Professional, Supervisory or Technical', 'Local', 1, NULL, NULL, '2019-03-23 00:12:28', '2019-03-23 00:12:28'),
(74, '5ba67b4c-92b0-44da-a606-e7e65c894b11', 'contractual', 'Software Developer', 'Kirlin-Gislason', 'Wholesale', 'Self-employed', 'Abroad', 1, NULL, NULL, '2019-08-02 09:32:43', '2019-08-02 09:32:43'),
(75, '1516869e-24d8-4329-89f9-815ed8a42e0f', 'contractual', 'Computer System Analyst', 'Brakus, Hackett and Ruecker', 'Trade / Investment', 'Managerial or Executive', 'Abroad', 1, NULL, NULL, '2019-12-13 21:42:31', '2019-12-13 21:42:31'),
(76, '5a9e5aa2-22fb-46b6-bad7-bc1a0e4eb3e0', 'self employed', 'Computer Hardware Engineer', 'Treutel Ltd', 'Food Industry', 'Rank or clerical', 'Local', 1, NULL, NULL, '2019-12-19 06:30:56', '2019-12-19 06:30:56'),
(77, 'b24dda16-0138-4e1e-b3fb-de0b63391981', 'temporary', 'Computer System Analyst', 'Blanda-Gibson', 'Telecommunication', 'Self-employed', 'Abroad', 1, NULL, NULL, '2019-03-20 11:15:41', '2019-03-20 11:15:41'),
(78, '36b1efbf-2967-4e26-8786-c32bf176f8eb', 'regular', 'Computer Hardware Engineer', 'Reichel-Daniel', 'Other', 'Self-employed', 'Local', 1, NULL, NULL, '2019-09-18 04:29:34', '2019-09-18 04:29:34'),
(79, '507b50a6-41e3-4e31-bdc5-6744f0febae8', 'regular', 'Database Administrator', 'Cremin Inc', 'Publishing', 'Professional, Supervisory or Technical', 'Local', 1, NULL, NULL, '2019-06-30 15:13:26', '2019-06-30 15:13:26'),
(80, '70e6e37c-a057-4a39-a5f9-07277d551fe2', 'contractual', 'Database Administrator', 'Jast and Sons', 'Logistics', 'Self-employed', 'Local', 1, NULL, NULL, '2019-12-25 12:51:13', '2019-12-25 12:51:13'),
(81, 'f8a83dc1-5866-4b60-b2cd-77bd0cc94298', 'contractual', 'Computer Hardware Engineer', 'Lockman-Swift', 'Trade / Investment', 'Rank or clerical', 'Local', 1, NULL, NULL, '2019-12-29 04:31:18', '2019-12-29 04:31:18'),
(82, '7bdf0a6b-b731-485f-9dc4-d7f6c9f4e6d8', 'temporary', 'Software Developer', 'Spencer Inc', 'Management', 'Self-employed', 'Local', 1, NULL, NULL, '2019-11-04 18:12:37', '2019-11-04 18:12:37'),
(83, '9be8d863-e6b9-4800-beae-68166baaf99c', 'regular', 'Computer Hardware Engineer', 'Heller-Erdman', 'Sales / Marketing', 'Self-employed', 'Abroad', 1, NULL, NULL, '2019-11-08 02:38:06', '2019-11-08 02:38:06'),
(84, 'ca933acc-2e58-445b-8b7b-8324b74a4077', 'contractual', 'Computer Hardware Engineer', 'Sauer and Sons', 'Sales / Marketing', 'Rank or clerical', 'Abroad', 1, NULL, NULL, '2019-12-17 17:29:18', '2019-12-17 17:29:18'),
(85, '76318359-81fc-4b28-81be-a16e3ac00ed7', 'temporary', 'Computer Hardware Engineer', 'Bergstrom Ltd', 'Sales / Marketing', 'Self-employed', 'Local', 1, NULL, NULL, '2019-01-04 10:31:11', '2019-01-04 10:31:11'),
(86, '9f389bc3-eb4a-4008-bd90-16202e6ffe88', 'self employed', 'Database Administrator', 'Medhurst, Hayes and Lockman', 'Trade / Investment', 'Self-employed', 'Local', 1, NULL, NULL, '2019-12-17 12:34:14', '2019-12-17 12:34:14'),
(87, 'cddabc0e-3d35-431f-b281-2254f6d7d4bc', 'temporary', 'Computer Hardware Engineer', 'Weber, Rosenbaum and Lindgren', 'E-Commerce', 'Managerial or Executive', 'Local', 1, NULL, NULL, '2019-08-12 12:02:08', '2019-08-12 12:02:08'),
(88, '7bce045d-1c17-448d-b2ea-b467f7081a9a', 'contractual', 'Computer Hardware Engineer', 'Ferry-Kutch', 'Wholesale', 'Managerial or Executive', 'Abroad', 1, NULL, NULL, '2019-05-17 02:40:58', '2019-05-17 02:40:58'),
(89, 'e0b968d7-f221-440a-b1fc-34227e0bd250', 'contractual', 'Database Administrator', 'Sawayn, Stiedemann and Renner', 'Trade / Investment', 'Rank or clerical', 'Local', 1, NULL, NULL, '2019-05-29 17:52:27', '2019-05-29 17:52:27'),
(90, 'e6229fe4-0924-4e93-b2f1-81b924a088a6', 'self employed', 'Computer Hardware Engineer', 'Reichert, Kreiger and Stracke', 'Telecommunication', 'Professional, Supervisory or Technical', 'Abroad', 1, NULL, NULL, '2019-12-03 15:52:43', '2019-12-03 15:52:43'),
(91, '1f551f0e-4ce2-42d3-95ff-f61b67b2e664', 'temporary', 'Computer System Analyst', 'Zieme, Mayert and Upton', 'Transportation', 'Professional, Supervisory or Technical', 'Abroad', 1, NULL, NULL, '2019-02-14 06:45:53', '2019-02-14 06:45:53'),
(92, 'ade8f315-d50b-499f-aa42-f778b24d32fe', 'temporary', 'Database Administrator', 'Lesch, White and Rodriguez', 'Other', 'Rank or clerical', 'Abroad', 1, NULL, NULL, '2019-07-12 00:32:40', '2019-07-12 00:32:40'),
(93, 'b4b6b7b0-c2a1-4eba-b219-10b3fedb7da4', 'contractual', 'Software Developer', 'Will PLC', 'Infomation Technology / Computer Science', 'Rank or clerical', 'Local', 1, NULL, NULL, '2019-07-26 19:36:12', '2019-07-26 19:36:12'),
(94, '3d748c0e-fe6e-476f-befb-3b20ff34983d', 'contractual', 'Computer Hardware Engineer', 'Runolfsdottir, Mitchell and Muller', 'Finance / Banking', 'Professional, Supervisory or Technical', 'Abroad', 1, NULL, NULL, '2019-08-26 09:46:20', '2019-08-26 09:46:20'),
(95, '37cca6c7-e7ab-4a54-9272-a0460fb989a5', 'regular', 'Software Developer', 'Hartmann-Larson', 'Accounting', 'Professional, Supervisory or Technical', 'Local', 1, NULL, NULL, '2019-11-25 13:16:12', '2019-11-25 13:16:12'),
(96, '97646a0e-943a-4c70-b0b5-109bb0e2c01e', 'self employed', 'Computer System Analyst', 'Brekke Group', 'Wholesale', 'Self-employed', 'Local', 1, NULL, NULL, '2019-03-25 15:55:42', '2019-03-25 15:55:42'),
(97, 'fac2b109-b47a-4542-bcab-ceb1bdf909dd', 'regular', 'Software Developer', 'Lebsack, Wolff and Dare', 'Wholesale', 'Professional, Supervisory or Technical', 'Abroad', 1, NULL, NULL, '2019-06-17 00:56:23', '2019-06-17 00:56:23'),
(98, 'c9c973b6-3951-41c4-a08e-3cebe9bb9707', 'self employed', 'Database Administrator', 'Waters Inc', 'Wholesale', 'Managerial or Executive', 'Abroad', 1, NULL, NULL, '2019-01-25 19:33:47', '2019-01-25 19:33:47'),
(99, 'eee4732e-2e37-4a49-b852-e70eb1d59b0a', 'regular', 'Computer Network Architect', 'Dooley-Watsica', 'Food Industry', 'Professional, Supervisory or Technical', 'Abroad', 1, NULL, NULL, '2019-03-10 10:14:15', '2019-03-10 10:14:15'),
(100, '50c61260-3115-4fb8-b825-50de573b2ae2', 'contractual', 'Computer Network Architect', 'Towne and Sons', 'Sales / Marketing', 'Self-employed', 'Abroad', 1, NULL, NULL, '2019-09-25 19:30:35', '2019-09-25 19:30:35');

-- --------------------------------------------------------

--
-- Table structure for table `applicants`
--

CREATE TABLE IF NOT EXISTS `applicants` (
  `id` bigint(20) unsigned NOT NULL,
  `uuid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `middle_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birth_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `civil_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `degree` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `college` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `course` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `year_graduated` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `occupation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `industry` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `job_level` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `place_of_work` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reason` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `application_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'applicant',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `applicants`
--

INSERT INTO `applicants` (`id`, `uuid`, `first_name`, `middle_name`, `last_name`, `email`, `birth_date`, `gender`, `address`, `civil_status`, `image`, `degree`, `college`, `course`, `year_graduated`, `contact_number`, `occupation`, `company_name`, `industry`, `job_level`, `place_of_work`, `reason`, `status`, `application_status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '7e56571d-1e68-4723-a3bf-c1236c732438', 'Jenifer', 'Schuppe', 'Reynolds', 'Jenifer Reynolds@example.mail', '1579662612', 'Female', '6258 Della Squares Apt. 437\nAnsleytown, NJ 84603-1320', 'Married', NULL, 'Diploma', 'CCS', 'Diploma in Computer Network Administration', '2019', '1-866-476-4078', 'Government Relations Manager', 'Romaguera, Wyman and Tillman', 'Other', 'Self-employed', 'Local', NULL, 'self employed', 'verified', NULL, '2019-10-03 11:54:11', '2019-10-03 11:54:11'),
(2, '8a72c3ce-2d5b-4785-9793-b23c77f2be3b', 'Duncan', 'Marks', 'Batz', 'Duncan Batz@example.mail', '1579808017', 'Male', '5532 Tyree Glens\nBoylechester, AL 81051', 'Separated', NULL, 'Bachelor', 'CCS', 'Diploma in Computer Network Administration', '2019', '1-866-798-1037', 'Financial Analyst', 'Adams Group', 'Logistics', 'Rank or clerical', 'Local', NULL, 'regular', 'verified', NULL, '2019-08-22 11:30:57', '2019-08-22 11:30:57'),
(3, '9b5f20b3-6268-4219-81dc-6c94df37521c', 'Cecile', 'Ruecker', 'Hermiston', 'Cecile Hermiston@example.mail', '1580511180', 'Female', '71311 Hodkiewicz Stream Apt. 479\nBlickbury, AZ 46728-4196', 'Single', NULL, 'Bachelor', 'CCS', 'Diploma in Application Development', '2019', '877-925-9624', 'Civil Enginner', 'Hill and Sons', 'Food Industry', 'Self-employed', 'Abroad', NULL, 'regular', 'verified', NULL, '2019-06-08 13:35:04', '2019-06-08 13:35:04'),
(4, 'b6cfaa5d-be87-4e95-9b0e-f102ad48d171', 'Adelle', 'Dare', 'Blick', 'Adelle Blick@example.mail', '1580225794', 'Female', '420 Larkin Well\nSouth Fletcherport, WV 00547-8674', 'Married', NULL, 'Diploma', 'CCS', 'Bachelor of Science in Computer Science Major in Application Development', '2019', '1-855-421-4719', 'Electrical Engineer', 'McKenzie-Stanton', 'Infomation Technology / Computer Science', 'Professional, Supervisory or Technical', 'Local', NULL, 'contractual', 'verified', NULL, '2019-07-07 20:53:40', '2019-07-07 20:53:40'),
(5, '3ffb0369-078b-4d8b-8d8a-3a0a922cdf67', 'Deon', 'Upton', 'Rau', 'Deon Rau@example.mail', '1579923083', 'Male', '756 Ivy Causeway\nNew Lilliefort, RI 36445', 'Separated', NULL, 'Bachelor', 'CCS', 'Diploma in Computer Network Administration', '2019', '1-877-648-3663', 'Electrical Engineer', 'Hudson-Ledner', 'E-Commerce', 'Professional, Supervisory or Technical', 'Local', NULL, 'self employed', 'verified', NULL, '2019-04-28 06:17:43', '2019-04-28 06:17:43'),
(6, '31accb2a-9587-45de-9fce-255b208800a7', 'Nettie', 'Schinner', 'Heidenreich', 'Nettie Heidenreich@example.mail', '1579923713', 'Female', '31064 Beatty Orchard\nSouth Shaunland, NM 88520-9001', 'Separated', NULL, 'Diploma', 'CCS', 'Diploma in Application Development', '2019', '866.342.3513', 'Teacher', 'Nolan, Rice and Johnson', 'Trade / Investment', 'Self-employed', 'Local', NULL, 'regular', 'verified', NULL, '2019-06-27 19:56:50', '2019-06-27 19:56:50'),
(7, '12193e7e-7b0d-46f1-ac73-e1f94aca4f7e', 'Brenna', 'Hickle', 'Steuber', 'Brenna Steuber@example.mail', '1579777034', 'Female', '47055 Stark Branch Suite 438\nEast Madilyn, IL 79457', 'Single', NULL, 'Diploma', 'CCS', 'Diploma in Computer Network Administration', '2019', '866-881-5818', 'Truck Mechanic', 'Metz, Brakus and Streich', 'Advertising', 'Professional, Supervisory or Technical', 'Abroad', NULL, 'contractual', 'verified', NULL, '2019-03-19 01:39:02', '2019-03-19 01:39:02'),
(8, 'd09414c5-2f26-4439-b923-9a7b5e58f65e', 'Jo', 'Ruecker', 'Glover', 'Jo Glover@example.mail', '1580009807', 'Male', '53784 Kamille Centers Suite 617\nAdamsburgh, MN 55879', 'Married', NULL, 'Bachelor', 'CCS', 'Diploma in Computer Network Administration', '2019', '866-580-0581', 'Truck Mechanic', 'Schulist, Murphy and Breitenberg', 'Telecommunication', 'Professional, Supervisory or Technical', 'Local', NULL, 'self employed', 'verified', NULL, '2019-11-10 01:24:00', '2019-11-10 01:24:00'),
(9, '90e459e3-3464-4f81-9767-0c1842eeaa12', 'Yvette', 'Wunsch', 'Schimmel', 'Yvette Schimmel@example.mail', '1580556053', 'Female', '650 Malinda Village Suite 544\nWendellshire, WV 93474-8893', 'Single', NULL, 'Diploma', 'CCS', 'Bachelor of Science in Computer Science Major in Social Computing', '2019', '877-947-0284', 'Clinical Research Scientist', 'Durgan-Welch', 'Insurance', 'Managerial or Executive', 'Abroad', NULL, 'regular', 'verified', NULL, '2019-03-04 13:22:09', '2019-03-04 13:22:09'),
(10, '316eacd8-4ac8-4467-a617-425161d5fc56', 'Clarabelle', 'Mitchell', 'Champlin', 'Clarabelle Champlin@example.mail', '1579718335', 'Female', '508 Alden Shore Suite 882\nEast Christop, ND 04152', 'Widowed', NULL, 'Diploma', 'CCS', 'Diploma in Application Development', '2019', '1-866-244-8996', 'Electronics Engineer', 'Kuphal LLC', 'Research & Development', 'Professional, Supervisory or Technical', 'Abroad', NULL, 'contractual', 'verified', NULL, '2019-07-24 04:27:59', '2019-07-24 04:27:59'),
(11, '846ba039-041e-4466-9e15-577eaa04d9a0', 'Cara', 'Rippin', 'Kemmer', 'Cara Kemmer@example.mail', '1579662396', 'Female', '313 Daugherty Ville Suite 196\nNew Abigale, WI 63179', 'Single', NULL, 'Diploma', 'CCS', 'Bachelor of Science in Computer Science Major in Application Development', '2019', '855.270.4406', 'Chemist', 'Gaylord Ltd', 'E-Commerce', 'Rank or clerical', 'Abroad', NULL, 'contractual', 'verified', NULL, '2019-03-14 14:51:37', '2019-03-14 14:51:37'),
(12, '3e126d7b-7d44-44be-a99c-f911230e0d58', 'Cali', 'Hermann', 'Price', 'Cali Price@example.mail', '1580094955', 'Female', '58182 Jovan Road\nLake Chynaton, ID 58478', 'Single', NULL, 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Application Development', '2019', '(844) 289-9320', 'Investor Relations Associate', 'Jaskolski Ltd', 'E-Commerce', 'Professional, Supervisory or Technical', 'Abroad', NULL, 'temporary', 'verified', NULL, '2019-03-31 07:21:55', '2019-03-31 07:21:55'),
(13, 'b81982d5-4dae-41ff-99c2-644b6dddabe2', 'Tremayne', 'Smith', 'Howe', 'Tremayne Howe@example.mail', '1579686848', 'Male', '27731 Lempi Points Suite 790\nNew Brenden, DC 88652', 'Widowed', NULL, 'Bachelor', 'CCS', 'Bachelor of Science in Information Technology Major in Information and Network Security', '2019', '(888) 843-0029', 'Biomedical Enginner', 'Yundt, Morissette and Jacobs', 'Manufacturing', 'Managerial or Executive', 'Local', NULL, 'temporary', 'verified', NULL, '2019-08-01 14:03:55', '2019-08-01 14:03:55'),
(14, 'b4afe0ea-a67f-448a-ad6d-319740d6841b', 'Jairo', 'Lubowitz', 'McDermott', 'Jairo McDermott@example.mail', '1580511638', 'Male', '4492 Ozella Island\nNorth Calebport, GA 80678-0342', 'Married', NULL, 'Bachelor', 'CCS', 'Bachelor of Science in Information Technology Major in Information and Network Security', '2019', '888.598.8537', 'Chemist', 'Parker Inc', 'Accounting', 'Rank or clerical', 'Local', NULL, 'temporary', 'verified', NULL, '2019-11-07 18:39:28', '2019-11-07 18:39:28'),
(15, '924c7c3a-5300-4817-9fc5-83229c9a1047', 'Shawna', 'Fritsch', 'Thiel', 'Shawna Thiel@example.mail', '1580358316', 'Female', '5609 Princess Mill Suite 385\nEast Assuntamouth, MT 72392-6246', 'Single', NULL, 'Bachelor', 'CCS', 'Diploma in Application Development', '2019', '(877) 969-5315', 'Actuary', 'Hirthe PLC', 'Management', 'Professional, Supervisory or Technical', 'Abroad', NULL, 'temporary', 'verified', NULL, '2019-11-18 07:05:32', '2019-11-18 07:05:32'),
(16, 'c0747ecc-b98a-4be2-ae9e-d59933983ee6', 'Danny', 'Muller', 'Mayer', 'Danny Mayer@example.mail', '1580451967', 'Male', '75216 McDermott Ports Suite 998\nRaheemside, WV 31222-6459', 'Single', NULL, 'Bachelor', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', '1-888-596-7526', 'Medical Assistant', 'Reichert-Nienow', 'Retail', 'Managerial or Executive', 'Local', NULL, 'self employed', 'verified', NULL, '2019-10-27 03:08:25', '2019-10-27 03:08:25'),
(17, '5139ebd5-8c61-4417-955c-53aef9a62d9d', 'Anya', 'Frami', 'O''Reilly', 'Anya O''Reilly@example.mail', '1579924289', 'Female', '323 Von Parkway\nKoeppview, GA 74432-8864', 'Widowed', NULL, 'Diploma', 'CCS', 'Diploma in Computer Network Administration', '2019', '(855) 284-2221', 'Diplomat', 'Prohaska-Barton', 'Research & Development', 'Self-employed', 'Abroad', NULL, 'contractual', 'verified', NULL, '2019-03-25 20:55:12', '2019-03-25 20:55:12'),
(18, '9f69512d-8e83-4db4-ab56-b65eb1d47b18', 'Dejon', 'Legros', 'Hagenes', 'Dejon Hagenes@example.mail', '1580095375', 'Male', '74328 Glover Neck\nPort Nannie, FL 68060-8447', 'Separated', NULL, 'Diploma', 'CCS', 'Bachelor of Science in Computer Science Major in Social Computing', '2019', '888.609.1896', 'Chemist', 'Streich Ltd', 'Wholesale', 'Self-employed', 'Local', NULL, 'temporary', 'verified', NULL, '2019-03-20 09:23:52', '2019-03-20 09:23:52'),
(19, '8d15373f-4953-4deb-91c6-7138d7a81d40', 'Freida', 'Kris', 'Jacobson', 'Freida Jacobson@example.mail', '1579801967', 'Female', '8739 Halvorson Parks\nDurganchester, FL 02541-1369', 'Single', NULL, 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Application Development', '2019', '1-888-296-7635', 'Public Relations and Marketing Manager', 'Zulauf, Herzog and Emard', 'Trade / Investment', 'Managerial or Executive', 'Local', NULL, 'self employed', 'verified', NULL, '2019-01-08 01:54:24', '2019-01-08 01:54:24'),
(20, '2bffad8a-c9e8-4e96-a3ac-b56933f2582c', 'Logan', 'King', 'Gutkowski', 'Logan Gutkowski@example.mail', '1580659126', 'Male', '468 Kyleigh Rest Suite 208\nLydafurt, CA 29531-8331', 'Married', NULL, 'Diploma', 'CCS', 'Bachelor of Science in Computer Science Major in Application Development', '2019', '(888) 497-4186', 'Travel Agent', 'Adams Ltd', 'Other', 'Professional, Supervisory or Technical', 'Abroad', NULL, 'temporary', 'verified', NULL, '2019-12-21 12:40:09', '2019-12-21 12:40:09'),
(21, '7f856c00-7290-4e34-8af5-6e4262bba532', 'Myra', 'Cormier', 'Gislason', 'Myra Gislason@example.mail', '1579770931', 'Female', '5863 Lemke Crossroad Suite 256\nNew Therese, OH 06842', 'Married', NULL, 'Bachelor', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', '866-428-9237', 'Biophysics', 'Schmeler Inc', 'Accounting', 'Self-employed', 'Abroad', NULL, 'contractual', 'verified', NULL, '2019-12-26 18:25:43', '2019-12-26 18:25:43'),
(22, '2f26ee46-95af-4c6a-97a3-71000e121867', 'Bonnie', 'Hartmann', 'Kohler', 'Bonnie Kohler@example.mail', '1580360775', 'Female', '853 Carolyn Bypass\nMagnusfurt, IA 94421', 'Single', NULL, 'Bachelor', 'CCS', 'Bachelor of Science in Information Technology Major in Information and Network Security', '2019', '800.616.5429', 'Medical Records and Health Information Technicians', 'Nikolaus, Brekke and Fritsch', 'Other', 'Rank or clerical', 'Local', NULL, 'self employed', 'verified', NULL, '2019-12-26 12:43:27', '2019-12-26 12:43:27'),
(23, '2fb7fa14-bde6-4031-b680-513212e7f479', 'Tessie', 'Ziemann', 'Barrows', 'Tessie Barrows@example.mail', '1579810376', 'Female', '591 Miller Cliff\nDashawntown, AR 25851', 'Separated', NULL, 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Social Computing', '2019', '(844) 230-0041', 'Automotive Enginner', 'Morissette, Swaniawski and Schoen', 'Food Industry', 'Managerial or Executive', 'Local', NULL, 'self employed', 'verified', NULL, '2019-08-20 11:11:12', '2019-08-20 11:11:12'),
(24, '515988d6-178e-4f6a-9ab4-59251d6b095a', 'Ed', 'Watsica', 'Schneider', 'Ed Schneider@example.mail', '1580428115', 'Male', '77823 Reece Prairie\nLanetown, NY 68843', 'Married', NULL, 'Bachelor', 'CCS', 'Diploma in Application Development', '2019', '(855) 814-0201', 'Chemist', 'Kutch, Bernier and Muller', 'Finance / Banking', 'Managerial or Executive', 'Abroad', NULL, 'contractual', 'verified', NULL, '2019-04-30 13:28:51', '2019-04-30 13:28:51'),
(25, 'ec58c01e-7069-4348-baac-454862cf1873', 'Lupe', 'Howell', 'Shanahan', 'Lupe Shanahan@example.mail', '1580633686', 'Female', '7778 Kling Circles\nYundtton, VA 64982', 'Separated', NULL, 'Bachelor', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', '888.643.1207', 'Intructors Position', 'Hauck Ltd', 'Wholesale', 'Managerial or Executive', 'Abroad', NULL, 'contractual', 'verified', NULL, '2019-08-18 15:57:49', '2019-08-18 15:57:49'),
(26, '3b6f4771-e84b-472e-b8d4-4fd82c0eb686', 'Kelsi', 'Hane', 'Hermiston', 'Kelsi Hermiston@example.mail', '1579810315', 'Female', '16401 Spinka Meadow Suite 104\nBechtelarberg, IA 22624-0949', 'Single', NULL, 'Bachelor', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', '800.596.2678', 'Biomedical Enginner', 'Schmidt-Robel', 'Research & Development', 'Managerial or Executive', 'Abroad', NULL, 'contractual', 'verified', NULL, '2019-06-27 17:18:00', '2019-06-27 17:18:00'),
(27, '0bbdd886-f782-4053-b9bf-60d2acd711f7', 'Natalia', 'Simonis', 'Stracke', 'Natalia Stracke@example.mail', '1579697758', 'Female', '1497 Hegmann Skyway\nEast Van, FL 48690', 'Separated', NULL, 'Diploma', 'CCS', 'Bachelor of Science in Computer Science Major in Application Development', '2019', '844.454.7837', 'Travel Agent', 'Mosciski Ltd', 'Other', 'Rank or clerical', 'Abroad', NULL, 'temporary', 'verified', NULL, '2019-02-19 18:31:51', '2019-02-19 18:31:51'),
(28, '228fb3ca-5449-40ba-8211-530b6e73f484', 'Nash', 'Osinski', 'Champlin', 'Nash Champlin@example.mail', '1580074177', 'Male', '687 Krystal Skyway Suite 803\nMullerview, IL 80321-8019', 'Widowed', NULL, 'Bachelor', 'CCS', 'Diploma in Computer Network Administration', '2019', '1-855-904-7919', 'Electronics Engineer', 'Carter, Mayer and Ryan', 'Sales / Marketing', 'Rank or clerical', 'Local', NULL, 'regular', 'verified', NULL, '2019-12-05 04:42:01', '2019-12-05 04:42:01'),
(29, 'e9a48bdf-bdb3-4c2d-97c0-74abb64064b0', 'Jaiden', 'Beahan', 'Renner', 'Jaiden Renner@example.mail', '1580139834', 'Male', '554 Zulauf Ferry Suite 906\nDaphneyport, VA 94780-3286', 'Single', NULL, 'Bachelor', 'CCS', 'Diploma in Computer Network Administration', '2019', '1-855-561-1031', 'Science Research Assistant', 'Osinski Inc', 'Infomation Technology / Computer Science', 'Professional, Supervisory or Technical', 'Local', NULL, 'temporary', 'verified', NULL, '2019-12-05 08:24:55', '2019-12-05 08:24:55'),
(30, 'e062b6c9-1763-428c-93ef-36c7f2a49309', 'Tristin', 'Mann', 'Funk', 'Tristin Funk@example.mail', '1580159085', 'Male', '354 Lakin Prairie\nLake Waltonshire, RI 63047-6323', 'Separated', NULL, 'Bachelor', 'CCS', 'Diploma in Computer Network Administration', '2019', '1-800-740-7259', 'Hotel Manager', 'Kassulke-Feeney', 'Management', 'Managerial or Executive', 'Local', NULL, 'self employed', 'verified', NULL, '2019-07-12 20:28:37', '2019-07-12 20:28:37'),
(31, 'ea690755-380a-4fda-9511-aa1b1df4f8ac', 'Kristy', 'Schumm', 'Heaney', 'Kristy Heaney@example.mail', '1580554840', 'Female', '475 Mitchell Flat Apt. 822\nEast Alessandroland, VA 70607', 'Separated', NULL, 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Social Computing', '2019', '888-247-2427', 'Travel Agent', 'Konopelski, Haley and Wintheiser', 'Finance / Banking', 'Self-employed', 'Abroad', NULL, 'contractual', 'verified', NULL, '2019-10-30 00:10:47', '2019-10-30 00:10:47'),
(32, '9c0c2bf9-1ddf-43e0-b714-33ee2827c617', 'Flossie', 'Yost', 'Bode', 'Flossie Bode@example.mail', '1580556165', 'Female', '1198 Humberto Crest\nBoyleland, AK 97417-0830', 'Single', NULL, 'Bachelor', 'CCS', 'Bachelor of Science in Information Technology Major in Information and Network Security', '2019', '1-877-931-0300', 'Spa Manager', 'Gislason Inc', 'Manufacturing', 'Rank or clerical', 'Abroad', NULL, 'contractual', 'verified', NULL, '2019-07-25 11:05:17', '2019-07-25 11:05:17'),
(33, '97a9e580-8763-47f0-93d2-f6d28b07a1e2', 'Devin', 'Dach', 'Haag', 'Devin Haag@example.mail', '1579580210', 'Male', '85375 Corkery Flat Suite 149\nLake Shemar, VT 02838-4731', 'Widowed', NULL, 'Bachelor', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', '1-800-837-2416', 'Hotel Manager', 'Vandervort-Ebert', 'Research & Development', 'Professional, Supervisory or Technical', 'Local', NULL, 'contractual', 'verified', NULL, '2019-08-08 12:46:02', '2019-08-08 12:46:02'),
(34, 'f41b6b4d-5f3f-4ce1-8a01-a76f1d8f226e', 'Myrl', 'Kilback', 'Kub', 'Myrl Kub@example.mail', '1579602033', 'Male', '24313 Rodrick Light Apt. 561\nBillybury, CA 57645', 'Separated', NULL, 'Diploma', 'CCS', 'Diploma in Application Development', '2019', '855-277-3959', 'Public Relations and Marketing Manager', 'Johnston-Hoeger', 'Accounting', 'Professional, Supervisory or Technical', 'Abroad', NULL, 'regular', 'verified', NULL, '2019-06-26 05:15:38', '2019-06-26 05:15:38'),
(35, 'e9d6f559-4633-4863-a31a-d6f2b7b02d5a', 'Cristina', 'Greenfelder', 'Ruecker', 'Cristina Ruecker@example.mail', '1580063356', 'Male', '97840 Luettgen Lane Suite 072\nMullerside, ID 67333', 'Separated', NULL, 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Social Computing', '2019', '800-988-8794', 'Librarian', 'Torphy-Terry', 'Other', 'Professional, Supervisory or Technical', 'Abroad', NULL, 'contractual', 'verified', NULL, '2019-12-25 09:48:08', '2019-12-25 09:48:08'),
(36, 'f352f64c-9aa5-4e30-a28b-40bdd96bea86', 'Seamus', 'Wehner', 'Abbott', 'Seamus Abbott@example.mail', '1580183265', 'Male', '7187 Verdie Stream Suite 879\nEast Adella, OR 93930', 'Single', NULL, 'Bachelor', 'CCS', 'Diploma in Computer Network Administration', '2019', '800-687-0755', 'Investor Relations Associate', 'Feest, White and Greenholt', 'Wholesale', 'Rank or clerical', 'Abroad', NULL, 'contractual', 'verified', NULL, '2019-06-06 22:01:26', '2019-06-06 22:01:26'),
(37, '71fd555e-b151-4573-bd0f-e8c6c4abf9c2', 'Dasia', 'Russel', 'Casper', 'Dasia Casper@example.mail', '1580540044', 'Female', '9082 Berge Turnpike\nDibbertchester, SC 05475', 'Widowed', NULL, 'Diploma', 'CCS', 'Diploma in Application Development', '2019', '866.870.4802', 'Scientist', 'Kessler-Kihn', 'Consultancy', 'Self-employed', 'Local', NULL, 'regular', 'verified', NULL, '2019-09-22 10:32:23', '2019-09-22 10:32:23'),
(38, 'd8f26f81-dcbf-48b3-bee6-6a71d1126f25', 'Alisa', 'Collier', 'Miller', 'Alisa Miller@example.mail', '1580438575', 'Female', '15372 Hassan Ports\nPort Dorcashaven, CO 45764-9322', 'Single', NULL, 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Social Computing', '2019', '800.814.8527', 'Public Relations and Marketing Manager', 'Hodkiewicz, Douglas and Robel', 'Other', 'Managerial or Executive', 'Local', NULL, 'regular', 'verified', NULL, '2019-01-31 10:53:40', '2019-01-31 10:53:40'),
(39, 'a383d7f0-0184-48f3-9d64-670c94a9a7e5', 'Ursula', 'Rippin', 'Ullrich', 'Ursula Ullrich@example.mail', '1580535394', 'Female', '777 Merl Extension Apt. 760\nWest Diego, WI 81872-2591', 'Married', NULL, 'Diploma', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', '844.850.1044', 'Budget Analyst', 'Fahey Ltd', 'Sales / Marketing', 'Professional, Supervisory or Technical', 'Abroad', NULL, 'self employed', 'verified', NULL, '2019-07-30 11:01:40', '2019-07-30 11:01:40'),
(40, 'fac03282-06fb-4d24-8131-cea2d10b706e', 'Daryl', 'Powlowski', 'Langosh', 'Daryl Langosh@example.mail', '1580526922', 'Male', '60938 Bartholome Meadow Apt. 505\nLake Guidoport, VT 29879', 'Single', NULL, 'Bachelor', 'CCS', 'Diploma in Computer Network Administration', '2019', '844.733.2857', 'Community Relations Manager', 'Denesik, Kshlerin and Hansen', 'Infomation Technology / Computer Science', 'Professional, Supervisory or Technical', 'Abroad', NULL, 'temporary', 'verified', NULL, '2019-04-30 05:29:48', '2019-04-30 05:29:48'),
(41, 'd206e6cb-faf4-4d87-a8da-5af4cdb1b710', 'Jonathan', 'Marquardt', 'Mohr', 'Jonathan Mohr@example.mail', '1579691824', 'Male', '73302 Marquardt Square Apt. 766\nBoyleshire, IA 48249', 'Single', NULL, 'Bachelor', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', '(800) 983-0728', 'Aerospace Enginner', 'O''Kon-Ruecker', 'Food Industry', 'Self-employed', 'Abroad', NULL, 'temporary', 'verified', NULL, '2019-01-26 07:21:13', '2019-01-26 07:21:13'),
(42, '62acd4a1-073b-433e-85f1-929c755f69b0', 'Name', 'Nikolaus', 'Halvorson', 'Name Halvorson@example.mail', '1580275878', 'Female', '4833 Will Village\nWest Darrickberg, FL 79270', 'Single', NULL, 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Application Development', '2019', '888.397.8607', 'Aerospace Enginner', 'Lynch Inc', 'Research & Development', 'Managerial or Executive', 'Abroad', NULL, 'contractual', 'verified', NULL, '2019-06-07 23:14:40', '2019-06-07 23:14:40'),
(43, '775d44df-ce67-4056-94c0-f486ed255afb', 'Bernice', 'Kovacek', 'Zulauf', 'Bernice Zulauf@example.mail', '1580164130', 'Female', '88406 Wilkinson Terrace Apt. 268\nBlicktown, AK 41227', 'Separated', NULL, 'Diploma', 'CCS', 'Diploma in Application Development', '2019', '877.818.4299', 'Intructors Position', 'Bashirian-Wiegand', 'Publishing', 'Professional, Supervisory or Technical', 'Local', NULL, 'regular', 'verified', NULL, '2019-06-04 06:25:32', '2019-06-04 06:25:32'),
(44, '3b9050e5-940c-4cff-815f-7751dbb7f540', 'Irwin', 'Mayer', 'Labadie', 'Irwin Labadie@example.mail', '1579901803', 'Male', '67511 Carter Turnpike\nSouth Letha, AK 00828', 'Married', NULL, 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Application Development', '2019', '800-673-6761', 'Automotive Mechanic', 'Terry, Roberts and Cartwright', 'Management', 'Self-employed', 'Abroad', NULL, 'contractual', 'verified', NULL, '2019-05-15 14:40:29', '2019-05-15 14:40:29'),
(45, 'fc56cc36-987d-41fb-8885-5e928399eb3f', 'Jayce', 'Torp', 'Nader', 'Jayce Nader@example.mail', '1579692401', 'Male', '1262 London Brooks Suite 379\nNew Justynmouth, WY 62869', 'Separated', NULL, 'Bachelor', 'CCS', 'Diploma in Application Development', '2019', '877.265.4002', 'Automotive Mechanic', 'Fay, Kshlerin and Simonis', 'Retail', 'Rank or clerical', 'Abroad', NULL, 'regular', 'verified', NULL, '2019-09-04 01:14:39', '2019-09-04 01:14:39'),
(46, '9dd08520-4936-4904-a403-56cfdbde1c10', 'Dessie', 'Witting', 'Lebsack', 'Dessie Lebsack@example.mail', '1580655798', 'Female', '42304 Irma View\nSporerhaven, DE 81473', 'Single', NULL, 'Diploma', 'CCS', 'Bachelor of Science in Computer Science Major in Application Development', '2019', '1-866-291-0625', 'Lawyer', 'Leuschke Inc', 'Management', 'Managerial or Executive', 'Abroad', NULL, 'self employed', 'verified', NULL, '2019-05-03 21:01:51', '2019-05-03 21:01:51'),
(47, '83f07d7a-af86-4707-b3e0-3ec014b6d696', 'Faye', 'Macejkovic', 'Heidenreich', 'Faye Heidenreich@example.mail', '1579558746', 'Female', '4006 Schulist Plains\nPort Yasmeenland, KY 22099', 'Widowed', NULL, 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Application Development', '2019', '800.246.4898', 'Financial Planner', 'O''Connell Ltd', 'E-Commerce', 'Professional, Supervisory or Technical', 'Abroad', NULL, 'self employed', 'verified', NULL, '2019-08-26 06:21:55', '2019-08-26 06:21:55'),
(48, '846fde34-d3df-4229-a5cd-f8e418ee9edd', 'Olin', 'Abernathy', 'Harris', 'Olin Harris@example.mail', '1579951031', 'Male', '56736 Easter View\nLake Tessiemouth, WV 07369', 'Married', NULL, 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Social Computing', '2019', '866-415-5110', 'Biomedical Enginner', 'Krajcik Inc', 'Trade / Investment', 'Rank or clerical', 'Abroad', NULL, 'self employed', 'verified', NULL, '2019-12-11 21:51:26', '2019-12-11 21:51:26'),
(49, '6cad82cd-a6ba-45dc-8359-0d577c7e2336', 'Joey', 'Marvin', 'Lesch', 'Joey Lesch@example.mail', '1579624429', 'Male', '6929 Mosciski Passage\nJoannyview, MD 42894', 'Widowed', NULL, 'Bachelor', 'CCS', 'Diploma in Computer Network Administration', '2019', '866.484.1037', 'Community Relations Manager', 'Wunsch LLC', 'Telecommunication', 'Professional, Supervisory or Technical', 'Abroad', NULL, 'self employed', 'verified', NULL, '2019-05-13 11:13:36', '2019-05-13 11:13:36'),
(50, '4c2e13c8-2869-4e45-9b60-a46585df7de9', 'Stefan', 'Ortiz', 'Schimmel', 'Stefan Schimmel@example.mail', '1580162030', 'Male', '938 Harmon Shore Apt. 643\nPort Maya, RI 53159', 'Separated', NULL, 'Bachelor', 'CCS', 'Diploma in Application Development', '2019', '855.860.4322', 'Biophysics', 'Koelpin-Bernier', 'Research & Development', 'Managerial or Executive', 'Abroad', NULL, 'regular', 'verified', NULL, '2019-01-17 18:01:25', '2019-01-17 18:01:25'),
(51, '7112e28d-575d-4b61-bd7d-59f9117e122b', 'Meghan', 'Crooks', 'Cummings', 'Meghan Cummings@example.mail', '1580485019', 'Female', '84055 Dayna Throughway\nNorth Allystad, VT 39954-4438', 'Separated', NULL, 'Bachelor', 'CCS', 'Diploma in Computer Network Administration', '2019', '1-888-620-2994', 'Software Developer', 'Wiza LLC', 'Trade / Investment', 'Self-employed', 'Local', NULL, 'contractual', 'verified', NULL, '2019-05-01 04:11:19', '2019-05-01 04:11:19'),
(52, '507fdbd8-618b-4797-8a9d-42cee307dcfe', 'Katrina', 'Brown', 'Auer', 'Katrina Auer@example.mail', '1580673926', 'Female', '89843 Wiegand Place\nSouth Stella, MN 64225-4473', 'Married', NULL, 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Application Development', '2019', '(800) 289-0100', 'Computer Network Architect', 'Koepp-Bernhard', 'E-Commerce', 'Professional, Supervisory or Technical', 'Abroad', NULL, 'contractual', 'verified', NULL, '2019-05-05 07:37:11', '2019-05-05 07:37:11'),
(53, '822798d7-cee0-44f9-9144-f6e101938c0c', 'Stuart', 'Schultz', 'Schultz', 'Stuart Schultz@example.mail', '1579709285', 'Male', '17162 Wilton Drive\nNorth Valentinport, FL 87972', 'Married', NULL, 'Bachelor', 'CCS', 'Diploma in Application Development', '2019', '844.218.0656', 'Database Administrator', 'Lesch-McCullough', 'Retail', 'Professional, Supervisory or Technical', 'Local', NULL, 'regular', 'verified', NULL, '2019-05-22 19:42:55', '2019-05-22 19:42:55'),
(54, '2a1ff151-20b4-42a2-8643-ba3263920836', 'Adella', 'Cummings', 'Deckow', 'Adella Deckow@example.mail', '1579734118', 'Female', '4446 Alexzander Fall Apt. 939\nLloydfort, CO 58546-4829', 'Married', NULL, 'Diploma', 'CCS', 'Bachelor of Science in Information Technology Major in Information and Network Security', '2019', '1-800-509-1148', 'Database Administrator', 'Monahan-Fisher', 'Advertising', 'Managerial or Executive', 'Abroad', NULL, 'regular', 'verified', NULL, '2019-10-18 21:18:45', '2019-10-18 21:18:45'),
(55, '71517d7c-0e0a-4c66-a668-5f91e5eed799', 'Adonis', 'Barton', 'Veum', 'Adonis Veum@example.mail', '1579580332', 'Male', '58829 Nader Shores\nFrederickbury, NY 69553-4969', 'Married', NULL, 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Social Computing', '2019', '888.472.2837', 'Database Administrator', 'Koch, Koss and Block', 'Manufacturing', 'Self-employed', 'Abroad', NULL, 'contractual', 'verified', NULL, '2019-05-26 09:06:05', '2019-05-26 09:06:05'),
(56, '5f44e824-bd22-4753-8fb3-01faf6eb5ced', 'Jazmyn', 'Oberbrunner', 'Schoen', 'Jazmyn Schoen@example.mail', '1580301389', 'Female', '6703 Ashtyn Streets\nBerneiceside, PA 04975', 'Married', NULL, 'Bachelor', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', '(866) 998-9842', 'Database Administrator', 'Schimmel, Abshire and Haley', 'Wholesale', 'Rank or clerical', 'Local', NULL, 'temporary', 'verified', NULL, '2019-06-12 09:37:20', '2019-06-12 09:37:20'),
(57, '63c47d39-b7b7-42a0-b066-a984315c1da4', 'Camilla', 'Schumm', 'Jast', 'Camilla Jast@example.mail', '1580082654', 'Female', '700 Smith Inlet Apt. 497\nDietrichville, MS 17889', 'Widowed', NULL, 'Bachelor', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', '800.371.5920', 'Software Developer', 'Miller-McClure', 'Logistics', 'Managerial or Executive', 'Local', NULL, 'contractual', 'verified', NULL, '2019-01-20 16:06:01', '2019-01-20 16:06:01'),
(58, '07439ee8-2d5f-46bb-a675-28fdfcbaedc4', 'Rex', 'Gutmann', 'Adams', 'Rex Adams@example.mail', '1580197512', 'Male', '521 Freida Meadow Suite 820\nSchuppeside, MO 27592-2409', 'Married', NULL, 'Diploma', 'CCS', 'Bachelor of Science in Computer Science Major in Application Development', '2019', '800-560-6513', 'Computer Network Architect', 'Gulgowski-McGlynn', 'Insurance', 'Rank or clerical', 'Abroad', NULL, 'contractual', 'verified', NULL, '2019-07-27 09:41:34', '2019-07-27 09:41:34'),
(59, 'b9b8ed58-5e9f-44b4-89d4-9e3d021a42a5', 'Wyman', 'Thompson', 'Cummings', 'Wyman Cummings@example.mail', '1580380707', 'Male', '1106 Elyssa Loaf\nNorth Yeseniaburgh, AZ 06604', 'Widowed', NULL, 'Bachelor', 'CCS', 'Bachelor of Science in Information Technology Major in Information and Network Security', '2019', '888-333-4904', 'Computer System Analyst', 'Hudson, Roob and Thompson', 'Infomation Technology / Computer Science', 'Managerial or Executive', 'Local', NULL, 'self employed', 'verified', NULL, '2019-09-19 20:33:31', '2019-09-19 20:33:31'),
(60, 'd0e52cfa-5804-4a43-88e6-b24337953d0b', 'Logan', 'Kohler', 'Kris', 'Logan Kris@example.mail', '1579580808', 'Male', '5210 Nikolas Forest\nWest Eunaview, VA 42825', 'Separated', NULL, 'Diploma', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', '(844) 779-9028', 'Database Administrator', 'Stokes-Harris', 'E-Commerce', 'Professional, Supervisory or Technical', 'Local', NULL, 'temporary', 'verified', NULL, '2019-02-06 02:46:24', '2019-02-06 02:46:24'),
(61, 'a550923d-1708-401e-b8ed-c8300a1dc10d', 'Fabiola', 'Sawayn', 'Klein', 'Fabiola Klein@example.mail', '1579667558', 'Female', '67163 Dietrich Underpass\nPercyland, WA 20197', 'Widowed', NULL, 'Bachelor', 'CCS', 'Bachelor of Science in Information Technology Major in Information and Network Security', '2019', '888-932-2701', 'Computer Hardware Engineer', 'Zulauf, VonRueden and Terry', 'Infomation Technology / Computer Science', 'Managerial or Executive', 'Abroad', NULL, 'regular', 'verified', NULL, '2019-03-08 17:05:04', '2019-03-08 17:05:04'),
(62, '62ed9569-3046-4aee-b6e9-20d8a77702e6', 'Laurence', 'Barton', 'Kirlin', 'Laurence Kirlin@example.mail', '1579639089', 'Female', '365 Hoppe Glen Suite 230\nNorth Melody, NH 85654-8839', 'Single', NULL, 'Bachelor', 'CCS', 'Bachelor of Science in Information Technology Major in Information and Network Security', '2019', '877-852-7591', 'Software Developer', 'Funk PLC', 'Sales / Marketing', 'Professional, Supervisory or Technical', 'Local', NULL, 'regular', 'verified', NULL, '2019-07-22 12:02:52', '2019-07-22 12:02:52'),
(63, '34ab9432-8f9c-40a1-af39-b59a7281dacf', 'Destiny', 'Lebsack', 'Sawayn', 'Destiny Sawayn@example.mail', '1580114226', 'Female', '2497 Murray Summit Apt. 841\nSouth Zechariahmouth, SD 79252', 'Single', NULL, 'Bachelor', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', '855.969.3558', 'Database Administrator', 'Walter, Bartoletti and Mohr', 'Other', 'Self-employed', 'Abroad', NULL, 'regular', 'verified', NULL, '2019-06-11 10:33:27', '2019-06-11 10:33:27'),
(64, 'f593079b-afcb-4d0e-a053-a123b3617014', 'Pamela', 'Fahey', 'Hirthe', 'Pamela Hirthe@example.mail', '1580599860', 'Female', '259 Bradtke Highway Suite 290\nNorth Vincent, KS 65313', 'Widowed', NULL, 'Diploma', 'CCS', 'Bachelor of Science in Computer Science Major in Application Development', '2019', '855-483-8510', 'Computer Hardware Engineer', 'Johnson, Waters and Hill', 'Research & Development', 'Rank or clerical', 'Abroad', NULL, 'temporary', 'verified', NULL, '2019-03-28 23:15:40', '2019-03-28 23:15:40'),
(65, '52adc49f-f453-45e5-a31f-6a61ee0b0107', 'Amparo', 'Will', 'Schowalter', 'Amparo Schowalter@example.mail', '1580629880', 'Male', '92471 Bailey Light\nElbertborough, NH 43015', 'Separated', NULL, 'Diploma', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', '(888) 395-5325', 'Database Administrator', 'Anderson Inc', 'Insurance', 'Professional, Supervisory or Technical', 'Local', NULL, 'regular', 'verified', NULL, '2019-06-14 11:18:12', '2019-06-14 11:18:12'),
(66, '334dcfb0-7d5e-4394-958d-f94ce6dcbebc', 'Iva', 'Johns', 'Hermann', 'Iva Hermann@example.mail', '1580431777', 'Female', '31891 Jazmyn Summit Suite 913\nEast Lorainehaven, MN 23418', 'Married', NULL, 'Bachelor', 'CCS', 'Diploma in Application Development', '2019', '888-950-0541', 'Computer Network Architect', 'Champlin, Champlin and Mann', 'Trade / Investment', 'Managerial or Executive', 'Local', NULL, 'contractual', 'verified', NULL, '2019-10-14 13:52:01', '2019-10-14 13:52:01'),
(67, 'becb249e-ac81-4a4e-934c-0011bd9137be', 'Marina', 'Kling', 'Marquardt', 'Marina Marquardt@example.mail', '1580497555', 'Female', '980 Lindgren Square\nNorth Johnathan, MD 24582-9604', 'Single', NULL, 'Bachelor', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', '1-800-490-8831', 'Computer Hardware Engineer', 'Nikolaus-Steuber', 'Food Industry', 'Self-employed', 'Local', NULL, 'self employed', 'verified', NULL, '2019-06-15 14:37:14', '2019-06-15 14:37:14'),
(68, 'd22c1f3f-cdb4-45c3-b1fd-711f18f7b96d', 'Stephanie', 'Kirlin', 'Flatley', 'Stephanie Flatley@example.mail', '1579745519', 'Female', '40898 Salvador Ranch\nSouth Wymanfurt, AL 26370', 'Single', NULL, 'Diploma', 'CCS', 'Diploma in Application Development', '2019', '1-844-238-4282', 'Computer Network Architect', 'Christiansen LLC', 'Publishing', 'Professional, Supervisory or Technical', 'Abroad', NULL, 'contractual', 'verified', NULL, '2019-06-23 16:08:54', '2019-06-23 16:08:54'),
(69, 'c63fb14d-8696-4067-a23a-79e2b9ff5b54', 'Neal', 'Boehm', 'Jakubowski', 'Neal Jakubowski@example.mail', '1580271741', 'Male', '42448 Watsica Centers\nEast Wilsontown, IA 24452', 'Separated', NULL, 'Diploma', 'CCS', 'Diploma in Application Development', '2019', '844-952-7816', 'Computer Hardware Engineer', 'Predovic-Daugherty', 'Publishing', 'Self-employed', 'Local', NULL, 'regular', 'verified', NULL, '2019-12-23 15:50:55', '2019-12-23 15:50:55'),
(70, 'e8cc9656-5943-45cd-bbab-7bf38644ac47', 'Jacklyn', 'Little', 'Skiles', 'Jacklyn Skiles@example.mail', '1580357865', 'Female', '820 Ziemann Crescent\nAlvertaland, IA 00265', 'Separated', NULL, 'Bachelor', 'CCS', 'Bachelor of Science in Information Technology Major in Information and Network Security', '2019', '855.847.7612', 'Computer Network Architect', 'Bruen-Ferry', 'Accounting', 'Rank or clerical', 'Local', NULL, 'contractual', 'verified', NULL, '2019-08-05 08:39:05', '2019-08-05 08:39:05'),
(71, '4c13a383-1850-4f8e-a7e5-d36e8059ebe9', 'Reba', 'Crona', 'Bahringer', 'Reba Bahringer@example.mail', '1579912064', 'Female', '437 Oberbrunner Harbors Apt. 149\nChristiansenfort, IA 86590-4074', 'Married', NULL, 'Bachelor', 'CCS', 'Diploma in Application Development', '2019', '866.931.5559', 'Computer System Analyst', 'Deckow LLC', 'Advertising', 'Self-employed', 'Abroad', NULL, 'contractual', 'verified', NULL, '2019-08-25 06:16:52', '2019-08-25 06:16:52'),
(72, '95b885cb-b74c-4712-9ed6-bdd0b7d0e00c', 'Mathias', 'Parker', 'Nader', 'Mathias Nader@example.mail', '1580241616', 'Male', '9020 Kshlerin Club Apt. 031\nLake Alexandrinestad, SC 32337', 'Single', NULL, 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Application Development', '2019', '(855) 668-3557', 'Computer Hardware Engineer', 'Mosciski, Murphy and Hill', 'Finance / Banking', 'Rank or clerical', 'Local', NULL, 'self employed', 'verified', NULL, '2019-10-03 23:01:35', '2019-10-03 23:01:35'),
(73, '44ef75b7-7479-4a8e-8c31-7d7984eab6d8', 'Norbert', 'Bailey', 'Mueller', 'Norbert Mueller@example.mail', '1580433058', 'Male', '4482 Beier Plains\nPort Geovanniside, SD 88428', 'Single', NULL, 'Bachelor', 'CCS', 'Diploma in Application Development', '2019', '1-888-639-6158', 'Computer System Analyst', 'Beier-Huel', 'Advertising', 'Professional, Supervisory or Technical', 'Local', NULL, 'temporary', 'verified', NULL, '2019-03-23 00:12:28', '2019-03-23 00:12:28'),
(74, '5ba67b4c-92b0-44da-a606-e7e65c894b11', 'Mae', 'Armstrong', 'Conn', 'Mae Conn@example.mail', '1579854865', 'Female', '9231 Alva Road\nWest Erynport, NJ 52565', 'Single', NULL, 'Diploma', 'CCS', 'Bachelor of Science in Information Technology Major in Information and Network Security', '2019', '(855) 508-5633', 'Software Developer', 'Kirlin-Gislason', 'Wholesale', 'Self-employed', 'Abroad', NULL, 'contractual', 'verified', NULL, '2019-08-02 09:32:43', '2019-08-02 09:32:43'),
(75, '1516869e-24d8-4329-89f9-815ed8a42e0f', 'Murray', 'Walter', 'Bergnaum', 'Murray Bergnaum@example.mail', '1580236409', 'Male', '18744 Liliane Cliff Suite 692\nAdolphuschester, NE 88980-0353', 'Widowed', NULL, 'Bachelor', 'CCS', 'Diploma in Computer Network Administration', '2019', '1-855-618-1008', 'Computer System Analyst', 'Brakus, Hackett and Ruecker', 'Trade / Investment', 'Managerial or Executive', 'Abroad', NULL, 'contractual', 'verified', NULL, '2019-12-13 21:42:31', '2019-12-13 21:42:31'),
(76, '5a9e5aa2-22fb-46b6-bad7-bc1a0e4eb3e0', 'Bernice', 'Orn', 'Will', 'Bernice Will@example.mail', '1579969842', 'Female', '8032 Gaylord Views Apt. 841\nGarettborough, HI 89851', 'Single', NULL, 'Bachelor', 'CCS', 'Diploma in Application Development', '2019', '844-450-4340', 'Computer Hardware Engineer', 'Treutel Ltd', 'Food Industry', 'Rank or clerical', 'Local', NULL, 'self employed', 'verified', NULL, '2019-12-19 06:30:56', '2019-12-19 06:30:56'),
(77, 'b24dda16-0138-4e1e-b3fb-de0b63391981', 'Dale', 'McCullough', 'Beier', 'Dale Beier@example.mail', '1579810673', 'Male', '116 Blaze River Apt. 279\nNew Makennaville, IL 68651', 'Widowed', NULL, 'Bachelor', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', '866-525-8598', 'Computer System Analyst', 'Blanda-Gibson', 'Telecommunication', 'Self-employed', 'Abroad', NULL, 'temporary', 'verified', NULL, '2019-03-20 11:15:41', '2019-03-20 11:15:41'),
(78, '36b1efbf-2967-4e26-8786-c32bf176f8eb', 'Earline', 'Willms', 'Pacocha', 'Earline Pacocha@example.mail', '1579829021', 'Female', '76742 Schultz Plain\nLaviniamouth, NC 96043', 'Married', NULL, 'Diploma', 'CCS', 'Diploma in Application Development', '2019', '1-800-924-5515', 'Computer Hardware Engineer', 'Reichel-Daniel', 'Other', 'Self-employed', 'Local', NULL, 'regular', 'verified', NULL, '2019-09-18 04:29:34', '2019-09-18 04:29:34'),
(79, '507b50a6-41e3-4e31-bdc5-6744f0febae8', 'Aglae', 'Maggio', 'Macejkovic', 'Aglae Macejkovic@example.mail', '1579833648', 'Female', '20855 Legros Estates Apt. 478\nVestaside, TN 43714-7037', 'Widowed', NULL, 'Bachelor', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', '877.878.4404', 'Database Administrator', 'Cremin Inc', 'Publishing', 'Professional, Supervisory or Technical', 'Local', NULL, 'regular', 'verified', NULL, '2019-06-30 15:13:26', '2019-06-30 15:13:26'),
(80, '70e6e37c-a057-4a39-a5f9-07277d551fe2', 'Eugene', 'Hyatt', 'Kris', 'Eugene Kris@example.mail', '1579812736', 'Male', '508 Kaylin Avenue Suite 757\nPort Alejandrinville, NE 90748', 'Widowed', NULL, 'Bachelor', 'CCS', 'Bachelor of Science in Information Technology Major in Information and Network Security', '2019', '877-454-3008', 'Database Administrator', 'Jast and Sons', 'Logistics', 'Self-employed', 'Local', NULL, 'contractual', 'verified', NULL, '2019-12-25 12:51:13', '2019-12-25 12:51:13'),
(81, 'f8a83dc1-5866-4b60-b2cd-77bd0cc94298', 'Dallas', 'Fritsch', 'Purdy', 'Dallas Purdy@example.mail', '1579557926', 'Male', '808 Olson Mission Apt. 824\nNew Janetbury, AR 02206', 'Widowed', NULL, 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Social Computing', '2019', '(800) 900-1803', 'Computer Hardware Engineer', 'Lockman-Swift', 'Trade / Investment', 'Rank or clerical', 'Local', NULL, 'contractual', 'verified', NULL, '2019-12-29 04:31:18', '2019-12-29 04:31:18'),
(82, '7bdf0a6b-b731-485f-9dc4-d7f6c9f4e6d8', 'Ezequiel', 'Hodkiewicz', 'Dicki', 'Ezequiel Dicki@example.mail', '1580392726', 'Male', '493 Lilla Station\nPort Alexandrea, IL 72733-7598', 'Married', NULL, 'Diploma', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', '1-866-287-6616', 'Software Developer', 'Spencer Inc', 'Management', 'Self-employed', 'Local', NULL, 'temporary', 'verified', NULL, '2019-11-04 18:12:37', '2019-11-04 18:12:37'),
(83, '9be8d863-e6b9-4800-beae-68166baaf99c', 'Anibal', 'Effertz', 'Runolfsdottir', 'Anibal Runolfsdottir@example.mail', '1579694401', 'Male', '706 Lizzie Trafficway\nSouth Erickland, MI 51258', 'Widowed', NULL, 'Bachelor', 'CCS', 'Bachelor of Science in Information Technology Major in Information and Network Security', '2019', '855.223.1943', 'Computer Hardware Engineer', 'Heller-Erdman', 'Sales / Marketing', 'Self-employed', 'Abroad', NULL, 'regular', 'verified', NULL, '2019-11-08 02:38:06', '2019-11-08 02:38:06'),
(84, 'ca933acc-2e58-445b-8b7b-8324b74a4077', 'Javon', 'Rowe', 'Bogisich', 'Javon Bogisich@example.mail', '1580503840', 'Male', '81521 Paucek Ranch\nFunkton, MS 05467', 'Single', NULL, 'Bachelor', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', '(844) 213-6731', 'Computer Hardware Engineer', 'Sauer and Sons', 'Sales / Marketing', 'Rank or clerical', 'Abroad', NULL, 'contractual', 'verified', NULL, '2019-12-17 17:29:18', '2019-12-17 17:29:18'),
(85, '76318359-81fc-4b28-81be-a16e3ac00ed7', 'Christop', 'Klein', 'DuBuque', 'Christop DuBuque@example.mail', '1580308006', 'Male', '26599 Adeline Lights\nLake Guiseppe, NY 88782-7508', 'Separated', NULL, 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Application Development', '2019', '877.208.5113', 'Computer Hardware Engineer', 'Bergstrom Ltd', 'Sales / Marketing', 'Self-employed', 'Local', NULL, 'temporary', 'verified', NULL, '2019-01-04 10:31:11', '2019-01-04 10:31:11'),
(86, '9f389bc3-eb4a-4008-bd90-16202e6ffe88', 'Sim', 'Weissnat', 'Mertz', 'Sim Mertz@example.mail', '1579959086', 'Male', '858 Jerrod Mall Apt. 687\nKohlerberg, MI 24131-9609', 'Widowed', NULL, 'Bachelor', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', '(844) 304-4942', 'Database Administrator', 'Medhurst, Hayes and Lockman', 'Trade / Investment', 'Self-employed', 'Local', NULL, 'self employed', 'verified', NULL, '2019-12-17 12:34:14', '2019-12-17 12:34:14'),
(87, 'cddabc0e-3d35-431f-b281-2254f6d7d4bc', 'Jalen', 'Langosh', 'Cormier', 'Jalen Cormier@example.mail', '1579907709', 'Male', '2587 Jaqueline Canyon Suite 687\nLaurettaland, AZ 56247-9915', 'Married', NULL, 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Social Computing', '2019', '877.219.1158', 'Computer Hardware Engineer', 'Weber, Rosenbaum and Lindgren', 'E-Commerce', 'Managerial or Executive', 'Local', NULL, 'temporary', 'verified', NULL, '2019-08-12 12:02:08', '2019-08-12 12:02:08'),
(88, '7bce045d-1c17-448d-b2ea-b467f7081a9a', 'Edgar', 'Reynolds', 'Connelly', 'Edgar Connelly@example.mail', '1579619454', 'Male', '69448 Gutmann Path Suite 172\nElzaton, CO 55037-5952', 'Married', NULL, 'Bachelor', 'CCS', 'Diploma in Computer Network Administration', '2019', '866.366.3365', 'Computer Hardware Engineer', 'Ferry-Kutch', 'Wholesale', 'Managerial or Executive', 'Abroad', NULL, 'contractual', 'verified', NULL, '2019-05-17 02:40:58', '2019-05-17 02:40:58'),
(89, 'e0b968d7-f221-440a-b1fc-34227e0bd250', 'Ryann', 'Mertz', 'Zboncak', 'Ryann Zboncak@example.mail', '1580658600', 'Male', '9709 Gwen Divide\nRogersshire, VT 18880', 'Separated', NULL, 'Bachelor', 'CCS', 'Diploma in Computer Network Administration', '2019', '(855) 223-0991', 'Database Administrator', 'Sawayn, Stiedemann and Renner', 'Trade / Investment', 'Rank or clerical', 'Local', NULL, 'contractual', 'verified', NULL, '2019-05-29 17:52:27', '2019-05-29 17:52:27'),
(90, 'e6229fe4-0924-4e93-b2f1-81b924a088a6', 'Abdullah', 'Luettgen', 'Rippin', 'Abdullah Rippin@example.mail', '1579810329', 'Male', '289 Schimmel Passage\nNorth Ransombury, NC 68058', 'Single', NULL, 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Application Development', '2019', '855.352.5748', 'Computer Hardware Engineer', 'Reichert, Kreiger and Stracke', 'Telecommunication', 'Professional, Supervisory or Technical', 'Abroad', NULL, 'self employed', 'verified', NULL, '2019-12-03 15:52:43', '2019-12-03 15:52:43'),
(91, '1f551f0e-4ce2-42d3-95ff-f61b67b2e664', 'Alfred', 'Murazik', 'Wiza', 'Alfred Wiza@example.mail', '1579860528', 'Male', '864 Unique Avenue Apt. 405\nNorth Christianaville, AZ 25455-7073', 'Single', NULL, 'Diploma', 'CCS', 'Bachelor of Science in Information Technology Major in Information and Network Security', '2019', '(877) 644-8267', 'Computer System Analyst', 'Zieme, Mayert and Upton', 'Transportation', 'Professional, Supervisory or Technical', 'Abroad', NULL, 'temporary', 'verified', NULL, '2019-02-14 06:45:53', '2019-02-14 06:45:53'),
(92, 'ade8f315-d50b-499f-aa42-f778b24d32fe', 'Finn', 'Jakubowski', 'Hane', 'Finn Hane@example.mail', '1580595604', 'Male', '660 Runolfsson Estate\nHarveymouth, OH 53611-4027', 'Single', NULL, 'Bachelor', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', '(855) 613-0180', 'Database Administrator', 'Lesch, White and Rodriguez', 'Other', 'Rank or clerical', 'Abroad', NULL, 'temporary', 'verified', NULL, '2019-07-12 00:32:40', '2019-07-12 00:32:40'),
(93, 'b4b6b7b0-c2a1-4eba-b219-10b3fedb7da4', 'Kendrick', 'Heathcote', 'Schmidt', 'Kendrick Schmidt@example.mail', '1579524557', 'Male', '8861 Toy Drive\nAshlystad, GA 15361', 'Widowed', NULL, 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Application Development', '2019', '1-844-743-8263', 'Software Developer', 'Will PLC', 'Infomation Technology / Computer Science', 'Rank or clerical', 'Local', NULL, 'contractual', 'verified', NULL, '2019-07-26 19:36:12', '2019-07-26 19:36:12'),
(94, '3d748c0e-fe6e-476f-befb-3b20ff34983d', 'Roderick', 'Schultz', 'Flatley', 'Roderick Flatley@example.mail', '1580662532', 'Male', '2675 Runte Rue Apt. 189\nNorth Sybleburgh, AL 24605', 'Married', NULL, 'Diploma', 'CCS', 'Bachelor of Science in Computer Science Major in Social Computing', '2019', '(844) 522-4517', 'Computer Hardware Engineer', 'Runolfsdottir, Mitchell and Muller', 'Finance / Banking', 'Professional, Supervisory or Technical', 'Abroad', NULL, 'contractual', 'verified', NULL, '2019-08-26 09:46:20', '2019-08-26 09:46:20'),
(95, '37cca6c7-e7ab-4a54-9272-a0460fb989a5', 'Veda', 'Nitzsche', 'Quigley', 'Veda Quigley@example.mail', '1580375533', 'Female', '9233 Loma Cove Apt. 603\nWest Lincolnmouth, ID 55245-5945', 'Separated', NULL, 'Bachelor', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', '844.716.8424', 'Software Developer', 'Hartmann-Larson', 'Accounting', 'Professional, Supervisory or Technical', 'Local', NULL, 'regular', 'verified', NULL, '2019-11-25 13:16:12', '2019-11-25 13:16:12'),
(96, '97646a0e-943a-4c70-b0b5-109bb0e2c01e', 'Lamont', 'Kerluke', 'Jakubowski', 'Lamont Jakubowski@example.mail', '1580670152', 'Male', '54831 Dooley Mill Suite 798\nAshtonview, WY 68094', 'Married', NULL, 'Bachelor', 'CCS', 'Diploma in Computer Network Administration', '2019', '(800) 934-5141', 'Computer System Analyst', 'Brekke Group', 'Wholesale', 'Self-employed', 'Local', NULL, 'self employed', 'verified', NULL, '2019-03-25 15:55:42', '2019-03-25 15:55:42'),
(97, 'fac2b109-b47a-4542-bcab-ceb1bdf909dd', 'Marion', 'Altenwerth', 'Bosco', 'Marion Bosco@example.mail', '1579747462', 'Female', '9283 Hamill Well Apt. 128\nHauckshire, RI 25495-2174', 'Married', NULL, 'Diploma', 'CCS', 'Bachelor of Science in Information Technology Major in Information and Network Security', '2019', '(888) 975-1239', 'Software Developer', 'Lebsack, Wolff and Dare', 'Wholesale', 'Professional, Supervisory or Technical', 'Abroad', NULL, 'regular', 'verified', NULL, '2019-06-17 00:56:23', '2019-06-17 00:56:23'),
(98, 'c9c973b6-3951-41c4-a08e-3cebe9bb9707', 'Stephania', 'Fahey', 'Erdman', 'Stephania Erdman@example.mail', '1580118330', 'Female', '1612 Klein Club\nNew Hilario, IL 86125', 'Separated', NULL, 'Diploma', 'CCS', 'Diploma in Application Development', '2019', '(888) 966-4784', 'Database Administrator', 'Waters Inc', 'Wholesale', 'Managerial or Executive', 'Abroad', NULL, 'self employed', 'verified', NULL, '2019-01-25 19:33:47', '2019-01-25 19:33:47'),
(99, 'eee4732e-2e37-4a49-b852-e70eb1d59b0a', 'Jaylen', 'Weimann', 'Huel', 'Jaylen Huel@example.mail', '1579536737', 'Male', '9704 Murphy Throughway Apt. 798\nTerryfurt, MD 02559', 'Separated', NULL, 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Social Computing', '2019', '888.534.8433', 'Computer Network Architect', 'Dooley-Watsica', 'Food Industry', 'Professional, Supervisory or Technical', 'Abroad', NULL, 'regular', 'verified', NULL, '2019-03-10 10:14:15', '2019-03-10 10:14:15'),
(100, '50c61260-3115-4fb8-b825-50de573b2ae2', 'Lea', 'Wyman', 'Swift', 'Lea Swift@example.mail', '1580505324', 'Female', '35559 Johnston Neck\nGradystad, CA 84285-1754', 'Separated', NULL, 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Social Computing', '2019', '855.778.1658', 'Computer Network Architect', 'Towne and Sons', 'Sales / Marketing', 'Self-employed', 'Abroad', NULL, 'contractual', 'verified', NULL, '2019-09-25 19:30:35', '2019-09-25 19:30:35');

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE IF NOT EXISTS `articles` (
  `id` bigint(20) unsigned NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `cover` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `posted_by` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'Admin',
  `view_counter` int(11) NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `title`, `body`, `cover`, `posted_by`, `view_counter`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Welcome Alumni', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sit amet mattis vulputate enim nulla aliquet porttitor. Erat pellentesque adipiscing commodo elit at imperdiet. Elementum integer enim neque volutpat. Mi tempus imperdiet nulla malesuada pellentesque elit eget gravida. Platea dictumst quisque sagittis purus sit amet volutpat consequat mauris. Erat imperdiet sed euismod nisi porta lorem. Augue lacus viverra vitae congue eu. Neque ornare aenean euismod elementum nisi quis eleifend. Hac habitasse platea dictumst quisque sagittis. Nibh cras pulvinar mattis nunc sed blandit.</p><p>Hac habitasse platea dictumst vestibulum. Interdum consectetur libero id faucibus nisl. Quam vulputate dignissim suspendisse in. Ultricies tristique nulla aliquet enim tortor at auctor urna nunc. Egestas sed tempus urna et pharetra pharetra massa massa ultricies. Consequat interdum varius sit amet mattis. Tincidunt lobortis feugiat vivamus at augue eget arcu dictum. Suspendisse ultrices gravida dictum fusce ut placerat. Et odio pellentesque diam volutpat commodo. Elit at imperdiet dui accumsan. At in tellus integer feugiat. Arcu non sodales neque sodales ut etiam sit amet. Aliquam sem et tortor consequat id porta nibh. Pulvinar mattis nunc sed blandit.</p>', 'uploads/images/umak2-Cover.jpg', 'Admin', 0, NULL, '2020-01-12 02:03:35', '2020-01-18 11:17:42'),
(2, 'Congrats boss Manny! Penge Balato', '<p>"AMONG MY ACHIEVEMENTS, THIS WILL BE THE MOST MEANINGFUL". World Boxing Champion and Philippine Senator Emmanuel "Manny" Pacquiao is UMak Batch 2019 AB Political Science Major in Local Government Administration (Executive Program) graduate.</p><p>In photo, UMak alumnus Pacquiao delivers his response on behalf of the graduates of the University.</p><p>WELCOME TO THE UMAK ALUMNI COMMUNITY, SEN. PACQUIAO!</p>', 'uploads/images/article1-Cover.jpg', 'Admin', 0, NULL, '2020-01-12 03:26:12', '2020-01-12 03:26:12'),
(3, 'UMAK POWER COUPLE IN 30TH SEA GAMES', '<p>UMAK POWER COUPLE IN 30TH SEA GAMES. Bachelor in Physical Wellness graduates Paul Marton Dela Cruz and Rachelle Anne Cabral-Dela Cruz win gold in the Mixed Team Compound event of Archery in the 30th SEA Games!<br><br>Thank you for inspiring and for making us proud, Paul and Rachelle! Mabuhay kayo!<br><br><a href="https://www.facebook.com/hashtag/wewinasone?epa=HASHTAG">#WeWinAsOne</a> <a href="https://www.facebook.com/hashtag/seagames2019?epa=HASHTAG">#SEAGames2019</a> <a href="https://www.facebook.com/hashtag/umakalumni?epa=HASHTAG">#UMakAlumni</a> <a href="https://www.facebook.com/hashtag/makatizen?epa=HASHTAG">#Makatizen</a><br><br>Photo courtesy: Josh Albelda (Rappler)<br>&nbsp;</p>', 'uploads/images/article2-Cover.jpg', 'Admin', 0, NULL, '2020-01-12 03:36:01', '2020-01-12 03:36:01'),
(4, 'Sarah Reyes was recognized in the 2nd UMak Outstanding Alumni Awards', '<p>Civil Engineering Licensure Examination passer Sarah Reyes was recognized in the 2nd UMak Outstanding Alumni Awards.</p><p>&nbsp;<a href="https://www.facebook.com/hashtag/wavingatyou?source=feed_text&amp;epa=HASHTAG&amp;__xts__%5B0%5D=68.ARDWnwFHtwVBHCO3xwX34y1p4XdrIWurXikSsNIjG9WgD8fz4E2kNRYZJEQ0v-TLyCsQBHuFucAHQCZL9vZV2ymLtd5ETC0Ft_gxlDJOu7L9pYnKHqiyerQCAHaTKIX1WEWoPNU5P4lZpSvp2LLlgYtMv-gI6ZRE7cw4CP_qkaukCAIZ7Mv1JdSi7b2YnSB_riWStmqmBUAarr0UFlFkNPMHtaC3B-3PwN39GS68mAs1EkvET1iII7YgOJ4piCwqo90g3BN0lzRkWCTwWZmFv3zIjfXZJhfVkwJYdI9rDVEu0YdUYs25BarzBYgJzdhaC4-0jYOPKVFlJqqyL0eEsCoFFFLetA-MmKrhqoN4lhvaAZ62NS3KU4OXUQxogHxoxBnVVhpCg-RuuPmjwT-dvGmuEqvfjl18JbXxqEo5UpnP97bitbFZAy4g5wIGotLef4KGPxh68Q0KR-LEy7pQNg&amp;__tn__=%2ANK-R">#WavingAtYou</a> <a href="https://www.facebook.com/hashtag/oaa2019?source=feed_text&amp;epa=HASHTAG&amp;__xts__%5B0%5D=68.ARDWnwFHtwVBHCO3xwX34y1p4XdrIWurXikSsNIjG9WgD8fz4E2kNRYZJEQ0v-TLyCsQBHuFucAHQCZL9vZV2ymLtd5ETC0Ft_gxlDJOu7L9pYnKHqiyerQCAHaTKIX1WEWoPNU5P4lZpSvp2LLlgYtMv-gI6ZRE7cw4CP_qkaukCAIZ7Mv1JdSi7b2YnSB_riWStmqmBUAarr0UFlFkNPMHtaC3B-3PwN39GS68mAs1EkvET1iII7YgOJ4piCwqo90g3BN0lzRkWCTwWZmFv3zIjfXZJhfVkwJYdI9rDVEu0YdUYs25BarzBYgJzdhaC4-0jYOPKVFlJqqyL0eEsCoFFFLetA-MmKrhqoN4lhvaAZ62NS3KU4OXUQxogHxoxBnVVhpCg-RuuPmjwT-dvGmuEqvfjl18JbXxqEo5UpnP97bitbFZAy4g5wIGotLef4KGPxh68Q0KR-LEy7pQNg&amp;__tn__=%2ANK-R">#OAA2019</a></p>', 'uploads/images/article3-Cover.jpg', 'Admin', 0, NULL, '2020-01-12 03:48:23', '2020-01-12 03:48:23'),
(5, 'The new Center for Alumni Affairs Officers.', '<p>Center for Alumni Affairs Officer-in-Charge Steve Villacorta (fourth from right) with the Association of UMak Alumni, Inc. Board of Trustees (from left) Vice President for External Affairs Jeffrey Oberas, Corporate Secretary Joel Advincula, PR Officer Ryan Andrew Dela Luna, President Myna Marie Nerona, Vice President for Internal Affairs Jernell Mari Sanchez, Treasurer Ramon Abad III, and Auditor Esmeraldo Delas Armas lead the 7th AUAI General Assembly.&nbsp;</p><p><a href="https://www.facebook.com/hashtag/wavingatyou?source=feed_text&amp;epa=HASHTAG&amp;__xts__%5B0%5D=68.ARC-0FKdD9VivpBKg0D1Ib3AmhExa3gWmbv0tjfc1b9LdAlMMmWDxerrt0yeF9cf0uNrxrg7IIvmcGANQyLMoKg507vocFTsY2ksGEpPhftCHcUbV30MAh7qYfexemF5h0zdAyeaya_VPrV3W-JJBGAE0d12hF8sYbUoQx3IxDlrn4bH7kU-6xJd3U7NYE4jyukJ0Xzkm5jkdm0c_3e0GnDvOmWpAwJEBnWvFJGcnysNmEyVCH_0wgkxDJ-peiuy6BRkPauGnVVAddvEviluMIz-dblVvzTslWv0fnUBIw4eulUMLS6ke7-gXPpK9068f6_Xl-E8jxm-9Dcjrer-mPiC64P_K9LdxQ1hvvRVLxZWDSF-cyDbXKIkkX4kc25P10wiXJyi3de-uIUBcZDkRpevOdJVOSpFcehXvPXVkDEa0oCEdULFLAWF-vh_z7ZAV1vo4zCPiPfv2TlGQheIWQ&amp;__tn__=%2ANK-R">#WavingAtYou</a> <a href="https://www.facebook.com/hashtag/oaa2019?source=feed_text&amp;epa=HASHTAG&amp;__xts__%5B0%5D=68.ARC-0FKdD9VivpBKg0D1Ib3AmhExa3gWmbv0tjfc1b9LdAlMMmWDxerrt0yeF9cf0uNrxrg7IIvmcGANQyLMoKg507vocFTsY2ksGEpPhftCHcUbV30MAh7qYfexemF5h0zdAyeaya_VPrV3W-JJBGAE0d12hF8sYbUoQx3IxDlrn4bH7kU-6xJd3U7NYE4jyukJ0Xzkm5jkdm0c_3e0GnDvOmWpAwJEBnWvFJGcnysNmEyVCH_0wgkxDJ-peiuy6BRkPauGnVVAddvEviluMIz-dblVvzTslWv0fnUBIw4eulUMLS6ke7-gXPpK9068f6_Xl-E8jxm-9Dcjrer-mPiC64P_K9LdxQ1hvvRVLxZWDSF-cyDbXKIkkX4kc25P10wiXJyi3de-uIUBcZDkRpevOdJVOSpFcehXvPXVkDEa0oCEdULFLAWF-vh_z7ZAV1vo4zCPiPfv2TlGQheIWQ&amp;__tn__=%2ANK-R">#OAA2019</a></p><p><br>&nbsp;</p>', 'uploads/images/article4-Cover.jpg', 'Admin', 0, NULL, '2020-01-12 03:50:01', '2020-01-12 03:50:01'),
(6, 'Necessitatibus harum sunt praesentium laborum maxime quo est quas.', 'Alice had never seen such a puzzled expression that she knew she had this fit) An obstacle that came between Him, and ourselves, and it. Don''t let him know she liked them best, For this must ever be.', 'https://picsum.photos/1200/400?random=37', 'Admin', 0, NULL, '2020-01-21 06:00:56', '2020-01-21 06:00:56'),
(7, 'Maxime sapiente dolorem voluptatem sed labore.', 'March Hare. ''Yes, please do!'' pleaded Alice. ''And ever since that,'' the Hatter went on at last, they must be what he did it,) he did it,) he did not dare to laugh; and, as there seemed to be a grin.', 'https://picsum.photos/1200/400?random=4', 'Admin', 0, NULL, '2020-01-21 06:00:56', '2020-01-21 06:00:56'),
(8, 'Non et quo autem rerum recusandae.', 'I''m sure I don''t want to be?'' it asked. ''Oh, I''m not the smallest idea how confusing it is to find her in a furious passion, and went on: ''--that begins with an important air, ''are you all ready?.', 'https://picsum.photos/1200/400?random=46', 'Admin', 0, NULL, '2020-01-21 06:00:56', '2020-01-21 06:00:56'),
(9, 'Quasi quo veniam aut et aliquid.', 'Alice was not an encouraging tone. Alice looked very uncomfortable. The moment Alice felt dreadfully puzzled. The Hatter''s remark seemed to be told so. ''It''s really dreadful,'' she muttered to.', 'https://picsum.photos/1200/400?random=10', 'Admin', 0, NULL, '2020-01-21 06:00:56', '2020-01-21 06:00:56'),
(10, 'Eum qui dolores officiis quis.', 'March Hare. Alice was so large a house, that she was saying, and the three gardeners, but she saw them, they were mine before. If I or she should push the matter with it. There could be no use in.', 'https://picsum.photos/1200/400?random=46', 'Admin', 0, NULL, '2020-01-21 06:00:56', '2020-01-21 06:00:56'),
(11, 'Sed eum ut assumenda qui id repudiandae quo.', 'I was sent for.'' ''You ought to be a great deal too far off to the three gardeners, but she remembered how small she was holding, and she very good-naturedly began hunting about for it, while the.', 'https://picsum.photos/1200/400?random=6', 'Admin', 0, NULL, '2020-01-21 06:00:56', '2020-01-21 06:00:56'),
(12, 'Et voluptatem et quas iste illo earum occaecati.', 'Alice; ''but when you come and join the dance? "You can really have no idea how to set about it; if I''m Mabel, I''ll stay down here till I''m somebody else"--but, oh dear!'' cried Alice, quite.', 'https://picsum.photos/1200/400?random=33', 'Admin', 0, NULL, '2020-01-21 06:00:56', '2020-01-21 06:00:56'),
(13, 'Unde dolor quae iusto aliquam.', 'English,'' thought Alice; ''I can''t go no lower,'' said the King; ''and don''t be particular--Here, Bill! catch hold of anything, but she could for sneezing. There was exactly the right house, because.', 'https://picsum.photos/1200/400?random=23', 'Admin', 0, NULL, '2020-01-21 06:00:56', '2020-01-21 06:00:56'),
(14, 'Numquam doloribus perferendis ad harum facere architecto dolore.', 'After these came the royal children, and everybody else. ''Leave off that!'' screamed the Queen. ''You make me larger, it must be kind to them,'' thought Alice, ''and those twelve creatures,'' (she was.', 'https://picsum.photos/1200/400?random=11', 'Admin', 0, NULL, '2020-01-21 06:00:56', '2020-01-21 06:00:56'),
(15, 'Non officiis qui voluptas ipsa laudantium alias necessitatibus explicabo.', 'Alice could only hear whispers now and then she looked back once or twice she had not gone (We know it to speak first, ''why your cat grins like that?'' ''It''s a Cheshire cat,'' said the Lory, as soon.', 'https://picsum.photos/1200/400?random=21', 'Admin', 0, NULL, '2020-01-21 06:00:56', '2020-01-21 06:00:56');

-- --------------------------------------------------------

--
-- Table structure for table `carousel`
--

CREATE TABLE IF NOT EXISTS `carousel` (
  `id` bigint(20) unsigned NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `heading` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `carousel`
--

INSERT INTO `carousel` (`id`, `image`, `title`, `description`, `heading`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'uploads/carousel/carousel1-Carousel.jpg', 'Carousel 1', NULL, NULL, NULL, '2020-01-24 00:41:35', '2020-01-24 01:00:51'),
(2, 'uploads/carousel/cover-Carousel.png', 'Carousel 2', NULL, NULL, NULL, '2020-02-05 13:28:13', '2020-02-05 13:28:13'),
(3, 'uploads/carousel/carousel3-Carousel.jpg', 'Carousel 3', NULL, NULL, NULL, '2020-02-05 13:31:47', '2020-02-05 13:31:47'),
(4, 'uploads/carousel/carousel5-Carousel.jpg', 'Carousel 4', NULL, NULL, NULL, '2020-02-05 13:31:53', '2020-02-05 13:31:53'),
(5, 'uploads/carousel/carousel6-Carousel.png', 'Carousel 5', NULL, NULL, NULL, '2020-02-05 13:32:00', '2020-02-05 13:32:00');

-- --------------------------------------------------------

--
-- Table structure for table `colleges`
--

CREATE TABLE IF NOT EXISTS `colleges` (
  `id` bigint(20) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `colleges`
--

INSERT INTO `colleges` (`id`, `name`, `code`, `created_at`, `updated_at`) VALUES
(1, 'Computer Science', 'CCS', '2019-07-10 02:39:59', '2019-07-10 02:39:59'),
(2, 'Business and Finance Studies', 'CBFS', '2019-07-10 02:39:59', '2019-07-10 02:39:59'),
(3, 'Continuing and Professional Studies', 'CCAPS', '2019-07-10 02:39:59', '2019-07-10 02:39:59'),
(4, 'Construction Science and Civil Engineering', 'CCSCE', '2019-07-10 02:39:59', '2019-07-10 02:39:59'),
(5, 'Governance and Public Policy', 'CGPP', '2019-07-10 02:39:59', '2019-07-10 02:39:59'),
(6, 'Human kinesthetic', 'CHK', '2019-07-10 02:39:59', '2019-07-10 02:39:59'),
(7, 'Maritime Leadership Innovation', 'CMLI', '2019-07-10 02:39:59', '2019-07-10 02:39:59'),
(8, 'Allied Health Studies', 'COAHS', '2019-07-10 02:39:59', '2019-07-10 02:39:59'),
(9, 'Education', 'COE', '2019-07-10 02:39:59', '2019-07-10 02:39:59'),
(10, 'Science', 'COS', '2019-07-10 02:39:59', '2019-07-10 02:39:59'),
(11, 'Tourism and Hospitality Management', 'CTHM', '2019-07-10 02:39:59', '2019-07-10 02:39:59'),
(12, 'Technology Management', 'CTM', '2019-07-10 02:39:59', '2019-07-10 02:39:59'),
(13, 'Arts and Letters', 'CAL', '2019-07-10 02:39:59', '2019-07-10 02:39:59');

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE IF NOT EXISTS `companies` (
  `id` bigint(20) unsigned NOT NULL,
  `company_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `industry` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `company_name`, `logo`, `industry`, `url`, `created_at`, `updated_at`) VALUES
(1, 'Radiantixxxxx', 'http://web.utrack/images/200x200.png', 'Science', 'www.Radiantix.com', '2019-08-12 06:01:57', '2019-08-14 09:02:56'),
(2, 'Parleynet', 'http://web.utrack/images/200x200.png', 'IT', 'www.Parleynet.com', '2019-08-12 06:01:57', '2019-08-12 06:01:57'),
(3, 'Izzby', 'http://web.utrack/images/200x200.png', 'IT', 'www.Izzby.com', '2019-08-12 06:01:57', '2019-08-12 06:01:57'),
(4, 'Verton', 'http://web.utrack/images/200x200.png', 'Law', 'www.Verton.com', '2019-08-12 06:01:57', '2019-08-12 06:01:57'),
(5, 'Imageflow', 'http://web.utrack/images/200x200.png', 'Entertainment', 'www.Imageflow.com', '2019-08-12 06:01:57', '2019-08-12 06:01:57'),
(6, 'Comfirm', 'http://web.utrack/images/200x200.png', 'Law', 'www.Comfirm.com', '2019-08-12 06:01:57', '2019-08-12 06:01:57'),
(7, 'Futuris', 'http://web.utrack/images/200x200.png', 'Law', 'www.Futuris.com', '2019-08-12 06:01:57', '2019-08-12 06:01:57'),
(8, 'Valpreal', 'http://web.utrack/images/200x200.png', 'Science', 'www.Valpreal.com', '2019-08-12 06:01:57', '2019-08-12 06:01:57'),
(9, 'Digial', 'http://web.utrack/images/200x200.png', 'Entertainment', 'www.Digial.com', '2019-08-12 06:01:57', '2019-08-12 06:01:57'),
(10, 'Daycore', 'http://web.utrack/images/200x200.png', 'Science', 'www.Daycore.com', '2019-08-12 06:01:57', '2019-08-12 06:01:57');

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE IF NOT EXISTS `course` (
  `id` bigint(20) unsigned NOT NULL,
  `degree` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `college` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`id`, `degree`, `name`, `college`, `created_at`, `updated_at`) VALUES
(1, 'Associate', 'Associate of Applied Science in Pharmacy Technology', 'COAHS', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(2, 'Bachelor', 'Bachelor of Science in Nursing', 'COAHS', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(3, 'Bachelor', 'Bachelor of Science in Radiologic Technology', 'COAHS', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(4, 'Bachelor', 'Bachelor of Science in Pharmacy', 'COAHS', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(5, 'Master', 'Master of Arts in Nursing', 'COAHS', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(6, 'Master', 'Master of Science in Radiologic Technology', 'COAHS', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(7, 'Master', 'Master of Business Administration in Healthcare Management', 'COAHS', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(8, 'Bachelor', 'Bachelor of Arts in Communication and Service Management', 'CAL', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(9, 'Bachelor', 'Bachelor in Multimedia Arts Major in Film', 'CAL', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(10, 'Bachelor', 'Bachelor in Multimedia Arts Major in Animation', 'CAL', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(11, 'Associate', 'Associate in Customer Service Communication', 'CAL', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(12, 'Bachelor', 'Bachelor of Science in Business Administration Major in Building and Property Management', 'CBFS', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(13, 'Bachelor', 'Bachelor of Science in Business Administration Major in Supply Management', 'CBFS', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(14, 'Bachelor', 'Bachelor of Science in Business Administration Major in Marketing Managment', 'CBFS', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(15, 'Bachelor', 'Bachelor of Science in Accountancy', 'CBFS', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(16, 'Bachelor', 'Bachelor of Science in Management Accounting', 'CBFS', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(17, 'Bachelor', 'Bachelor of Science in Financial Management', 'CBFS', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(18, 'Bachelor', 'Bachelor of Science in Supply Management', 'CBFS', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(19, 'Bachelor', 'Bachelor of Science in Office Administration', 'CBFS', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(20, 'Associate', 'Associate in Building and Property Management', 'CBFS', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(21, 'Associate', 'Associate in Entrepreneurship', 'CBFS', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(22, 'Associate', 'Associate in Office Management Technology', 'CBFS', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(23, 'Associate', 'Associate in Sales Management', 'CBFS', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(24, 'Associate', 'Associate in Supply Management', 'CBFS', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(25, 'Bachelor', 'Bachelor of Science in Computer Science Major in Application Development', 'CCS', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(26, 'Bachelor', 'Bachelor of Science in Computer Science Major in Social Computing', 'CCS', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(27, 'Bachelor', 'Bachelor of Science in Computational and Data Sciences', 'CCS', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(28, 'Bachelor', 'Bachelor of Science in Information Technology Major in Information and Network Security', 'CCS', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(29, 'Diploma', 'Diploma in Application Development', 'CCS', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(30, 'Diploma', 'Diploma in Computer Network Administration', 'CCS', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(31, 'Bachelor', 'Bachelor of Science in Civil Engineering', 'CCSCE', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(32, 'Bachelor', 'Bachelor of Science in Construction Engineering Technology (Ladderized Program)', 'CCSCE', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(33, 'Diploma', 'Diploma in Construction Engineering Management', 'CCSCE', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(34, 'Bachelor', 'Bachelor in Elementary Education Major in Early Childhood Education', 'COE', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(35, 'Bachelor', 'Bachelor in Elementary Education Major in Special Education', 'COE', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(36, 'Bachelor', 'Bachelor of Secondary Education Major in Filipino', 'COE', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(37, 'Bachelor', 'Bachelor of Secondary Education Major in General Science', 'COE', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(38, 'Bachelor', 'Bachelor of Secondary Education Major in English', 'COE', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(39, 'Bachelor', 'Bachelor of Secondary Education Major in Mathematics', 'COE', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(40, 'Bachelor', 'Bachelor of Secondary Education Major in Social Studies', 'COE', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(41, 'Master', 'Master of Arts in Education Major in Educational Management', 'COE', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(42, 'Master', 'Master of Arts in Education Major in Guidance and Counseling', 'COE', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(43, 'Master', 'Master of Arts in Special Education Major in Mental Retardation and Autism', 'COE', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(44, 'Doctoral', 'Doctor of Education Major in Innovative Education Management', 'COE', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(45, 'Doctoral', 'Doctor of Philosophy in Special Education', 'COE', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(46, 'Bachelor', 'Bachelor of Arts in Political Science Major in Local Government Administration', 'CGPP', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(47, 'Bachelor', 'Bachelor of Arts in Political Science Major in Policy Management', 'CGPP', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(48, 'Bachelor', 'Bachelor of Arts in Political Science Major in Paralegal Studies', 'CGPP', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(49, 'Certificate', 'Certificate in Barangay Governance(Modular Program)', 'CGPP', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(50, 'Master', 'Master in Public Administration Major in Local Governance(regular)', 'CGPP', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(51, 'Master', 'Master in Public Administration Major in Local Governance(modular)', 'CGPP', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(52, 'Diploma', 'Diploma in Marine Transportation', 'CMLI', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(53, 'Bachelor', 'Bachelor’s Degree in Marine Transportation', 'CMLI', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(54, 'Diploma', 'Diploma in Marine Engineering', 'CMLI', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(55, 'Bachelor', 'Bachelor’s Degree in Marine Engineering', 'CMLI', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(56, 'Bachelor', 'Bachelor of Science in Psychology', 'COS', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(57, 'Bachelor', 'Bachelor of Science in Building Technology Management', 'CTM', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(58, 'Bachelor', 'Bachelor of Science in Electrical Technology', 'CTM', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(59, 'Bachelor', 'Bachelor of Science in Electronics and Telecommunication Technology', 'CTM', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(60, 'Bachelor', 'Bachelor in Automotive Technology', 'CTM', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(61, 'Diploma', 'Diploma in Industrial Facilities Technology', 'CTM', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(62, 'Diploma', 'Diploma in Electrical Technology', 'CTM', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(63, 'Associate', 'Associate in Electronics Technology', 'CTM', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(64, 'Certificate', 'Certificate in Building Technology Management', 'CTM', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(65, 'Bachelor', 'Bachelor of Science in Hospitality Management', 'CTHM', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(66, 'Bachelor', 'Bachelor of Science in Tourism Management', 'CTHM', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(67, 'Associate', 'Associate in Hospitality Management', 'CTHM', '2020-01-09 10:19:17', '2020-01-09 10:19:17'),
(68, 'Bachelor', 'Bachelor in Physical Wellness Major in Sports Management', 'CHK', '2020-01-09 10:19:17', '2020-01-09 10:19:17');

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE IF NOT EXISTS `feedback` (
  `id` bigint(20) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `first_job`
--

CREATE TABLE IF NOT EXISTS `first_job` (
  `id` bigint(20) unsigned NOT NULL,
  `uuid` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_first_job` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reason_accepting` text COLLATE utf8mb4_unicode_ci,
  `how_long_did_take` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `competency_skills` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `first_job`
--

INSERT INTO `first_job` (`id`, `uuid`, `is_first_job`, `reason_accepting`, `how_long_did_take`, `competency_skills`, `created_at`, `updated_at`) VALUES
(1, '246a1a08-20a7-4746-9dda-c667c262c3fd', 'no', 'Other reason(s)', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-04 09:07:14', '2020-01-04 09:07:14'),
(2, '7f94bdd3-3812-4ca9-9c0d-78f7a1e8ac6e', 'no', 'Salaries and benefits', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-04 09:07:15', '2020-01-04 09:07:15'),
(3, 'bfb72799-77b6-4d77-9dcd-55c0473650e7', 'yes', 'Salaries and benefits', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-04 09:07:15', '2020-01-04 09:07:15'),
(4, 'ebc09d94-d2fb-4061-b6ce-080b6003072a', 'yes', 'Salaries and benefits', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-04 09:07:15', '2020-01-04 09:07:15'),
(5, '315fd273-1148-47af-b011-d4648105dcb2', 'yes', 'Other reason(s)', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-04 09:07:15', '2020-01-04 09:07:15'),
(6, '2f97d25b-d096-435b-b131-a72434ca8831', 'no', 'Salaries and benefits', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-04 09:07:15', '2020-01-04 09:07:15'),
(7, 'f5b603b4-b7f4-4e05-938b-3aec08b669c4', 'yes', 'Salaries and benefits', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-04 09:07:15', '2020-01-04 09:07:15'),
(8, 'eab45238-b689-4117-9ffa-f6da65b50409', 'yes', 'Salaries and benefits, Carrer challenge, Related to special skill, Related to course or program of study, Proximity to residence, Peer influence, Family influence, Other reason(s)', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-04 09:20:11', '2020-01-04 09:20:11'),
(9, 'b40b1775-2084-464f-bd6d-9ca6dd6777f1', 'no', 'Salaries and benefits', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-10 01:59:48', '2020-01-10 01:59:48'),
(10, '815b951d-b617-4f11-846c-96c3b6b7497a', 'no', 'Other reason(s)', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-10 01:59:48', '2020-01-10 01:59:48'),
(11, '0884c613-ff09-4420-8ca9-393cfba9093c', 'yes', 'Other reason(s)', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-10 01:59:48', '2020-01-10 01:59:48'),
(12, '3819cd5b-1a0b-4a65-9708-d49463c5ad2c', 'yes', 'Other reason(s)', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-10 01:59:48', '2020-01-10 01:59:48'),
(13, '3e8a87c8-82fe-4689-9fc6-a128cb0f386b', 'no', 'Other reason(s)', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-10 01:59:48', '2020-01-10 01:59:48'),
(14, 'b40b1775-2084-464f-bd6d-9ca6dd6777f1', 'no', 'Salaries and benefits', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-10 14:02:51', '2020-01-10 14:02:51'),
(15, '815b951d-b617-4f11-846c-96c3b6b7497a', 'no', 'Other reason(s)', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-10 14:02:51', '2020-01-10 14:02:51'),
(16, '0884c613-ff09-4420-8ca9-393cfba9093c', 'yes', 'Other reason(s)', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-10 14:02:51', '2020-01-10 14:02:51'),
(17, '3819cd5b-1a0b-4a65-9708-d49463c5ad2c', 'yes', 'Other reason(s)', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-10 14:02:51', '2020-01-10 14:02:51'),
(18, '3e8a87c8-82fe-4689-9fc6-a128cb0f386b', 'no', 'Other reason(s)', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-10 14:02:52', '2020-01-10 14:02:52'),
(19, '246a1a08-20a7-4746-9dda-c667c262c3fd', 'no', 'Other reason(s)', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-10 14:03:31', '2020-01-10 14:03:31'),
(20, '7f94bdd3-3812-4ca9-9c0d-78f7a1e8ac6e', 'no', 'Salaries and benefits', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-10 14:03:31', '2020-01-10 14:03:31'),
(21, 'bfb72799-77b6-4d77-9dcd-55c0473650e7', 'yes', 'Salaries and benefits', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-10 14:03:31', '2020-01-10 14:03:31'),
(22, 'ebc09d94-d2fb-4061-b6ce-080b6003072a', 'yes', 'Salaries and benefits', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-10 14:03:31', '2020-01-10 14:03:31'),
(23, '315fd273-1148-47af-b011-d4648105dcb2', 'yes', 'Other reason(s)', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-10 14:03:31', '2020-01-10 14:03:31'),
(24, '2f97d25b-d096-435b-b131-a72434ca8831', 'no', 'Salaries and benefits', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-10 14:03:31', '2020-01-10 14:03:31'),
(25, 'f5b603b4-b7f4-4e05-938b-3aec08b669c4', 'yes', 'Salaries and benefits', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-10 14:03:31', '2020-01-10 14:03:31'),
(26, '2d859ea3-2bd3-48cd-a7a8-b1c6b0c50394', 'yes', 'Salaries and benefits, Carrer challenge, Related to special skill, Related to course or program of study, Proximity to residence, Peer influence, Family influence, Other reason(s)', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-11 00:41:28', '2020-01-11 00:41:28'),
(27, '246a1a08-20a7-4746-9dda-c667c262c3fd', 'no', 'Other reason(s)', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-11 00:46:44', '2020-01-11 00:46:44'),
(28, '7f94bdd3-3812-4ca9-9c0d-78f7a1e8ac6e', 'no', 'Salaries and benefits', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-11 00:46:44', '2020-01-11 00:46:44'),
(29, 'bfb72799-77b6-4d77-9dcd-55c0473650e7', 'yes', 'Salaries and benefits', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-11 00:46:44', '2020-01-11 00:46:44'),
(30, 'ebc09d94-d2fb-4061-b6ce-080b6003072a', 'yes', 'Salaries and benefits', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-11 00:46:44', '2020-01-11 00:46:44'),
(31, '315fd273-1148-47af-b011-d4648105dcb2', 'yes', 'Other reason(s)', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-11 00:46:44', '2020-01-11 00:46:44'),
(32, '2f97d25b-d096-435b-b131-a72434ca8831', 'no', 'Salaries and benefits', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-11 00:46:45', '2020-01-11 00:46:45'),
(33, 'f5b603b4-b7f4-4e05-938b-3aec08b669c4', 'yes', 'Salaries and benefits', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-11 00:46:45', '2020-01-11 00:46:45'),
(34, 'b40b1775-2084-464f-bd6d-9ca6dd6777f1', 'no', 'Salaries and benefits', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-11 00:46:50', '2020-01-11 00:46:50'),
(35, '815b951d-b617-4f11-846c-96c3b6b7497a', 'no', 'Other reason(s)', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-11 00:46:50', '2020-01-11 00:46:50'),
(36, '0884c613-ff09-4420-8ca9-393cfba9093c', 'yes', 'Other reason(s)', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-11 00:46:50', '2020-01-11 00:46:50'),
(37, '3819cd5b-1a0b-4a65-9708-d49463c5ad2c', 'yes', 'Other reason(s)', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-11 00:46:50', '2020-01-11 00:46:50'),
(38, '3e8a87c8-82fe-4689-9fc6-a128cb0f386b', 'no', 'Other reason(s)', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-11 00:46:50', '2020-01-11 00:46:50'),
(39, '246a1a08-20a7-4746-9dda-c667c262c3fd', 'no', 'Other reason(s)', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-11 00:48:20', '2020-01-11 00:48:20'),
(40, '7f94bdd3-3812-4ca9-9c0d-78f7a1e8ac6e', 'no', 'Salaries and benefits', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-11 00:48:20', '2020-01-11 00:48:20'),
(41, 'bfb72799-77b6-4d77-9dcd-55c0473650e7', 'yes', 'Salaries and benefits', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-11 00:48:20', '2020-01-11 00:48:20'),
(42, 'ebc09d94-d2fb-4061-b6ce-080b6003072a', 'yes', 'Salaries and benefits', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-11 00:48:20', '2020-01-11 00:48:20'),
(43, '315fd273-1148-47af-b011-d4648105dcb2', 'yes', 'Other reason(s)', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-11 00:48:20', '2020-01-11 00:48:20'),
(44, '2f97d25b-d096-435b-b131-a72434ca8831', 'no', 'Salaries and benefits', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-11 00:48:20', '2020-01-11 00:48:20'),
(45, 'f5b603b4-b7f4-4e05-938b-3aec08b669c4', 'yes', 'Salaries and benefits', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-11 00:48:20', '2020-01-11 00:48:20'),
(46, 'b40b1775-2084-464f-bd6d-9ca6dd6777f1', 'no', 'Salaries and benefits', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-11 00:48:34', '2020-01-11 00:48:34'),
(47, '815b951d-b617-4f11-846c-96c3b6b7497a', 'no', 'Other reason(s)', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-11 00:48:34', '2020-01-11 00:48:34'),
(48, '0884c613-ff09-4420-8ca9-393cfba9093c', 'yes', 'Other reason(s)', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-11 00:48:34', '2020-01-11 00:48:34'),
(49, '3819cd5b-1a0b-4a65-9708-d49463c5ad2c', 'yes', 'Other reason(s)', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-11 00:48:34', '2020-01-11 00:48:34'),
(50, '3e8a87c8-82fe-4689-9fc6-a128cb0f386b', 'no', 'Other reason(s)', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-11 00:48:34', '2020-01-11 00:48:34'),
(51, 'f72084b0-88f4-4b15-a3bd-b72c12c2d80d', 'no', 'Salaries and benefits', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-11 03:14:57', '2020-01-11 03:14:57'),
(52, '36903c28-d10b-4474-91cb-293e1e9f5e9a', 'yes', 'Salaries and benefits', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-11 03:14:57', '2020-01-11 03:14:57'),
(53, '6d105b20-a4a7-46b1-8952-ee865d3dde64', 'yes', 'Salaries and benefits', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-11 03:14:57', '2020-01-11 03:14:57'),
(54, '8c11848e-4e17-4252-8388-8b0f1a99f97f', 'yes', 'Salaries and benefits', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-11 03:14:57', '2020-01-11 03:14:57'),
(55, '4b235fec-c48f-4481-89a9-7258ed2debee', 'no', 'Salaries and benefits', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-11 03:14:57', '2020-01-11 03:14:57'),
(56, 'f9ae5701-c975-449c-a0e7-f33cb1c13725', 'no', 'Salaries and benefits', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-11 03:14:57', '2020-01-11 03:14:57'),
(57, 'e13ba779-66a2-4b09-9ea6-9e7ef72d6527', 'yes', 'Salaries and benefits', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-11 03:14:57', '2020-01-11 03:14:57'),
(58, '7885d8ed-46f7-40ac-8a87-c0be1609987a', 'no', 'Other reason(s)', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-11 03:15:07', '2020-01-11 03:15:07'),
(59, '83e45252-933d-4e53-b151-09b0fcd8870f', 'yes', 'Other reason(s)', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-11 03:15:07', '2020-01-11 03:15:07'),
(60, 'd137e4da-9848-46a1-b21a-a7bcafa7edaa', 'no', 'Other reason(s)', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-11 03:15:07', '2020-01-11 03:15:07'),
(61, '3f70e67a-e4d4-4295-815a-de5a57f50173', 'no', 'Salaries and benefits', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-11 03:15:08', '2020-01-11 03:15:08'),
(62, 'bb8eab42-2c97-4719-a97d-8b4f02a2df6f', 'no', 'Salaries and benefits', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-11 03:15:08', '2020-01-11 03:15:08'),
(63, '91f2f566-1860-48d8-b294-1f356b83f3d6', 'no', 'Salaries and benefits', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-11 03:15:08', '2020-01-11 03:15:08'),
(64, '72f2fe72-6a01-436e-bc4c-c06c1ef9d763', 'yes', 'Salaries and benefits, Carrer challenge, Related to special skill, Related to course or program of study, Proximity to residence, Peer influence, Family influence, Other reason(s)', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-19 01:43:23', '2020-01-19 01:43:23'),
(65, '7433160c-35c0-47cb-b2f3-0768bb5fb8ae', 'no', 'Salaries and benefits, Carrer challenge, Related to special skill, Related to course or program of study, Proximity to residence, Peer influence, Family influence, Other reason(s)', '1 to 3 months', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-19 06:09:35', '2020-01-19 06:09:35'),
(66, '65bc553b-84d5-42a4-9386-22b17e26f5c8', 'yes', 'Salaries and benefits, Carrer challenge, Related to special skill, Related to course or program of study, Proximity to residence, Peer influence, Family influence, Other reason(s)', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-19 06:19:49', '2020-01-19 06:19:49'),
(67, '06fda258-a3e9-4015-bb60-c930886f15b8', 'yes', 'Salaries and benefits, Carrer challenge, Related to special skill, Related to course or program of study, Proximity to residence, Peer influence, Family influence, Other reason(s)', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-19 06:28:23', '2020-01-19 06:28:23'),
(68, '6ea76019-00ef-4813-9144-52739c0d0747', 'yes', 'Salaries and benefits, Carrer challenge, Related to special skill, Related to course or program of study, Proximity to residence, Peer influence, Family influence, Other reason(s)', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-19 06:39:56', '2020-01-19 06:39:56'),
(69, 'ee2c034c-b55d-47cf-96f4-ddbad2388824', 'yes', 'Salaries and benefits, Carrer challenge, Related to special skill, Related to course or program of study, Proximity to residence, Peer influence, Family influence, Other reason(s)', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-19 06:47:05', '2020-01-19 06:47:05'),
(70, '78a43040-9c61-43f3-b638-864b7d77b5b8', 'yes', 'Salaries and benefits, Carrer challenge, Related to special skill, Related to course or program of study, Proximity to residence, Peer influence, Family influence, Other reason(s)', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-26 00:11:25', '2020-01-26 00:11:25'),
(71, 'dfe09934-43de-4a8c-bbe9-30da107e41b2', 'yes', 'Salaries and benefits, Carrer challenge, Related to special skill, Related to course or program of study, Proximity to residence, Peer influence, Family influence, Other reason(s)', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-27 07:45:39', '2020-01-27 07:45:39'),
(72, 'b5d26459-e996-4c75-9aeb-390fc54d9c7e', 'yes', 'Salaries and benefits, Carrer challenge, Related to special skill, Related to course or program of study, Proximity to residence, Peer influence, Family influence, Other reason(s)', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-27 08:00:05', '2020-01-27 08:00:05'),
(73, '60ed6828-75fc-4210-ba05-2adffc8c7783', 'yes', 'Salaries and benefits, Carrer challenge, Related to special skill, Related to course or program of study, Proximity to residence, Peer influence, Family influence, Other reason(s)', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-27 15:36:23', '2020-01-27 15:36:23'),
(74, 'cd701f39-60b9-458e-9389-6b3a0bc7cec7', 'yes', 'Salaries and benefits, Carrer challenge, Related to special skill, Related to course or program of study, Proximity to residence, Peer influence, Family influence, Other reason(s)', 'Less than a month', 'Communication Skills, Human Relations Skills, Entrepreneurial Skills, Information Technology Skills, Problem-solving Skills, Critical Thinking Skills, Other Skills', '2020-01-27 15:57:24', '2020-01-27 15:57:24'),
(75, 'd1760aee-ea97-4a7e-8400-168d05adb2e7', 'yes', 'Salaries and benefits', 'Less than a month', 'Communication Skills, Human Relations Skills', '2020-02-02 17:22:02', '2020-02-02 17:22:02');

-- --------------------------------------------------------

--
-- Table structure for table `graduates`
--

CREATE TABLE IF NOT EXISTS `graduates` (
  `id` bigint(20) unsigned NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `middle_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `degree` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `college` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `course` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `year_graduated` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `graduates`
--

INSERT INTO `graduates` (`id`, `first_name`, `middle_name`, `last_name`, `gender`, `degree`, `college`, `course`, `year_graduated`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Jenifer', 'Schuppe', 'Reynolds', 'Female', 'Diploma', 'CCS', 'Diploma in Computer Network Administration', '2019', NULL, '2020-02-03 00:46:33', '2020-02-03 00:46:33'),
(2, 'Duncan', 'Marks', 'Batz', 'Male', 'Bachelor', 'CCS', 'Diploma in Computer Network Administration', '2019', NULL, '2020-02-03 00:46:33', '2020-02-03 00:46:33'),
(3, 'Cecile', 'Ruecker', 'Hermiston', 'Female', 'Bachelor', 'CCS', 'Diploma in Application Development', '2019', NULL, '2020-02-03 00:46:33', '2020-02-03 00:46:33'),
(4, 'Adelle', 'Dare', 'Blick', 'Female', 'Diploma', 'CCS', 'Bachelor of Science in Computer Science Major in Application Development', '2019', NULL, '2020-02-03 00:46:33', '2020-02-03 00:46:33'),
(5, 'Deon', 'Upton', 'Rau', 'Male', 'Bachelor', 'CCS', 'Diploma in Computer Network Administration', '2019', NULL, '2020-02-03 00:46:33', '2020-02-03 00:46:33'),
(6, 'Nettie', 'Schinner', 'Heidenreich', 'Female', 'Diploma', 'CCS', 'Diploma in Application Development', '2019', NULL, '2020-02-03 00:46:33', '2020-02-03 00:46:33'),
(7, 'Brenna', 'Hickle', 'Steuber', 'Female', 'Diploma', 'CCS', 'Diploma in Computer Network Administration', '2019', NULL, '2020-02-03 00:46:33', '2020-02-03 00:46:33'),
(8, 'Jo', 'Ruecker', 'Glover', 'Male', 'Bachelor', 'CCS', 'Diploma in Computer Network Administration', '2019', NULL, '2020-02-03 00:46:33', '2020-02-03 00:46:33'),
(9, 'Yvette', 'Wunsch', 'Schimmel', 'Female', 'Diploma', 'CCS', 'Bachelor of Science in Computer Science Major in Social Computing', '2019', NULL, '2020-02-03 00:46:33', '2020-02-03 00:46:33'),
(10, 'Clarabelle', 'Mitchell', 'Champlin', 'Female', 'Diploma', 'CCS', 'Diploma in Application Development', '2019', NULL, '2020-02-03 00:46:33', '2020-02-03 00:46:33'),
(11, 'Cara', 'Rippin', 'Kemmer', 'Female', 'Diploma', 'CCS', 'Bachelor of Science in Computer Science Major in Application Development', '2019', NULL, '2020-02-03 00:46:33', '2020-02-03 00:46:33'),
(12, 'Cali', 'Hermann', 'Price', 'Female', 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Application Development', '2019', NULL, '2020-02-03 00:46:33', '2020-02-03 00:46:33'),
(13, 'Tremayne', 'Smith', 'Howe', 'Male', 'Bachelor', 'CCS', 'Bachelor of Science in Information Technology Major in Information and Network Security', '2019', NULL, '2020-02-03 00:46:33', '2020-02-03 00:46:33'),
(14, 'Jairo', 'Lubowitz', 'McDermott', 'Male', 'Bachelor', 'CCS', 'Bachelor of Science in Information Technology Major in Information and Network Security', '2019', NULL, '2020-02-03 00:46:33', '2020-02-03 00:46:33'),
(15, 'Shawna', 'Fritsch', 'Thiel', 'Female', 'Bachelor', 'CCS', 'Diploma in Application Development', '2019', NULL, '2020-02-03 00:46:33', '2020-02-03 00:46:33'),
(16, 'Danny', 'Muller', 'Mayer', 'Male', 'Bachelor', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', NULL, '2020-02-03 00:46:33', '2020-02-03 00:46:33'),
(17, 'Anya', 'Frami', 'O''Reilly', 'Female', 'Diploma', 'CCS', 'Diploma in Computer Network Administration', '2019', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(18, 'Dejon', 'Legros', 'Hagenes', 'Male', 'Diploma', 'CCS', 'Bachelor of Science in Computer Science Major in Social Computing', '2019', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(19, 'Freida', 'Kris', 'Jacobson', 'Female', 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Application Development', '2019', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(20, 'Logan', 'King', 'Gutkowski', 'Male', 'Diploma', 'CCS', 'Bachelor of Science in Computer Science Major in Application Development', '2019', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(21, 'Myra', 'Cormier', 'Gislason', 'Female', 'Bachelor', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(22, 'Bonnie', 'Hartmann', 'Kohler', 'Female', 'Bachelor', 'CCS', 'Bachelor of Science in Information Technology Major in Information and Network Security', '2019', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(23, 'Tessie', 'Ziemann', 'Barrows', 'Female', 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Social Computing', '2019', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(24, 'Ed', 'Watsica', 'Schneider', 'Male', 'Bachelor', 'CCS', 'Diploma in Application Development', '2019', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(25, 'Lupe', 'Howell', 'Shanahan', 'Female', 'Bachelor', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(26, 'Kelsi', 'Hane', 'Hermiston', 'Female', 'Bachelor', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(27, 'Natalia', 'Simonis', 'Stracke', 'Female', 'Diploma', 'CCS', 'Bachelor of Science in Computer Science Major in Application Development', '2019', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(28, 'Nash', 'Osinski', 'Champlin', 'Male', 'Bachelor', 'CCS', 'Diploma in Computer Network Administration', '2019', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(29, 'Jaiden', 'Beahan', 'Renner', 'Male', 'Bachelor', 'CCS', 'Diploma in Computer Network Administration', '2019', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(30, 'Tristin', 'Mann', 'Funk', 'Male', 'Bachelor', 'CCS', 'Diploma in Computer Network Administration', '2019', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(31, 'Kristy', 'Schumm', 'Heaney', 'Female', 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Social Computing', '2019', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(32, 'Flossie', 'Yost', 'Bode', 'Female', 'Bachelor', 'CCS', 'Bachelor of Science in Information Technology Major in Information and Network Security', '2019', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(33, 'Devin', 'Dach', 'Haag', 'Male', 'Bachelor', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(34, 'Myrl', 'Kilback', 'Kub', 'Male', 'Diploma', 'CCS', 'Diploma in Application Development', '2019', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(35, 'Cristina', 'Greenfelder', 'Ruecker', 'Male', 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Social Computing', '2019', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(36, 'Seamus', 'Wehner', 'Abbott', 'Male', 'Bachelor', 'CCS', 'Diploma in Computer Network Administration', '2019', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(37, 'Dasia', 'Russel', 'Casper', 'Female', 'Diploma', 'CCS', 'Diploma in Application Development', '2019', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(38, 'Alisa', 'Collier', 'Miller', 'Female', 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Social Computing', '2019', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(39, 'Ursula', 'Rippin', 'Ullrich', 'Female', 'Diploma', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(40, 'Daryl', 'Powlowski', 'Langosh', 'Male', 'Bachelor', 'CCS', 'Diploma in Computer Network Administration', '2019', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(41, 'Jonathan', 'Marquardt', 'Mohr', 'Male', 'Bachelor', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(42, 'Name', 'Nikolaus', 'Halvorson', 'Female', 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Application Development', '2019', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(43, 'Bernice', 'Kovacek', 'Zulauf', 'Female', 'Diploma', 'CCS', 'Diploma in Application Development', '2019', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(44, 'Irwin', 'Mayer', 'Labadie', 'Male', 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Application Development', '2019', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(45, 'Jayce', 'Torp', 'Nader', 'Male', 'Bachelor', 'CCS', 'Diploma in Application Development', '2019', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(46, 'Dessie', 'Witting', 'Lebsack', 'Female', 'Diploma', 'CCS', 'Bachelor of Science in Computer Science Major in Application Development', '2019', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(47, 'Faye', 'Macejkovic', 'Heidenreich', 'Female', 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Application Development', '2019', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(48, 'Olin', 'Abernathy', 'Harris', 'Male', 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Social Computing', '2019', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(49, 'Joey', 'Marvin', 'Lesch', 'Male', 'Bachelor', 'CCS', 'Diploma in Computer Network Administration', '2019', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(50, 'Stefan', 'Ortiz', 'Schimmel', 'Male', 'Bachelor', 'CCS', 'Diploma in Application Development', '2019', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(51, 'Meghan', 'Crooks', 'Cummings', 'Female', 'Bachelor', 'CCS', 'Diploma in Computer Network Administration', '2019', NULL, '2020-02-03 00:46:49', '2020-02-03 00:46:49'),
(52, 'Katrina', 'Brown', 'Auer', 'Female', 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Application Development', '2019', NULL, '2020-02-03 00:46:49', '2020-02-03 00:46:49'),
(53, 'Stuart', 'Schultz', 'Schultz', 'Male', 'Bachelor', 'CCS', 'Diploma in Application Development', '2019', NULL, '2020-02-03 00:46:49', '2020-02-03 00:46:49'),
(54, 'Adella', 'Cummings', 'Deckow', 'Female', 'Diploma', 'CCS', 'Bachelor of Science in Information Technology Major in Information and Network Security', '2019', NULL, '2020-02-03 00:46:49', '2020-02-03 00:46:49'),
(55, 'Adonis', 'Barton', 'Veum', 'Male', 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Social Computing', '2019', NULL, '2020-02-03 00:46:49', '2020-02-03 00:46:49'),
(56, 'Jazmyn', 'Oberbrunner', 'Schoen', 'Female', 'Bachelor', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', NULL, '2020-02-03 00:46:49', '2020-02-03 00:46:49'),
(57, 'Camilla', 'Schumm', 'Jast', 'Female', 'Bachelor', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', NULL, '2020-02-03 00:46:49', '2020-02-03 00:46:49'),
(58, 'Rex', 'Gutmann', 'Adams', 'Male', 'Diploma', 'CCS', 'Bachelor of Science in Computer Science Major in Application Development', '2019', NULL, '2020-02-03 00:46:49', '2020-02-03 00:46:49'),
(59, 'Wyman', 'Thompson', 'Cummings', 'Male', 'Bachelor', 'CCS', 'Bachelor of Science in Information Technology Major in Information and Network Security', '2019', NULL, '2020-02-03 00:46:49', '2020-02-03 00:46:49'),
(60, 'Logan', 'Kohler', 'Kris', 'Male', 'Diploma', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', NULL, '2020-02-03 00:46:49', '2020-02-03 00:46:49'),
(61, 'Fabiola', 'Sawayn', 'Klein', 'Female', 'Bachelor', 'CCS', 'Bachelor of Science in Information Technology Major in Information and Network Security', '2019', NULL, '2020-02-03 00:46:49', '2020-02-03 00:46:49'),
(62, 'Laurence', 'Barton', 'Kirlin', 'Female', 'Bachelor', 'CCS', 'Bachelor of Science in Information Technology Major in Information and Network Security', '2019', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(63, 'Destiny', 'Lebsack', 'Sawayn', 'Female', 'Bachelor', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(64, 'Pamela', 'Fahey', 'Hirthe', 'Female', 'Diploma', 'CCS', 'Bachelor of Science in Computer Science Major in Application Development', '2019', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(65, 'Amparo', 'Will', 'Schowalter', 'Male', 'Diploma', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(66, 'Iva', 'Johns', 'Hermann', 'Female', 'Bachelor', 'CCS', 'Diploma in Application Development', '2019', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(67, 'Marina', 'Kling', 'Marquardt', 'Female', 'Bachelor', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(68, 'Stephanie', 'Kirlin', 'Flatley', 'Female', 'Diploma', 'CCS', 'Diploma in Application Development', '2019', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(69, 'Neal', 'Boehm', 'Jakubowski', 'Male', 'Diploma', 'CCS', 'Diploma in Application Development', '2019', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(70, 'Jacklyn', 'Little', 'Skiles', 'Female', 'Bachelor', 'CCS', 'Bachelor of Science in Information Technology Major in Information and Network Security', '2019', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(71, 'Reba', 'Crona', 'Bahringer', 'Female', 'Bachelor', 'CCS', 'Diploma in Application Development', '2019', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(72, 'Mathias', 'Parker', 'Nader', 'Male', 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Application Development', '2019', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(73, 'Norbert', 'Bailey', 'Mueller', 'Male', 'Bachelor', 'CCS', 'Diploma in Application Development', '2019', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(74, 'Mae', 'Armstrong', 'Conn', 'Female', 'Diploma', 'CCS', 'Bachelor of Science in Information Technology Major in Information and Network Security', '2019', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(75, 'Murray', 'Walter', 'Bergnaum', 'Male', 'Bachelor', 'CCS', 'Diploma in Computer Network Administration', '2019', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(76, 'Bernice', 'Orn', 'Will', 'Female', 'Bachelor', 'CCS', 'Diploma in Application Development', '2019', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(77, 'Dale', 'McCullough', 'Beier', 'Male', 'Bachelor', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(78, 'Earline', 'Willms', 'Pacocha', 'Female', 'Diploma', 'CCS', 'Diploma in Application Development', '2019', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(79, 'Aglae', 'Maggio', 'Macejkovic', 'Female', 'Bachelor', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(80, 'Eugene', 'Hyatt', 'Kris', 'Male', 'Bachelor', 'CCS', 'Bachelor of Science in Information Technology Major in Information and Network Security', '2019', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(81, 'Dallas', 'Fritsch', 'Purdy', 'Male', 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Social Computing', '2019', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(82, 'Ezequiel', 'Hodkiewicz', 'Dicki', 'Male', 'Diploma', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(83, 'Anibal', 'Effertz', 'Runolfsdottir', 'Male', 'Bachelor', 'CCS', 'Bachelor of Science in Information Technology Major in Information and Network Security', '2019', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(84, 'Javon', 'Rowe', 'Bogisich', 'Male', 'Bachelor', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(85, 'Christop', 'Klein', 'DuBuque', 'Male', 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Application Development', '2019', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(86, 'Sim', 'Weissnat', 'Mertz', 'Male', 'Bachelor', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(87, 'Jalen', 'Langosh', 'Cormier', 'Male', 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Social Computing', '2019', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(88, 'Edgar', 'Reynolds', 'Connelly', 'Male', 'Bachelor', 'CCS', 'Diploma in Computer Network Administration', '2019', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(89, 'Ryann', 'Mertz', 'Zboncak', 'Male', 'Bachelor', 'CCS', 'Diploma in Computer Network Administration', '2019', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(90, 'Abdullah', 'Luettgen', 'Rippin', 'Male', 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Application Development', '2019', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(91, 'Alfred', 'Murazik', 'Wiza', 'Male', 'Diploma', 'CCS', 'Bachelor of Science in Information Technology Major in Information and Network Security', '2019', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(92, 'Finn', 'Jakubowski', 'Hane', 'Male', 'Bachelor', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(93, 'Kendrick', 'Heathcote', 'Schmidt', 'Male', 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Application Development', '2019', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(94, 'Roderick', 'Schultz', 'Flatley', 'Male', 'Diploma', 'CCS', 'Bachelor of Science in Computer Science Major in Social Computing', '2019', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(95, 'Veda', 'Nitzsche', 'Quigley', 'Female', 'Bachelor', 'CCS', 'Bachelor of Science in Computational and Data Sciences', '2019', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(96, 'Lamont', 'Kerluke', 'Jakubowski', 'Male', 'Bachelor', 'CCS', 'Diploma in Computer Network Administration', '2019', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(97, 'Marion', 'Altenwerth', 'Bosco', 'Female', 'Diploma', 'CCS', 'Bachelor of Science in Information Technology Major in Information and Network Security', '2019', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(98, 'Stephania', 'Fahey', 'Erdman', 'Female', 'Diploma', 'CCS', 'Diploma in Application Development', '2019', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(99, 'Jaylen', 'Weimann', 'Huel', 'Male', 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Social Computing', '2019', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(100, 'Lea', 'Wyman', 'Swift', 'Female', 'Bachelor', 'CCS', 'Bachelor of Science in Computer Science Major in Social Computing', '2019', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50');

-- --------------------------------------------------------

--
-- Table structure for table `industries`
--

CREATE TABLE IF NOT EXISTS `industries` (
  `id` bigint(20) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `industries`
--

INSERT INTO `industries` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Manufacturing', '2020-01-15 14:18:28', '2020-01-15 14:18:28'),
(2, 'Finance / Banking', '2020-01-15 14:18:28', '2020-01-15 14:18:28'),
(3, 'Management', '2020-01-15 14:18:28', '2020-01-15 14:18:28'),
(4, 'Transportation', '2020-01-15 14:18:28', '2020-01-15 14:18:28'),
(5, 'Retail', '2020-01-15 14:18:28', '2020-01-15 14:18:28'),
(6, 'Infomation Technology / Computer Science', '2020-01-15 14:18:28', '2020-01-15 14:18:28'),
(7, 'Advertising', '2020-01-15 14:18:28', '2020-01-15 14:18:28'),
(8, 'Insurance', '2020-01-15 14:18:28', '2020-01-15 14:18:28'),
(9, 'Trade / Investment', '2020-01-15 14:18:28', '2020-01-15 14:18:28'),
(10, 'Sales / Marketing', '2020-01-15 14:18:28', '2020-01-15 14:18:28'),
(11, 'Telecommunication', '2020-01-15 14:18:28', '2020-01-15 14:18:28'),
(12, 'Publishing', '2020-01-15 14:18:28', '2020-01-15 14:18:28'),
(13, 'Food Industry', '2020-01-15 14:18:28', '2020-01-15 14:18:28'),
(14, 'Consultancy', '2020-01-15 14:18:28', '2020-01-15 14:18:28'),
(15, 'Wholesale', '2020-01-15 14:18:28', '2020-01-15 14:18:28'),
(16, 'Accounting', '2020-01-15 14:18:28', '2020-01-15 14:18:28'),
(17, 'Research & Development', '2020-01-15 14:18:28', '2020-01-15 14:18:28'),
(18, 'Logistics', '2020-01-15 14:18:28', '2020-01-15 14:18:28'),
(19, 'E-Commerce', '2020-01-15 14:18:28', '2020-01-15 14:18:28'),
(20, 'Other', '2020-01-15 14:18:28', '2020-01-15 14:18:28');

-- --------------------------------------------------------

--
-- Table structure for table `job_posts`
--

CREATE TABLE IF NOT EXISTS `job_posts` (
  `id` bigint(20) unsigned NOT NULL,
  `position` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `salary` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `requirements` text COLLATE utf8mb4_unicode_ci,
  `company_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `end_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `job_posts`
--

INSERT INTO `job_posts` (`id`, `position`, `salary`, `description`, `requirements`, `company_id`, `post_date`, `end_date`, `url`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'employee', '38,458.17', 'Reprehenderit enim consequat nostrud fugiat reprehenderit elit ipsum sit. Exercitation officia irure occaecat aute eiusmod minim excepteur pariatur magna reprehenderit laboris aute.', 'Pariatur in dolore non ut exercitation Lorem adipisicing enim pariatur aliqua veniam cillum. Excepteur nulla amet ut ipsum velit minim proident id ullamco.', '1', '09/06/2019', '09/13/2019', 'www.Radiantix.com', NULL, '2019-08-12 06:01:57', '2019-08-12 06:01:57'),
(2, 'manager', '19,113.22', 'Elit duis nostrud excepteur cupidatat commodo. Fugiat non eu aliquip adipisicing pariatur consequat reprehenderit Lorem eu.', 'Labore ipsum dolore cupidatat nulla ex est sint excepteur sint enim nulla ad incididunt. Labore labore aute magna nulla ullamco Lorem ullamco aute ea cupidatat commodo.', '1', '12/04/2019', '12/11/2019', 'www.Radiantix.com', NULL, '2019-08-12 06:01:57', '2019-08-12 06:01:57'),
(3, 'employee', '20,900.66', 'Elit exercitation culpa incididunt qui duis laboris officia aliquip occaecat commodo non ad est. Culpa id id elit exercitation ipsum.', 'Occaecat nulla velit proident cillum est qui. Commodo Lorem Lorem ut deserunt.', '1', '05/26/2019', '06/02/2019', 'www.Radiantix.com', NULL, '2019-08-12 06:01:57', '2019-08-12 06:01:57'),
(4, 'employee', '34,600.16', 'Consectetur velit proident non exercitation velit laborum. Lorem culpa eu cupidatat in excepteur officia.', 'Sunt sunt in sint velit ullamco incididunt. Nisi voluptate consectetur aliquip cillum laboris officia consequat.', '1', '02/03/2019', '02/10/2019', 'www.Radiantix.com', NULL, '2019-08-12 06:01:57', '2019-08-12 06:01:57'),
(5, 'secretary', '19,322.60', 'Eu deserunt sint eu aute eu dolore incididunt sint cillum aliqua tempor sint. Non labore ad do elit sint elit cupidatat aute ipsum quis commodo non cupidatat et.', 'Exercitation irure ad ullamco est culpa do esse. Nulla adipisicing commodo voluptate qui ad velit.', '1', '06/27/2019', '07/04/2019', 'www.Radiantix.com', NULL, '2019-08-12 06:01:57', '2019-08-12 06:01:57'),
(6, 'manager', '13,179.53', 'Occaecat est dolor elit ea ad dolore cupidatat ullamco occaecat duis cillum. Sunt non laboris quis pariatur elit consectetur anim Lorem pariatur veniam aliqua Lorem duis excepteur.', 'Cupidatat veniam pariatur veniam veniam dolor est et commodo reprehenderit excepteur. Ipsum adipisicing aliqua ipsum dolor.', '2', '02/18/2019', '02/25/2019', 'www.Parleynet.com', NULL, '2019-08-12 06:01:57', '2019-08-12 06:01:57'),
(7, 'employee', '10,898.43', 'Ullamco labore mollit do eu ex sint laborum excepteur consectetur. Nulla ex esse sit cillum proident exercitation aute cupidatat.', 'In ea elit aute consequat aute et dolore dolor ea. Ex consequat fugiat id consectetur dolor Lorem ullamco incididunt nisi velit officia aliquip sunt pariatur.', '2', '01/30/2020', '02/06/2020', 'www.Parleynet.com', NULL, '2019-08-12 06:01:57', '2019-08-12 06:01:57'),
(8, 'secretary', '30,279.28', 'Dolor occaecat consequat minim amet pariatur exercitation laboris ipsum voluptate irure ullamco. Eu excepteur ad labore nulla dolor eiusmod veniam tempor ut.', 'Fugiat reprehenderit eiusmod veniam excepteur dolore nulla dolore tempor reprehenderit eu et occaecat culpa. Ea laboris labore qui magna eiusmod reprehenderit nostrud id consequat laborum.', '2', '09/17/2019', '09/24/2019', 'www.Parleynet.com', NULL, '2019-08-12 06:01:57', '2019-08-12 06:01:57'),
(9, 'secretary', '24,537.04', 'Est veniam occaecat veniam mollit eu. Elit qui proident quis ex officia sunt sit proident amet deserunt aliqua esse.', 'Commodo sint duis amet consectetur aliqua. Laborum magna nisi laboris commodo ex consequat occaecat mollit do consectetur.', '2', '12/10/2019', '12/17/2019', 'www.Parleynet.com', NULL, '2019-08-12 06:01:57', '2019-08-12 06:01:57'),
(10, 'employee', '23,245.33', 'Sint quis sit ex occaecat ex reprehenderit in. Consequat anim elit mollit magna minim nisi do consectetur ipsum reprehenderit magna sunt consectetur aute.', 'Magna excepteur eiusmod excepteur duis occaecat ex. Ex esse laborum et veniam elit.', '2', '02/04/2019', '02/11/2019', 'www.Parleynet.com', NULL, '2019-08-12 06:01:57', '2019-08-12 06:01:57'),
(11, 'secretary', '17,230.62', 'Labore et sit pariatur ipsum anim ut aliqua et ex quis magna anim voluptate. Sunt labore Lorem Lorem id do do minim in sunt aute exercitation reprehenderit minim.', 'Nulla velit esse do nisi do ut tempor irure magna. Non adipisicing aliqua in amet Lorem elit laborum.', '3', '06/07/2019', '06/14/2019', 'www.Izzby.com', NULL, '2019-08-12 06:01:57', '2019-08-12 06:01:57'),
(12, 'manager', '46,474.35', 'Fugiat cupidatat ex velit ea qui nostrud Lorem. Cillum non excepteur amet pariatur voluptate eu consequat ullamco.', 'Consectetur ut irure laboris excepteur ullamco ex eu est id commodo. Minim ea elit et ea.', '3', '07/03/2019', '07/10/2019', 'www.Izzby.com', NULL, '2019-08-12 06:01:57', '2019-08-12 06:01:57'),
(13, 'manager', '17,106.85', 'Dolore deserunt id voluptate sit adipisicing ullamco nisi. Magna nisi eu nulla sunt ex quis anim amet ea eu pariatur eu exercitation culpa.', 'Mollit occaecat laborum in elit pariatur nostrud excepteur. In elit veniam enim mollit anim aute.', '3', '12/12/2019', '12/19/2019', 'www.Izzby.com', NULL, '2019-08-12 06:01:57', '2019-08-12 06:01:57'),
(14, 'secretary', '32,547.55', 'Amet consectetur ipsum ipsum proident exercitation incididunt fugiat commodo aliquip est elit. Excepteur commodo exercitation amet adipisicing quis.', 'Aliqua nulla labore fugiat esse cillum pariatur. Deserunt adipisicing sint consequat excepteur ad cupidatat tempor.', '3', '03/09/2019', '03/16/2019', 'www.Izzby.com', NULL, '2019-08-12 06:01:57', '2019-08-12 06:01:57'),
(15, 'manager', '19,739.74', 'Veniam fugiat pariatur mollit ea. Consectetur do enim ea aliqua Lorem reprehenderit est ipsum id culpa Lorem dolor.', 'Non ut nulla et consequat irure do do eiusmod amet culpa exercitation voluptate. Magna amet aliquip anim qui qui quis exercitation consectetur do in id.', '3', '11/26/2019', '12/03/2019', 'www.Izzby.com', NULL, '2019-08-12 06:01:57', '2019-08-12 06:01:57'),
(16, 'employee', '22,681.71', 'Sunt sit ipsum non irure exercitation duis cupidatat excepteur ea excepteur do fugiat veniam officia. Sunt laborum cupidatat ad aute ullamco tempor esse.', 'Quis ullamco occaecat adipisicing in mollit do irure non. Nulla ipsum aute ex anim sint excepteur ex qui duis ex cupidatat consectetur veniam dolore.', '4', '03/21/2019', '03/28/2019', 'www.Verton.com', NULL, '2019-08-12 06:01:57', '2019-08-12 06:01:57'),
(17, 'secretary', '41,059.39', 'Ad adipisicing tempor ullamco minim deserunt laborum. Enim quis minim proident laboris fugiat dolor minim tempor velit culpa adipisicing consequat.', 'Culpa commodo culpa Lorem eu. Eu adipisicing fugiat ipsum irure cillum occaecat.', '4', '11/13/2019', '11/20/2019', 'www.Verton.com', NULL, '2019-08-12 06:01:57', '2019-08-12 06:01:57'),
(18, 'manager', '49,066.07', 'Proident fugiat officia eiusmod laboris exercitation ea cupidatat fugiat consequat. Duis ea qui nulla do.', 'Duis ipsum dolore quis non deserunt quis culpa. Ullamco ad minim amet tempor minim sunt tempor ullamco veniam.', '4', '04/19/2019', '04/26/2019', 'www.Verton.com', NULL, '2019-08-12 06:01:57', '2019-08-12 06:01:57'),
(19, 'secretary', '38,433.22', 'Labore ea dolor tempor eu ut dolor nulla mollit ut eu ad anim. Reprehenderit occaecat amet et consequat deserunt ipsum nostrud pariatur reprehenderit.', 'Ea excepteur duis aliqua mollit Lorem non adipisicing. Consequat officia mollit est dolor excepteur proident consectetur velit esse veniam ea aliquip reprehenderit.', '4', '01/08/2019', '01/15/2019', 'www.Verton.com', NULL, '2019-08-12 06:01:57', '2019-08-12 06:01:57'),
(20, 'manager', '34,077.21', 'Labore eu sint proident commodo elit in id. Est reprehenderit in ut ullamco adipisicing reprehenderit exercitation.', 'Amet est cupidatat in et esse ex ad qui dolore aliqua esse tempor. Eu velit adipisicing anim cupidatat officia in sint.', '4', '12/03/2019', '12/10/2019', 'www.Verton.com', NULL, '2019-08-12 06:01:57', '2019-08-12 06:01:57'),
(21, 'manager', '30,394.21', 'Aliquip cupidatat aliquip ea pariatur commodo sint excepteur. Nulla cupidatat ea ea ut nulla cillum ut commodo cupidatat excepteur incididunt eiusmod labore.', 'Nulla exercitation eiusmod eiusmod irure velit cillum laborum nostrud consequat. Aute dolore in cillum consectetur culpa aliquip aliqua aliqua sit quis cupidatat.', '5', '07/11/2019', '07/18/2019', 'www.Imageflow.com', NULL, '2019-08-12 06:01:57', '2019-08-12 06:01:57'),
(22, 'manager', '46,126.22', 'Magna nostrud cillum pariatur duis ullamco consectetur duis Lorem. Quis laborum ex incididunt commodo aute.', 'Reprehenderit ea cupidatat laborum excepteur exercitation dolore Lorem cupidatat commodo excepteur. Tempor ex amet duis ex nostrud sit sit enim sit nulla duis anim.', '5', '03/30/2019', '04/06/2019', 'www.Imageflow.com', NULL, '2019-08-12 06:01:57', '2019-08-12 06:01:57'),
(23, 'secretary', '10,274.43', 'Eu magna tempor tempor aute enim proident commodo tempor sunt ad aliqua incididunt velit. Et adipisicing amet ullamco nulla ipsum.', 'Laborum tempor eu nostrud tempor. Eu dolor anim pariatur eiusmod veniam occaecat ut culpa officia ipsum consectetur elit reprehenderit.', '5', '12/14/2019', '12/21/2019', 'www.Imageflow.com', NULL, '2019-08-12 06:01:57', '2019-08-12 06:01:57'),
(24, 'secretary', '22,355.35', 'Incididunt laboris sit amet esse voluptate in proident cillum. Cupidatat non non pariatur nisi eu aliqua.', 'Dolor deserunt ut dolor laboris sint minim quis. Cillum ipsum minim eu pariatur deserunt dolor excepteur sint nulla aute non veniam voluptate laboris.', '5', '03/07/2019', '03/14/2019', 'www.Imageflow.com', NULL, '2019-08-12 06:01:57', '2019-08-12 06:01:57'),
(25, 'employee', '41,447.75', 'Dolor officia nulla officia velit sit eu labore. Fugiat consequat consectetur dolore veniam velit eiusmod in ea.', 'Proident quis incididunt incididunt ut ut sunt elit occaecat sint veniam. Ipsum incididunt magna laboris incididunt et enim aliqua consequat dolore velit mollit eiusmod.', '5', '12/21/2019', '12/28/2019', 'www.Imageflow.com', NULL, '2019-08-12 06:01:57', '2019-08-12 06:01:57'),
(26, 'secretary', '38,125.68', 'Voluptate in qui ipsum consectetur ea. Esse nostrud minim ut laboris consequat dolor excepteur commodo deserunt ex voluptate.', 'Et ad et officia nisi nisi ut. Minim officia laboris non ex enim enim amet adipisicing minim mollit magna tempor nulla id.', '6', '12/01/2019', '12/08/2019', 'www.Comfirm.com', NULL, '2019-08-12 06:01:57', '2019-08-12 06:01:57'),
(27, 'employee', '39,392.40', 'Mollit cillum magna ex anim in consectetur culpa veniam officia elit consequat id sit veniam. Elit velit dolore proident id mollit ullamco voluptate incididunt laborum.', 'Dolore anim dolore est minim enim laborum fugiat nostrud deserunt enim nostrud. Velit occaecat labore consectetur non duis enim consectetur officia reprehenderit duis consequat.', '6', '11/12/2019', '11/19/2019', 'www.Comfirm.com', NULL, '2019-08-12 06:01:57', '2019-08-12 06:01:57'),
(28, 'secretary', '45,956.39', 'Officia mollit est excepteur culpa mollit aliquip deserunt anim adipisicing. Duis proident sint excepteur enim labore.', 'Est nulla ullamco do laborum pariatur velit fugiat eiusmod quis pariatur. Minim sint adipisicing officia et proident duis exercitation eiusmod.', '6', '01/06/2019', '01/13/2019', 'www.Comfirm.com', NULL, '2019-08-12 06:01:57', '2019-08-12 06:01:57'),
(29, 'employee', '47,282.94', 'Culpa fugiat dolor ullamco Lorem voluptate fugiat Lorem anim irure eiusmod culpa deserunt laboris. Incididunt est proident irure qui ad.', 'Duis aute et voluptate incididunt sunt excepteur deserunt velit aliquip elit. Duis incididunt minim sint laboris non Lorem elit laboris labore est reprehenderit qui cillum.', '6', '08/01/2019', '08/08/2019', 'www.Comfirm.com', NULL, '2019-08-12 06:01:57', '2019-08-12 06:01:57'),
(30, 'manager', '32,580.71', 'Aliquip Lorem dolore proident voluptate magna fugiat sint aute deserunt proident. Velit sunt mollit anim minim.', 'Amet nulla velit aliquip anim labore commodo laborum nisi non eu dolore ullamco ullamco amet. Elit aliqua sint culpa enim consequat culpa ex nulla qui.', '6', '11/28/2019', '12/05/2019', 'www.Comfirm.com', NULL, '2019-08-12 06:01:57', '2019-08-12 06:01:57'),
(31, 'secretary', '27,946.28', 'Minim amet est incididunt labore duis labore aliqua qui id consequat. Aute laborum sunt non ullamco occaecat consectetur esse ullamco est enim.', 'Qui aute in eu elit laborum laborum ut aliqua pariatur. Incididunt id dolor magna mollit veniam proident.', '7', '01/04/2020', '01/11/2020', 'www.Futuris.com', NULL, '2019-08-12 06:01:57', '2019-08-12 06:01:57'),
(32, 'secretary', '45,882.45', 'Elit sit ea dolore veniam magna incididunt aliquip proident adipisicing incididunt magna. Minim culpa enim id amet ex occaecat.', 'Cupidatat deserunt ea labore exercitation mollit sunt nulla minim. Dolor nisi qui amet mollit velit nostrud duis et anim non.', '7', '09/28/2019', '10/05/2019', 'www.Futuris.com', NULL, '2019-08-12 06:01:57', '2019-08-12 06:01:57'),
(33, 'employee', '42,586.46', 'Aliquip est est minim laborum adipisicing veniam minim magna do. Non nostrud occaecat officia amet tempor ad aute tempor sint fugiat.', 'Fugiat ex exercitation laborum aliqua irure ipsum pariatur minim aute ad magna ea. Voluptate do aliqua laborum quis veniam deserunt labore enim.', '7', '03/16/2019', '03/23/2019', 'www.Futuris.com', NULL, '2019-08-12 06:01:57', '2019-08-12 06:01:57'),
(34, 'employee', '28,671.76', 'Adipisicing qui culpa enim commodo labore eiusmod. Quis mollit enim do cupidatat ut velit irure commodo voluptate.', 'Sunt mollit do enim sit elit. Lorem in aliquip quis sit veniam nisi in.', '7', '01/15/2019', '01/22/2019', 'www.Futuris.com', NULL, '2019-08-12 06:01:57', '2019-08-12 06:01:57'),
(35, 'employee', '35,264.11', 'Enim nulla pariatur cillum amet magna non pariatur magna laboris esse nisi eiusmod irure minim. Occaecat aute ea aliquip eiusmod anim.', 'Adipisicing excepteur exercitation non exercitation mollit veniam sint veniam reprehenderit dolor adipisicing sint. Aliquip minim dolor ullamco sint nostrud dolore eiusmod occaecat incididunt veniam veniam.', '7', '12/25/2019', '01/01/2020', 'www.Futuris.com', NULL, '2019-08-12 06:01:57', '2019-08-12 06:01:57'),
(36, 'employee', '25,093.48', 'Aute labore consectetur nulla consectetur. Nostrud aute voluptate reprehenderit est fugiat anim sunt laboris.', 'Nisi enim qui veniam ut ut fugiat do officia irure. Dolore do magna dolore aliqua cillum.', '8', '01/25/2020', '02/01/2020', 'www.Valpreal.com', NULL, '2019-08-12 06:01:57', '2019-08-12 06:01:57'),
(37, 'secretary', '45,552.48', 'Quis cillum dolore eu anim ut quis Lorem dolor sunt proident voluptate eu ea ipsum. Qui est ea consectetur proident sint commodo proident do qui eiusmod id et anim irure.', 'Laborum dolore ullamco id veniam voluptate ad eiusmod duis nisi incididunt Lorem sunt. Ad ipsum esse elit consequat eu.', '8', '08/07/2019', '08/14/2019', 'www.Valpreal.com', NULL, '2019-08-12 06:01:57', '2019-08-12 06:01:57'),
(38, 'manager', '15,164.97', 'Irure est reprehenderit quis mollit pariatur qui ut cillum pariatur exercitation pariatur voluptate. Officia nulla pariatur fugiat fugiat aliquip ullamco esse magna velit.', 'Consectetur laboris irure irure veniam do nostrud consectetur nulla mollit anim irure. Pariatur enim eu qui duis Lorem proident.', '8', '12/06/2019', '12/13/2019', 'www.Valpreal.com', NULL, '2019-08-12 06:01:57', '2019-08-12 06:01:57'),
(39, 'manager', '39,099.14', 'Ex dolor ad eiusmod Lorem eu irure commodo minim commodo Lorem magna amet. Eu sit laborum elit aliquip magna duis nulla mollit quis Lorem ea.', 'Ut dolore commodo quis anim ea eiusmod. Consectetur velit cillum eu reprehenderit qui.', '8', '04/23/2019', '04/30/2019', 'www.Valpreal.com', NULL, '2019-08-12 06:01:57', '2019-08-12 06:01:57'),
(40, 'employee', '26,473.84', 'Nostrud aliquip enim duis minim labore incididunt. Officia aliqua aute sunt est incididunt commodo velit officia laborum id proident ullamco.', 'Aute velit velit reprehenderit tempor veniam. Occaecat nulla consectetur veniam nisi ut irure magna dolor.', '8', '06/29/2019', '07/06/2019', 'www.Valpreal.com', NULL, '2019-08-12 06:01:57', '2019-08-12 06:01:57'),
(41, 'manager', '37,899.04', 'Ex occaecat est Lorem ad in quis. Exercitation mollit magna anim labore culpa mollit dolor ea.', 'Tempor dolore minim aliqua excepteur adipisicing eiusmod adipisicing laborum quis Lorem enim nostrud. Nostrud magna culpa irure tempor est eiusmod sit id ipsum.', '9', '04/01/2019', '04/08/2019', 'www.Digial.com', NULL, '2019-08-12 06:01:57', '2019-08-12 06:01:57'),
(42, 'employee', '15,538.87', 'Quis et tempor aliqua eiusmod. Esse dolor culpa Lorem Lorem ut incididunt id duis voluptate eiusmod.', 'Et sit laboris aute duis minim esse magna ex. Consequat ex culpa eu aute fugiat consequat sit officia.', '9', '12/30/2019', '01/06/2020', 'www.Digial.com', NULL, '2019-08-12 06:01:57', '2019-08-12 06:01:57'),
(43, 'manager', '13,339.79', 'Eiusmod quis nisi id et culpa cupidatat voluptate mollit ipsum irure irure. Incididunt voluptate adipisicing sunt laboris sunt occaecat.', 'Do do elit tempor eiusmod cillum sunt mollit nostrud irure non occaecat Lorem. Voluptate non proident amet irure dolore.', '9', '08/19/2019', '08/26/2019', 'www.Digial.com', NULL, '2019-08-12 06:01:57', '2019-08-12 06:01:57'),
(44, 'manager', '14,876.71', 'Et tempor ipsum cillum cillum adipisicing. Anim commodo sint cillum nulla id ullamco tempor sunt sint.', 'Nisi id laborum consectetur officia. Incididunt sunt sint ullamco enim ullamco in ipsum consectetur veniam et.', '9', '10/03/2019', '10/10/2019', 'www.Digial.com', NULL, '2019-08-12 06:01:57', '2019-08-12 06:01:57'),
(45, 'secretary', '41,340.12', 'Irure pariatur eiusmod ipsum fugiat ullamco ut. Sit exercitation cillum et eiusmod.', 'Consectetur eu cupidatat ut duis est do. Veniam velit Lorem ad officia ut.', '9', '11/18/2019', '11/25/2019', 'www.Digial.com', NULL, '2019-08-12 06:01:57', '2019-08-12 06:01:57'),
(46, 'employee', '44,859.82', 'Ipsum mollit excepteur nisi ea ea officia excepteur veniam do quis. Nisi pariatur mollit fugiat adipisicing consequat ea sit anim eu fugiat occaecat sit.', 'Do incididunt elit ipsum eu ex sint sunt et exercitation. Et exercitation sint consequat occaecat culpa aliquip reprehenderit ipsum sit eu incididunt nulla dolore irure.', '10', '08/14/2019', '08/21/2019', 'www.Daycore.com', NULL, '2019-08-12 06:01:57', '2019-08-12 06:01:57'),
(47, 'manager', '26,889.78', 'Qui dolore dolor fugiat magna sit sint mollit est reprehenderit ea elit in adipisicing occaecat. Proident irure exercitation minim cupidatat magna.', 'Lorem quis aliqua incididunt culpa eu sunt dolore Lorem nostrud sit minim ullamco deserunt. Velit id id reprehenderit duis do eu voluptate nostrud veniam dolor officia cupidatat deserunt labore.', '10', '06/29/2019', '07/06/2019', 'www.Daycore.com', NULL, '2019-08-12 06:01:57', '2019-08-12 06:01:57'),
(48, 'secretary', '13,098.90', 'Sint dolor est mollit Lorem incididunt nostrud esse non eu nulla sint. Occaecat sint nulla anim sit tempor cillum consectetur eiusmod esse deserunt sunt reprehenderit et ad.', 'Culpa magna in esse tempor amet tempor anim quis minim. Eiusmod mollit eu labore officia quis.', '10', '08/16/2019', '08/23/2019', 'www.Daycore.com', NULL, '2019-08-12 06:01:57', '2019-08-12 06:01:57'),
(49, 'manager', '31,077.69', 'Eiusmod pariatur nulla minim nulla irure aliquip aliqua laboris sunt laboris. Non ad id excepteur voluptate enim.', 'Adipisicing anim aute fugiat laboris eu. Sint ipsum non culpa adipisicing.', '10', '08/29/2019', '09/05/2019', 'www.Daycore.com', NULL, '2019-08-12 06:01:57', '2019-08-12 06:01:57'),
(50, 'employee', '36,868.83', 'Esse tempor eiusmod eiusmod nisi anim in non esse occaecat pariatur velit esse et. Deserunt anim laborum excepteur labore duis sint excepteur.', 'Proident ullamco aute proident veniam nisi esse sit magna dolore. Eiusmod dolore nostrud consectetur et.', '10', '02/01/2019', '02/08/2019', 'www.Daycore.com', NULL, '2019-08-12 06:01:57', '2019-08-12 06:01:57');

-- --------------------------------------------------------

--
-- Table structure for table `job_title`
--

CREATE TABLE IF NOT EXISTS `job_title` (
  `id` bigint(20) unsigned NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `industry` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `job_title`
--

INSERT INTO `job_title` (`id`, `title`, `industry`, `created_at`, `updated_at`) VALUES
(1, 'Software Developer', 'Information Technology', '2020-02-02 16:59:32', '2020-02-02 16:59:32'),
(2, 'Database Administrator', 'Information Technology', '2020-02-02 16:59:32', '2020-02-02 16:59:32'),
(3, 'Computer Hardware Engineer', 'Information Technology', '2020-02-02 16:59:32', '2020-02-02 16:59:32'),
(4, 'Computer System Analyst', 'Information Technology', '2020-02-02 16:59:32', '2020-02-02 16:59:32'),
(5, 'Computer Network Architect', 'Information Technology', '2020-02-02 16:59:32', '2020-02-02 16:59:32'),
(6, 'Pharmacy Technician', 'Health Care', '2020-02-02 16:59:32', '2020-02-02 16:59:32'),
(7, 'Medical Assistant', 'Health Care', '2020-02-02 16:59:32', '2020-02-02 16:59:32'),
(8, 'Phlebotomists', 'Health Care', '2020-02-02 16:59:32', '2020-02-02 16:59:32'),
(9, 'Dental Assistants', 'Health Care', '2020-02-02 16:59:32', '2020-02-02 16:59:32'),
(10, 'Medical Records and Health Information Technicians', 'Health Care', '2020-02-02 16:59:32', '2020-02-02 16:59:32'),
(11, 'Urban Planner', 'Media', '2020-02-02 16:59:32', '2020-02-02 16:59:32'),
(12, 'Lawyer', 'Media', '2020-02-02 16:59:32', '2020-02-02 16:59:32'),
(13, 'Librarian', 'Media', '2020-02-02 16:59:32', '2020-02-02 16:59:32'),
(14, 'Diplomat', 'Media', '2020-02-02 16:59:32', '2020-02-02 16:59:32'),
(15, 'Teacher', 'Media', '2020-02-02 16:59:32', '2020-02-02 16:59:32'),
(16, 'Financial Analyst', 'Business services / Accounting and Legal', '2020-02-02 16:59:32', '2020-02-02 16:59:32'),
(17, 'Financial Planner', 'Business services / Accounting and Legal', '2020-02-02 16:59:32', '2020-02-02 16:59:32'),
(18, 'Investor Relations Associate', 'Business services / Accounting and Legal', '2020-02-02 16:59:32', '2020-02-02 16:59:32'),
(19, 'Budget Analyst', 'Business services / Accounting and Legal', '2020-02-02 16:59:32', '2020-02-02 16:59:32'),
(20, 'Actuary', 'Business services / Accounting and Legal', '2020-02-02 16:59:32', '2020-02-02 16:59:32'),
(21, 'Aerospace Enginner', 'Construction, repair, and maintenance', '2020-02-02 16:59:32', '2020-02-02 16:59:32'),
(22, 'Agricultural Engineer', 'Construction, repair, and maintenance', '2020-02-02 16:59:32', '2020-02-02 16:59:32'),
(23, 'Automotive Enginner', 'Construction, repair, and maintenance', '2020-02-02 16:59:32', '2020-02-02 16:59:32'),
(24, 'Biomedical Enginner', 'Construction, repair, and maintenance', '2020-02-02 16:59:32', '2020-02-02 16:59:32'),
(25, 'Civil Enginner', 'Construction, repair, and maintenance', '2020-02-02 16:59:32', '2020-02-02 16:59:32'),
(26, 'Tutors', 'Education', '2020-02-02 16:59:32', '2020-02-02 16:59:32'),
(27, 'Teacher', 'Education', '2020-02-02 16:59:32', '2020-02-02 16:59:32'),
(28, 'Intructors Position', 'Education', '2020-02-02 16:59:32', '2020-02-02 16:59:32'),
(29, 'Vice President of Student Affairs', 'Education', '2020-02-02 16:59:32', '2020-02-02 16:59:32'),
(30, 'Technical Trainer', 'Education', '2020-02-02 16:59:32', '2020-02-02 16:59:32'),
(31, 'Government Relations Manager', 'Government', '2020-02-02 16:59:32', '2020-02-02 16:59:32'),
(32, 'Consultant', 'Government', '2020-02-02 16:59:32', '2020-02-02 16:59:32'),
(33, 'Community Relations Manager', 'Government', '2020-02-02 16:59:32', '2020-02-02 16:59:32'),
(34, 'Public Relations and Marketing Manager', 'Government', '2020-02-02 16:59:32', '2020-02-02 16:59:32'),
(35, 'Deck Officer', 'Shipping Industry', '2020-02-02 16:59:32', '2020-02-02 16:59:32'),
(36, 'Analyst Maritime Operations', 'Shipping Industry', '2020-02-02 16:59:32', '2020-02-02 16:59:32'),
(37, 'First Engineer', 'Shipping Industry', '2020-02-02 16:59:32', '2020-02-02 16:59:32'),
(38, 'Seaman', 'Shipping Industry', '2020-02-02 16:59:32', '2020-02-02 16:59:32'),
(39, 'Science Research Assistant', 'Medical', '2020-02-02 16:59:32', '2020-02-02 16:59:32'),
(40, 'Chemist', 'Medical', '2020-02-02 16:59:32', '2020-02-02 16:59:32'),
(41, 'Clinical Research Scientist', 'Medical', '2020-02-02 16:59:32', '2020-02-02 16:59:32'),
(42, 'Scientist', 'Medical', '2020-02-02 16:59:32', '2020-02-02 16:59:32'),
(43, 'Biophysics', 'Medical', '2020-02-02 16:59:32', '2020-02-02 16:59:32'),
(44, 'Electrical Engineer', 'Industrial Technology Management', '2020-02-02 16:59:32', '2020-02-02 16:59:32'),
(45, 'Electronics Engineer', 'Industrial Technology Management', '2020-02-02 16:59:32', '2020-02-02 16:59:32'),
(46, 'Automotive Technician', 'Industrial Technology Management', '2020-02-02 16:59:32', '2020-02-02 16:59:32'),
(47, 'Automotive Mechanic', 'Industrial Technology Management', '2020-02-02 16:59:32', '2020-02-02 16:59:32'),
(48, 'Truck Mechanic', 'Industrial Technology Management', '2020-02-02 16:59:32', '2020-02-02 16:59:32'),
(49, 'Hotel Manager', 'Restaurant, Bars and Foor Services', '2020-02-02 16:59:32', '2020-02-02 16:59:32'),
(50, 'Spa Manager', 'Restaurant, Bars and Foor Services', '2020-02-02 16:59:32', '2020-02-02 16:59:32'),
(51, 'Travel Agent', 'Restaurant, Bars and Foor Services', '2020-02-02 16:59:32', '2020-02-02 16:59:32'),
(52, 'Tour Operator', 'Restaurant, Bars and Foor Services', '2020-02-02 16:59:32', '2020-02-02 16:59:32'),
(53, 'Event and Conference Organizer', 'Restaurant, Bars and Foor Services', '2020-02-02 16:59:32', '2020-02-02 16:59:32');

-- --------------------------------------------------------

--
-- Table structure for table `mailbox_inbound_emails`
--

CREATE TABLE IF NOT EXISTS `mailbox_inbound_emails` (
  `id` int(10) unsigned NOT NULL,
  `message_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=98 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_06_19_113803_create_colleges_table', 1),
(43, '2019_07_12_141811_create_feedback_table', 9),
(44, '2019_07_17_023826_create_mailbox_inbound_emails_table', 10),
(52, '2014_10_12_000000_create_admin_table', 13),
(53, '2019_08_11_170022_create_job_posts_table', 14),
(54, '2019_08_11_171727_create_companies_table', 14),
(67, '2019_07_04_150753_create_graduate_table', 21),
(75, '2019_11_15_203953_create_reasons_table', 24),
(77, '2020_01_03_221622_create_first_job_table', 26),
(83, '2019_12_19_220441_create_course_table', 28),
(84, '2020_01_10_092607_create_suggestions_table', 29),
(85, '2019_07_12_115106_create_articles_table', 30),
(87, '2020_01_15_220219_create_industries_table', 31),
(88, '2019_06_20_074848_create_alumni_table', 32),
(89, '2019_06_20_075006_create_alumni_educations_table', 32),
(90, '2019_06_20_084031_create_alumni_employments_table', 32),
(93, '2019_09_01_194125_create_applicants_table', 34),
(94, '2019_07_16_150324_create_payments_table', 35),
(95, '2019_08_30_050814_create_partners_table', 35),
(96, '2019_08_21_170753_create_carousel_table', 36),
(97, '2020_02_03_001232_create_job_title_table', 37);

-- --------------------------------------------------------

--
-- Table structure for table `partners`
--

CREATE TABLE IF NOT EXISTS `partners` (
  `id` bigint(20) unsigned NOT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `posted_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `validity_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `partners`
--

INSERT INTO `partners` (`id`, `logo`, `name`, `description`, `category`, `posted_date`, `validity_date`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'images/200x200.png', 'Ratke-Oberbrunner', 'id', 'Others', 'January 22, 2020', 'February 05, 2020', NULL, '2020-01-22 07:23:50', '2020-01-22 07:23:50'),
(2, 'images/200x200.png', 'Funk-Gerhold', 'vitae', 'Gadgets and Technology', 'January 22, 2020', 'January 28, 2020', NULL, '2020-01-22 07:23:50', '2020-01-22 07:23:50'),
(3, 'images/200x200.png', 'Reinger LLC', 'commodi', 'Tours and Travels', 'January 22, 2020', 'January 30, 2020', NULL, '2020-01-22 07:23:50', '2020-01-22 07:23:50'),
(4, 'images/200x200.png', 'Nikolaus Ltd', 'quaerat', 'Clothing', 'January 22, 2020', 'January 24, 2020', NULL, '2020-01-22 07:23:50', '2020-01-22 07:23:50'),
(5, 'images/200x200.png', 'Hilpert Group', 'rerum', 'Gadgets and Technology', 'January 22, 2020', 'January 24, 2020', NULL, '2020-01-22 07:23:50', '2020-01-22 07:23:50'),
(6, 'images/200x200.png', 'Kessler-Volkman', 'rem', 'Clothing', 'January 22, 2020', 'January 31, 2020', NULL, '2020-01-22 07:23:50', '2020-01-22 07:23:50'),
(7, 'images/200x200.png', 'Labadie Inc', 'et', 'Gadgets and Technology', 'January 22, 2020', 'January 24, 2020', NULL, '2020-01-22 07:23:50', '2020-01-22 07:23:50'),
(8, 'images/200x200.png', 'Stroman Inc', 'provident', 'Tours and Travels', 'January 22, 2020', 'January 30, 2020', NULL, '2020-01-22 07:23:50', '2020-01-22 07:23:50'),
(9, 'images/200x200.png', 'Botsford, Parker and Crist', 'at', 'Clothing', 'January 22, 2020', 'February 02, 2020', NULL, '2020-01-22 07:23:50', '2020-01-22 07:23:50'),
(10, 'images/200x200.png', 'Wehner-Romaguera', 'adipisci', 'Others', 'January 22, 2020', 'February 05, 2020', NULL, '2020-01-22 07:23:50', '2020-01-22 07:23:50'),
(11, 'images/200x200.png', 'Upton-Windler', 'iste', 'Services', 'January 22, 2020', 'January 26, 2020', NULL, '2020-01-22 07:23:50', '2020-01-22 07:23:50'),
(12, 'images/200x200.png', 'Considine, Paucek and Kulas', 'et', 'Gadgets and Technology', 'January 22, 2020', 'January 26, 2020', NULL, '2020-01-22 07:23:50', '2020-01-22 07:23:50'),
(13, 'images/200x200.png', 'Bruen PLC', 'qui', 'Gadgets and Technology', 'January 22, 2020', 'February 03, 2020', NULL, '2020-01-22 07:23:50', '2020-01-22 07:23:50'),
(14, 'images/200x200.png', 'Wehner-Miller', 'quaerat', 'Restaurant and Bars', 'January 22, 2020', 'January 29, 2020', NULL, '2020-01-22 07:23:50', '2020-01-22 07:23:50'),
(15, 'images/200x200.png', 'Swaniawski Group', 'incidunt', 'Tours and Travels', 'January 22, 2020', 'January 24, 2020', NULL, '2020-01-22 07:23:50', '2020-01-22 07:23:50'),
(16, 'images/200x200.png', 'Feest, Barrows and Ryan', 'dolor', 'Gadgets and Technology', 'January 22, 2020', 'January 31, 2020', NULL, '2020-01-22 07:23:50', '2020-01-22 07:23:50'),
(17, 'images/200x200.png', 'Kertzmann-Christiansen', 'praesentium', 'Others', 'January 22, 2020', 'January 29, 2020', NULL, '2020-01-22 07:23:50', '2020-01-22 07:23:50'),
(18, 'images/200x200.png', 'Willms, Stark and Ritchie', 'magnam', 'Tours and Travels', 'January 22, 2020', 'February 02, 2020', NULL, '2020-01-22 07:23:50', '2020-01-22 07:23:50'),
(19, 'images/200x200.png', 'Bartoletti LLC', 'eos', 'Tours and Travels', 'January 22, 2020', 'January 24, 2020', NULL, '2020-01-22 07:23:50', '2020-01-22 07:23:50'),
(20, 'images/200x200.png', 'Hayes, Toy and Jaskolski', 'consequatur', 'Services', 'January 22, 2020', 'February 01, 2020', NULL, '2020-01-22 07:23:50', '2020-01-22 07:23:50');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE IF NOT EXISTS `payments` (
  `id` bigint(20) unsigned NOT NULL,
  `uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `delivery_method` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'pick-up',
  `delivery_fee` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `membership_fee` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `delivery_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alumni_id_fee` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '50',
  `payment_method` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pick-up',
  `total_amount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount_paid` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_paid` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `proof_of_payment` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `uuid`, `delivery_method`, `delivery_fee`, `membership_fee`, `delivery_address`, `alumni_id_fee`, `payment_method`, `total_amount`, `amount_paid`, `is_paid`, `proof_of_payment`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '7e56571d-1e68-4723-a3bf-c1236c732438', 'deliver with Mindanao', '160', '50', '67699 Fisher Underpass\nMurazikmouth, AZ 51918-5359', '50', 'Bank', '260', '260', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:33', '2020-02-03 00:46:33'),
(2, '8a72c3ce-2d5b-4785-9793-b23c77f2be3b', 'deliver with Visayas', '160', '150', '4577 Jettie Camp\nBayerstad, NY 74458', '50', 'COOP', '360', '360', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:33', '2020-02-03 00:46:33'),
(3, '9b5f20b3-6268-4219-81dc-6c94df37521c', 'deliver with Luzon', '120', '150', '518 Reagan Route\nCoytown, CO 88128', '50', 'Bank', '320', '320', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:33', '2020-02-03 00:46:33'),
(4, 'b6cfaa5d-be87-4e95-9b0e-f102ad48d171', 'deliver with Visayas', '160', '50', '15869 Zulauf Land Suite 765\nSouth Cortezshire, GA 41091-1614', '50', 'Bank', '260', '260', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:33', '2020-02-03 00:46:33'),
(5, '3ffb0369-078b-4d8b-8d8a-3a0a922cdf67', 'deliver with Visayas', '160', '150', '78688 Robbie Shore Suite 772\nNorth Arne, SC 74402-2622', '50', 'COOP', '360', '360', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:33', '2020-02-03 00:46:33'),
(6, '31accb2a-9587-45de-9fce-255b208800a7', 'deliver with Mindanao', '160', '50', '234 Rigoberto Lane Suite 807\nSatterfieldland, WI 58429', '50', 'COOP', '260', '260', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:33', '2020-02-03 00:46:33'),
(7, '12193e7e-7b0d-46f1-ac73-e1f94aca4f7e', 'deliver with Visayas', '160', '50', '57772 Monica Squares Apt. 456\nLake Constantin, AR 43977', '50', 'Bank', '260', '260', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:33', '2020-02-03 00:46:33'),
(8, 'd09414c5-2f26-4439-b923-9a7b5e58f65e', 'deliver with Luzon', '120', '150', '5996 Huels Parks Suite 023\nBirdieton, CT 62222-0891', '50', 'COOP', '320', '320', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:33', '2020-02-03 00:46:33'),
(9, '90e459e3-3464-4f81-9767-0c1842eeaa12', 'deliver with Mindanao', '160', '50', '739 Olson Parkways\nParisianstad, WI 41825', '50', 'COOP', '260', '260', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:33', '2020-02-03 00:46:33'),
(10, '316eacd8-4ac8-4467-a617-425161d5fc56', 'deliver with Luzon', '120', '50', '14272 Hill Crossing Apt. 369\nShanonburgh, AR 67711', '50', 'COOP', '220', '220', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:33', '2020-02-03 00:46:33'),
(11, '846ba039-041e-4466-9e15-577eaa04d9a0', 'pick-up', '0', '50', '14272 Hill Crossing Apt. 369\nShanonburgh, AR 67711', '50', 'COOP', '100', '100', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:33', '2020-02-03 00:46:33'),
(12, '3e126d7b-7d44-44be-a99c-f911230e0d58', 'deliver with Visayas', '160', '150', '956 Madilyn Mews\nWest Waldoburgh, IL 02591', '50', 'Bank', '360', '360', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:33', '2020-02-03 00:46:33'),
(13, 'b81982d5-4dae-41ff-99c2-644b6dddabe2', 'deliver with Visayas', '160', '150', '185 Ena Mission Suite 088\nNew Justinastad, AK 06054-1378', '50', 'COOP', '360', '360', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:33', '2020-02-03 00:46:33'),
(14, 'b4afe0ea-a67f-448a-ad6d-319740d6841b', 'pick-up', '0', '150', '185 Ena Mission Suite 088\nNew Justinastad, AK 06054-1378', '50', 'Bank', '200', '200', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:33', '2020-02-03 00:46:33'),
(15, '924c7c3a-5300-4817-9fc5-83229c9a1047', 'pick-up', '0', '150', '185 Ena Mission Suite 088\nNew Justinastad, AK 06054-1378', '50', 'COOP', '200', '200', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:33', '2020-02-03 00:46:33'),
(16, 'c0747ecc-b98a-4be2-ae9e-d59933983ee6', 'pick-up', '0', '150', '185 Ena Mission Suite 088\nNew Justinastad, AK 06054-1378', '50', 'Bank', '200', '200', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:33', '2020-02-03 00:46:33'),
(17, '5139ebd5-8c61-4417-955c-53aef9a62d9d', 'pick-up', '0', '50', '185 Ena Mission Suite 088\nNew Justinastad, AK 06054-1378', '50', 'Bank', '100', '100', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:33', '2020-02-03 00:46:33'),
(18, '9f69512d-8e83-4db4-ab56-b65eb1d47b18', 'deliver with Luzon', '120', '50', '84442 Mayert Estates\nJaydenshire, ME 36332-1803', '50', 'COOP', '220', '220', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(19, '8d15373f-4953-4deb-91c6-7138d7a81d40', 'deliver with Mindanao', '160', '150', '967 Mossie River Suite 367\nLavernaside, TN 30908-1528', '50', 'COOP', '360', '360', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(20, '2bffad8a-c9e8-4e96-a3ac-b56933f2582c', 'deliver with Luzon', '120', '50', '41836 Kilback Unions Suite 341\nEast Elseberg, AR 68294-7724', '50', 'Bank', '220', '220', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(21, '7f856c00-7290-4e34-8af5-6e4262bba532', 'deliver with Luzon', '120', '150', '191 O''Keefe Divide Suite 221\nMireilleview, GA 84750', '50', 'Bank', '320', '320', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(22, '2f26ee46-95af-4c6a-97a3-71000e121867', 'deliver with Luzon', '120', '150', '30129 Magnus Rue Apt. 104\nSouth Austenstad, MD 22738-6348', '50', 'COOP', '320', '320', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(23, '2fb7fa14-bde6-4031-b680-513212e7f479', 'pick-up', '0', '150', '30129 Magnus Rue Apt. 104\nSouth Austenstad, MD 22738-6348', '50', 'COOP', '200', '200', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(24, '515988d6-178e-4f6a-9ab4-59251d6b095a', 'deliver with Luzon', '120', '150', '67079 Barton Port\nSouth Peggie, MN 31747-4029', '50', 'COOP', '320', '320', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(25, 'ec58c01e-7069-4348-baac-454862cf1873', 'deliver with Visayas', '160', '150', '3951 Ritchie Passage\nDarrontown, NJ 21780', '50', 'Bank', '360', '360', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(26, '3b6f4771-e84b-472e-b8d4-4fd82c0eb686', 'deliver with Mindanao', '160', '150', '95611 Tania Glens\nWest Abdielmouth, IA 25122', '50', 'Bank', '360', '360', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(27, '0bbdd886-f782-4053-b9bf-60d2acd711f7', 'deliver with Mindanao', '160', '50', '7233 Herzog Fall Suite 972\nKarinastad, UT 55676-2092', '50', 'Bank', '260', '260', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(28, '228fb3ca-5449-40ba-8211-530b6e73f484', 'deliver with Luzon', '120', '150', '56784 Veda Coves\nEribertoville, SD 91607-8885', '50', 'Bank', '320', '320', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(29, 'e9a48bdf-bdb3-4c2d-97c0-74abb64064b0', 'pick-up', '0', '150', '56784 Veda Coves\nEribertoville, SD 91607-8885', '50', 'COOP', '200', '200', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(30, 'e062b6c9-1763-428c-93ef-36c7f2a49309', 'deliver with Mindanao', '160', '150', '6980 Hickle Isle\nKirlintown, IN 49196', '50', 'Bank', '360', '360', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(31, 'ea690755-380a-4fda-9511-aa1b1df4f8ac', 'deliver with Mindanao', '160', '150', '577 Ankunding Oval Apt. 682\nLake Cloydburgh, MT 31782-2643', '50', 'Bank', '360', '360', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(32, '9c0c2bf9-1ddf-43e0-b714-33ee2827c617', 'deliver with Luzon', '120', '150', '94655 Bogan Island\nSouth Enos, NH 81434', '50', 'COOP', '320', '320', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(33, '97a9e580-8763-47f0-93d2-f6d28b07a1e2', 'deliver with Mindanao', '160', '150', '2295 Ivory Glen\nSouth Brigitteton, ND 73911', '50', 'Bank', '360', '360', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(34, 'f41b6b4d-5f3f-4ce1-8a01-a76f1d8f226e', 'deliver with Luzon', '120', '50', '49128 Dawn Ports Apt. 449\nNew Albert, NJ 86625', '50', 'Bank', '220', '220', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(35, 'e9d6f559-4633-4863-a31a-d6f2b7b02d5a', 'deliver with Visayas', '160', '150', '2003 Ford Causeway Suite 704\nMarksfurt, WY 35488-3487', '50', 'COOP', '360', '360', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(36, 'f352f64c-9aa5-4e30-a28b-40bdd96bea86', 'pick-up', '0', '150', '2003 Ford Causeway Suite 704\nMarksfurt, WY 35488-3487', '50', 'COOP', '200', '200', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(37, '71fd555e-b151-4573-bd0f-e8c6c4abf9c2', 'deliver with Mindanao', '160', '50', '815 Felicita Skyway Suite 320\nSchadenfurt, VA 71867', '50', 'COOP', '260', '260', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(38, 'd8f26f81-dcbf-48b3-bee6-6a71d1126f25', 'deliver with Mindanao', '160', '150', '692 Sandra Corners\nMurphyburgh, LA 91012-7925', '50', 'Bank', '360', '360', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(39, 'a383d7f0-0184-48f3-9d64-670c94a9a7e5', 'deliver with Mindanao', '160', '50', '209 Stiedemann Junction\nSmithamside, KS 73405-0112', '50', 'COOP', '260', '260', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(40, 'fac03282-06fb-4d24-8131-cea2d10b706e', 'deliver with Visayas', '160', '150', '8984 Brayan Curve\nNew Norris, SD 20366-6224', '50', 'Bank', '360', '360', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(41, 'd206e6cb-faf4-4d87-a8da-5af4cdb1b710', 'deliver with Visayas', '160', '150', '57129 Bechtelar Street\nEast Alenashire, LA 07441', '50', 'COOP', '360', '360', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(42, '62acd4a1-073b-433e-85f1-929c755f69b0', 'pick-up', '0', '150', '57129 Bechtelar Street\nEast Alenashire, LA 07441', '50', 'COOP', '200', '200', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(43, '775d44df-ce67-4056-94c0-f486ed255afb', 'deliver with Visayas', '160', '50', '58830 Hahn Village Suite 120\nEichmannville, CO 80642', '50', 'Bank', '260', '260', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(44, '3b9050e5-940c-4cff-815f-7751dbb7f540', 'pick-up', '0', '150', '58830 Hahn Village Suite 120\nEichmannville, CO 80642', '50', 'COOP', '200', '200', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(45, 'fc56cc36-987d-41fb-8885-5e928399eb3f', 'deliver with Luzon', '120', '150', '820 Donnelly Estates\nPort Chauncey, IA 57064-9802', '50', 'Bank', '320', '320', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(46, '9dd08520-4936-4904-a403-56cfdbde1c10', 'deliver with Mindanao', '160', '50', '98143 Kreiger Mews\nCarolannemouth, VT 39606-9905', '50', 'Bank', '260', '260', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(47, '83f07d7a-af86-4707-b3e0-3ec014b6d696', 'deliver with Luzon', '120', '150', '4438 Zemlak Brook\nWolfmouth, AL 18039', '50', 'Bank', '320', '320', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(48, '846fde34-d3df-4229-a5cd-f8e418ee9edd', 'pick-up', '0', '150', '4438 Zemlak Brook\nWolfmouth, AL 18039', '50', 'Bank', '200', '200', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(49, '6cad82cd-a6ba-45dc-8359-0d577c7e2336', 'deliver with Visayas', '160', '150', '2264 Natalie Flats Suite 481\nNorth Yadira, AZ 85136-1785', '50', 'Bank', '360', '360', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(50, '4c2e13c8-2869-4e45-9b60-a46585df7de9', 'deliver with Luzon', '120', '150', '95877 Auer Gardens Apt. 823\nNorth Greg, WI 28085-6645', '50', 'Bank', '320', '320', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:34', '2020-02-03 00:46:34'),
(51, '7112e28d-575d-4b61-bd7d-59f9117e122b', 'deliver with Luzon', '120', '150', '936 Rosetta Keys\nEast Brenda, NV 40995-8822', '50', 'Bank', '320', '320', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:49', '2020-02-03 00:46:49'),
(52, '507fdbd8-618b-4797-8a9d-42cee307dcfe', 'pick-up', '0', '150', '936 Rosetta Keys\nEast Brenda, NV 40995-8822', '50', 'Bank', '200', '200', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:49', '2020-02-03 00:46:49'),
(53, '822798d7-cee0-44f9-9144-f6e101938c0c', 'deliver with Mindanao', '160', '150', '62627 Johnnie Heights\nNew Shanahaven, MD 04665', '50', 'Bank', '360', '360', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:49', '2020-02-03 00:46:49'),
(54, '2a1ff151-20b4-42a2-8643-ba3263920836', 'deliver with Luzon', '120', '50', '10521 Myra Motorway Suite 415\nNorth Jamisonchester, IL 58189-8065', '50', 'COOP', '220', '220', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:49', '2020-02-03 00:46:49'),
(55, '71517d7c-0e0a-4c66-a668-5f91e5eed799', 'deliver with Mindanao', '160', '150', '38500 Rempel Creek\nGerlachport, TX 56351-0685', '50', 'COOP', '360', '360', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:49', '2020-02-03 00:46:49'),
(56, '5f44e824-bd22-4753-8fb3-01faf6eb5ced', 'deliver with Mindanao', '160', '150', '95508 Effertz Lights\nNewtonborough, WY 16327', '50', 'COOP', '360', '360', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:49', '2020-02-03 00:46:49'),
(57, '63c47d39-b7b7-42a0-b066-a984315c1da4', 'pick-up', '0', '150', '95508 Effertz Lights\nNewtonborough, WY 16327', '50', 'Bank', '200', '200', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:49', '2020-02-03 00:46:49'),
(58, '07439ee8-2d5f-46bb-a675-28fdfcbaedc4', 'pick-up', '0', '50', '95508 Effertz Lights\nNewtonborough, WY 16327', '50', 'COOP', '100', '100', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:49', '2020-02-03 00:46:49'),
(59, 'b9b8ed58-5e9f-44b4-89d4-9e3d021a42a5', 'deliver with Luzon', '120', '150', '407 Upton Field\nSimeonview, DE 41596', '50', 'COOP', '320', '320', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:49', '2020-02-03 00:46:49'),
(60, 'd0e52cfa-5804-4a43-88e6-b24337953d0b', 'deliver with Visayas', '160', '50', '157 Eleanora Mount\nEast Garnet, NC 70028-7153', '50', 'Bank', '260', '260', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:49', '2020-02-03 00:46:49'),
(61, 'a550923d-1708-401e-b8ed-c8300a1dc10d', 'deliver with Visayas', '160', '150', '82859 Morar Turnpike Suite 231\nTressaville, AZ 26266', '50', 'Bank', '360', '360', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:49', '2020-02-03 00:46:49'),
(62, '62ed9569-3046-4aee-b6e9-20d8a77702e6', 'deliver with Luzon', '120', '150', '6587 Mosciski Corners\nEast Phyllis, NM 25108', '50', 'Bank', '320', '320', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(63, '34ab9432-8f9c-40a1-af39-b59a7281dacf', 'deliver with Luzon', '120', '150', '12496 Jeremy Flat Suite 308\nOrvillemouth, ME 07456', '50', 'COOP', '320', '320', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(64, 'f593079b-afcb-4d0e-a053-a123b3617014', 'deliver with Visayas', '160', '50', '11296 Trevor Inlet\nNew Danfurt, GA 57734', '50', 'COOP', '260', '260', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(65, '52adc49f-f453-45e5-a31f-6a61ee0b0107', 'deliver with Visayas', '160', '50', '7739 Keith Route Apt. 795\nKristianview, NC 96923-0818', '50', 'Bank', '260', '260', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(66, '334dcfb0-7d5e-4394-958d-f94ce6dcbebc', 'deliver with Luzon', '120', '150', '4118 Bechtelar Junctions Suite 551\nDemarioside, LA 71995', '50', 'COOP', '320', '320', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(67, 'becb249e-ac81-4a4e-934c-0011bd9137be', 'deliver with Mindanao', '160', '150', '4337 Kessler Stravenue Suite 406\nNew Nathanielbury, NE 59473-2138', '50', 'Bank', '360', '360', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(68, 'd22c1f3f-cdb4-45c3-b1fd-711f18f7b96d', 'pick-up', '0', '50', '4337 Kessler Stravenue Suite 406\nNew Nathanielbury, NE 59473-2138', '50', 'Bank', '100', '100', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(69, 'c63fb14d-8696-4067-a23a-79e2b9ff5b54', 'pick-up', '0', '50', '4337 Kessler Stravenue Suite 406\nNew Nathanielbury, NE 59473-2138', '50', 'COOP', '100', '100', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(70, 'e8cc9656-5943-45cd-bbab-7bf38644ac47', 'deliver with Visayas', '160', '150', '201 Schiller Alley Suite 365\nPort Claudia, GA 34983', '50', 'COOP', '360', '360', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(71, '4c13a383-1850-4f8e-a7e5-d36e8059ebe9', 'deliver with Luzon', '120', '150', '917 Wiegand Drives Apt. 643\nSchmelerfurt, AL 02893', '50', 'COOP', '320', '320', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(72, '95b885cb-b74c-4712-9ed6-bdd0b7d0e00c', 'deliver with Mindanao', '160', '150', '22281 Iliana Station Apt. 754\nNew Harmonmouth, IL 82272', '50', 'COOP', '360', '360', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(73, '44ef75b7-7479-4a8e-8c31-7d7984eab6d8', 'deliver with Visayas', '160', '150', '49911 Briana Underpass\nWest Carmen, HI 93972-1142', '50', 'COOP', '360', '360', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(74, '5ba67b4c-92b0-44da-a606-e7e65c894b11', 'deliver with Luzon', '120', '50', '32342 Goldner Prairie Suite 458\nEast Bobbie, TN 95336-2879', '50', 'Bank', '220', '220', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(75, '1516869e-24d8-4329-89f9-815ed8a42e0f', 'deliver with Visayas', '160', '150', '4524 Magdalena Knolls Apt. 887\nWest Jennifer, NV 26982', '50', 'Bank', '360', '360', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(76, '5a9e5aa2-22fb-46b6-bad7-bc1a0e4eb3e0', 'pick-up', '0', '150', '4524 Magdalena Knolls Apt. 887\nWest Jennifer, NV 26982', '50', 'Bank', '200', '200', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(77, 'b24dda16-0138-4e1e-b3fb-de0b63391981', 'deliver with Luzon', '120', '150', '70912 Denesik Way\nSawaynmouth, PA 94620', '50', 'COOP', '320', '320', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(78, '36b1efbf-2967-4e26-8786-c32bf176f8eb', 'deliver with Mindanao', '160', '50', '3353 Dedric Lane Suite 901\nStehrside, AL 63198-6240', '50', 'Bank', '260', '260', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(79, '507b50a6-41e3-4e31-bdc5-6744f0febae8', 'deliver with Luzon', '120', '150', '6440 Wellington Expressway Apt. 405\nNew Justinechester, SC 49371', '50', 'COOP', '320', '320', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(80, '70e6e37c-a057-4a39-a5f9-07277d551fe2', 'deliver with Visayas', '160', '150', '627 Rigoberto Greens\nElsafurt, ME 55084-9080', '50', 'Bank', '360', '360', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(81, 'f8a83dc1-5866-4b60-b2cd-77bd0cc94298', 'deliver with Visayas', '160', '150', '699 Moore Locks\nNataliefort, NJ 78478', '50', 'COOP', '360', '360', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(82, '7bdf0a6b-b731-485f-9dc4-d7f6c9f4e6d8', 'deliver with Mindanao', '160', '50', '888 Zoe Harbor Suite 465\nNathanialchester, IN 75524-4756', '50', 'Bank', '260', '260', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(83, '9be8d863-e6b9-4800-beae-68166baaf99c', 'pick-up', '0', '150', '888 Zoe Harbor Suite 465\nNathanialchester, IN 75524-4756', '50', 'Bank', '200', '200', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(84, 'ca933acc-2e58-445b-8b7b-8324b74a4077', 'deliver with Visayas', '160', '150', '42088 Grant Stravenue\nEast Teagan, MS 45312', '50', 'Bank', '360', '360', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(85, '76318359-81fc-4b28-81be-a16e3ac00ed7', 'deliver with Visayas', '160', '150', '189 Ruecker Estates\nEleanoreland, KY 86334', '50', 'COOP', '360', '360', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(86, '9f389bc3-eb4a-4008-bd90-16202e6ffe88', 'deliver with Mindanao', '160', '150', '9961 Fatima Village\nWillhaven, NE 99667-7020', '50', 'Bank', '360', '360', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(87, 'cddabc0e-3d35-431f-b281-2254f6d7d4bc', 'deliver with Visayas', '160', '150', '1303 Stroman Forge\nSouth Annabellton, CA 42809', '50', 'Bank', '360', '360', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(88, '7bce045d-1c17-448d-b2ea-b467f7081a9a', 'deliver with Visayas', '160', '150', '838 Petra Trace\nLake Glenniemouth, MO 34633-2336', '50', 'Bank', '360', '360', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(89, 'e0b968d7-f221-440a-b1fc-34227e0bd250', 'deliver with Luzon', '120', '150', '841 Kessler Run Apt. 702\nO''Konbury, UT 01684', '50', 'Bank', '320', '320', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(90, 'e6229fe4-0924-4e93-b2f1-81b924a088a6', 'deliver with Visayas', '160', '150', '39234 Lubowitz Park\nLeathatown, MA 17206', '50', 'Bank', '360', '360', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(91, '1f551f0e-4ce2-42d3-95ff-f61b67b2e664', 'deliver with Mindanao', '160', '50', '7225 Scottie Lake Suite 326\nDestinyhaven, IL 69621-2494', '50', 'Bank', '260', '260', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(92, 'ade8f315-d50b-499f-aa42-f778b24d32fe', 'deliver with Luzon', '120', '150', '169 Lang Dam Apt. 429\nDavonville, MD 57146', '50', 'Bank', '320', '320', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(93, 'b4b6b7b0-c2a1-4eba-b219-10b3fedb7da4', 'deliver with Mindanao', '160', '150', '16220 Patricia Curve Apt. 704\nNorth Amani, WA 17849', '50', 'Bank', '360', '360', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(94, '3d748c0e-fe6e-476f-befb-3b20ff34983d', 'pick-up', '0', '50', '16220 Patricia Curve Apt. 704\nNorth Amani, WA 17849', '50', 'Bank', '100', '100', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(95, '37cca6c7-e7ab-4a54-9272-a0460fb989a5', 'pick-up', '0', '150', '16220 Patricia Curve Apt. 704\nNorth Amani, WA 17849', '50', 'Bank', '200', '200', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(96, '97646a0e-943a-4c70-b0b5-109bb0e2c01e', 'deliver with Mindanao', '160', '150', '500 Layne Shoal Suite 731\nWest Lauraton, MS 18693', '50', 'Bank', '360', '360', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(97, 'fac2b109-b47a-4542-bcab-ceb1bdf909dd', 'pick-up', '0', '50', '500 Layne Shoal Suite 731\nWest Lauraton, MS 18693', '50', 'COOP', '100', '100', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(98, 'c9c973b6-3951-41c4-a08e-3cebe9bb9707', 'deliver with Luzon', '120', '50', '4123 Dietrich Prairie\nSouth Abbey, MT 69115', '50', 'Bank', '220', '220', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(99, 'eee4732e-2e37-4a49-b852-e70eb1d59b0a', 'deliver with Visayas', '160', '150', '23211 Bauch Extensions Apt. 223\nBreanneburgh, IA 89211-8224', '50', 'COOP', '360', '360', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50'),
(100, '50c61260-3115-4fb8-b825-50de573b2ae2', 'deliver with Luzon', '120', '150', '89424 Gina Isle Apt. 252\nEast Vickie, TX 87882', '50', 'COOP', '320', '320', NULL, NULL, 'paid', NULL, '2020-02-03 00:46:50', '2020-02-03 00:46:50');

-- --------------------------------------------------------

--
-- Table structure for table `reasons`
--

CREATE TABLE IF NOT EXISTS `reasons` (
  `id` bigint(20) unsigned NOT NULL,
  `reason` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reasons`
--

INSERT INTO `reasons` (`id`, `reason`) VALUES
(1, 'Advance or further study'),
(2, 'No job opportunity'),
(3, 'Family concern and decided not to find a job'),
(4, 'Did not look for a job'),
(5, 'Health-related reason(s)'),
(6, 'Lack of work experience\n'),
(7, 'other reason(s)');

-- --------------------------------------------------------

--
-- Table structure for table `suggestions`
--

CREATE TABLE IF NOT EXISTS `suggestions` (
  `id` bigint(20) unsigned NOT NULL,
  `uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `suggestion` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `suggestions`
--

INSERT INTO `suggestions` (`id`, `uuid`, `suggestion`, `created_at`, `updated_at`) VALUES
(1, '2d859ea3-2bd3-48cd-a7a8-b1c6b0c50394', 'Hello World!', '2020-01-11 00:41:28', '2020-01-11 00:41:28'),
(2, '06fda258-a3e9-4015-bb60-c930886f15b8', 'asa', '2020-01-19 06:28:23', '2020-01-19 06:28:23');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admin_email_unique` (`email`);

--
-- Indexes for table `alumni`
--
ALTER TABLE `alumni`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `alumni_email_unique` (`email`),
  ADD UNIQUE KEY `uuid` (`uuid`);

--
-- Indexes for table `alumni_educations`
--
ALTER TABLE `alumni_educations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `alumni_employments`
--
ALTER TABLE `alumni_employments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `applicants`
--
ALTER TABLE `applicants`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `applicants_email_unique` (`email`);

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `carousel`
--
ALTER TABLE `carousel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `colleges`
--
ALTER TABLE `colleges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `first_job`
--
ALTER TABLE `first_job`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `graduates`
--
ALTER TABLE `graduates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `industries`
--
ALTER TABLE `industries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_posts`
--
ALTER TABLE `job_posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_title`
--
ALTER TABLE `job_title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mailbox_inbound_emails`
--
ALTER TABLE `mailbox_inbound_emails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `partners`
--
ALTER TABLE `partners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reasons`
--
ALTER TABLE `reasons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suggestions`
--
ALTER TABLE `suggestions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `alumni`
--
ALTER TABLE `alumni`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=101;
--
-- AUTO_INCREMENT for table `alumni_educations`
--
ALTER TABLE `alumni_educations`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=101;
--
-- AUTO_INCREMENT for table `alumni_employments`
--
ALTER TABLE `alumni_employments`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=101;
--
-- AUTO_INCREMENT for table `applicants`
--
ALTER TABLE `applicants`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=101;
--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `carousel`
--
ALTER TABLE `carousel`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `colleges`
--
ALTER TABLE `colleges`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `course`
--
ALTER TABLE `course`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=69;
--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `first_job`
--
ALTER TABLE `first_job`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=76;
--
-- AUTO_INCREMENT for table `graduates`
--
ALTER TABLE `graduates`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=101;
--
-- AUTO_INCREMENT for table `industries`
--
ALTER TABLE `industries`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `job_posts`
--
ALTER TABLE `job_posts`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `job_title`
--
ALTER TABLE `job_title`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT for table `mailbox_inbound_emails`
--
ALTER TABLE `mailbox_inbound_emails`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=98;
--
-- AUTO_INCREMENT for table `partners`
--
ALTER TABLE `partners`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=101;
--
-- AUTO_INCREMENT for table `reasons`
--
ALTER TABLE `reasons`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `suggestions`
--
ALTER TABLE `suggestions`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
